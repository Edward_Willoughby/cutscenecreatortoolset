/********************************************************************
*	Function definitions for the Audio class.
*********************************************************************/

/********************************************************************
*	Include the header file.
*********************************************************************/
#include "Audio.h"

#include <sstream>

#include "Macros.h"

/********************************************************************
*	Defines, constants and local variables.
*********************************************************************/
Audio* Audio::sp_audio = nullptr;


/**
* Constructor for the Audio class.
*/
Audio::Audio()
	: m_lastError( nullptr)
{
	unsigned int version;
	int drivers;

	// Create the FMOD interface object.
	if( !this->FMODSuccess( FMOD::System_Create( &mp_system))) {
		this->Shutdown();
		return;
	}

	// Get the version number of FMOD.
	if( !this->FMODSuccess( mp_system->getVersion( &version))) {
		this->Shutdown();
		return;
	}

	// Ensure that the version number of FMOD is correct.
	if( version < FMOD_VERSION) {
		std::ostringstream out;
		out << "Using old version of FMOD: " << version << " instead of " << FMOD_VERSION << ".";
		m_lastError = const_cast<char*>( out.str().c_str());

		this->Shutdown();
		return;
	}

	// Check the number of sound cards and match FMOD to the first one.
	if( !this->FMODSuccess( mp_system->getNumDrivers( &drivers))) {
		this->Shutdown();
		return;
	}

	if( drivers == 0) {
		if( !this->FMODSuccess( mp_system->setOutput( FMOD_OUTPUTTYPE_NOSOUND))) {
			this->Shutdown();
			return;
		}
	}
	else {
		FMOD_CAPS caps;
		FMOD_SPEAKERMODE speakerMode;
		char name[256];

		// Get the capabilities of the default (0) sound card.
		if( !this->FMODSuccess( mp_system->getDriverCaps( 0, &caps, 0, &speakerMode))) {
			this->Shutdown();
			return;
		}

		// Set the speaker mode to match that in control panel.
		if( !this->FMODSuccess( mp_system->setSpeakerMode( speakerMode))) {
			this->Shutdown();
			return;
		}

		// Increase the buffer size if the user has Acceleration slider set to off.
		if( caps & FMOD_CAPS_HARDWARE_EMULATED) {
			if( !this->FMODSuccess( mp_system->setDSPBufferSize( 1024, 10))) {
				this->Shutdown();
				return;
			}
		}

		// Get the name of the driver.
		if( !this->FMODSuccess( mp_system->getDriverInfo( 0, name, 256, 0))) {
			this->Shutdown();
			return;
		}

		// SigmaTel sound devices crackle for some reason if the format is P.C.M. 16-bit. P.C.M.
		// floating point output seems to solve it.
		if( strstr( name, "SigmaTel")) {
			if( !this->FMODSuccess( mp_system->setSoftwareFormat( 48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR))) {
				this->Shutdown();
				return;
			}
		}
	}

	// Initialise FMOD.
	FMOD_RESULT result;
	result = mp_system->init( MAX_AUDIO_CHANNELS, FMOD_INIT_NORMAL, 0);
	if( result == FMOD_ERR_OUTPUT_CREATEBUFFER) {
		if( !this->FMODSuccess( mp_system->setSpeakerMode( FMOD_SPEAKERMODE_STEREO))) {
			this->Shutdown();
			return;
		}

		if( !this->FMODSuccess( mp_system->init( MAX_AUDIO_CHANNELS, FMOD_INIT_NORMAL, 0))) {
			this->Shutdown();
			return;
		}
	}

	// Clear the audio channels array.
	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i)
		mp_sounds[i] = nullptr;
}

/**
* Destructor for the Audio class. Although this is a singleton class, I've created the instance of
* the Audio class by using 'static Audio' instead of dynamically creating a pointer when it's first
* used and thus doesn't need deleting.
*/
Audio::~Audio() {
	this->Shutdown();
}

/**
* This function should be called everytime the Audio class is accessed. This ensures that only one
* instance of the Audio class is created and used, and keeping the class as a singleton.
*
* @return A reference to the Audio instance.
*/
Audio& Audio::GetInstance() {
	//static Audio m_audio;
	if( sp_audio == nullptr)
		sp_audio = new Audio();

	// Ensure that the instance was created properly, with no errors.
	if( sp_audio->mp_system == nullptr)
		ASSERT_ALWAYS();

	return *sp_audio;
}

/**
* Loads an audio file into memory so it's not as processor intensive when playing (this should only
* be used for small audio files, such as sound effects).
*
* @param fileName :: The name of the audio file to cache.
*/
void Audio::LoadAudio( const char* fileName) {
	ASSERT( mp_system != nullptr);

	int index( this->FindEmptyChannel());

	this->CreateSound( index, fileName, true);
}

/**
* Checks whether a specified audio file is playing or not. If the audio file can't be found (i.e.
* it hasn't been loaded at any point) then the function will return false.
*
* @param fileName :: The name of the audio file.
*
* @return True if the file is currently playing AND isn't paused.
*/
bool Audio::IsPlaying( const char* fileName) const {
	ASSERT( mp_system != nullptr);

	int index( this->FindChannel( fileName));
	bool playing( false);

	if( index >= 0) {
		mp_sounds[index]->mp_channel->isPlaying( &playing);

		if( playing) {
			mp_sounds[index]->mp_channel->getPaused( &playing);
			playing = !playing;
		}
	}

	return playing;
}

/**
* Plays an audio file once. This should NEVER be called on something that shouldn't be played
* multiple times simultaneously. E.g. if a music file has 'PlayAudioOneShot()' called on it twice
* then it will end up playing two instances at the same time; it's designed for use by sound
* effects (such as explosions). Of course, the first time a music file is played, it should be
* played using this function, but use 'PauseAudio()' to stop and start it from there on.
*
* @param fileName	:: The name of the audio file.
* @param volume		:: Default: 1.0f. The volume to play the audio file at.
*/
void Audio::PlayAudioOneShot( const char* fileName, float volume /*= 1.0f*/) {
	ASSERT( mp_system != nullptr);

	int index( this->FindEmptyChannel());

	// Create the sound.
	this->CreateSound( index, fileName, false);

	// Begin playing the sound.
	this->FMODSuccess( mp_sounds[index]->mp_channel->setVolume( volume));
	this->FMODSuccess( mp_sounds[index]->mp_channel->setPaused( false));
}

/**
* Plays an audio channel. This should be used to start off an audio file or to reset one. It
* shouldn't be used to play multiple instances of the same file as it will just end up resetting it
* over and over; use 'PlayAudioOneShot()' for this.
*
* @param fileName	:: The name of the audio file.
* @param volume		:: Default: 1.0f. The volume to play the audio file at.
*/
void Audio::PlayAudio( const char* fileName, float volume /*= 1.0f*/) {
	ASSERT( mp_system != nullptr);

	int index( this->FindChannel( fileName));

	// If the file name wasn't found anywhere in the existing files, then create it.
	if( index < 0) {
		index = this->FindEmptyChannel();

		// Create the sound.
		this->CreateSound( index, fileName, false);
	}

	// Set the channel to the start of the audio file and begin playing the sound.
	this->FMODSuccess( mp_sounds[index]->mp_channel->setPosition( 0, FMOD_TIMEUNIT_PCM));
	this->FMODSuccess( mp_sounds[index]->mp_channel->setVolume( volume));
	this->FMODSuccess( mp_sounds[index]->mp_channel->setPaused( false));
}

/**
* Toggles an audio file's play status. If it's playing then it gets paused, but if it's already
* paused then it begins playing again.
*
* @param fileName :: The name of the audio file.
*/
void Audio::PauseAudio( const char* fileName) {
	ASSERT( mp_system != nullptr);

	int index( this->FindChannel( fileName));

	// If the file was found, then pause/unpause it.
	if( index >= 0) {
		bool isPaused;

		this->FMODSuccess( mp_sounds[index]->mp_channel->getPaused( &isPaused));
		this->FMODSuccess( mp_sounds[index]->mp_channel->setPaused( !isPaused));
	}
}

/**
* Stops the audio file from playing and resets it. If the file doesn't already exist then it does
* nothing.
*
* @param fileName :: The name of the audio file to be stopped.
*/
void Audio::StopAudio( const char* fileName) {
	ASSERT( mp_system != nullptr);

	int index( this->FindChannel( fileName));

	if( index >= 0) {
		this->FMODSuccess( mp_sounds[index]->mp_channel->setPosition( 0, FMOD_TIMEUNIT_PCM));
		this->FMODSuccess( mp_sounds[index]->mp_channel->setPaused( true));
	}
}

/**
* Gets the volums of a specified audio clip.
*
* @param fileName	:: The name of the audio file.
*
* @return The volume of the audio clip (between 0.0f and 1.0f).
*/
float Audio::GetVolume( const char* fileName) {
	ASSERT( mp_system != nullptr);

	int index( this->FindChannel( fileName));
	float volume( 0.0f);

	if( index >= 0)
		this->FMODSuccess( mp_sounds[index]->mp_channel->getVolume( &volume));

	return volume;
}

/**
* Sets the volume of a specified audio clip.
*
* @param fileName	:: The name of the audio file.
* @param volume		:: Default: 1.0f. The volume to play the audio file at.
*/
void Audio::SetVolume( const char* fileName, float volume /*= 1.0f*/) {
	ASSERT( mp_system != nullptr);

	int index( this->FindChannel( fileName));

	if( index >= 0)
		this->FMODSuccess( mp_sounds[index]->mp_channel->setVolume( volume));
}

/**
* Gets the length of the audio clip, in milliseconds.
*
* @param fileName	:: The name of the audio file.
*
* @return The length of the audio clip, in milliseconds. If '0' then the file doesn't exist.
*/
unsigned int Audio::GetDuration( const char* fileName) {
	ASSERT( mp_system != nullptr);

	int index( this->FindChannel( fileName));
	unsigned int length( 0);

	if( index >= 0)
		this->FMODSuccess( mp_sounds[index]->mp_sound->getLength( &length, FMOD_TIMEUNIT_MS));

	return length;
}

/**
* Sets the playback position of an audio clip. If the position is set to -1 then it'll set the
* audio clip to the end. The position should be in milliseconds; i.e. if you want to skip to five
* seconds into the audio clip, then send through 5000 as the second parameter.
*
* @param fileName	:: The name of the audio file.
* @param pos		:: Default: -1. The position in the audio file.
*/
void Audio::SetPlayPosition( const char* fileName, int pos /*= -1*/) {
	ASSERT( mp_system != nullptr);

	int index( this->FindChannel( fileName));
	unsigned int length( 0);
	mp_sounds[index]->mp_sound->getLength( &length, FMOD_TIMEUNIT_MS);

	if( index >= 0) {
		if( pos == -1 || pos > (int)length)
			pos = length;
		
		this->FMODSuccess( mp_sounds[index]->mp_channel->setPosition( pos, FMOD_TIMEUNIT_MS));
		this->FMODSuccess( mp_sounds[index]->mp_channel->setPaused( false));
	}
}

/**
* Updates anything that needs to be.
*/
void Audio::Update() {
	ASSERT( mp_system != nullptr);

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		if( mp_sounds[i] == nullptr)
			continue;
		
		bool isPaused;
		FMOD_RESULT result = mp_sounds[i]->mp_channel->getPaused( &isPaused);

		// If the handle is invalid then the file has finished playing and can be deleted.
		if( result == FMOD_ERR_INVALID_HANDLE)
			SafeDelete( mp_sounds[i]);
	}

	mp_system->update();
}

/**
* Stops all the audio files from playing.
*/
void Audio::StopAllAudio() {
	ASSERT( mp_system != nullptr);

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		if( mp_sounds[i] == nullptr)
			continue;

		this->FMODSuccess( mp_sounds[i]->mp_channel->setPosition( 0, FMOD_TIMEUNIT_PCM));
		this->FMODSuccess( mp_sounds[i]->mp_channel->setPaused( true));
	}
}

/**
* Deletes all the currently stored audio information (save for the FMOD::System, so it can be used
* by the application as well as the Audio Shutdown() function).
*/
void Audio::DeleteAllAudio() {
	ASSERT( mp_system != nullptr);

	// Just to be sure nothing goes wrong, stop all audio from playing before deleting it.
	this->StopAllAudio();

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i)
		SafeDelete( mp_sounds[i]);
}

/**
* Locates the index number of an empty slot in the sounds array.
*
* @return The position of the empty channel in the Audio array. The value is negative if no empty
*			slot was found.
*/
int Audio::FindEmptyChannel() const {
	ASSERT( mp_system != nullptr);

	int index( -1);

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		if( mp_sounds[i] == nullptr) {
			index = i;
			break;
		}
	}

	// If no empty slot was found then bitch about it now instead of somewhere down the line.
	if( index < 0)
		ASSERT_ALWAYS();

	return index;
}

/**
* Locates the index number of the audio file.
*
* @param fileName :: The name of the audio file.
*
* @return The position of the channel in the Audio array. If the value is negative then it means
*			the file wasn't found.
*/
int Audio::FindChannel( const char* fileName) const {
	ASSERT( mp_system != nullptr);

	int index( -1);

	for( int i( 0); i < MAX_AUDIO_CHANNELS; ++i) {
		// Check that the sound exists before trying to access an invalid instance.
		if( mp_sounds[i] == nullptr) {
			continue;
		}
		// If the instance exists, then check the file name.
		else if( mp_sounds[i]->m_fileName == fileName) {
			index = i;
			break;
		}
	}

	return index;
}

/**
* Loads an audio file in and prepares it for play by storing it in the array at the given index.
* This function is used through-out the Audio class and thus files are created and paused because
* the calling function may not always want the file to be played straight away.
*
* @param index		:: The position in the array that the sound should be inserted.
* @param fileName	:: The name of the audio file.
* @param cache		:: Whether the sound should be loaded into memory or streamed from disk.
*
* @return 
*/
void Audio::CreateSound( int index, const char* fileName, bool cache) {
	ASSERT( mp_system != nullptr);

	FMOD::Sound* p_sound;
	FMOD::Channel* p_channel;
	FMOD_RESULT (__stdcall FMOD::System::*funcPointer)( const char*, FMOD_MODE, FMOD_CREATESOUNDEXINFO*, FMOD::Sound**);

	// If the audio file should be cached then load it into memory with FMOD::System::createSound.
	if( cache)
		funcPointer = &FMOD::System::createSound;
	else
		funcPointer = &FMOD::System::createStream;

	this->FMODSuccess( (mp_system->*funcPointer)( fileName, FMOD_DEFAULT, 0, &p_sound));
	this->FMODSuccess( mp_system->playSound( FMOD_CHANNEL_FREE, p_sound, true, &p_channel));

	// Create the container for the audio file's information and store it in the array.
	AudioContainer* p_container = new AudioContainer( p_sound, p_channel, fileName);
	mp_sounds[index] = p_container;
}

/**
* Deletes all the audio variables and shuts down FMOD.
*/
void Audio::Shutdown() {
	this->DeleteAllAudio();

	mp_system->release();
}

/**
* Checks that the FMOD_RESULT means that the operation succeeded. If it didn't, then it records the
* error message and return false.
*
* @param result :: The variable returned by most FMOD functions.
*
* @return True if the FMOD_RESULT indicates nothing went wrong.
*/
bool Audio::FMODSuccess( FMOD_RESULT result) {
	if( result != FMOD_OK)
		m_lastError = const_cast<char*>( FMOD_ErrorString( result));

	return( result == FMOD_OK);
}