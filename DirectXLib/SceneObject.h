/********************************************************************
*
*	CLASS		:: SceneObject
*	DESCRIPTION	:: Generic container for any object that is part of the cut scene (whether that's
*					part of the scenery or a character).
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 29
*
********************************************************************/

#ifndef SceneObjectH
#define SceneObjectH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <vector>

#include <D3D10_1.h>	// I've only included this because otherwise it moans.
#include <D3DX10math.h>

class CommonMesh;

class ActionBase;
class D3DCamera;
//class HumanModel;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class SceneObject {
	public:
		/// Constructor.
		//SceneObject( HumanModel* p_mesh, const char* name, const D3DXVECTOR3& pos, const D3DXVECTOR3& dir);
		/// Constructor.
		SceneObject( CommonMesh* p_mesh, const char* name, const D3DXVECTOR3& pos, const D3DXVECTOR3& dir);
		/// Destructor.
		~SceneObject();

		void StoreMeshParameters( const char* type, float param1, float param2, float param3);
		void GetMeshParameters( const char* &type, float& param1, float& param2, float& param3) const;
		
		void AddAction( ActionBase* p_action);
		void DeleteAction( const char* actionName);
		void SortActions();

		unsigned int GetLastFrame() const;
		void GetActionInfo( const char* &actionName, int& start, int& end, int index) const;
		void GetActionInfo( const char* actionName, const char* &actionType, int& start, int& end) const;
		void GetActionInfoAtFrame( const char* actionName, int frame, float& param1, float& param2, float& param3, const char* &param4) const;
		bool IsActionNameValid( const char* actionName) const;

		void Update( unsigned int frame);
		void Render();

		D3DXVECTOR3 GetSize() const;

		void AttachCamera( D3DCamera* p_cam);
		//void DetachCamera();

		bool GetVisibility() const;
		void ToggleVisibility( bool visible);

		const char* m_name;

		D3DXVECTOR3 m_position;
		D3DXVECTOR3 m_rotation;
		D3DXVECTOR3 m_direction;

		D3DXVECTOR3 m_camOffset;
		
	private:
		void SetInfo( const char* name, const D3DXVECTOR3& pos, const D3DXVECTOR3& dir);
		void ResetInfo();

		//HumanModel* mp_human;
		CommonMesh* mp_mesh;

		D3DCamera*	mp_camera;

		const char*	m_meshType;
		float		m_meshParam1, m_meshParam2, m_meshParam3;

		std::vector<ActionBase*> mp_actions;

		bool m_visible;

		D3DXVECTOR3 m_defPos;
		D3DXVECTOR3 m_defRot;
		D3DXVECTOR3 m_defDir;
		bool		m_defVis;
		
		/// Private copy constructor to prevent accidental multiple instances.
		SceneObject( const SceneObject& other);
		/// Private assignment operator to prevent accidental multiple instances.
		SceneObject& operator=( const SceneObject& other);
		
};

/*******************************************************************/
#endif	// #ifndef SceneObjectH
