/********************************************************************
*
*	CLASS		:: D3DCamera
*	DESCRIPTION	:: Holds the data to setup the camera.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 28
*
********************************************************************/

#ifndef D3DCameraH
#define D3DCameraH

/********************************************************************
*	Include libraries and header files.
********************************************************************/

#include <D3D10_1.h>	// I've only included this because otherwise it moans.
#include <D3DX10math.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class D3DCamera {
	public:
		/// Constructor.
		D3DCamera();
		/// Destructor.
		~D3DCamera();

		/// Updates the camera.
		void Update();

		/// Creates and sets the view matrix.
		D3DXMATRIX ApplyViewMatrixLH() const;
		/// Creates and sets the perspective matrix.
		D3DXMATRIX ApplyPerspectiveMatrixLH( float zNear, float zFar) const;
		/// Creates and sets the orthographic matrix.
		D3DXMATRIX ApplyOrthoMatrixLH( float zNear, float zFar) const;

		/// Unlinks the camera from an object so it no longer follows it.
		//void DetachCamera();
		
		float m_rotX, m_rotY;
		float m_zDist;

		D3DXVECTOR3 m_position;
		D3DXVECTOR3 m_lookAt;
		D3DXVECTOR3 m_up;

	private:


		/// Private copy constructor to prevent accidental multiple instances.
		D3DCamera( const D3DCamera& other);
		/// Private assignment operator to prevent accidental multiple instances.
		D3DCamera& operator=( const D3DCamera& other);
		
};

/*******************************************************************/
#endif  // #ifndef D3DCameraH