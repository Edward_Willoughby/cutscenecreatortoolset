/********************************************************************
*	Function definitions for the InputManager class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "InputManager.h"

#include <d3d11.h>
#include <D3DX10math.h>

#include "Application.h"

/********************************************************************
*	Defines, constants and local variables.
********************************************************************/
/// Private pointer to the Application because it's more efficient than going through the
/// static pointer every time. I have to have the pointer in here else 'class Application' will
/// screw over the Application class when I include D3DCamera in there.
static NS_DirectXLib::Application* sp_app = nullptr;


/**
* Constructor.
*/
InputManager::InputManager() {
	sp_app = NS_DirectXLib::Application::sp_app;

	for( int i( 0); i < NUM_OF_KEYS; ++i)
		m_prevStates[i] = false;
}

/**
* Destructor.
*/
InputManager::~InputManager() {}

/**
* This function should be called everytime the InputManager class is accessed. This ensures that
* only one instance of the InputManager class is created and used, and keeping the class as a
* singleton.
*
* @return A reference to the InputManager instance.
*/
InputManager& InputManager::GetInstance() {
	static InputManager input;

	return input;
}

/**
* Returns whether or not a specified key is down. This is a debounced version of
* InputManager::IsKeyHeld() and thus can't be a constant function because it has to record the
* previous button state.
*
* @param key :: The ASCII code of the key.
*
* @return True if the key is down this frame and wasn't last frame.
*/
bool InputManager::IsKeyPressed( int key) {
	if( sp_app->IsInFocus()) {
		// Get whether the key is down this frame.
		bool isPressed( this->IsDown( key));

		// If the key is down this frame and WASN'T last frame then return true.
		if( isPressed && !m_prevStates[key]) {
			// Set the previous state to true to debounce next frame.
			m_prevStates[key] = true;
			return true;
		}
		// If the key isn't down this frame then set the previous state to prepare for the next
		// frame. If the key is down this frame and debouncing has occured then the previous state
		// will be set to true (which is what it'll be anyway).
		m_prevStates[key] = isPressed;
	}

	// If the application isn't in focus or the key shouldn't return true, then return false.
	return false;
}

/**
* Returns whether or not a specified key is down. For debouncing, use InputManager::IsKeyPressed().
*
* @param key :: The ASCII code of the key.
*
* @return True if the key is down this frame.
*/
bool InputManager::IsKeyHeld( int key) const {
	if( sp_app->IsInFocus()) {
		// Get whether the key is down this frame.
		bool isPressed( this->IsDown( key));

		return isPressed;
	}

	// If the application isn't in focus or the key wasn't down this frame or last frame.
	return false;
}

/**
* Gets the position of the cursor within the bounds of the window.
*
* @param pos :: The vector to hold the mouse's x and y positions.
*
* @return True if the window is in focus.
*/
bool InputManager::GetMousePos( D3DXVECTOR2& pos) const {
	if( !sp_app->IsInFocus())
		return false;

	POINT mouse;
	RECT rect;
	HWND handle( sp_app->GetClientHandle());

	GetCursorPos( &mouse);
	GetClientRect( handle, &rect);
	ScreenToClient( handle, &mouse);

	pos.x = mouse.x;
	pos.y = mouse.y;

	// Set an invalid position if the cursor is outside the bounds of the window.
	if( pos.x < 0 || pos.x > rect.right || pos.y < 0 || pos.y > rect.bottom) {
		pos.x = -1;
		pos.y = -1;
	}

	return true;
}

/**
* Sets the position of the mouse within the bounds of the window.
*
* @param pos :: The x and y positions to set the mouse to.
*
* @return True if the window is in focus.
*/
bool InputManager::SetMousePos( const D3DXVECTOR2& pos) {
	if( !sp_app->IsInFocus())
		return false;

	POINT mouse;
	mouse.x = pos.x;
	mouse.y = pos.y;

	// Convert the mouse position back to screen coordinates before setting them.
	ClientToScreen( sp_app->GetClientHandle(), &mouse);
	SetCursorPos( mouse.x, mouse.y);

	return true;
}

/**
* Returns whether or not a specified key is down.
*
* @param key :: The ASCII code of the key.
*
* @return True if the key is down this frame.
*/
bool InputManager::IsDown( int key) const {
	SHORT val = GetAsyncKeyState( key);

	if( val & 0x8000)
		return true;

	return false;
}