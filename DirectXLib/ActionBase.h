/********************************************************************
*
*	CLASS		:: ActionBase
*	DESCRIPTION	:: The base class for any actions to be performed in the cut scene.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 30
*
********************************************************************/

#ifndef ActionBaseH
#define ActionBaseH

/********************************************************************
*	Include libraries and header files.
********************************************************************/

class SceneObject;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class ActionBase {
	public:
		/// Constructor.
		ActionBase( const char* name, unsigned int firstFrame, SceneObject* p_target);
		/// Destructor.
		~ActionBase();

		virtual const char* GetTypeName() const = 0;
		
		virtual unsigned int CalculateDuration() = 0;

		virtual void GoToFrame( unsigned int frame) = 0;
		virtual void GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4) = 0;

		const char* GetName() const;
		unsigned int GetFirstFrame() const;
		unsigned int GetLastFrame() const;
		unsigned int GetDuration() const;

		// This should only be called in the 'CalculateLastFrame()' function.
		void SetLastFrame( unsigned int duration);

		/// 'Less than' operator.
		static bool ActionCompare( ActionBase* a, ActionBase* b);

		SceneObject* mp_target;
		
	private:
		const char* m_name;

		unsigned int m_frameFirst;
		unsigned int m_frameLast;
		
		/// Private copy constructor to prevent accidental multiple instances.
		ActionBase( const ActionBase& other);
		/// Private assignment operator to prevent accidental multiple instances.
		ActionBase& operator=( const ActionBase& other);
		
};

/*******************************************************************/
#endif	// #ifndef ActionBaseH
