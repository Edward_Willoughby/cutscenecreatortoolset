/********************************************************************
*
*	CLASS		:: InputManager
*	DESCRIPTION	:: Accesses the keyboard and mouse input.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 11
*
********************************************************************/

#ifndef InputManagerH
#define InputManagerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/

class D3DXVECTOR2;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class InputManager {
	public:
		/// Destructor.
		~InputManager();

		/// Returns a reference to the InputManager.
		static InputManager& GetInstance();

		/// Returns whether or not a specified key is down and performs debouncing.
		bool IsKeyPressed( int key);
		/// Returns whether or not a specified key is down.
		bool IsKeyHeld( int key) const;

		/// Get the position of the mouse.
		bool GetMousePos( D3DXVECTOR2& pos) const;
		/// Sets the position of the mouse.
		bool SetMousePos( const D3DXVECTOR2& pos);

	protected:


	private:
		/// Returns whether or not a specified key is down.
		bool IsDown( int key) const;

		// I'm pretty sure it only goes up to 256, but to make it easier just in case...
		static const int NUM_OF_KEYS = 256;

		bool m_prevStates[NUM_OF_KEYS];

		/// Private constructor to keep the class as a singleton.
		InputManager();
		/// Private copy constructor to prevent accidental multiple instances.
		InputManager( const InputManager& other);
		/// Private assignment operator to prevent accidental multiple instances.
		InputManager& operator=( const InputManager& other);
		
};

/*******************************************************************/
#endif	// #ifndef InputManagerH