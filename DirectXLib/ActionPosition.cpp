/********************************************************************
*	Function definitions for the ActionPosition class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "ActionPosition.h"

#include "SceneObject.h"
/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/


/**
* Constructor.
*/
ActionPosition::ActionPosition( const char* actionName, unsigned int frame, SceneObject* p_target, const D3DXVECTOR3& pos)
	: ActionBase( actionName, frame, p_target)
	, m_position( pos)
{
	this->SetLastFrame( this->CalculateDuration());
}

/**
* Destructor.
*/
ActionPosition::~ActionPosition() {
	
}

/**
* Gets the textual name of this class.
*
* @return The textual name of this class.
*/
const char* ActionPosition::GetTypeName() const {
	return "PositionAt";
}

/**
* 
*/
void ActionPosition::CalculateActionLength( int& length) {
	length = 0;
}

/**
* 
*
* @param :: 
*
* @return 
*/
unsigned int ActionPosition::CalculateDuration() {
	return 0;
}

void ActionPosition::GoToFrame( unsigned int frame) {
	if( frame < this->GetFirstFrame())
		return;
	else /*if( frame >= this->GetFirstFrame())*/
		mp_target->m_position = m_position;
}

void ActionPosition::GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4) {
	D3DXVECTOR3 pos;

	if( frame < this->GetFirstFrame())
		return;
	else /*if( frame >= this->GetFirstFrame())*/
		pos = m_position;

	param1 = pos.x;
	param2 = pos.y;
	param3 = pos.z;
}