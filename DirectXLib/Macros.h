/********************************************************************
*
*	CLASS		:: Macros
*	DESCRIPTION	:: Has macros, free functions, defines and constants to make life easier.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2013 / 12 / 24
*
********************************************************************/

#ifndef MacrosH
#define MacrosH

/********************************************************************
*	Include libraries and header files
********************************************************************/
#include <intrin.h>
#include <stdio.h>

#include <D3D10_1.h>	// I've only included this because otherwise it moans
#include <D3DX10math.h>

/********************************************************************
*	Include namespaces.
********************************************************************/


/********************************************************************
*	Defines and constants.
********************************************************************/
/// Not really sure how else to access this at the moment, so I'm going to put a constant in here
/// for now.
static const float FRAME_RATE = 60.0f;

/// An easy way of obtaining a zeroed vector.
static const D3DXVECTOR3 ZeroVector = D3DXVECTOR3( 0.0f, 0.0f, 0.0f);
/// An east way of obtaining a forward vector.
static const D3DXVECTOR3 ForwardVector = D3DXVECTOR3( 0.0f, 0.0f, 1.0f);

/********************************************************************
*	Functions.
********************************************************************/
/**
* A function to linearly interpolate between two float variables.
*
* @param first	:: The value to interpolate from.
* @param second	:: The value to interpolate to.
* @param interp	:: The amount to interpolate between the other two parameters. This value MUST be
*					between 0 and 1.
*
* @return The interpolated value between the first two parameters based on the third.
*/
static float Lerp( float first, float second, float interp)
{
	// Overall equation:
	// (1 - t)*a + t*b;
	float t( interp);
	
	float prog1( 1-t);
	float prog2( prog1 * first);
	float prog3( t * second);
	float prog4( prog2 + prog3);

	return prog4;
}

/********************************************************************
*	Macros.
********************************************************************/
/// Quick macro to delete variables from the heap.
#define SafeDelete( x)		\
{							\
	if( x != nullptr) {		\
		delete x;			\
		x = nullptr;		\
	}						\
}

/// Quick macro to delete arrays from the heap.
#define SafeDeleteArray( x)	\
{							\
	if( x != nullptr) {		\
		delete[] x;			\
		x = nullptr;		\
	}						\
}

/// Macro to ensure the result good, otherwise it hits a breakpoint and provides some debug
/// information.
#define ASSERT( expression)										\
{																\
	if( !( expression))											\
	{															\
		printf( "********** ASSERT HIT! **********\n");			\
		printf( "File: %s\nLine: %s\n", __FILE__, __LINE__);	\
		printf( "********* END OF ASSERT *********\n");			\
		__debugbreak();											\
	}															\
}

/// This assertion macro is for when the program has reached a line of code that it shouldn't have
/// if execution was running as intended.
#define ASSERT_ALWAYS()										\
{															\
	printf( "********** ASSERT HIT! **********\n");			\
	printf( "File: %s\nLine: %s\n", __FILE__, __LINE__);	\
	printf( "********* END OF ASSERT *********\n");			\
	__debugbreak();											\
}


/*******************************************************************/
#endif	// #ifndef MacrosH