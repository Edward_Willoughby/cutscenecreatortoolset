/********************************************************************
*
*	CLASS		:: ActionPosition
*	DESCRIPTION	:: Action class for positioning an object directly at a location without regards to
*					the object's movement speed; this action will always be done in one frame.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 02 / 04
*
********************************************************************/

#ifndef ActionPositionH
#define ActionPositionH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <D3D10_1.h>	// I've only included this because otherwise it moans.
#include <D3DX10math.h>

#include "ActionBase.h"

class SceneObject;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class ActionPosition : public ActionBase {
	public:
		/// Constructor.
		ActionPosition( const char* actionName, unsigned int frame, SceneObject* p_target, const D3DXVECTOR3& pos);
		/// Destructor.
		~ActionPosition();

		const char* GetTypeName() const;

		static void CalculateActionLength( int& length);
		
		unsigned int CalculateDuration();

		void GoToFrame( unsigned int frame);
		void GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4);
		
	private:
		D3DXVECTOR3 m_position;
		
		/// Private copy constructor to prevent accidental multiple instances.
		ActionPosition( const ActionPosition& other);
		/// Private assignment operator to prevent accidental multiple instances.
		ActionPosition& operator=( const ActionPosition& other);
		
};

/*******************************************************************/
#endif	// #ifndef ActionPositionH
