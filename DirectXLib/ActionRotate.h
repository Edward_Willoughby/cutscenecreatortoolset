/********************************************************************
*
*	CLASS		:: ActionRotate
*	DESCRIPTION	:: Action class for rotating a SceneObject during a cut scene.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 03 / 01
*
********************************************************************/

#ifndef ActionRotateH
#define ActionRotateH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <D3D10_1.h>	// I've only included this because otherwise it moans.
#include <D3DX10math.h>

#include "ActionBase.h"

class SceneObject;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class ActionRotate : public ActionBase {
	public:
		/// Constructor.
		ActionRotate( const char* name, unsigned int firstFrame, SceneObject* p_target, const D3DXVECTOR3& rot);
		/// Destructor.
		~ActionRotate();

		const char* GetTypeName() const;

		static void CalculateActionLength( D3DXVECTOR3& rotation, int& length);
		
		unsigned int CalculateDuration();

		void GoToFrame( unsigned int frame);
		void GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4);
		
	private:
		D3DXVECTOR3 m_rotTotal;
		D3DXVECTOR3 m_rotFrame;  // The amount travelled in a single frame.
		float		m_turningSpeed;

		/// Private copy constructor to prevent accidental multiple instances.
		ActionRotate( const ActionRotate& other);
		/// Private assignment operator to prevent accidental multiple instances.
		ActionRotate& operator=( const ActionRotate& other);
		
};

/*******************************************************************/
#endif	// #ifndef ActionRotateH