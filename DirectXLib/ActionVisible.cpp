/********************************************************************
*	Function definitions for the ActionVisible class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "ActionVisible.h"

#include "Macros.h"
#include "SceneObject.h"
/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
/// Temporary fix for setting the speed of objects. This needs to be externally customisable at a
/// later stage though.
const float SPEED_Turning = 0.1f;


/**
* Constructor.
*/
ActionVisible::ActionVisible( const char* name, unsigned int firstFrame, SceneObject* p_target, const bool& visible)
	: ActionBase( name, firstFrame, p_target)
	, m_visible( visible)
{
	this->SetLastFrame( this->CalculateDuration());
}

/**
* Destructor.
*/
ActionVisible::~ActionVisible() {
	
}

/**
* Gets the textual name of this class.
*
* @return The textual name of this class.
*/
const char* ActionVisible::GetTypeName() const {
	return "Visible";
}

/**
* 
*/
void ActionVisible::CalculateActionLength( int& length) {
	// A visibility toggle will always happen on the specified frame... Fuck off, am I making
	// meshes fade out.
	length = 0;
}

/**
* 
*
* @param :: 
*
* @return 
*/
unsigned int ActionVisible::CalculateDuration() {
	mp_target->Update( this->GetFirstFrame());

	unsigned int counter( 0);

	return counter;
}

void ActionVisible::GoToFrame( unsigned int frame) {
	if( frame < this->GetFirstFrame())
		return;
	else /*if( frame >= this->GetLastFrame())*/
		mp_target->ToggleVisibility( m_visible);
}

void ActionVisible::GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4) {
	if( frame < this->GetFirstFrame()) {
		return;
	}
	else { /*if( frame >= this->GetLastFrame())*/
		if( m_visible)
			param1 = 1.0f;
		else
			param1 = 0.0f;
	}
}