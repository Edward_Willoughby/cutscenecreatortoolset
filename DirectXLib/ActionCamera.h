/********************************************************************
*
*	CLASS		:: ActionCamera
*	DESCRIPTION	:: Action class for positioning the camera during a cut scene.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 03 / 16
*
********************************************************************/

#ifndef ActionCameraH
#define ActionCameraH

/********************************************************************
*	Include libraries and header files.
********************************************************************/

#include "ActionBase.h"

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class ActionCamera : public ActionBase {
	public:
		/// Constructor.
		ActionCamera( const char* actionName, unsigned int firstFrame, SceneObject* p_target, const char* objectToTrack);
		/// Destructor.
		~ActionCamera();

		const char* GetTypeName() const;

		static void CalculateActionLength( int& length);

		unsigned int CalculateDuration();

		void GoToFrame( unsigned int frame);
		void GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4);

	private:
		const char* m_objectToTrack;

		/// Private copy constructor to prevent accidental multiple instances.
		ActionCamera( const ActionCamera& other);
		/// Private assignment operator to prevent accidental multiple instances.
		ActionCamera& operator=( const ActionCamera& other);

};

/*******************************************************************/
#endif	// #ifndef ActionCameraH