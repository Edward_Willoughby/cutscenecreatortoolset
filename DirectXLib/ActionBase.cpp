/********************************************************************
*	Function definitions for the ActionBase class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "ActionBase.h"

#include <sstream>
#include <Windows.h>
/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/


/**
* Constructor.
*/
ActionBase::ActionBase( const char* name, unsigned int firstFrame, SceneObject* p_target)
	: mp_target( p_target)
	, m_name( name)
	, m_frameFirst( firstFrame)
	, m_frameLast( 0)
{}

/**
* Destructor.
*/
ActionBase::~ActionBase() {
	
}

/**
* 
*/
const char* ActionBase::GetName() const {
	return m_name;
}

/**
* 
*
* @param :: 
*
* @return 
*/
unsigned int ActionBase::GetFirstFrame() const {
	return m_frameFirst;
}

unsigned int ActionBase::GetLastFrame() const {
	return m_frameLast;
}

unsigned int ActionBase::GetDuration() const {
	return( m_frameLast - m_frameFirst);
}

void ActionBase::SetLastFrame( unsigned int duration) {
	m_frameLast = this->GetFirstFrame() + duration;
}

/**
* 'Less than' operator.
*
* @param a	:: The first 'ActionBase' instance to be compared.
* @param b	:: The second 'ActionBase' instance to be compared.
*
* @return True if the first parameter starts before the second parameter.
*/
bool ActionBase::ActionCompare( ActionBase* a, ActionBase* b) {
	if( a->GetFirstFrame() < b->GetFirstFrame())
		return true;
	
	return false;
}
//bool ActionBase::operator<( ActionBase& other) const {
//	std::ostringstream os;
//	os << "Names: " << m_name << " & " << other.m_name << ". Frames: " << m_frameFirst << " & " << other.m_frameFirst << "\n";
//	MessageBox( nullptr, os.str().c_str(), "Application::SaveScene", MB_OK);
//
//	if( m_frameFirst < other.m_frameFirst)
//		return true;
//	
//	return false;
//}