/********************************************************************
*	Function definitions for the Application class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Application.h"

#include <chrono>
#include <fstream>
#include <sstream>

#include "CommonMesh.h"

#include "ActionAudio.h"
#include "ActionBase.h"
#include "ActionCamera.h"
#include "ActionMove.h"
#include "ActionPosition.h"
#include "ActionRotate.h"
#include "ActionVisible.h"
#include "Audio.h"
#include "D3DCamera.h"
#include "InputManager.h"
#include "LuaReader.h"
#include "Macros.h"
#include "SceneObject.h"

/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
using namespace NS_DirectXLib;

/// Initialise the static pointer to the Application to prevent it being wild pointer.
Application* Application::sp_app = nullptr;

/// Array of valid default meshes; additional meshes will need to be loaded in... which I'm
/// probably not going to bother implementing.
static const char* gs_meshNames[][4] = {
	{ "Cone", "Base Radius", "Height", "" },
	{ "Cube", "Width", "Height", "Depth" },
	{ "Cylinder", "Top Radius", "Base Radius", "Height" },
	{ "Sphere", "Radius", "", "" },
	{ "Teapot", "", "", ""},
	{ "Torus", "Inner Radius", "Outer Radius", "" },
};
static const size_t gs_meshNamesCount = sizeof( gs_meshNames) / sizeof( gs_meshNames[0]);

/// Array of valid action types. At a later date I'll implement this as a vector that becomes
/// populated when the action classes are initialised... somehow. But for now, they're hard-coded.
/// The additional parameters aren't being used (though hopefully they will at a later date) and
/// what's needed for each action type has been hard-coded into the 'NewAction' form.
static const char* gs_actionTypeNames[][4] = {
	{ "MoveTo",		"X", "Y", "Z" },
	{ "PositionAt",	"X", "Y", "Z" },
	{ "RotateTo",	"X", "Y", "Z" },
	{ "Visible",	"Visisble?", "", "" },
	{ "Audio",		"File name", "", "" },
};
static const size_t gs_actionTypeNamesCount = sizeof( gs_actionTypeNames) / sizeof( gs_actionTypeNames[0]);

/// Constants for the names of the Lua functions.
static const char* gs_frame			= "SetFrame";
static const char* gs_createObject	= "CreateObject";
static const char* gs_positionAt	= "PositionObjectAt";
static const char* gs_moveTo		= "MoveObjectTo";
static const char* gs_rotateTo		= "RotateObjectTo";
static const char* gs_visibility	= "SetObjectVisibility";
static const char* gs_audioLoad		= "LoadAudio";
static const char* gs_audioPlay		= "PlayAudio";
static const char* gs_camera		= "LinkCamThirdPerson";

/**
* I need a constructor just so I know what 'm_autoPlay' is set to before using it in 'HandleStart()'.
*/
Application::Application()
	: m_autoPlay( false)
{}

/**
* Deals with any initialisation that needs to be done before the application first begins rendering.
* Any opening of files, reading of data or intensive calculations should be done here to keep the
* program running smoothly during run-time.
*
* @return True if nothing failed to initialise. False will end the program without rendering
			anything.
*/
bool Application::HandleStart() {
	// If the Application's static pointer has already been set then this is begin called twice
	// before being properly shut down... which isn't good.
	ASSERT( sp_app == nullptr);
	sp_app = this;

	// Set the title of the window... not really needed for the way this is being used, but set it
	// none-the-less.
	this->SetWindowTitle( "Scene");

	// Create the cameras.
	mp_camera = new D3DCamera();
	mp_sceneCamera = new D3DCamera();
	mp_sceneCamera->Update();

	m_frameCount = 0.0f;
	m_wireframe = false;
	m_reload = false;

	m_playAudio = true;
	m_globalCam = false;

	m_loadingFromFile = false;

	//// Update the camera position here so it can be re-positioned later if it's part of a hierarchy.
	//float camZ( 100.0f), rotAngle( 7.75f);

	this->CreateCameraObject();

	// Register the Lua functions so that the Lua A.P.I. recognises them.
	LuaReader::Instance().RegisterFunction( gs_frame,			LuaLink_SetFrame);
	LuaReader::Instance().RegisterFunction( gs_createObject,	LuaLink_CreateObject);
	LuaReader::Instance().RegisterFunction( gs_positionAt,		LuaLink_PositionObjectAt);
	LuaReader::Instance().RegisterFunction( gs_moveTo,			LuaLink_MoveObjectTo);
	LuaReader::Instance().RegisterFunction( gs_rotateTo,		LuaLink_RotateObjectTo);
	LuaReader::Instance().RegisterFunction( gs_visibility,		LuaLink_SetObjectVisibility);
	//LuaReader::Instance().RegisterFunction( gs_audioLoad,		LuaLink_LoadAudio);
	LuaReader::Instance().RegisterFunction( gs_audioPlay,		LuaLink_PlayAudio);
	LuaReader::Instance().RegisterFunction( gs_camera,			LuaLink_LinkCamThirdPerson);

	// Perform any additional initialisation required in the base class.
	if (!this->CommonApp::HandleStart())
		return false;

	if( m_autoPlay)
		this->InitForViewing( m_scriptName);

	return true;
}

/**
* Handles any de-initialisation to be done at the end of the program.
*/
void Application::HandleStop() {
	// Clear the scene objects.
	this->DeleteScene();

	// Delete the cameras.
	SafeDelete( mp_camera);
	SafeDelete( mp_sceneCamera);

	// Delete the audio.
	SafeDelete( Audio::sp_audio);

	// Perform any de-initialisation required in the base class.
	this->CommonApp::HandleStop();
}

/**
* This function is called every frame to deal with the updating of variables.
*/
void Application::HandleUpdate() {
	// If the independent program is running, increment the frame.
	if( m_autoPlay)
		if( m_frame < m_frameLast)
			++m_frame;

	// Update FMOD and the audio.
	Audio::GetInstance().Update();

	// Update the camera.
	mp_camera->Update();

	// Toggle the wireframe mode.
	if( InputManager::GetInstance().IsKeyPressed( 'W')) {
		m_wireframe = !m_wireframe;
		this->SetRasterizerState( false, m_wireframe);
	}

	// Update all the objects in the scene.
	for( std::vector<SceneObject*>::iterator it( mp_scene.begin()); it != mp_scene.end(); ++it)
		(*it)->Update( m_frame);

	// Create the cut scene camera if it doesn't already exist.
	SceneObject* p_object( this->FindObject( "Obj_Camera"));
	if( p_object == nullptr) {
		this->CreateCameraObject();
		p_object = this->FindObject( "Obj_Camera");
	}
	p_object->Update( m_frame);

	ActionAudio::s_prevFrame = m_frame;
}

/**
* Handles all the rendering for the program.
*/
void Application::HandleRender() {
	this->Render3D();

	this->Render2D();

	++m_frameCount;
}

/**
* Renders all the meshes that require a perspective camera.
*/
void Application::Render3D() {
	this->EnableDirectionalLight( 1, D3DXVECTOR3( -1.0f, -1.0f, -1.0f), D3DXVECTOR3( 0.55f, 0.55f, 0.65f));

	D3DCamera* p_cam( m_globalCam ? mp_camera : mp_sceneCamera);
	p_cam->ApplyViewMatrixLH();
	p_cam->ApplyPerspectiveMatrixLH( 1.5f, 5000.0f);

	this->Clear( D3DXVECTOR4( 0.3f, 0.3f, 4.0f, 1.0f));

	// Render the objects in the scene.
	for( std::vector<SceneObject*>::iterator it( mp_scene.begin()); it != mp_scene.end(); ++it)
		(*it)->Render();
}

/**
* Renders all the orthographic meshes (i.e. G.U.I. and particle effects).
*/
void Application::Render2D() {
	D3DCamera* p_cam( m_globalCam ? mp_camera : mp_sceneCamera);
	p_cam->ApplyOrthoMatrixLH( 1.0f, 250.0f);

	D3DXMATRIX matView;
	D3DXMatrixTranslation( &matView, 0.0f, 0.0f, 2.0f);
	this->SetViewMatrix( matView);

	D3DXMATRIX matWorld;
	D3DXMatrixIdentity( &matWorld);
	this->SetWorldMatrix( matWorld);

	this->SetDepthStencilState( true, true);
	this->SetRasterizerState( false, m_wireframe);
	this->SetBlendState( true);

	// Render 2D stuff... again, once I get ON my fat arse and implement the rest of the code.
}

/**
* Reloads the shader files so that modifications can be done during run-time and the result can be
* seen immediately instead of re-compiling the entire solution.
*/
void Application::ReloadShaders() {

}

/**
* Initialise the program for viewing a cut scene in the independent viewer.
*
* @param scriptName :: The name of the script that contains the cut scene information.
*
* @return True if no errors occurred.
*/
bool Application::InitForViewing( const char* scriptName) {
	std::ifstream in( scriptName, std::ios::in);
	if( !in.is_open()) {
		MessageBox( nullptr, "Couldn't open script.", "Application::InitForViewing", MB_OK);
		return false;
	}

	// Load in the scene.
	this->OpenScene( scriptName);

	// Stop the camera object from being rendered.
	SceneObject* p_object( this->FindObject( "Obj_Camera"));
	if( p_object == nullptr)
		this->CreateCameraObject();
	ActionBase* p_action = new ActionVisible( this->GetValidActionName(), 0, p_object, false);
	p_object->AddAction( p_action);

	// Move the frame position to the start.
	this->GoToFrame( 0);

	return true;
}

/**
* Searches for an object in the scene with the specified name. If it was found, then it returns a
* pointer to it, otherwise it returns 'nullptr'.
*
* @param name	:: The name of the object.
*
* @return A pointer to the object that was found (or 'nullptr' if it wasn't found).
*/
SceneObject* Application::FindObject( const char* name) const {
	SceneObject* p_object( nullptr);

	for( std::vector<SceneObject*>::const_iterator it( mp_scene.begin()); it != mp_scene.end(); ++it) {
		if( strcmp( (*it)->m_name, name) == 0) {
			p_object = (*it);
			break;
		}
	}

	return p_object;
}

/**
* Deletes all the objects in the cut scene.
*/
void Application::DeleteScene() {
	// Delete all of the objects.
	for( std::vector<SceneObject*>::iterator it( mp_scene.begin()); it != mp_scene.end(); ++it)
		SafeDelete( *it);
	mp_scene.clear();

	// Delete all of the independant actions.
	//for( std::vector<ActionBase*>::iterator it( mp_actions.begin()); it != mp_actions.end(); ++it)
	//	SafeDelete( *it);
	//mp_actions.clear();

	// Delete all of the audio files.
	Audio::GetInstance().DeleteAllAudio();
}

/**
* Creates a 'SceneObject' that'll be used to tell the camera what to do.
*/
void Application::CreateCameraObject() {
	this->CreateObject( "Obj_Camera", "Sphere", 1.0f);
	SceneObject* p_object( this->FindObject( "Obj_Camera"));
	p_object->AttachCamera( mp_sceneCamera);
}

/**
* Deletes a specified object from the scene.
*
* @param name	:: The name of the object.
*/
void Application::DeleteObject( const char* name) {
	for( std::vector<SceneObject*>::iterator it( mp_scene.begin()); it != mp_scene.end(); ++it) {
		if( strcmp( (*it)->m_name, name) == 0) {
			SafeDelete( *it);
			mp_scene.erase( it);
			break;
		}
	}

	float param1( 0.0f), param2( 0.0f), param3( 0.0f);
	int index( 0), start( 0), end( 0);
	const char* actionName( "");
	const char* targetObject( "");
	SceneObject* p_object( this->FindObject( "Obj_Camera"));
	p_object->GetActionInfo( actionName, start, end, index);
	while( actionName != "") {
		p_object->GetActionInfoAtFrame( actionName, start, param1, param2, param3, targetObject);

		if( strcmp( name, targetObject) == 0) {
			p_object->DeleteAction( actionName);
			break;
		}

		++index;
		p_object->GetActionInfo( actionName, start, end, index);
	}
}


//void CreateHuman( const std::string& name, const float& scale = 1.0f);
//void SetHumanPosition( const std::string& name, const D3DXVECTOR3& pos);
//void SetObjectRotation( const std::string& name, const D3DXVECTOR3& rotation);
//void MoveObjectTo( const std::string& name, const D3DXVECTOR3& pos);
//void RotateObjectTo( const std::string& name, const D3DXVECTOR3& pos);
//void SetObjectVisibility( const std::string& name, const bool& vis);
//void NextStep();
//void LoadAudio( char* fileName);
//void PlayAudio( char* fileName, const float& volume);
//void WaitForAudio( char* fileName);
//void RunScript( const std::string& fileName);
//void LinkCamThirdPerson( char* name);

/**
* Clears the current scene.
*/
void Application::ClearScene() {
	this->DeleteScene();
}

/**
* Saves the current scene.
*/
void Application::SaveScene( const char* fileName) {
	const char* namePrefix = "Object";

	// Sort all the actions in the scene so they're in chronological order within each object.
	for( size_t i( 0); i < mp_scene.size(); ++i)
		mp_scene[i]->SortActions();

	// Get the date.
	time_t t = time( 0);
	tm* time = localtime( &t);

	// Open the file to begin writing to.
	std::ofstream output;
	output.open( fileName, std::ios::out);

	char buffer[256];
	int fileNameIndex( strlen( fileName));
	while( fileName[fileNameIndex] != '\\')
		--fileNameIndex;
	strcpy( buffer, &fileName[fileNameIndex+1]);

	std::ostringstream date;
	date << 1900 + time->tm_year << " / ";
	// Add the month (with a '0' before if it's less than 10).
	if( time->tm_mon < 10)
		date << "0";
	date << time->tm_mon << " / ";
	// Add the day (with a '0' before if it's less than 10).
	if( time->tm_mday < 10)
		date << "0";
	date << time->tm_mday;

	// Output the comment block at the top that details some information.
	output << "-------------------------------------------------\n"
		   << "---- SCRIPT :- " << buffer << "\n"
		   << "---- DATE   :- " << date.str() << "\n"
		   << "-------------------------------------------------\n"
		   << "\n";

	// Create the frame number variable and the function to increment it.
	output << "-------------------------------------------------\n"
		   << "-- Variable and function to increment the frame number.\n"
		   << "-------------------------------------------------\n"
		   << "varFrame = -1\n"
		   << "\n"
		   << "function IncrementFrame()\n"
		   << "\tvarFrame = varFrame + 1\n"
		   << "\treturn varFrame\n"
		   << "end\n"
		   << "\n";

	// Output the names of the objects.
	output << "-------------------------------------------------\n"
		   << "-- Names for all the objects in the scene.\n"
		   << "-------------------------------------------------\n";
	for( size_t i( 0); i < mp_scene.size(); ++i)
		output << namePrefix << mp_scene[i]->m_name << " = \"" << mp_scene[i]->m_name << "\"\n";
	// A little gap.
	output << "\n";

	// Create all of the objects.
	output << "-------------------------------------------------\n"
		   << "-- Create all of the scene's objects.\n"
		   << "-------------------------------------------------\n";
	for( size_t i( 0); i < mp_scene.size(); ++i) {
		const char* type( "");
		float param[3] = { 0.0f, 0.0f, 0.0f };
		int meshType( 0);
		
		mp_scene[i]->GetMeshParameters( type, param[0], param[1], param[2]);

		for( meshType; meshType < gs_meshNamesCount; ++meshType)
			if( strcmp( type, gs_meshNames[meshType][0]) == 0)
				break;

		if( meshType == gs_meshNamesCount)
			ASSERT_ALWAYS();

		output << gs_createObject << "( "
			   << namePrefix << mp_scene[i]->m_name << ", "
			   << "\"" << type << "\"";

		for( int j( 0); j < 3; ++j) {
			if( strcmp( gs_meshNames[meshType][j+1], "") != 0)
				output << ", " << param[j];
			else
				break;
		}

		// Move on to the next line.
		output << ")\n";
	}
	// A little gap.
	output << "\n";

	// List all of the actions, setting the frame as we go.
	output << "-------------------------------------------------\n"
		   << "-- Start of the scene's actions.\n"
		   << "-------------------------------------------------\n";

	int* indicies = new int[mp_scene.size()];
	unsigned int lastFrame( 0);
	this->GetLastFrame( lastFrame);

	// Initialise the indicies array.
	for( int i( 0); i < mp_scene.size(); ++i)
		indicies[i] = 0;

	// Run through all the frames.
	for( unsigned int i( 0); i <= lastFrame; ++i) {
		// Output the frame number.
		output << "-- Frame " << i << "\n"
			   << gs_frame << "( IncrementFrame())\n"
			   << "\n";

		// Run through all the objects in the scene.
		for( int obj( 0); obj < mp_scene.size(); ++obj) {
			const char* name( "");
			int start( 0), end( 0);

			// Keep outputting the object's actions while the action's start frame is equal to the
			// current frame.
			mp_scene[obj]->GetActionInfo( name, start, end, indicies[obj]);
			while( name != "" && start == i) {
				const char* funcName( "");
				const char* type( "");
				int actionIndex( 0);
				float param[3] = { 0.0f, 0.0f, 0.0f };
				const char* param4 = "";

				// I need to call this to get the type... nothing else.
				mp_scene[obj]->GetActionInfo( name, type, start, end);
				mp_scene[obj]->GetActionInfoAtFrame( name, end, param[0], param[1], param[2], param4);

				// Get the name of the function for the action type.
				if( strcmp( type, "MoveTo") == 0)			funcName = gs_moveTo;
				else if( strcmp( type, "PositionAt") == 0)	funcName = gs_positionAt;
				else if( strcmp( type, "RotateTo") == 0)	funcName = gs_rotateTo;
				else if( strcmp( type, "Visible") == 0)		funcName = gs_visibility;
				else if( strcmp( type, "Audio") == 0)		funcName = gs_audioPlay;
				else if( strcmp( type, "Camera") == 0)		funcName = gs_camera;

				output << funcName << "( " << namePrefix << mp_scene[obj]->m_name << ", \"" << name << "\"";

				for( actionIndex; actionIndex < gs_actionTypeNamesCount; ++actionIndex)
					if( strcmp( type, gs_actionTypeNames[actionIndex][0]) == 0)
						break;

				if( strcmp( type, "Visible") == 0) {
					output << ", " << ((param[0] == 1.0f) ? "true" : "false");
				}
				else if( strcmp( type, "Audio") == 0) {
					output << ", \"" << param4 << "\", " << param[0];
				}
				else if( strcmp( type, "Camera") == 0) {
					output << ", \"" << param4 << "\"";
				}
				else {
					// Something went wrong.
					if( actionIndex == gs_meshNamesCount)
						ASSERT_ALWAYS();

					for( int j( 0); j < 3; ++j) {
						if( strcmp( gs_actionTypeNames[actionIndex][j+1], "") != 0)
							output << ", " << param[j];
						else
							break;
					}
				}
				output << ")\n";

				// Get the next action's information.
				++indicies[obj];
				mp_scene[obj]->GetActionInfo( name, start, end, indicies[obj]);
			}
		}

		// Create a space between the last action and the next frame.
		output << "\n";
	}

	SafeDeleteArray( indicies);

	// Close the file.
	output.close();
}

/**
* Opens a saved scene.
*/
void Application::OpenScene( const char* fileName) {
	// Clear the scene first.
	this->DeleteScene();

	OutputDebugString( fileName);

	bool audioWasPlaying( this->GetAudioPlaying());

	this->SetAudioPlaying( false);
	m_loadingFromFile = true;
	LuaReader::Instance().ReadScript( fileName);
	m_loadingFromFile = false;
	this->SetAudioPlaying( audioWasPlaying);
}

/**
* Moves the cut scene to the specified frame.
*
* @param frame :: The frame number to move the cut scene to.
*/
void Application::GoToFrame( unsigned int frame) {
	m_frameLast = m_frame;
	m_frame = frame;
}

/**
* Gets the last frame of the currently stored cut scene. Taking in to account all of the existing
* actions (including those attached to objects and the 'free' actions).
*
* @param frame :: The variable that will hold the frame number of the last frame.
*/
void Application::GetLastFrame( unsigned int& frame) {
	frame = 0;

	// Run through all the actions of each object.
	for( std::vector<SceneObject*>::iterator it( mp_scene.begin()); it != mp_scene.end(); ++it)
		if( (*it)->GetLastFrame() > frame)
			frame = (*it)->GetLastFrame();

	// Run through all the actions which aren't associated with any object.
	//for( std::vector<ActionBase*>::iterator it( mp_actions.begin()); it != mp_actions(); ++it)
	//	if( (*it)->GetLastFrame() > frame)
	//		frame = (*it)->GetLastFrame();
}

/**
* Creates an object for the cut scene with the specified details. The three 'param' parameters are
* all dependant on the 'type' of the object. For more information on what these parameters
* represent, look at the 'CommonMesh::New*obj*Mesh()' functions.
*
* @param name	:: The name of the object; this MUST be unique.
* @param type	:: The type of object (e.g. "Sphere", "Cube", "Torus" etc.).
* @param param1	:: Default: 0.0f. The first parameter.
* @param param2 :: Default: 0.0f. The second parameter.
* @param param3 :: Default: 0.0f. The third parameter.
*/
void Application::CreateObject( const char* name, const char* type, float param1 /*= 0.0f*/, float param2 /*= 0.0f*/, float param3 /*= 0.0f*/) {
	// Position the object at the centre of the scene.
	unsigned int dim( 10);
	CommonMesh* p_mesh( nullptr);
	D3DXVECTOR3 pos( ZeroVector);

	// Create a shape defined by the parameters.
	if( strcmp( type, "Sphere") == 0) {
		p_mesh = CommonMesh::NewSphereMesh( this, param1, dim, dim);
		pos.y = param1;
	}
	else if( strcmp( type, "Cone") == 0) {
		p_mesh = CommonMesh::NewCylinderMesh( this, 0.0f, param1, param2, dim, dim);
		pos.y = param2 / 2;
	}
	else if( strcmp( type, "Cylinder") == 0) {
		p_mesh = CommonMesh::NewCylinderMesh( this, param1, param2, param3, dim, dim);
		pos.y = param3 / 2;
	}
	else if( strcmp( type, "Cube") == 0) {
		p_mesh = CommonMesh::NewBoxMesh( this, param1, param2, param3);
		pos.y = param3 / 2;
	}
	else if( strcmp( type, "Teapot") == 0) {
		p_mesh = CommonMesh::NewTeapotMesh( this);
		pos.y = 2.0f;
	}
	else if( strcmp( type, "Torus") == 0) {
		p_mesh = CommonMesh::NewTorusMesh( this, param1, param2, dim, dim);
		pos.y = param1;
	}
	else {
		MessageBox( HWND_DESKTOP, "Application::CreateObject: Invalid shape.", "Object Creation Error", MB_OK);
		return;
	}

	// If there's already a model with this name, delete it.
	if( this->FindObject( name) != nullptr)
		this->DeleteObject( name);

	// Create the object.
	SceneObject* p_object = new SceneObject( p_mesh, name, pos, ForwardVector);
	p_object->StoreMeshParameters( type, param1, param2, param3);

	// Don't add the default actions if the objects are being created from a file.
	if( !m_loadingFromFile) {
		// Add a few actions so that the object will be 'reset' when the frame counter is reset (so
		// that if it does other actions and then goes back to 0 it'll be reset correctly).
		ActionBase* p_actPos = new ActionPosition( "Act_InitialPosition", 0, p_object, pos);
		//ActionBase* p_actRot = new ActionRotation( "Act_InitialRotation", 0, p_object, D3DXVECTOR3( 0.0f, 0.0f, 0.0f));
		ActionBase* p_actVis = new ActionVisible( "Act_InitialVisibility", 0, p_object, true);

		p_object->AddAction( p_actPos);
		//p_object->AddAction( p_actRot);
		p_object->AddAction( p_actVis);
	}

	// Then add it to the scene.
	mp_scene.push_back( p_object);
}

/**
* Adds an action to a object. The three 'param' parameters are all dependant on the 'type' of the
* action. For more information on what these parameters represent, look at the 'gs_actionNames'
* array which gives the names of all the available actions. These actions are text representations
* of class; e.g. "MoveTo" is for the class "ActionMove". The 'param' parameters are used in the
* constructors of these classes.
*
* @param objectName	:: The name of the object that the action will be attached to. If this name is
*						empty (i.e. "") then the action will be treated as a 'free' action.
* @param actionType	:: The type of action that will be created (e.g. movement, rotation, visibility
*						etc.).
* @param param1		:: Default: 0.0f. The first parameter.
* @param param2		:: Default: 0.0f. The second parameter.
* @param param3		:: Default: 0.0f. The third parameter.
*/
void Application::AddAction( const char* objectName, const char* actionName, const char* actionType, float param1 /*= 0.0f*/, float param2 /*= 0.0f*/, float param3 /*= 0.0f*/, const char* param4 /*= ""*/) {
	SceneObject* p_object( this->FindObject( objectName));
	ActionBase* p_action( nullptr);

	if( p_object == nullptr) {
		MessageBox( HWND_DESKTOP, "For now, attach actions to an object.", "Application::AddAction", MB_OK);
		return;
	}

	if( strcmp( actionType, "MoveTo") == 0)
		p_action = new ActionMove( actionName, m_frame, p_object, D3DXVECTOR3( param1, param2, param3));
	else if( strcmp( actionType, "PositionAt") == 0)
		p_action = new ActionPosition( actionName, m_frame, p_object, D3DXVECTOR3( param1, param2, param3));
	else if( strcmp( actionType, "RotateTo") == 0)
		p_action = new ActionRotate( actionName, m_frame, p_object, D3DXVECTOR3( param1, param2, param3));
	else if( strcmp( actionType, "Visible") == 0)
		p_action = new ActionVisible( actionName, m_frame, p_object, param1 == 1.0f);
	else if( strcmp( actionType, "Audio") == 0)
		p_action = new ActionAudio( actionName, m_frame, p_object, param4, AAudio::AAudio_Play, param1);
	else if( strcmp( actionType, "Camera") == 0)
		p_action = new ActionCamera( actionName, m_frame, p_object, param4);

	if( p_action == nullptr) {
		MessageBox( HWND_DESKTOP, "Invalid action type.", "Application::AddAction", MB_OK);
		return;
	}

	p_object->AddAction( p_action);
}

/**
* Edits an existing action to have new information.
*
* @param objectName		:: The name of the object that the action if attached to. If this name is
*							empty (i.e. "") then the action will be treated as a 'free' action.
* @param actionName		:: The name of the action to be modified.
* @param newActionName	:: The new name of the action.
* @param param1			:: The first parameter.
* @param param2			:: The second parameter.
* @param param3			:: The third parameter.
*/
void Application::EditAction( const char* objectName, const char* actionName, const char* newActionName, float param1, float param2, float param3, const char* param4) {
	SceneObject* p_object( this->FindObject( objectName));

	if( p_object == nullptr) {
		MessageBox( HWND_DESKTOP, "Invalid object name.", "Application::EditAction", MB_OK);
		return;
	}

	// At the moment, it's going to be easier to just get the action's information, delete it, and
	// create a new one to add.
	ActionBase* p_action( nullptr);
	const char* actionType( "");
	int start( 0), end( 0);
	p_object->GetActionInfo( actionName, actionType, start, end);
	p_object->DeleteAction( actionName);

	if( strcmp( actionType, "MoveTo") == 0)
		p_action = new ActionMove( newActionName, start, p_object, D3DXVECTOR3( param1, param2, param3));
	else if( strcmp( actionType, "PositionAt") == 0)
		p_action = new ActionPosition( newActionName, start, p_object, D3DXVECTOR3( param1, param2, param3));
	else if( strcmp( actionType, "RotateTo") == 0)
		p_action = new ActionRotate( newActionName, start, p_object, D3DXVECTOR3( param1, param2, param3));
	else if( strcmp( actionType, "Visible") == 0)
		p_action = new ActionVisible( newActionName, start, p_object, param1 == 1.0f);
	else if( strcmp( actionType, "Audio") == 0)
		p_action = new ActionAudio( actionName, start, p_object, param4, AAudio::AAudio_Play, param1);

	if( p_action == nullptr) {
		MessageBox( HWND_DESKTOP, "Invalid action type.", "Application::EditAction", MB_OK);
		return;
	}

	p_object->AddAction( p_action);
}

/**
* Deletes an existing action.
*
* @param objectName	:: The name of the object.
* @param actionName	:: The name of the action to be deleted.
*/
void Application::DeleteAction( const char* objectName, const char* actionName) {
	SceneObject* p_object( this->FindObject( objectName));

	if( p_object == nullptr)
		return;

	p_object->DeleteAction( actionName);
}

/**
* Gets the position of a specified object.
*
* @param name	:: The name of the object.
* @param x		:: The variable to hold the object's x position.
* @param y		:: The variable to hold the object's y position.
* @param z		:: The variable to hold the object's z position.
*/
void Application::GetPosition( const char* objectName, float& x, float& y, float& z) {
	SceneObject* p_object( this->FindObject( objectName));

	// If the object wasn't found then don't modify the values.
	if( p_object != nullptr) {
		x = p_object->m_position.x;
		y = p_object->m_position.y;
		z = p_object->m_position.z;
	}
}

/**
* Gets the rotation of a specified object.
*
* @param name	:: The name of the object.
* @param x		:: The variable to hold the object's x rotation.
* @param y		:: The variable to hold the object's y rotation.
* @param z		:: The variable to hold the object's z rotation.
*/
void Application::GetRotation( const char* objectName, float& x, float& y, float& z) {
	SceneObject* p_object( this->FindObject( objectName));

	// If the object wasn't found then don't modify the values.
	if( p_object != nullptr) {
		x = p_object->m_rotation.x;
		y = p_object->m_rotation.y;
		z = p_object->m_rotation.z;
	}
}

/*
* Gets the visibility of an object in the scene.
*
* @param name	:: The name of the object.
* @param vis	:: The variable to hold the object's visibility.
*
* @return True if there were no errors.
*/
void Application::GetVisibility( const char* objectName, bool& vis) {
	SceneObject* p_object( this->FindObject( objectName));

	if( p_object != nullptr)
		vis = p_object->GetVisibility();
}

/**
* Gets the name of an object. This is to be used when running through all the objects in the scene
* without wanting to get the information of a specific object; e.g. spew out a list of the current
* objects in the scene to the scene graph window.
*
* @param index :: The position of the object in the 'mp_scene' vector.
*
* @return The name of the object at the parameter's position. If the parameter was invalid, "" is
*			returned.
*/
const char* Application::GetObjectName( int index) const {
	if( index >= 0 && index < mp_scene.size())
		return mp_scene[index]->m_name;

	return "";
}

/**
* Gets the name of an action. This is to be used when running through all the actions of a certain
* object without wanting to get a specific action's information; e.g. spew out a list of the
* current actions attached to a specific object.
*
* @param objectName	:: The name of the object.
* @param index		:: The position of the action in the object's action vector.
*
* @return The name of the action. If either parameter is invalid, it returns "".
*/
const char* Application::GetActionName( const char* objectName, int index) const {
	SceneObject* p_object( this->FindObject( objectName));
	const char* name = "";
	int a( 0), b( 0);

	if( p_object != nullptr)
		p_object->GetActionInfo( name, a, b, index);

	return name;
}

/**
* Gets the attributes of a specified object. This is to be used when running through all the
* actions of a certain object to search for something; e.g. that a frame position does/doesn't
* overlap with an existing action.
* 
* @param objectName	:: The name of the object.
* @param start		:: The frame position the action starts at.
* @param end		:: The frame position the action ends at.
* @param index		:: The index of the action in the object's action vector.
*
* @return The name of the action. If the 'objectName' or 'index' is invalid, then it'll return "".
*/
const char* Application::GetObjectsActionInfo( const char* objectName, int& start, int& end, int index) const {
	SceneObject* p_object( this->FindObject( objectName));
	const char* actionName = "";

	if( p_object == nullptr) {
		start = 0;
		end = 0;
	}
	else {
		p_object->GetActionInfo( actionName, start, end, index);
	}

	return actionName;
}

/**
* Gets the attributes of a specified action from its object. If the object or action name is
* invalid, then the actionType will be "" and the start and end values will be 0.
*
* @param objectName	:: The name of the object.
* @param actionName	:: The name of the action.
* @param actionType	:: The type of the action.
* @param start		:: The frame position the action starts at.
* @param end		:: The frame position the action ends at.
*/
void Application::GetObjectsActionInfo( const char* objectName, const char* actionName, const char* &actionType, int& start, int& end) const {
	SceneObject* p_object( this->FindObject( objectName));

	if( p_object == nullptr) {
		actionType = "";
		start = 0;
		end = 0;
	}
	else {
		p_object->GetActionInfo( actionName, actionType, start, end);
	}
}

/**
* Gets the information about an action at the specified frame.
*
* @param objectName	:: The name of the object.
* @param actionName	:: The name of the action.
* @param frame		:: The frame position to extract the information at.
* @param param1		:: The first parameter.
* @param param2		:: The second parameter.
* @param param3		:: The third parameter.
* @param param4		:: The fourth parameter.
*/
void Application::GetActionInfoAtFrame( const char* objectName, const char* actionName, int frame, float& param1, float& param2, float& param3, const char* &param4) {
	SceneObject* p_object( this->FindObject( objectName));

	if( p_object == nullptr) {
		param1 = 0.0f;
		param2 = 0.0f;
		param3 = 0.0f;
		param4 = "";
	}
	else {
		p_object->GetActionInfoAtFrame( actionName, frame, param1, param2, param3, param4);
	}
}


/**
* Gets the name of a valid model that an object can use as its mesh.
*
* @param index	:: The index position of a model name in the 'gs_meshNames' array.
*
* @return The name of a valid model.
*/
const char* Application::GetName(int index) const {
	if( index >= 0 && index < gs_meshNamesCount)
		return gs_meshNames[index][0];

	// If this line is hit then it means the index value was invalid.
	return "";
}

/**
* Gets the name of a valid action type that an action can use as its type.
*
* @param index	:: The index position of an action type name in the 'gs_actionTypeNames' array.
*
* @return The name of a valid action type.
*/
const char* Application::GetActionTypeName(int index) const {
	if( index >= 0 && index < gs_actionTypeNamesCount)
		return gs_actionTypeNames[index][0];

	// If this line is hit then it means the index value was invalid.
	return "";
}

/**
* Gets a valid name for an object.
*
* @return The valid name.
*/
const char* Application::GetValidObjectName() const {
	int count = 0;
	char buffer[256];
	bool taken( true);
	
	while( taken) {
		std::ostringstream name;
		taken = false;
		name << "object";
		if( count < 100)
			name << "0";
		if( count < 10)
			name << "0";
		name << count;

		for( std::vector<SceneObject*>::const_iterator it( mp_scene.begin()); it != mp_scene.end(); ++it) {
			if( strcmp( (*it)->m_name, name.str().c_str()) == 0) {
				++count;
				taken = true;
				break;
			}
		}

		if( !taken)
			strcpy_s( buffer, 256, name.str().c_str());
	}

	return buffer;
}

/**
* Gets a valid name for an action.
*
* @return The valid name.
*/
const char* Application::GetValidActionName() const {
	int count = 0;
	char buffer[256];
	bool taken( true);
	
	while( taken) {
		std::ostringstream name;
		taken = false;
		name << "action";
		if( count < 100)
			name << "0";
		if( count < 10)
			name << "0";
		name << count;

		for( std::vector<SceneObject*>::const_iterator it( mp_scene.begin()); it != mp_scene.end(); ++it) {
			if( !(*it)->IsActionNameValid( name.str().c_str())) {
				++count;
				taken = true;
				break;
			}
		}

		// Uncomment once free actions have been implemented.
		//for( std::vector<ActionBase*>::const_iterator it( mp_actions.begin()); it != mp_actions.end(); ++it) {
		//	if( strcmp( (*it)->GetName(), name.str().c_str()) == 0) {
		//		++count;
		//		taken = true;
		//		break;
		//	}
		//}

		if( !taken)
			strcpy_s( buffer, 256, name.str().c_str());
	}

	return buffer;
}

/**
* Checks an action name to see if it's valid.
*
* @param actionName	:: The name of the action.
*
* @return True if the name doesn't conflict with any existing actions.
*/
bool Application::IsActionNameValid( const char* actionName) const {
	for( std::vector<SceneObject*>::const_iterator it( mp_scene.begin()); it != mp_scene.end(); ++it)
		if( !(*it)->IsActionNameValid( actionName))
			return false;

	//for( std::vector<ActionBase*>::const_iterator it( mp_actions.begin()); it != mp_actions.end(); ++it)
	//	if( strcmp( (*it)->GetName(), actionName.str().c_str()) == 0)
	//		return false;

	// If the function gets to this point, then it's not being used anywhere.
	return true;
}

/**
* Retrieves the names of the parameters required for the model.
*
* @param model	:: The name of the mesh.
* @param param1	:: The variable to hold the first parameter name.
* @param param2	:: The variable to hold the second parameter name.
* @param param3	:: The variable to hold the third parameter name.
*/
void Application::GetObjectParameterName( const char* mesh, const char* &param1, const char* &param2, const char* &param3) const {
	int index( 0);
	for( index; index < gs_meshNamesCount; ++index)
		if( strcmp( mesh, gs_meshNames[index][0]) == 0)
			break;

	// The mesh name was invalid and wasn't found in the array.
	if( index == gs_meshNamesCount) {
		param1 = "";
		param2 = "";
		param3 = "";
	}
	else {
		param1 = gs_meshNames[index][1];
		param2 = gs_meshNames[index][2];
		param3 = gs_meshNames[index][3];
	}
}

/**
* Retrieves the names of the parameters required for the action.
*
* @param action	:: The name of the action.
* @param param1	:: The variable to hold the first parameter name.
* @param param2	:: The variable to hold the second parameter name.
* @param param3	:: The variable to hold the third parameter name.
*/
void Application::GetActionParameterName( const char* mesh, const char* &param1, const char* &param2, const char* &param3) const {
	int index( 0);
	for( index; index < gs_actionTypeNamesCount; ++index)
		if( strcmp( mesh, gs_actionTypeNames[index][0]) == 0)
			break;

	// The mesh name was invalid and wasn't found in the array.
	if( index == gs_actionTypeNamesCount) {
		param1 = "";
		param2 = "";
		param3 = "";
	}
	else {
		param1 = gs_actionTypeNames[index][1];
		param2 = gs_actionTypeNames[index][2];
		param3 = gs_actionTypeNames[index][3];
	}
}

/**
* Calculates the end frame of an action.
*
* @param actionType	:: The type of action.
* @param xStart		:: The starting x value of the object.
* @param yStart		:: The starting y value of the object.
* @param zStart		:: The starting z value of the object.
* @param xEnd		:: The ending x value of the object.
* @param yEnd		:: The ending y value of the object.
* @param zEnd		:: The ending z value of the object.
* @param length		:: The length of the action.
*/
void Application::CalculateActionLength( const char* actionType, float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd, const char* textParam, int& length) {
	if( strcmp( actionType, "MoveTo") == 0)
		ActionMove::CalculateActionLength( D3DXVECTOR3( xStart, yStart, zStart), D3DXVECTOR3( xEnd, yEnd, zEnd), length);
	else if( strcmp( actionType, "PositionAt") == 0)
		ActionPosition::CalculateActionLength( length);
	else if( strcmp( actionType, "RotateTo") == 0)
		ActionRotate::CalculateActionLength( D3DXVECTOR3( xEnd - xStart, yEnd - yStart, zEnd - zStart), length);
	else if( strcmp( actionType, "Visible") == 0)
		ActionVisible::CalculateActionLength( length);
	else if( strcmp( actionType, "Audio") == 0)
		ActionAudio::CalculateActionLength( textParam, length);
	else
		MessageBox( HWND_DESKTOP, "Invalid action name.", "Application::CalculateActionLength", MB_OK);
}

/**
* Set whether audio should be played or not.
*
* @param play :: Set to true to allow audio to play.
*/
void Application::SetAudioPlaying( bool play) {
	m_playAudio = play;
	if( m_playAudio)
		ActionAudio::s_prevFrame = -1;
}

/**
* Get whether audio should be played or not.
*
* @return True if audio will play.
*/
bool Application::GetAudioPlaying() const {
	return m_playAudio;
}

/**
* Stops any existing audio files from playing.
*/
void Application::StopAudio() {
	Audio::sp_audio->StopAllAudio();
}

/**
* Sets whether the toolset should look through the scene camera or the global camera.
*
* @param globalCam :: Set to true if the cut scene should render from the global camera.
*/
void Application::UseGlobalCamera( bool globalCam) {
	m_globalCam = globalCam;
}

/**
* Gets whether the toolset should look through the scene camera or the global camera.
*
* @return True if the cut scene should render from the global camera.
*/
bool Application::UseGlobalCamera() {
	return m_globalCam;
}

/**
* Orbits the camera by a specified amount.
*
* @param x	:: The mouse's x movement.
* @param y	:: The mouse's y movement.
*
* @return True if there were no errors.
*/
void Application::CameraOrbit( int x, int y) {
	// If a crazy value is passed through, ignore it.
	if( abs( x) > 50 || abs( y) > 50)
		return;

	mp_camera->m_rotX += x;
	mp_camera->m_rotY += y;
}

/**
* Zooms the camera by a specified amount.
*
* @param x	:: The mouse's x movement.
* @param y	:: The mouse's y movement.
*
* @return True if there were no errors.
*/
void Application::CameraZoom( int x, int y) {
	// If a crazy value is passed through, ignore it.
	if( abs( x) > 50 || abs( y) > 50)
		return;

	// One is negative and one is positive, so find the difference and zoom based on the difference.
	if( (x > 0 && y < 0) || (x < 0 && y > 0)) {
		x = x + y;
		y = 0;
	}

	float length( (float)sqrt( (float)(x*x) + (float)(y*y)));
	//D3DXVECTOR3 direction( mp_camera->m_position - mp_camera->m_lookAt);
	//D3DXVec3Normalize( &direction, &direction);

	// Both negative, so flip the length variable (the minus would've been multiplied out).
	if( x <= 0 && y <= 0)
		length *= -1;

	mp_camera->m_zDist += length;
	//direction = direction * length;
	//mp_camera->m_position += direction;
}

/**
* TEST FUNCTION - This just tests to see if something works.
*/
void Application::TestFunction( const char* &test) const {
	test = gs_meshNames[0][0];
}


/********************************************************************
*	The following functions are exposed to Lua.
********************************************************************/
/**
* 
*/
int Application::LuaLink_SetFrame() {
	int n( 0);

	// Get the number of parameters.
	ASSERT( LuaReader::Instance().GetNumberOfParameters( n, 1));

	// Get the parameter.
	ASSERT( LuaReader::Instance().GetParameter<int>( 1, LuaReturnType::LRT_Int, n));

	// Call the function.
	Application::sp_app->GoToFrame( n);

	// Return how many things have been pushed onto the stack.
	return 0;
}

/**
* 
*/
int Application::LuaLink_CreateObject() {
	int n( 0);
	const char* name( "");
	const char* type( "");
	float param[3] = { 0.0f, 0.0f, 0.0f };

	// Get the number of parameters.
	ASSERT( LuaReader::Instance().GetNumberOfParameters( n, 2));

	// Get the parameters.
	ASSERT( LuaReader::Instance().GetParameterString( 1, name));
	ASSERT( LuaReader::Instance().GetParameterString( 2, type));
	for( int i( 0); i < n - 2; ++i)
		ASSERT( LuaReader::Instance().GetParameter<float>( i + 3, LuaReturnType::LRT_Float, param[i]));

	// Call the function.
	if( strcmp( name, "Obj_Camera") == 0)
		Application::sp_app->CreateCameraObject();
	else
		Application::sp_app->CreateObject( name, type, param[0], param[1], param[2]);

	// Return how many things have been pushed onto the stack.
	return 0;
}

/**
* 
*/
int Application::LuaLink_PositionObjectAt() {
	int n( 0);
	const char* nameObj( "");
	const char* nameAct( "");
	float param[3] = { 0.0f, 0.0f, 0.0f };

	// Get the number of parameters.
	ASSERT( LuaReader::Instance().GetNumberOfParameters( n, 5));

	// Get the parameters.
	ASSERT( LuaReader::Instance().GetParameterString( 1, nameObj));
	ASSERT( LuaReader::Instance().GetParameterString( 2, nameAct));
	for( int i( 0); i < 3; ++i)
		ASSERT( LuaReader::Instance().GetParameter<float>( i + 3, LuaReturnType::LRT_Float, param[i]));

	// Call the function.
	Application::sp_app->AddAction( nameObj, nameAct, "PositionAt", param[0], param[1], param[2]);

	// Return how many things have been pushed onto the stack.
	return 0;
}

/**
* 
*/
int Application::LuaLink_MoveObjectTo() {
	int n( 0);
	const char* nameObj( "");
	const char* nameAct( "");
	float param[3] = { 0.0f, 0.0f, 0.0f };

	// Get the number of parameters.
	ASSERT( LuaReader::Instance().GetNumberOfParameters( n, 5));

	// Get the parameters.
	ASSERT( LuaReader::Instance().GetParameterString( 1, nameObj));
	ASSERT( LuaReader::Instance().GetParameterString( 2, nameAct));
	for( int i( 0); i < 3; ++i)
		ASSERT( LuaReader::Instance().GetParameter<float>( i + 3, LuaReturnType::LRT_Float, param[i]));

	// Call the function.
	Application::sp_app->AddAction( nameObj, nameAct, "MoveTo", param[0], param[1], param[2]);

	// Return how many things have been pushed onto the stack.
	return 0;
}

/**
* 
*/
int Application::LuaLink_RotateObjectTo() {
	int n( 0);
	const char* nameObj( "");
	const char* nameAct( "");
	float param[3] = { 0.0f, 0.0f, 0.0f };

	// Get the number of parameters.
	ASSERT( LuaReader::Instance().GetNumberOfParameters( n, 5));

	// Get the parameters.
	ASSERT( LuaReader::Instance().GetParameterString( 1, nameObj));
	ASSERT( LuaReader::Instance().GetParameterString( 2, nameAct));
	for( int i( 0); i < 3; ++i)
		ASSERT( LuaReader::Instance().GetParameter<float>( i + 3, LuaReturnType::LRT_Float, param[i]));

	// Call the function.
	Application::sp_app->AddAction( nameObj, nameAct, "RotateTo", param[0], param[1], param[2]);

	// Return how many things have been pushed onto the stack.
	return 0;
}

/**
* 
*/
int Application::LuaLink_SetObjectVisibility() {
	int n( 0);
	const char* nameObj( "");
	const char* nameAct( "");
	bool param( true);

	// Get the number of parameters.
	ASSERT( LuaReader::Instance().GetNumberOfParameters( n, 3));

	// Get the parameters.
	ASSERT( LuaReader::Instance().GetParameterString( 1, nameObj));
	ASSERT( LuaReader::Instance().GetParameterString( 2, nameAct));
	ASSERT( LuaReader::Instance().GetParameter<bool>( 3, LuaReturnType::LRT_Bool, param));

	// Call the function.
	Application::sp_app->AddAction( nameObj, nameAct, "Visible", param ? 1.0f : 0.0f);

	// Return how many things have been pushed onto the stack.
	return 0;
}

///**
//* 
//*/
//int Application::LuaLink_LoadAudio() {
//
//}

/**
* 
*/
int Application::LuaLink_PlayAudio() {
	int n( 0);
	const char* nameObj( "");
	const char* nameAct( "");
	const char* nameFile( "");
	float volume( 0.0f);

	// Get the number of parameters.
	ASSERT( LuaReader::Instance().GetNumberOfParameters( n, 4));

	// Get the parameters.
	ASSERT( LuaReader::Instance().GetParameterString( 1, nameObj));
	ASSERT( LuaReader::Instance().GetParameterString( 2, nameAct));
	ASSERT( LuaReader::Instance().GetParameterString( 3, nameFile));
	ASSERT( LuaReader::Instance().GetParameter<float>( 4, LuaReturnType::LRT_Float, volume));

	// Call the function.
	Application::sp_app->AddAction( nameObj, nameAct, "Audio", volume, 0.0f, 0.0f, nameFile);

	// Return how many things have been pushed onto the stack.
	return 0;
}

/**
* 
*/
int Application::LuaLink_LinkCamThirdPerson() {
	int n( 0);
	const char* nameObj( "");
	const char* nameAct( "");
	const char* nameTarget( "");

	// Get the number of parameters.
	ASSERT( LuaReader::Instance().GetNumberOfParameters( n, 3));

	// Get the parameters.
	ASSERT( LuaReader::Instance().GetParameterString( 1, nameObj));
	ASSERT( LuaReader::Instance().GetParameterString( 2, nameAct));
	ASSERT( LuaReader::Instance().GetParameterString( 3, nameTarget));

	// Call the function.
	Application::sp_app->AddAction( nameObj, nameAct, "Camera", 0.0f, 0.0f, 0.0f, nameTarget);

	// Return how many things have been pushed onto the stack.
	return 0;
}