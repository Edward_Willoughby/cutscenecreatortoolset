/********************************************************************
*	Function definitions for the ActionRotate class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "ActionRotate.h"

#include "Macros.h"
#include "SceneObject.h"
/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
/// Temporary fix for setting the speed of objects. This needs to be externally customisable at a
/// later stage though.
const float SPEED_Turning = 1.0f;


/**
* Constructor.
*/
ActionRotate::ActionRotate( const char* name, unsigned int firstFrame, SceneObject* p_target, const D3DXVECTOR3& rot)
	: ActionBase( name, firstFrame, p_target)
	, m_rotTotal( rot)
	, m_rotFrame( ZeroVector)
	, m_turningSpeed( SPEED_Turning)
{
	this->SetLastFrame( this->CalculateDuration());
}

/**
* Destructor.
*/
ActionRotate::~ActionRotate() {
	
}

/**
* Gets the textual name of this class.
*
* @return The textual name of this class.
*/
const char* ActionRotate::GetTypeName() const {
	return "RotateTo";
}

/**
* 
*/
void ActionRotate::CalculateActionLength( D3DXVECTOR3& rotation, int& length) {
	D3DXVECTOR3 rotFrame;
	D3DXVec3Normalize( &rotFrame, &rotation);
	rotFrame *= SPEED_Turning;

	// Get the largest value so that the discrepency due to floating point math is as insignificant
	// as possible.
	float largest( max( fabs( rotation.x), fabs( rotation.y)));
	largest = max( largest, fabs( rotation.z));
	float perFrame( max( fabs( rotFrame.x), fabs( rotFrame.y)));
	perFrame = max( perFrame, fabs( rotFrame.z));

	unsigned int counter( 0);
	while( largest > 0.0f) {
		largest -= perFrame;
		++counter;
	}
}

/**
* 
*
* @param :: 
*
* @return 
*/
unsigned int ActionRotate::CalculateDuration() {
	mp_target->Update( this->GetFirstFrame());

	D3DXVec3Normalize( &m_rotFrame, &m_rotTotal);
	m_rotFrame *= m_turningSpeed;

	// Get the largest value so that the discrepency due to floating point math is as insignificant
	// as possible.
	float largest( max( fabs( m_rotTotal.x), fabs( m_rotTotal.y)));
	largest = max( largest, fabs( m_rotTotal.z));
	float perFrame( max( fabs( m_rotFrame.x), fabs( m_rotFrame.y)));
	perFrame = max( perFrame, fabs( m_rotFrame.z));

	unsigned int counter( 0);
	while( largest > 0.0f) {
		largest -= perFrame;
		++counter;
	}

	return counter;
}

void ActionRotate::GoToFrame( unsigned int frame) {
	D3DXVECTOR3 rot;

	if( frame <= this->GetFirstFrame()) {
		return;
	}
	else if( frame >= this->GetLastFrame()) {
		rot = m_rotTotal;
	}
	else { 
		rot = m_rotFrame * (frame - this->GetFirstFrame());
	}

	mp_target->m_rotation += rot;
}

void ActionRotate::GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4) {
	D3DXVECTOR3 rot;

	if( frame <= this->GetFirstFrame()) {
		return;
	}
	else if( frame >= this->GetLastFrame()) {
		rot = m_rotTotal;
	}
	else { 
		rot = m_rotFrame * (frame - this->GetFirstFrame());
	}

	param1 = rot.x;
	param2 = rot.y;
	param3 = rot.z;
}