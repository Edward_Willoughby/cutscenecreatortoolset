/********************************************************************
*	Function definitions for the SceneObject class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "SceneObject.h"

#include <algorithm>
#include <sstream>

#include "Application.h"	// 'Application.h' NEEDS to be included before 'CommonMesh.h'.
#include "CommonMesh.h"

#include "ActionBase.h"
#include "ActionPosition.h"
#include "D3DCamera.h"
#include "Macros.h"
/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
/// Private pointer to the Application because it's more efficient than going through the
/// static pointer every time. I have to have the pointer in here else 'class Application' will
/// screw over the Application class when I include D3DCamera in there.
static NS_DirectXLib::Application* sp_app = nullptr;

const float SPEED_Moving = 0.1f;
const float SPEED_Turning = 1.0f;


///**
//* Constructor.
//*/
//SceneObject::SceneObject( HumanModel* p_mesh, const char* name, const D3DXVECTOR3& pos, const D3DXVECTOR3& dir)
//	: mp_human( p_mesh)
//	, mp_mesh( nullptr)
//{
//	this->SetInfo( name, pos, dir);
//}

/**
* Constructor.
*/
SceneObject::SceneObject( CommonMesh* p_mesh, const char* name, const D3DXVECTOR3& pos, const D3DXVECTOR3& dir)
	: //mp_human( nullptr)
	 mp_mesh( p_mesh)
{
	this->SetInfo( name, pos, dir);
}

/**
* Destructor.
*/
SceneObject::~SceneObject() {
	// Delete all of the actions.
	for( std::vector<ActionBase*>::iterator it( mp_actions.begin()); it != mp_actions.end(); ++it)
		SafeDelete( *it);

	// Delete the model.
	//SafeDelete( mp_human);
	SafeDelete( mp_mesh);
}

/**
* 
*/
void SceneObject::StoreMeshParameters( const char* type, float param1, float param2, float param3) {
	m_meshType = type;
	m_meshParam1 = param1;
	m_meshParam2 = param2;
	m_meshParam3 = param3;
}

/**
* 
*/
void SceneObject::GetMeshParameters( const char* &type, float& param1, float& param2, float& param3) const {
	type = m_meshType;
	param1 = m_meshParam1;
	param2 = m_meshParam2;
	param3 = m_meshParam3;
}

/**
* 
*/
void SceneObject::AddAction( ActionBase* p_action) {
	if( p_action != nullptr)
		mp_actions.push_back( p_action);
}

/**
* 
*/
void SceneObject::DeleteAction( const char* actionName) {
	for( std::vector<ActionBase*>::iterator it( mp_actions.begin()); it != mp_actions.end(); ++it) {
		if( strcmp( (*it)->GetName(), actionName) == 0) {
			delete *it;
			mp_actions.erase( it);
			break;
		}
	}
}

/**
* 
*/
void SceneObject::SortActions() {
	//std::ostringstream before;
	//for( size_t i( 0); i < mp_actions.size(); ++i)
	//	before << "Object name: " << m_name << ". Action name: " << mp_actions[i]->GetName() << "\n";
	//MessageBox( nullptr, before.str().c_str(), "Application::SaveScene", MB_OK);

	std::stable_sort( mp_actions.begin(), mp_actions.end(), ActionBase::ActionCompare);

	//std::ostringstream after;
	//for( size_t i( 0); i < mp_actions.size(); ++i)
	//	after << "Object name: " << m_name << ". Action name: " << mp_actions[i]->GetName() << "\n";
	//MessageBox( nullptr, after.str().c_str(), "Application::SaveScene", MB_OK);
}

/**
* 
*
* @param :: 
*
* @return 
*/
unsigned int SceneObject::GetLastFrame() const {
	unsigned int lastFrame( 0);

	for( std::vector<ActionBase*>::const_iterator it( mp_actions.begin()); it != mp_actions.end(); ++it)
		if( (*it)->GetLastFrame() > lastFrame)
			lastFrame = (*it)->GetLastFrame();

	return lastFrame;
}

/**
* 
*/
void SceneObject::GetActionInfo( const char* &actionName, int& start, int& end, int index) const {
	if( index < 0 || index >= mp_actions.size()) {
		actionName = "";
		start = 0;
		end = 0;
	}
	else {
		actionName = mp_actions[index]->GetName();
		start = mp_actions[index]->GetFirstFrame();
		end = mp_actions[index]->GetLastFrame();
	}
}

/**
* 
*/
void SceneObject::GetActionInfo( const char* actionName, const char* &actionType, int& start, int& end) const {
	actionType = "";
	start = 0;
	end = 0;

	for( size_t i( 0); i < mp_actions.size(); ++i) {
		if( strcmp( actionName, mp_actions[i]->GetName()) == 0) {
			actionType = mp_actions[i]->GetTypeName();
			start = mp_actions[i]->GetFirstFrame();
			end = mp_actions[i]->GetLastFrame();
		}
	}
}

/**
* 
*/
void SceneObject::GetActionInfoAtFrame( const char* actionName, int frame, float& param1, float& param2, float& param3, const char* &param4) const {
	param1 = param2 = param3 = 0.0f;

	for( size_t i( 0); i < mp_actions.size(); ++i) {
		if( strcmp( actionName, mp_actions[i]->GetName()) == 0) {
			mp_actions[i]->GetInfoAtFrame( frame, param1, param2, param3, param4);
		}
	}
}

/**
* 
*/
bool SceneObject::IsActionNameValid( const char* actionName) const {
	bool valid( true);

	for( std::vector<ActionBase*>::const_iterator it( mp_actions.begin()); it != mp_actions.end(); ++it) {
		if( strcmp( (*it)->GetName(), actionName) == 0) {
			valid = false;
			break;
		}
	}

	return valid;
}

/**
* 
*/
void SceneObject::Update( unsigned int frame) {
	// Reset the values.
	this->ResetInfo();

	// Update all of this object's actions to the specified frame.
	for( std::vector<ActionBase*>::iterator it( mp_actions.begin()); it != mp_actions.end(); ++it)
		(*it)->GoToFrame( frame);

	// If this object is for the camera then set the camera it's linked to to its position. This
	// object has been updated to the correct position by the 'ActionCamera's so move the camera to
	// it.
	if( mp_camera != nullptr) {
		//mp_camera->m_position = m_position;
		//mp_camera->m_lookAt = m_position;

		//m_camOffset.y *= 2.5f;
		//m_camOffset.z *= 15.0f;

		//mp_camera->m_position.x += sin( float( D3DXToRadian( m_rotation.y+45.0f))) * m_camOffset.z;
		//mp_camera->m_position.y += m_camOffset.y;
		//mp_camera->m_position.z += cos( float( D3DXToRadian( m_rotation.y+45.0f))) * m_camOffset.z;

		mp_camera->m_lookAt = m_position;
		
		D3DXVECTOR3 UpVector( 0.0f,  1.0f,  0.0f);
		D3DXVECTOR3 UnitForward( 0.0f, 0.0f, -1.0f);
		D3DXMATRIX RotationMatrix;
		D3DXMatrixRotationYawPitchRoll( &RotationMatrix, D3DXToRadian( m_rotation.y), D3DXToRadian( m_rotation.x + 45.0f), D3DXToRadian( m_rotation.z));
		D3DXVECTOR4 posOut;
		D3DXVECTOR4 UpOut;

		D3DXVec3Transform( &posOut, &UnitForward, &RotationMatrix);
		D3DXVec3Transform( &UpOut, &UpVector, &RotationMatrix);

		m_camOffset.x = 0;
		D3DXVec3Length(&m_camOffset);

		posOut *= D3DXVec3Length(&m_camOffset) * 5;

		mp_camera->m_position = D3DXVECTOR3( posOut.x, posOut.y, posOut.z);
		mp_camera->m_position += m_position;
		mp_camera->m_up = D3DXVECTOR3( UpOut.x, UpOut.y, UpOut.z);
	}

	//// The HumanModel has a different update system, so I have to pass through the positional
	//// changes now.
	//if( mp_human != nullptr) {
	//	mp_human->SetPosition( m_position);
	//	mp_human->SetRotation( m_rotation);
	//	mp_human->Update();
	//}
}

/**
* 
*/
void SceneObject::Render() {
	// Don't render the object if it's invisible.
	if( !m_visible)
		return;

	/*if( mp_human != nullptr) {
		mp_human->Render();
	}
	else*/ if( mp_mesh != nullptr) {
		D3DXMATRIX matWorld, matTrans, matRotX, matRotY, matRotZ;
		D3DXMatrixTranslation( &matTrans, m_position.x, m_position.y, m_position.z);
		D3DXMatrixRotationX( &matRotX, float( D3DXToRadian( m_rotation.x)));
		D3DXMatrixRotationY( &matRotY, float( D3DXToRadian( m_rotation.y)));
		D3DXMatrixRotationZ( &matRotZ, float( D3DXToRadian( m_rotation.z)));

		matWorld = matRotX * matRotY * matRotZ * matTrans;
		sp_app->SetWorldMatrix( matWorld);

		mp_mesh->Draw();
	}
}

/**
* 
*/
D3DXVECTOR3 SceneObject::GetSize() const {
	if( mp_mesh == nullptr)
		return ZeroVector;

	D3DXVECTOR3 min, max;

	mp_mesh->GetSubsetLocalAABB( 0, &min, &max);

	return max - min;
}

/**
* 
*/
void SceneObject::AttachCamera( D3DCamera* p_cam) {
	mp_camera = p_cam;
}

/**
* 
*/
bool SceneObject::GetVisibility() const {
	return m_visible;
}

/**
* 
*/
void SceneObject::ToggleVisibility( bool visible) {
	m_visible = visible;
}

/**
* 
*/
void SceneObject::SetInfo( const char* name, const D3DXVECTOR3& pos, const D3DXVECTOR3& dir) {
	sp_app = NS_DirectXLib::Application::sp_app;

	m_name = name;
	m_position = m_defPos = pos;
	m_rotation = m_defRot = ZeroVector;
	m_direction = m_defDir = dir;

	m_visible = m_defVis = true;

	mp_camera = nullptr;
	m_camOffset = ZeroVector;
}

/**
* 
*/
void SceneObject::ResetInfo() {
	m_position	= m_defPos;
	m_rotation	= m_defRot;
	m_direction = m_defDir;
	m_visible	= m_defVis;
}