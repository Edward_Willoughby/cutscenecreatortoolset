/********************************************************************
*
*	CLASS		:: Application
*	DESCRIPTION	:: The main loop for running the cut scenes. This class will be the master of
*					updating and rendering the cut scenes.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 27
*
********************************************************************/

#ifndef ApplicationH
#define ApplicationH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
/// These two may be needed, but I'm not sure... Uncomment them if something doesn't work in the
/// header, but if it's only something in the '.cpp' that doesn't work, then move them to there.
//#include <stdio.h>
//#include <windows.h>
#include <vector>

#include <D3D11.h>

#include "CommonApp.h"

//#include "LuaAPI.h"

//class ActionBase;
//class ActionCamera;
//class ActionMove;
//class ActionRotate;
//class ActionVisibility;
class D3DCamera;
//class D3DHeightmap;
//class HumanModel;
class SceneObject;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// This define makes the compile time shorter by excluding scarcely used windows functions.
#define WIN32_LEAN_AND_MEAN


/*******************************************************************/
namespace NS_DirectXLib
{
	class Application: public CommonApp {
		public:
			static Application* sp_app;

			Application();

			/// Initialise the program for viewing a cut scene in the independent viewer.
			bool InitForViewing( const char* scriptName);

			/// Returns whether the window is currently the active application.
			bool IsInFocus() const {		return CommonApp::IsInFocus(); }
			/// Returns the handle to the window that the Application is rendering to.
			HWND GetClientHandle() const {	return m_hWnd; }
			/// For when the cut scene is being played automatically in the independant executable.
			void SetAutoPlay( bool play, const char* scriptName) { m_autoPlay = play; m_scriptName = scriptName; }

			/// Clears the current scene.
			void ClearScene();
			/// Saves the current scene.
			void SaveScene( const char* fileName);
			/// Opens a saved scene.
			void OpenScene( const char* fileName);

			/// Moves the cut scene to the specified frame.
			void GoToFrame( unsigned int frame);
			/// Gets the last frame of the currently stored cut scene.
			void GetLastFrame( unsigned int& frame);
			/// Creates an object based on the parameters.
			void CreateObject( const char* name, const char* type, float param1 = 0.0f, float param2 = 0.0f, float param3 = 0.0f);
			/// Creates a 'SceneObject' that'll be used to tell the camera what to do.
			void CreateCameraObject();
			/// Deletes a specific object.
			void DeleteObject( const char* name);
			/// Adds an action to a specified object.
			void AddAction( const char* objectName, const char* actionName, const char* actionType, float param1 = 0.0f, float param2 = 0.0f, float param3 = 0.0f, const char* param4 = "");
			/// Edits an existing action to have new information.
			void EditAction( const char* objectName, const char* actionName, const char* newActionName, float param1, float param2, float param3, const char* param4);
			/// Deletes an existing action.
			void DeleteAction( const char* objectName, const char* actionName);
			/// Gets the position of a specified object.
			void GetPosition( const char* objectName, float& x, float& y, float& z);
			/// Gets the rotation of a specified object.
			void GetRotation( const char* objectName, float& x, float& y, float& z);
			/// Gets the visibility of an object in the scene.
			void GetVisibility( const char* objectName, bool& vis);

			/// Gets the name of an object.
			const char* GetObjectName( int index) const;

			/// Gets the name of an action.
			const char* GetActionName( const char* objectName, int index) const;
			/// Gets the information pertaining to an object's action.
			const char* GetObjectsActionInfo( const char* objectName, int& start, int& end, int index) const;
			/// Gets the information pertaining to an object's action.
			void GetObjectsActionInfo( const char* objectName, const char* actionName, const char* &actionType, int& start, int& end) const;
			/// Gets the information about an action at the specified frame.
			void GetActionInfoAtFrame( const char* objectName, const char* actionName, int frame, float& param1, float& param2, float& param3, const char* &param4);

			/// Gets the name of a valid model that an object can use as its mesh.
			const char* GetName( int index) const;
			/// Gets the name of a valid action type that an action can use as its type.
			const char* GetActionTypeName( int index) const;
			/// Gets a valid name for an object.
			const char* GetValidObjectName() const;
			/// Gets a valid name for an action.
			const char* GetValidActionName() const;
			/// Checks an action name to see if it's valid.
			bool IsActionNameValid( const char* actionName) const;
			/// Gets the names of the parameters required for the model.
			void GetObjectParameterName( const char* mesh, const char* &param1, const char* &param2, const char* &param3) const;
			/// Gets the names of the parameters required for the action.
			void GetActionParameterName( const char* action, const char* &param1, const char* &param2, const char* &param3) const;
			/// Calculates the length of an action.
			void CalculateActionLength( const char* actionType, float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd, const char* textParam, int& length);

			/// Set whether audio should be played or not.
			void SetAudioPlaying( bool play);
			/// Get whether audio should be played or not.
			bool GetAudioPlaying() const;
			/// Stops any existing audio files from playing.
			void StopAudio();

			/// Sets whether the toolset should look through the scene camera or the global camera.
			void UseGlobalCamera( bool globalCam);
			/// Gets whether the toolset should look through the scene camera or the global camera.
			bool UseGlobalCamera();

			/// Orbits the camera by a specified amount.
			void CameraOrbit( int x, int y);
			/// Zooms the camera by a specified amount.
			void CameraZoom( int x, int y);
			
			/// Returns a pointer to an object if it was found.
			SceneObject* FindObject( const char* name) const;

			/// TEST FUNCTION - This just tests to see if something works.
			void TestFunction( const char* &test) const;

		protected:
			/// Inherited function to handle initialisation.
			bool HandleStart();
			/// Inherited function to handle de-initialisation.
			void HandleStop();
			/// Inherited function to handle updates each frame.
			void HandleUpdate();
			/// Inherited function to handle rendering of the scene.
			void HandleRender();

			/// Renders objects in perspective view.
			void Render3D();
			/// Renders objects in orthographic view.
			void Render2D();

		private:
			/// Reloads the shaders.
			void ReloadShaders();
			/// Reloads the Lua script.
			void ReloadLuaScript();

			/// Deletes the entire scene.
			void DeleteScene();

			// Functions used by the Lua A.P.I.
			//void CreateHuman( const std::string& name, const float& scale = 1.0f);
			//void SetHumanPosition( const std::string& name, const D3DXVECTOR3& pos);
			//void SetObjectRotation( const std::string& name, const D3DXVECTOR3& rotation);
			//void MoveObjectTo( const std::string& name, const D3DXVECTOR3& pos);
			//void RotateObjectTo( const std::string& name, const D3DXVECTOR3& pos);
			//void SetObjectVisibility( const std::string& name, const bool& vis);
			//void NextStep();
			//void LoadAudio( char* fileName);
			//void PlayAudio( char* fileName, const float& volume);
			//void WaitForAudio( char* fileName);
			//void RunScript( const std::string& fileName);
			//void LinkCamThirdPerson( char* name);

			float	m_frameCount;
			bool	m_wireframe;
			bool	m_reload;

			bool	m_playAudio;
			bool	m_globalCam;

			bool	m_loadingFromFile;

			//LuaAPI m_lua;
			//char* m_luaCode;

			std::vector<SceneObject*>	mp_scene;
			//std::vector<ActionBase*>	mp_actions;
			unsigned int				m_frame;
			unsigned int				m_frameLast;
			bool						m_autoPlay;
			const char*					m_scriptName;
			//bool						m_playingCutScene;
			//bool						m_playingFrame;

			D3DCamera*	mp_sceneCamera;
			D3DCamera*	mp_camera;
			//float			m_camZ;
			//float			m_rotAngle;

			// Functions exposed to Lua.
			static int LuaLink_SetFrame();
			static int LuaLink_CreateObject();
			static int LuaLink_PositionObjectAt();
			static int LuaLink_MoveObjectTo();
			static int LuaLink_RotateObjectTo();
			static int LuaLink_SetObjectVisibility();
			//static int LuaLink_LoadAudio();
			static int LuaLink_PlayAudio();
			static int LuaLink_LinkCamThirdPerson();

			/// Private assignment operator to prevent copies.
			Application& operator=( const Application& other);

	};
}  // namespace NS_DirectXLib

/*******************************************************************/
#endif	// #ifndef ApplicationH