/********************************************************************
*
*	CLASS		:: ActionVisible
*	DESCRIPTION	:: Action class for toggling the visibility of a SceneObject during a cut scene.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 03 / 02
*
********************************************************************/

#ifndef ActionVisibleH
#define ActionVisibleH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <D3D10_1.h>	// I've only included this because otherwise it moans.
#include <D3DX10math.h>

#include "ActionBase.h"

class SceneObject;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class ActionVisible : public ActionBase {
	public:
		/// Constructor.
		ActionVisible( const char* name, unsigned int firstFrame, SceneObject* p_target, const bool& visible);
		/// Destructor.
		~ActionVisible();

		const char* GetTypeName() const;

		static void CalculateActionLength( int& length);
		
		unsigned int CalculateDuration();

		void GoToFrame( unsigned int frame);
		void GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4);
		
	private:
		bool m_visible;

		/// Private copy constructor to prevent accidental multiple instances.
		ActionVisible( const ActionVisible& other);
		/// Private assignment operator to prevent accidental multiple instances.
		ActionVisible& operator=( const ActionVisible& other);
		
};

/*******************************************************************/
#endif	// #ifndef ActionVisibleH