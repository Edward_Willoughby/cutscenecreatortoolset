/********************************************************************
*	Function definitions for the ActionAudio class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "ActionAudio.h"

#include <fstream>

#include "Application.h"
#include "Audio.h"
#include "Macros.h"
#include "SceneObject.h"
/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
int ActionAudio::s_prevFrame = -1;


/**
* 
*/
ActionAudio::ActionAudio( const char* actionName, unsigned int frame, SceneObject* p_target, const char* fileName, AAudio action, const float& volume /*= 1.0f*/)
	: ActionBase( actionName, frame, p_target)
	, m_fileName( fileName)
	, m_action( action)
	, m_volume( volume)
	, m_prevFrame( -1)
{
	this->SetLastFrame( this->CalculateDuration());
}

/**
* 
*/
ActionAudio::~ActionAudio() {
	
}

/**
* 
*/
const char* ActionAudio::GetTypeName() const {
	return "Audio";
}

/**
* 
*/
void ActionAudio::CalculateActionLength( const char* fileName, int& length) {
	Audio::GetInstance().LoadAudio( fileName);
	unsigned int duration( Audio::GetInstance().GetDuration( fileName));
	float fps( 1 / FRAME_RATE);
	fps *= 1000;
	
	// According to some testing I did, using a loop to count the number of frames is more accurate
	// than just doing 'counter = length / fps'. I have no idea why, I can only assume it's to do
	// with the inaccuracy of floating point values.
	while( duration > 0) {
		++length;
		duration -= fps;
	}

	Audio::GetInstance().StopAudio( fileName);
}

/**
* 
*/
unsigned int ActionAudio::CalculateDuration() {
	int length( 0);

	// The code for this function is identical to the static version, so just use that.
	ActionAudio::CalculateActionLength( m_fileName, length);

	return (unsigned int)length;
}

/**
* 
*/
void ActionAudio::GoToFrame( unsigned int frame) {
	Audio& audio( Audio::GetInstance());
	bool consecutive( false);
	
	// Stop any audio and reset the previous frame variable if audio shouldn't be played.
	if( !NS_DirectXLib::Application::sp_app->GetAudioPlaying()) {
		audio.StopAudio( m_fileName);
		m_prevFrame = -1;
		return;
	}

	// I need to record what step was done last so that if the steps are going sequencially I don't
	// keep 'skipping' to the correct place in the audio file. If I didn't do this it would sound
	// very 'jittery' because the frame rate is inexact and it would be slightly off most of the
	// time.
	//if( frame == (m_prevFrame + 1))
	if( frame == (s_prevFrame + 1))
		consecutive = true;
	m_prevFrame = frame;

	if( frame == this->GetFirstFrame()) {
		audio.PlayAudio( m_fileName, m_volume);
	}
	else if( frame < this->GetFirstFrame()) {
		audio.StopAudio( m_fileName);
	}
	else if( frame >= this->GetLastFrame()) {
		audio.StopAudio( m_fileName);
	}
	else {
		if( !consecutive) {
			audio.SetVolume( m_fileName, m_volume);
			audio.SetPlayPosition( m_fileName, ((float)(frame - this->GetFirstFrame()) / FRAME_RATE) * 1000);
		}
	}
}

/**
* 
*/
void ActionAudio::GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4) {
	// Not really frame-dependant, but it's the only way of getting this information.
	param1 = m_volume;
	param2 = (float)m_action;
	param3 = 0.0f;
	param4 = m_fileName;
}

/**
* 
*/
const char* ActionAudio::GetAudioFile() const {
	return m_fileName;
}

/**
* 
*/
AAudio ActionAudio::GetAudioAction() const {
	return m_action;
}

/**
* 
*/
float ActionAudio::GetVolume() const {
	return m_volume;
}