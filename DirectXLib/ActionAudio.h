/********************************************************************
*
*	CLASS		:: ActionAudio
*	DESCRIPTION	:: Holds the information required to play/wait for audio during a cut-scene.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 03 / 15
*
********************************************************************/

#ifndef ActionAudioH
#define ActionAudioH

/********************************************************************
*	Include libraries and header files.
********************************************************************/

#include "ActionBase.h"

class SceneObject;
/********************************************************************
*	Defines and constants.
********************************************************************/
/// Enumeration for what the action should do to the audio file.
enum AAudio {
	AAudio_Play = 0,
	AAudio_Stop,
	AAudio_StopAll,
	AAudio_WaitFor,
};


/*******************************************************************/
class ActionAudio : public ActionBase {
	public:
		/// Constructor.
		ActionAudio( const char* actionName, unsigned int frame, SceneObject* p_target, const char* fileName, AAudio action, const float& volume = 1.0f);
		/// Destructor.
		~ActionAudio();

		const char* GetTypeName() const;

		static void CalculateActionLength( const char* fileName, int& length);

		unsigned int CalculateDuration();
		
		void GoToFrame( unsigned int frame);
		void GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4);

		const char* GetAudioFile() const;
		AAudio GetAudioAction() const;
		float GetVolume() const;

		static int		s_prevFrame;

	private:
		const char*	m_fileName;
		AAudio		m_action;

		float m_volume;

		int m_prevFrame;
		
		/// Private copy constructor to prevent accidental multiple instances.
		ActionAudio( const ActionAudio& other);
		/// Private assignment operator to prevent accidental multiple instances.
		ActionAudio& operator=( const ActionAudio& other);
		
};

/*******************************************************************/
#endif	// #ifndef ActionAudioH
