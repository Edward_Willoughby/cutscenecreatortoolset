/********************************************************************
*	Function definitions for ActionCamera class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "ActionCamera.h"

#include "Application.h"
#include "D3DCamera.h"
#include "Macros.h"
#include "SceneObject.h"
/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/


/**
* 
*/
ActionCamera::ActionCamera( const char* actionName, unsigned int firstFrame, SceneObject* p_target, const char* objectToTrack)
	: ActionBase( actionName, firstFrame, p_target)
	, m_objectToTrack( objectToTrack)
{
	this->SetLastFrame( this->CalculateDuration());
}

/**
* 
*/
ActionCamera::~ActionCamera() {

}

/**
* 
*/
const char* ActionCamera::GetTypeName() const {
	return "Camera";
}

/**
* 
*/
void ActionCamera::CalculateActionLength( int& length) {
	length = 0;
}

/**
* 
*/
unsigned int ActionCamera::CalculateDuration() {
	return 0;
}

/**
* 
*/
void ActionCamera::GoToFrame( unsigned int frame) {
	if( frame >= this->GetFirstFrame()) {
		SceneObject* p_object( NS_DirectXLib::Application::sp_app->FindObject( m_objectToTrack));
		mp_target->m_position = p_object->m_position;
		mp_target->m_rotation = p_object->m_rotation;

		mp_target->m_camOffset = p_object->GetSize();
	}
}

/**
* 
*/
void ActionCamera::GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4) {
	if( frame >= this->GetFirstFrame())
		param4 = m_objectToTrack;
}