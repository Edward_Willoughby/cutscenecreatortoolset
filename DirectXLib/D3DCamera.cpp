/********************************************************************
*	Function definitions for the D3DCamera class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "D3DCamera.h"

#include "Application.h"
#include "Macros.h"

/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
/// Private pointer to the Application because it's more efficient than going through the
/// static pointer every time. I have to have the pointer in here else 'class Application' will
/// screw over the Application class when I include D3DCamera in there.
static NS_DirectXLib::Application* sp_app = nullptr;


/**
* Constructor.
*/
D3DCamera::D3DCamera()
	: m_rotX( D3DXToDegree( 7.5f))
	, m_rotY( D3DXToDegree( 7.5f))
	, m_zDist( 100.0f)
	, m_position(	ZeroVector)
	, m_lookAt(		ZeroVector)
	, m_up(			ZeroVector)
{
	sp_app = NS_DirectXLib::Application::sp_app;
}

/**
* Destructor.
*/
D3DCamera::~D3DCamera() {

}

/**
* Updates the camera.
*/
void D3DCamera::Update() {
	//m_position	= D3DXVECTOR3( sin( D3DXToRadian( m_rotX))*m_zDist, cos( D3DXToRadian( m_rotY))*m_zDist, cos( D3DXToRadian( m_rotX))*m_zDist);
	m_lookAt	= D3DXVECTOR3( 0.0f,  4.0f,  0.0f);
	m_up		= D3DXVECTOR3( 0.0f,  1.0f,  0.0f);

	D3DXVECTOR3 UnitForward( 0.0f, 0.0f, -1.0f);
	D3DXMATRIX RotationMatrix;
	D3DXMatrixRotationYawPitchRoll( &RotationMatrix, D3DXToRadian( m_rotX), D3DXToRadian( m_rotY), 0.0f);

	D3DXVECTOR4 posOut;
	D3DXVECTOR4 UpOut;

	D3DXVec3Transform( &posOut, &UnitForward, &RotationMatrix);
	D3DXVec3Transform( &UpOut, &m_up, &RotationMatrix);

	posOut *= m_zDist;

	m_position = D3DXVECTOR3( posOut.x, posOut.y, posOut.z);
	m_up = D3DXVECTOR3( UpOut.x, UpOut.y, UpOut.z);

}

/**
* Creates the view matrix from the class's member variables and applies it to the Application. It
* also returns the view matrix for further use, if requried.
*
* @return The view matrix, based on this class's member variables.
*/
D3DXMATRIX D3DCamera::ApplyViewMatrixLH() const {
	D3DXMATRIX matView;

	D3DXMatrixLookAtLH( &matView, &m_position, &m_lookAt, &m_up);

	sp_app->SetViewMatrix( matView);

	return matView;
}

/**
* Creates the perspective projection matrix from the parameters and applies it to the Application.
* It also returns the perspective projection matrix for further use, if required.
*
* @param zNear	:: The closest the camera can see before it clips objects from view.
* @param zFar	:: The farthest the camera can see before it clips objects from view.
*
* @return The perspective projection matrix, based on this function's parameters.
*/
D3DXMATRIX D3DCamera::ApplyPerspectiveMatrixLH( float zNear, float zFar) const {
	D3DXMATRIX matProj;

	D3DXMatrixPerspectiveFovLH( &matProj, float( D3DX_PI / 4.0f), 2.0f, zNear, zFar);

	sp_app->SetProjectionMatrix( matProj);

	return matProj;
}

/**
* Creates the orthographic projection matrix from the parameters and applies it to the Application.
* It also returns the orthographic projection matrix for further use, if required.
*
* @param zNear	:: The closest the camera can see before it clips objects from view.
* @param zFar	:: The farthest the camera can see before it clips objects from view.
*
* @return The orthographic projection matrix, based on this function's parameters.
*/
D3DXMATRIX D3DCamera::ApplyOrthoMatrixLH( float zNear, float zFar) const {
	D3DXMATRIX matOrtho;
	float width, height;

	sp_app->GetWindowSize( &width, &height);

	D3DXMatrixOrthoLH( &matOrtho, width, height, zNear, zFar);

	sp_app->SetProjectionMatrix( matOrtho);

	return matOrtho;
}

///**
//* Unlinks the camera from an object so it no longer follows it.
//*/
//void D3DCamera::DetachCamera() {
//
//}