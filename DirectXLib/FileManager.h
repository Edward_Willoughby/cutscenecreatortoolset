/********************************************************************
*
*	CLASS		:: FileManager
*	DESCRIPTION	:: Handles all of the reading and writing to files.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 03 / 09
*
********************************************************************/

#ifndef FileManagerH
#define FileManagerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include "Application.h"

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class FileManager {
	public:
		/// Returns a reference to the FileManager class.
		static FileManager& Instance();

		void SaveScene( const char* fileName);
		void LoadScene( const char* fileName);

	private:
		friend NS_DirectXLib::Application;


		/// Private constructor to keep the class as a singleton.
		FileManager();
		/// Private copy constructor to prevent accidental multiple instances.
		FileManager( const FileManager& other);
		/// Private assignment operator to prevent accidental multiple instances.
		FileManager& operator=( const FileManager& other);

};

/*******************************************************************/
#endif	// #ifndef FileManagerH