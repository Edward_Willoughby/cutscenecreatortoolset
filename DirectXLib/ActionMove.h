/********************************************************************
*
*	CLASS		:: ActionMove
*	DESCRIPTION	:: Action class for moving a SceneObject during a cut scene.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 30
*
********************************************************************/

#ifndef ActionMoveH
#define ActionMoveH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <D3D10_1.h>	// I've only included this because otherwise it moans.
#include <D3DX10math.h>

#include "ActionBase.h"

class SceneObject;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class ActionMove : public ActionBase {
	public:
		/// Constructor.
		ActionMove( const char* name, unsigned int firstFrame, SceneObject* p_target, const D3DXVECTOR3& pos);
		/// Destructor.
		~ActionMove();

		const char* GetTypeName() const;

		static void CalculateActionLength( D3DXVECTOR3& start, D3DXVECTOR3& end, int& length);
		
		unsigned int CalculateDuration();

		void GoToFrame( unsigned int frame);
		void GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4);

	private:
		D3DXVECTOR3 m_posStart;
		D3DXVECTOR3 m_posFrame;  // The amount travelled in a single frame.
		D3DXVECTOR3 m_posEnd;
		float		m_movementSpeed;

		/// Private copy constructor to prevent accidental multiple instances.
		ActionMove( const ActionMove& other);
		/// Private assignment operator to prevent accidental multiple instances.
		ActionMove& operator=( const ActionMove& other);
		
};

/*******************************************************************/
#endif	// #ifndef ActionMoveH
