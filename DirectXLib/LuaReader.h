/********************************************************************
*
*	CLASS		:: LuaReader
*	DESCRIPTION	:: Handles all Lua interaction, given the file and function names that can be used.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2013 / 12 / 24
*
*********************************************************************/

#ifndef LuaReaderH
#define LuaReaderH

/********************************************************************
*	Include libraries and header files.
*********************************************************************/
#include <vector>

// This NEEDS to be included otherwise the compiler will bitch about unresolved external shite
// ... took me a good few days to discover this.
#pragma comment( lib, "lua5.1.lib")

#include <lua.hpp>

/********************************************************************
*	Include namespaces.
*********************************************************************/


/********************************************************************
*	Defines and constants.
*********************************************************************/
/**
* A list of all the possible return types that I'm willing to implement for the Lua A.P.I.
* at this time... More are possible from the Lua interpreter, but I don't understand them
* well enough to implement them at the moment.
*/
enum LuaReturnType {
	LRT_Bool = 0,
	LRT_Int,
	LRT_Float,
	LRT_Double,
};


/********************************************************************/
class LuaReader {
	public:
		/// Returns a reference to the LuaReader.
		static LuaReader& Instance();

		/// Typedef for the function prototype that is permitted to be registered.
		typedef int (*LuaFunction) (void);

		/// Opens a C++ function to be called by a Lua script.
		void RegisterFunction( const char* functionName, LuaFunction function);

		/// Stores the name of a Lua script file to be read.
		void SetScriptToRead( const char* fileName);
		/// Reads a Lua script file and executes it.
		void ReadScript( const char* fileName = nullptr);

		/// Obtains the name of the last Lua function called.
		const char* GetFunctionName();
		/// Obtains the number of parameters of the last Lua function read in.
		bool GetNumberOfParameters( int& number, int min, int max = -1);
		
		/**
		* Error checks and returns a parameter from the last read Lua function.
		*
		* @param index		:: The position of the parameter to be located (i.e. the first parameter
		*						is at index position '1').
		* @param type		:: The type of variable that should be found (only numbers or boolean).
		* @param variable	:: Where the value of the parameter will be stored.
		*
		* @return True if the parameter was found and valid.
		*/
		template<class T>
		bool GetParameter( int index, LuaReturnType type, T& variable)
		{
			bool success( false);

			switch( type) {
				// For a boolean parameter...
				case LRT_Bool:
					if( lua_isboolean( mp_lua, index)) {
						variable = bool( lua_toboolean( mp_lua, index));
						success = true;
					}
					break;
				// For an integral parameter...
				case LRT_Int:
					if( lua_isnumber( mp_lua, index)) {
						variable = int( lua_tointeger( mp_lua, index));
						success = true;
					}
					break;
				// For a float parameter...
				case LRT_Float:
					if( lua_isnumber( mp_lua, index)) {
						variable = float( lua_tonumber( mp_lua, index));
						success = true;
					}
					break;
				// For a double parameter...
				case LRT_Double:
					if( lua_isnumber( mp_lua, index)) {
						variable = double( lua_tonumber( mp_lua, index));
						success = true;
					}
					break;
			}

			return success;
		}

		/**
		* Error checks and returns a parameter from the last read Lua function. This function is
		* specifically for strings as it moans at me if I have it in the other function due to it
		* trying to convert a 'const char*' to a 'float'.
		*
		* @param index		:: The position of the parameter to be located (i.e. the first parameter
		*						is at index position '1').
		* @param variable	:: Where the value of the parameter will be stored.
		*
		* @return True if the parameter was found and valid.
		*/
		template<class T>
		bool GetParameterString( int index, T& variable)
		{
			if( lua_isstring( mp_lua, index)) {
				variable = lua_tostring( mp_lua, index);
				return true;
			}

			return false;
		}


	protected:


	private:
		/// Function called by ALL Lua scripts that re-directs them to the member function.
		static int LuaFunctionInterpreter( lua_State* p_lua);
		/// Interprets the given Lua function name to call the related C++ one.
		int LuaFunctionNameInterpreter( const char* functionName);

		lua_State*	mp_lua;
		const char*	mp_fileName;

		typedef std::pair<LuaFunction, const char*> FunctionPair;
		typedef std::vector<FunctionPair> FunctionVector;
		FunctionVector m_functions;

		/// Private constructor to prevent multiple instances.
		LuaReader();
		/// Private destructor.
		~LuaReader();
		/// Private copy constructor to prevent multiple instances.
		LuaReader( const LuaReader& other);
		/// Private assignment operator to prevent multiple instances.
		LuaReader& operator=( const LuaReader& other);
		
};

/********************************************************************/
#endif	// #ifndef LuaReaderH