/********************************************************************
*
*	CLASS		:: Audio
*	DESCRIPTION	:: Loads, stores and runs all the code for audio files.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 05
*
*********************************************************************/

#ifndef AudioH
#define AudioH

/********************************************************************
*	Include libraries and header files.
*********************************************************************/

#include "fmod.hpp"
#include "fmod_errors.h"

/********************************************************************
*	Include namespaces.
*********************************************************************/


/********************************************************************
*	Defines and constants.
*********************************************************************/
// The maximum number of sounds that can be played/stored at any one time. If the program requires
// more sounds to be played then either delete existing/unneeded sounds or increase the size of
// this constant.
const int MAX_AUDIO_CHANNELS = 100;


/********************************************************************/
class Audio {
	public:
		/// Destructor.
		~Audio();

		/// Returns a reference to, what should be, the only instance of the Audio class.
		static Audio& GetInstance();

		/// Loads an audio file into memory so it's less processor intensive to play.
		void LoadAudio( const char* fileName);

		/// Checks whether a file is currently playing or not.
		bool IsPlaying( const char* fileName) const;

		/// Plays an audio file once, regardless of whether it's already being played or not.
		void PlayAudioOneShot( const char* fileName, float volume = 1.0f);
		/// Plays an audio file, or resets it if it already exists.
		void PlayAudio( const char* fileName, float volume = 1.0f);
		/// Pauses/Unpauses an audio file.
		void PauseAudio( const char* fileName);
		/// Stops an audio file from playing and resets it to the start.
		void StopAudio( const char* fileName);

		/// Gets the volums of an audio clip.
		float GetVolume( const char* fileName);
		/// Sets the volume of an audio clip.
		void SetVolume( const char* fileName, float volume = 1.0f);
		/// Gets the length of the audio clip, in milliseconds.
		unsigned int GetDuration( const char* fileName);
		/// Sets the position of the audio clip.
		void SetPlayPosition( const char* fileName, int pos = -1);

		/// Updates the FMOD system and clears out old handles in the array.
		void Update();

		/// Stops all audio files from playing.
		void StopAllAudio();

		/// Deletes all the audio files currently stored in the Audio class.
		void DeleteAllAudio();

		static Audio*	sp_audio;

	protected:


	private:
		/**
		* Holds the FMOD sound and channel instances that are required to modify an audio file
		* during play, as well as the name of the file to identify it.
		*/
		struct AudioContainer {
			AudioContainer( FMOD::Sound* p_sound, FMOD::Channel* p_channel, const char* fileName)
				: mp_sound( p_sound)
				, mp_channel( p_channel)
				, m_fileName( fileName)
			{}

			~AudioContainer() {
				mp_channel->stop();
				mp_sound->release();
			}

			FMOD::Sound*	mp_sound;
			FMOD::Channel*	mp_channel;
			const char*		m_fileName;
		};
		
		/// Finds an empty slot in the array to insert a new Audio file into.
		int FindEmptyChannel() const;
		/// Finds a slot in the array with the corresponding file name.
		int FindChannel( const char* fileName) const;

		/// Creates a sound and inserts it into the array.
		void CreateSound( int index, const char* fileName, bool cache);

		/// Shuts down the Audio class and deletes the FMOD system.
		void Shutdown();

		/// Checks the result of an FMOD function and stores any error message it provides.
		bool FMODSuccess( FMOD_RESULT result);

		FMOD::System*	mp_system;
		AudioContainer*	mp_sounds[MAX_AUDIO_CHANNELS];

		char* m_lastError;
		
		/// Private constructor to keep the class as a singleton.
		Audio();
		/// Private copy constructor to prevent multiple instances.
		Audio( const Audio& other);
		/// Private assignment operator to prevent multiple instances.
		Audio& operator=( const Audio& other);
		
};

/********************************************************************/
#endif	// #ifndef AudioH