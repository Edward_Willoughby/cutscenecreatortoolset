/********************************************************************
*	Function definitions for the ActionMove class.
********************************************************************/

/********************************************************************
*	Include header file.
********************************************************************/
#include "ActionMove.h"

#include "Macros.h"
#include "SceneObject.h"
/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
/// Temporary fix for setting the speed of objects. This needs to be externally customisable at a
/// later stage though.
const float SPEED_Moving = 0.1f;


/**
* Constructor.
*/
ActionMove::ActionMove( const char* name, unsigned int firstFrame, SceneObject* p_target, const D3DXVECTOR3& pos)
	: ActionBase( name, firstFrame, p_target)
	, m_posStart( ZeroVector)
	, m_posFrame( ZeroVector)
	, m_posEnd( pos)
	, m_movementSpeed( SPEED_Moving)
{
	this->SetLastFrame( this->CalculateDuration());
}

/**
* Destructor.
*/
ActionMove::~ActionMove() {
	
}

/**
* Gets the textual name of this class.
*
* @return The textual name of this class.
*/
const char* ActionMove::GetTypeName() const {
	return "MoveTo";
}

/**
* 
*/
void ActionMove::CalculateActionLength( D3DXVECTOR3& start, D3DXVECTOR3& end, int& length) {
	D3DXVECTOR3 distance( end - start);
	D3DXVECTOR3 direction( ZeroVector);

	// Calculate how much to move per frame.
	D3DXVec3Normalize( &direction, &distance);
	direction = D3DXVECTOR3( direction.x * SPEED_Moving, direction.y * SPEED_Moving, direction.z * SPEED_Moving);

	// Calculate the total distance.
	float totalDistance( fabs( distance.x) + fabs( distance.y) + fabs( distance.z));
	float totalDirection( fabs( direction.x) + fabs( direction.y) + fabs( direction.z));

	length = 0;
	while( totalDistance > 0.0f) {
		totalDistance -= totalDirection;
		++length;
	}
}

/**
* 
*
* @param :: 
*
* @return 
*/
unsigned int ActionMove::CalculateDuration() {
	mp_target->Update( this->GetFirstFrame());
	m_posStart = mp_target->m_position;

	D3DXVECTOR3 distance( m_posEnd - m_posStart);
	D3DXVECTOR3 direction( ZeroVector);

	// Calculate how much to move per frame.
	D3DXVec3Normalize( &direction, &distance);
	direction = D3DXVECTOR3( direction.x * m_movementSpeed, direction.y * m_movementSpeed, direction.z * m_movementSpeed);
	m_posFrame = direction;

	// Calculate the total distance.
	float totalDistance( fabs( distance.x) + fabs( distance.y) + fabs( distance.z));
	float totalDirection( fabs( direction.x) + fabs( direction.y) + fabs( direction.z));

	unsigned int counter( 0);
	while( totalDistance > 0.0f) {
		totalDistance -= totalDirection;
		++counter;
	}

	return counter;
}

void ActionMove::GoToFrame( unsigned int frame) {
	D3DXVECTOR3 pos;

	if( frame <= this->GetFirstFrame()) {
		return;
	}
	else if( frame >= this->GetLastFrame()) {
		pos = m_posEnd;
	}
	else { 
		pos = m_posStart + (m_posFrame * (frame - this->GetFirstFrame()));
	}

	mp_target->m_position = pos;
}

void ActionMove::GetInfoAtFrame( unsigned int frame, float& param1, float& param2, float& param3, const char* &param4) {
	D3DXVECTOR3 pos;

	if( frame <= this->GetFirstFrame()) {
		pos = m_posStart;
	}
	else if( frame >= this->GetLastFrame()) {
		pos = m_posEnd;
	}
	else { 
		pos = m_posStart + (m_posFrame * (frame - this->GetFirstFrame()));
	}

	param1 = pos.x;
	param2 = pos.y;
	param3 = pos.z;
}