
#include <fstream>
#include <sstream>
#include <string>
#include <Windows.h>

#include <Application.h>


static const char* gs_configFile( "config.txt");


int WINAPI WinMain( HINSTANCE, HINSTANCE, LPSTR, int)
{
	NS_DirectXLib::Application app;
	std::string scriptName( "");

	// Load in the config file and set the script file.
	std::ifstream in( gs_configFile);
	std::string line;

	// Ensure the config file was found.
	if( !in.is_open()) {
		MessageBox( nullptr, "Config file not found.", "Error: Config Missing", MB_OK);
		return 0;
	}

	// Find the line of text in the config file that specifies the name of the script.
	while( std::getline( in, line)) {
		std::istringstream stream( line.substr( line.find( "=") + 1));

		if( line.find( "fileName") != -1)
			stream >> scriptName;
	}
	scriptName += ".cs";

	// Get the path of the executable.
	char buffer[256];
	GetModuleFileName( GetModuleHandle( nullptr), buffer, 256);
	int size = strlen( buffer) - 1;

	// Remove the executable's file name so it's just the directory.
	while( buffer[size] != '\\') {
		buffer[size] = 0;
		--size;
	}
	++size;

	// Append the file name to the path and add a null character to the end.
	int i( 0);
	for( i; i < scriptName.size(); ++i)
		buffer[size + i] = scriptName[i];
	buffer[size + i] = 0;

	app.SetAutoPlay( true, buffer);

	// Run the program.
	Run( &app);

	return 0;
}