﻿namespace NS_CutsceneCreatorToolset
{
    partial class NewActionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPosZStart = new System.Windows.Forms.TextBox();
            this.tbPosYStart = new System.Windows.Forms.TextBox();
            this.tbPosXStart = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lblPosZStart = new System.Windows.Forms.Label();
            this.lblPosYStart = new System.Windows.Forms.Label();
            this.lblPosXStart = new System.Windows.Forms.Label();
            this.lblPosStart = new System.Windows.Forms.Label();
            this.tbNameAction = new System.Windows.Forms.TextBox();
            this.tbNameObject = new System.Windows.Forms.TextBox();
            this.lblNameAction = new System.Windows.Forms.Label();
            this.lblNameObject = new System.Windows.Forms.Label();
            this.cbActionType = new System.Windows.Forms.ComboBox();
            this.lblActionType = new System.Windows.Forms.Label();
            this.tbRotZStart = new System.Windows.Forms.TextBox();
            this.tbRotYStart = new System.Windows.Forms.TextBox();
            this.tbRotXStart = new System.Windows.Forms.TextBox();
            this.lblRotZStart = new System.Windows.Forms.Label();
            this.lblRotYStart = new System.Windows.Forms.Label();
            this.lblRotXStart = new System.Windows.Forms.Label();
            this.lblRotStart = new System.Windows.Forms.Label();
            this.tbFrameStart = new System.Windows.Forms.TextBox();
            this.lblFrameStart = new System.Windows.Forms.Label();
            this.tbFrameEnd = new System.Windows.Forms.TextBox();
            this.lblFrameEnd = new System.Windows.Forms.Label();
            this.tbRotZEnd = new System.Windows.Forms.TextBox();
            this.tbRotYEnd = new System.Windows.Forms.TextBox();
            this.tbRotXEnd = new System.Windows.Forms.TextBox();
            this.lblRotZEnd = new System.Windows.Forms.Label();
            this.lblRotYEnd = new System.Windows.Forms.Label();
            this.lblRotXEnd = new System.Windows.Forms.Label();
            this.lblRotEnd = new System.Windows.Forms.Label();
            this.tbPosZEnd = new System.Windows.Forms.TextBox();
            this.tbPosYEnd = new System.Windows.Forms.TextBox();
            this.tbPosXEnd = new System.Windows.Forms.TextBox();
            this.lblPosZEnd = new System.Windows.Forms.Label();
            this.lblPosYEnd = new System.Windows.Forms.Label();
            this.lblPosXEnd = new System.Windows.Forms.Label();
            this.lblPosEnd = new System.Windows.Forms.Label();
            this.cbVisible = new System.Windows.Forms.CheckBox();
            this.tbFileName = new System.Windows.Forms.TextBox();
            this.lblFileName = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblObjToTrack = new System.Windows.Forms.Label();
            this.cbObjToTrack = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // tbPosZStart
            // 
            this.tbPosZStart.Location = new System.Drawing.Point(261, 145);
            this.tbPosZStart.Name = "tbPosZStart";
            this.tbPosZStart.Size = new System.Drawing.Size(45, 20);
            this.tbPosZStart.TabIndex = 33;
            // 
            // tbPosYStart
            // 
            this.tbPosYStart.Location = new System.Drawing.Point(187, 145);
            this.tbPosYStart.Name = "tbPosYStart";
            this.tbPosYStart.Size = new System.Drawing.Size(45, 20);
            this.tbPosYStart.TabIndex = 31;
            // 
            // tbPosXStart
            // 
            this.tbPosXStart.Location = new System.Drawing.Point(113, 145);
            this.tbPosXStart.Name = "tbPosXStart";
            this.tbPosXStart.Size = new System.Drawing.Size(45, 20);
            this.tbPosXStart.TabIndex = 29;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(167, 282);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(86, 282);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 9;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lblPosZStart
            // 
            this.lblPosZStart.AutoSize = true;
            this.lblPosZStart.Location = new System.Drawing.Point(238, 148);
            this.lblPosZStart.Name = "lblPosZStart";
            this.lblPosZStart.Size = new System.Drawing.Size(17, 13);
            this.lblPosZStart.TabIndex = 43;
            this.lblPosZStart.Text = "Z:";
            // 
            // lblPosYStart
            // 
            this.lblPosYStart.AutoSize = true;
            this.lblPosYStart.Location = new System.Drawing.Point(164, 148);
            this.lblPosYStart.Name = "lblPosYStart";
            this.lblPosYStart.Size = new System.Drawing.Size(17, 13);
            this.lblPosYStart.TabIndex = 42;
            this.lblPosYStart.Text = "Y:";
            // 
            // lblPosXStart
            // 
            this.lblPosXStart.AutoSize = true;
            this.lblPosXStart.Location = new System.Drawing.Point(90, 148);
            this.lblPosXStart.Name = "lblPosXStart";
            this.lblPosXStart.Size = new System.Drawing.Size(17, 13);
            this.lblPosXStart.TabIndex = 41;
            this.lblPosXStart.Text = "X:";
            // 
            // lblPosStart
            // 
            this.lblPosStart.AutoSize = true;
            this.lblPosStart.Location = new System.Drawing.Point(12, 148);
            this.lblPosStart.Name = "lblPosStart";
            this.lblPosStart.Size = new System.Drawing.Size(72, 13);
            this.lblPosStart.TabIndex = 40;
            this.lblPosStart.Text = "Start Position:";
            // 
            // tbNameAction
            // 
            this.tbNameAction.Location = new System.Drawing.Point(93, 32);
            this.tbNameAction.Name = "tbNameAction";
            this.tbNameAction.Size = new System.Drawing.Size(100, 20);
            this.tbNameAction.TabIndex = 1;
            this.tbNameAction.Validating += new System.ComponentModel.CancelEventHandler(this.ActionNameValidation);
            // 
            // tbNameObject
            // 
            this.tbNameObject.Location = new System.Drawing.Point(93, 6);
            this.tbNameObject.Name = "tbNameObject";
            this.tbNameObject.Size = new System.Drawing.Size(100, 20);
            this.tbNameObject.TabIndex = 26;
            // 
            // lblNameAction
            // 
            this.lblNameAction.AutoSize = true;
            this.lblNameAction.Location = new System.Drawing.Point(12, 35);
            this.lblNameAction.Name = "lblNameAction";
            this.lblNameAction.Size = new System.Drawing.Size(71, 13);
            this.lblNameAction.TabIndex = 37;
            this.lblNameAction.Text = "Action Name:";
            // 
            // lblNameObject
            // 
            this.lblNameObject.AutoSize = true;
            this.lblNameObject.Location = new System.Drawing.Point(12, 9);
            this.lblNameObject.Name = "lblNameObject";
            this.lblNameObject.Size = new System.Drawing.Size(79, 13);
            this.lblNameObject.TabIndex = 25;
            this.lblNameObject.Text = "Object\'s Name:";
            // 
            // cbActionType
            // 
            this.cbActionType.FormattingEnabled = true;
            this.cbActionType.Location = new System.Drawing.Point(93, 59);
            this.cbActionType.Name = "cbActionType";
            this.cbActionType.Size = new System.Drawing.Size(100, 21);
            this.cbActionType.TabIndex = 2;
            this.cbActionType.SelectedIndexChanged += new System.EventHandler(this.cbActionType_SelectedIndexChanged);
            // 
            // lblActionType
            // 
            this.lblActionType.AutoSize = true;
            this.lblActionType.Location = new System.Drawing.Point(12, 62);
            this.lblActionType.Name = "lblActionType";
            this.lblActionType.Size = new System.Drawing.Size(67, 13);
            this.lblActionType.TabIndex = 45;
            this.lblActionType.Text = "Action Type:";
            // 
            // tbRotZStart
            // 
            this.tbRotZStart.Location = new System.Drawing.Point(261, 171);
            this.tbRotZStart.Name = "tbRotZStart";
            this.tbRotZStart.Size = new System.Drawing.Size(45, 20);
            this.tbRotZStart.TabIndex = 48;
            // 
            // tbRotYStart
            // 
            this.tbRotYStart.Location = new System.Drawing.Point(187, 171);
            this.tbRotYStart.Name = "tbRotYStart";
            this.tbRotYStart.Size = new System.Drawing.Size(45, 20);
            this.tbRotYStart.TabIndex = 47;
            // 
            // tbRotXStart
            // 
            this.tbRotXStart.Location = new System.Drawing.Point(113, 171);
            this.tbRotXStart.Name = "tbRotXStart";
            this.tbRotXStart.Size = new System.Drawing.Size(45, 20);
            this.tbRotXStart.TabIndex = 46;
            // 
            // lblRotZStart
            // 
            this.lblRotZStart.AutoSize = true;
            this.lblRotZStart.Location = new System.Drawing.Point(238, 174);
            this.lblRotZStart.Name = "lblRotZStart";
            this.lblRotZStart.Size = new System.Drawing.Size(17, 13);
            this.lblRotZStart.TabIndex = 52;
            this.lblRotZStart.Text = "Z:";
            // 
            // lblRotYStart
            // 
            this.lblRotYStart.AutoSize = true;
            this.lblRotYStart.Location = new System.Drawing.Point(164, 174);
            this.lblRotYStart.Name = "lblRotYStart";
            this.lblRotYStart.Size = new System.Drawing.Size(17, 13);
            this.lblRotYStart.TabIndex = 51;
            this.lblRotYStart.Text = "Y:";
            // 
            // lblRotXStart
            // 
            this.lblRotXStart.AutoSize = true;
            this.lblRotXStart.Location = new System.Drawing.Point(90, 174);
            this.lblRotXStart.Name = "lblRotXStart";
            this.lblRotXStart.Size = new System.Drawing.Size(17, 13);
            this.lblRotXStart.TabIndex = 50;
            this.lblRotXStart.Text = "X:";
            // 
            // lblRotStart
            // 
            this.lblRotStart.AutoSize = true;
            this.lblRotStart.Location = new System.Drawing.Point(12, 174);
            this.lblRotStart.Name = "lblRotStart";
            this.lblRotStart.Size = new System.Drawing.Size(75, 13);
            this.lblRotStart.TabIndex = 49;
            this.lblRotStart.Text = "Start Rotation:";
            // 
            // tbFrameStart
            // 
            this.tbFrameStart.Location = new System.Drawing.Point(82, 86);
            this.tbFrameStart.Name = "tbFrameStart";
            this.tbFrameStart.Size = new System.Drawing.Size(54, 20);
            this.tbFrameStart.TabIndex = 54;
            // 
            // lblFrameStart
            // 
            this.lblFrameStart.AutoSize = true;
            this.lblFrameStart.Location = new System.Drawing.Point(12, 89);
            this.lblFrameStart.Name = "lblFrameStart";
            this.lblFrameStart.Size = new System.Drawing.Size(64, 13);
            this.lblFrameStart.TabIndex = 53;
            this.lblFrameStart.Text = "Start Frame:";
            // 
            // tbFrameEnd
            // 
            this.tbFrameEnd.Location = new System.Drawing.Point(209, 86);
            this.tbFrameEnd.Name = "tbFrameEnd";
            this.tbFrameEnd.Size = new System.Drawing.Size(54, 20);
            this.tbFrameEnd.TabIndex = 56;
            this.tbFrameEnd.Text = "Error";
            // 
            // lblFrameEnd
            // 
            this.lblFrameEnd.AutoSize = true;
            this.lblFrameEnd.Location = new System.Drawing.Point(142, 89);
            this.lblFrameEnd.Name = "lblFrameEnd";
            this.lblFrameEnd.Size = new System.Drawing.Size(61, 13);
            this.lblFrameEnd.TabIndex = 55;
            this.lblFrameEnd.Text = "End Frame:";
            // 
            // tbRotZEnd
            // 
            this.tbRotZEnd.Location = new System.Drawing.Point(261, 243);
            this.tbRotZEnd.Name = "tbRotZEnd";
            this.tbRotZEnd.Size = new System.Drawing.Size(45, 20);
            this.tbRotZEnd.TabIndex = 8;
            this.tbRotZEnd.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // tbRotYEnd
            // 
            this.tbRotYEnd.Location = new System.Drawing.Point(187, 243);
            this.tbRotYEnd.Name = "tbRotYEnd";
            this.tbRotYEnd.Size = new System.Drawing.Size(45, 20);
            this.tbRotYEnd.TabIndex = 7;
            this.tbRotYEnd.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // tbRotXEnd
            // 
            this.tbRotXEnd.Location = new System.Drawing.Point(113, 243);
            this.tbRotXEnd.Name = "tbRotXEnd";
            this.tbRotXEnd.Size = new System.Drawing.Size(45, 20);
            this.tbRotXEnd.TabIndex = 6;
            this.tbRotXEnd.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // lblRotZEnd
            // 
            this.lblRotZEnd.AutoSize = true;
            this.lblRotZEnd.Location = new System.Drawing.Point(238, 246);
            this.lblRotZEnd.Name = "lblRotZEnd";
            this.lblRotZEnd.Size = new System.Drawing.Size(17, 13);
            this.lblRotZEnd.TabIndex = 70;
            this.lblRotZEnd.Text = "Z:";
            // 
            // lblRotYEnd
            // 
            this.lblRotYEnd.AutoSize = true;
            this.lblRotYEnd.Location = new System.Drawing.Point(164, 246);
            this.lblRotYEnd.Name = "lblRotYEnd";
            this.lblRotYEnd.Size = new System.Drawing.Size(17, 13);
            this.lblRotYEnd.TabIndex = 69;
            this.lblRotYEnd.Text = "Y:";
            // 
            // lblRotXEnd
            // 
            this.lblRotXEnd.AutoSize = true;
            this.lblRotXEnd.Location = new System.Drawing.Point(90, 246);
            this.lblRotXEnd.Name = "lblRotXEnd";
            this.lblRotXEnd.Size = new System.Drawing.Size(17, 13);
            this.lblRotXEnd.TabIndex = 68;
            this.lblRotXEnd.Text = "X:";
            // 
            // lblRotEnd
            // 
            this.lblRotEnd.AutoSize = true;
            this.lblRotEnd.Location = new System.Drawing.Point(12, 246);
            this.lblRotEnd.Name = "lblRotEnd";
            this.lblRotEnd.Size = new System.Drawing.Size(72, 13);
            this.lblRotEnd.TabIndex = 67;
            this.lblRotEnd.Text = "End Rotation:";
            // 
            // tbPosZEnd
            // 
            this.tbPosZEnd.Location = new System.Drawing.Point(261, 217);
            this.tbPosZEnd.Name = "tbPosZEnd";
            this.tbPosZEnd.Size = new System.Drawing.Size(45, 20);
            this.tbPosZEnd.TabIndex = 5;
            this.tbPosZEnd.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // tbPosYEnd
            // 
            this.tbPosYEnd.Location = new System.Drawing.Point(187, 217);
            this.tbPosYEnd.Name = "tbPosYEnd";
            this.tbPosYEnd.Size = new System.Drawing.Size(45, 20);
            this.tbPosYEnd.TabIndex = 4;
            this.tbPosYEnd.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // tbPosXEnd
            // 
            this.tbPosXEnd.Location = new System.Drawing.Point(113, 217);
            this.tbPosXEnd.Name = "tbPosXEnd";
            this.tbPosXEnd.Size = new System.Drawing.Size(45, 20);
            this.tbPosXEnd.TabIndex = 3;
            this.tbPosXEnd.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // lblPosZEnd
            // 
            this.lblPosZEnd.AutoSize = true;
            this.lblPosZEnd.Location = new System.Drawing.Point(238, 220);
            this.lblPosZEnd.Name = "lblPosZEnd";
            this.lblPosZEnd.Size = new System.Drawing.Size(17, 13);
            this.lblPosZEnd.TabIndex = 63;
            this.lblPosZEnd.Text = "Z:";
            // 
            // lblPosYEnd
            // 
            this.lblPosYEnd.AutoSize = true;
            this.lblPosYEnd.Location = new System.Drawing.Point(164, 220);
            this.lblPosYEnd.Name = "lblPosYEnd";
            this.lblPosYEnd.Size = new System.Drawing.Size(17, 13);
            this.lblPosYEnd.TabIndex = 62;
            this.lblPosYEnd.Text = "Y:";
            // 
            // lblPosXEnd
            // 
            this.lblPosXEnd.AutoSize = true;
            this.lblPosXEnd.Location = new System.Drawing.Point(90, 220);
            this.lblPosXEnd.Name = "lblPosXEnd";
            this.lblPosXEnd.Size = new System.Drawing.Size(17, 13);
            this.lblPosXEnd.TabIndex = 61;
            this.lblPosXEnd.Text = "X:";
            // 
            // lblPosEnd
            // 
            this.lblPosEnd.AutoSize = true;
            this.lblPosEnd.Location = new System.Drawing.Point(12, 220);
            this.lblPosEnd.Name = "lblPosEnd";
            this.lblPosEnd.Size = new System.Drawing.Size(69, 13);
            this.lblPosEnd.TabIndex = 60;
            this.lblPosEnd.Text = "End Position:";
            // 
            // cbVisible
            // 
            this.cbVisible.AutoSize = true;
            this.cbVisible.Location = new System.Drawing.Point(209, 8);
            this.cbVisible.Name = "cbVisible";
            this.cbVisible.Size = new System.Drawing.Size(62, 17);
            this.cbVisible.TabIndex = 71;
            this.cbVisible.Text = "Visible?";
            this.cbVisible.UseVisualStyleBackColor = true;
            // 
            // tbFileName
            // 
            this.tbFileName.Location = new System.Drawing.Point(93, 145);
            this.tbFileName.Name = "tbFileName";
            this.tbFileName.ReadOnly = true;
            this.tbFileName.Size = new System.Drawing.Size(170, 20);
            this.tbFileName.TabIndex = 73;
            this.tbFileName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbFileName_MouseClick);
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(12, 148);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(60, 13);
            this.lblFileName.TabIndex = 72;
            this.lblFileName.Text = "File Name: ";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "MP3 Files|*.mp3|WAV Files|*wav";
            // 
            // lblObjToTrack
            // 
            this.lblObjToTrack.AutoSize = true;
            this.lblObjToTrack.Location = new System.Drawing.Point(12, 148);
            this.lblObjToTrack.Name = "lblObjToTrack";
            this.lblObjToTrack.Size = new System.Drawing.Size(84, 13);
            this.lblObjToTrack.TabIndex = 74;
            this.lblObjToTrack.Text = "Object to Track:";
            // 
            // cbObjToTrack
            // 
            this.cbObjToTrack.FormattingEnabled = true;
            this.cbObjToTrack.Location = new System.Drawing.Point(93, 145);
            this.cbObjToTrack.Name = "cbObjToTrack";
            this.cbObjToTrack.Size = new System.Drawing.Size(170, 21);
            this.cbObjToTrack.TabIndex = 75;
            // 
            // NewActionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 317);
            this.Controls.Add(this.cbObjToTrack);
            this.Controls.Add(this.lblObjToTrack);
            this.Controls.Add(this.tbFileName);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.cbVisible);
            this.Controls.Add(this.tbRotZEnd);
            this.Controls.Add(this.tbRotYEnd);
            this.Controls.Add(this.tbRotXEnd);
            this.Controls.Add(this.lblRotZEnd);
            this.Controls.Add(this.lblRotYEnd);
            this.Controls.Add(this.lblRotXEnd);
            this.Controls.Add(this.lblRotEnd);
            this.Controls.Add(this.tbPosZEnd);
            this.Controls.Add(this.tbPosYEnd);
            this.Controls.Add(this.tbPosXEnd);
            this.Controls.Add(this.lblPosZEnd);
            this.Controls.Add(this.lblPosYEnd);
            this.Controls.Add(this.lblPosXEnd);
            this.Controls.Add(this.lblPosEnd);
            this.Controls.Add(this.tbFrameEnd);
            this.Controls.Add(this.lblFrameEnd);
            this.Controls.Add(this.tbFrameStart);
            this.Controls.Add(this.lblFrameStart);
            this.Controls.Add(this.tbRotZStart);
            this.Controls.Add(this.tbRotYStart);
            this.Controls.Add(this.tbRotXStart);
            this.Controls.Add(this.lblRotZStart);
            this.Controls.Add(this.lblRotYStart);
            this.Controls.Add(this.lblRotXStart);
            this.Controls.Add(this.lblRotStart);
            this.Controls.Add(this.lblActionType);
            this.Controls.Add(this.cbActionType);
            this.Controls.Add(this.tbPosZStart);
            this.Controls.Add(this.tbPosYStart);
            this.Controls.Add(this.tbPosXStart);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.lblPosZStart);
            this.Controls.Add(this.lblPosYStart);
            this.Controls.Add(this.lblPosXStart);
            this.Controls.Add(this.lblPosStart);
            this.Controls.Add(this.tbNameAction);
            this.Controls.Add(this.tbNameObject);
            this.Controls.Add(this.lblNameAction);
            this.Controls.Add(this.lblNameObject);
            this.Name = "NewActionForm";
            this.Text = "Create New Action";
            this.Shown += new System.EventHandler(this.NewActionForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPosZStart;
        private System.Windows.Forms.TextBox tbPosYStart;
        private System.Windows.Forms.TextBox tbPosXStart;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lblPosZStart;
        private System.Windows.Forms.Label lblPosYStart;
        private System.Windows.Forms.Label lblPosXStart;
        private System.Windows.Forms.Label lblPosStart;
        private System.Windows.Forms.TextBox tbNameAction;
        private System.Windows.Forms.TextBox tbNameObject;
        private System.Windows.Forms.Label lblNameAction;
        private System.Windows.Forms.Label lblNameObject;
        private System.Windows.Forms.ComboBox cbActionType;
        private System.Windows.Forms.Label lblActionType;
        private System.Windows.Forms.TextBox tbRotZStart;
        private System.Windows.Forms.TextBox tbRotYStart;
        private System.Windows.Forms.TextBox tbRotXStart;
        private System.Windows.Forms.Label lblRotZStart;
        private System.Windows.Forms.Label lblRotYStart;
        private System.Windows.Forms.Label lblRotXStart;
        private System.Windows.Forms.Label lblRotStart;
        private System.Windows.Forms.TextBox tbFrameStart;
        private System.Windows.Forms.Label lblFrameStart;
        private System.Windows.Forms.TextBox tbFrameEnd;
        private System.Windows.Forms.Label lblFrameEnd;
        private System.Windows.Forms.TextBox tbRotZEnd;
        private System.Windows.Forms.TextBox tbRotYEnd;
        private System.Windows.Forms.TextBox tbRotXEnd;
        private System.Windows.Forms.Label lblRotZEnd;
        private System.Windows.Forms.Label lblRotYEnd;
        private System.Windows.Forms.Label lblRotXEnd;
        private System.Windows.Forms.Label lblRotEnd;
        private System.Windows.Forms.TextBox tbPosZEnd;
        private System.Windows.Forms.TextBox tbPosYEnd;
        private System.Windows.Forms.TextBox tbPosXEnd;
        private System.Windows.Forms.Label lblPosZEnd;
        private System.Windows.Forms.Label lblPosYEnd;
        private System.Windows.Forms.Label lblPosXEnd;
        private System.Windows.Forms.Label lblPosEnd;
        private System.Windows.Forms.CheckBox cbVisible;
        private System.Windows.Forms.TextBox tbFileName;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label lblObjToTrack;
        private System.Windows.Forms.ComboBox cbObjToTrack;
    }
}