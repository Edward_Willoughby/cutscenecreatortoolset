//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////
//
// You can get syntax highlighting for HLSL files:
//
// 1. Visit Tools|Options, Text Editor\File Extension
//
// 2. Enter `hlsl' in the Extension field, and select `Microsoft Visual
//    C++' from the Editor dropdown. Click Add.
//
// 3. Close and re-open any hlsl files you had open already.
//
// As you might guess, this makes Visual Studio interpret HLSL as C++.
// So it isn't quite perfect, and you get red underlines everywhere.
// On the bright side, you at least get brace matching, and syntax
// highlighting for comments.
//
//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// See the CommonApp comments for the names of the globals it looks for
// and sets automatically.
cbuffer CommonApp
{
	float4x4 g_WVP;
	float4 g_lightDirections[MAX_NUM_LIGHTS];
	float3 g_lightColours[MAX_NUM_LIGHTS];
	int g_numLights;
	float4x4 g_InvXposeW;
	float4x4 g_W;
};

// Add your own globals into your own cbuffer, or just make them
// global. The globals that are outside any explicit cbuffer go
// into a special cbuffer called "$Globals".
cbuffer MyApp
{
	float	g_frameCount;
	float3	g_waveOrigin;
}

struct VSInput
{
	float4 pos:POSITION;
	float4 colour:COLOUR0;
	float3 normal:NORMAL;
	float2 tex:TEXCOORD;
};

struct PSInput
{
	float4 pos:SV_Position;
	float4 colour:COLOUR0;
	float3 normal:NORMAL;
	float2 tex:TEXCOORD;
	float4 mat:COLOUR1;
};

struct PSOutput
{
	float4 colour:SV_Target;
};

Texture2D g_materialMap;
Texture2D g_texture0;		// moss
Texture2D g_texture1;		// grass
Texture2D g_texture2;		// asphalt

SamplerState g_sampler;

void VSMain(const VSInput input, out PSInput output)
{
	//// modify the colour based on the height of the vertex
	//output.colour = input.colour;
	//output.colour.r = 0.8;//input.pos.y / 50;
	//output.colour.g = 0.6;//input.pos.y / 80;
	//output.colour.b /= input.pos.y * 0.2;

	//// copy over the vertex's position
	//output.pos = input.pos;
	//// if the vertex is deeply blue then make it 'wobble' up and down
	//if( output.colour.b > 0.65)
	//	output.pos.y += sin( radians( (g_frameCount*2)+output.pos.z+output.pos.x)*2);


	output.pos = input.pos;
	// multiply the vertex by the world view projection matrix
	output.pos = mul( output.pos, g_WVP);

	// get the correct pixel position on the material map for this vertex
	float2 tex_pos;
	float4 Tex;
	tex_pos.x = ( input.pos.x + 512) / 1024;
	tex_pos.y = (-input.pos.z + 512) / 1024;

	Tex = g_materialMap.SampleLevel( g_sampler, tex_pos, 0);

	// send through the texture colours and texture position to the pixel shader
	output.colour = Tex;
	output.tex = input.tex;

	//// if the vertex is deeply blue (i.e. asphalt) then make it 'wobble' up and down
	//if( output.colour.b > 0.65)
	//	output.pos.y += sin( radians( (g_frameCount*2)+output.pos.z+output.pos.x)*2)*5;

	// transfer the normals without modifying it
	output.normal = input.normal;
}

void PSMain(const PSInput input, out PSOutput output)
{
	output.colour = input.colour;

	float3 moss, grass, asphalt, blended;
	float3 pixel_colour = { 0, 0, 0 };

	// obtain the texture colour from the texture files
	moss = g_texture0.Sample( g_sampler, input.tex);
	grass = g_texture1.Sample( g_sampler, input.tex);
	asphalt = g_texture2.Sample( g_sampler, input.tex);

	// obtain the interpolated texture colours based on the texture map colours
	moss = lerp( pixel_colour, moss, input.colour.x);
	grass = lerp( pixel_colour, grass, input.colour.y);
	asphalt = lerp( pixel_colour, asphalt, input.colour.z);

	blended = (moss + grass + asphalt);

	// set the initial ambient lighting (otherwise it's really dark)
	output.colour.xyz = 0.5;
	
	// modify the pixel's colour based on the number of lights that can see it
	for( int i = 0; i < g_numLights; ++i) {
		float4 strength = dot( input.normal, g_lightDirections[i]);
		output.colour.xyz += strength.xyz * g_lightColours[i];
	}

	// apply the blended textures
	output.colour.xyz *= blended;
}