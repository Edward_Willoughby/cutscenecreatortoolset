-------------------------------------------------
---- SCRIPT :- WarCraftIIIScene.cs
---- DATE   :- 2014 / 03 / 30
-------------------------------------------------

-------------------------------------------------
-- Variable and function to increment the frame number.
-------------------------------------------------
varFrame = -1

function IncrementFrame()
	varFrame = varFrame + 1
	return varFrame
end

-------------------------------------------------
-- Names for all the objects in the scene.
-------------------------------------------------
ObjectObj_Camera = "Obj_Camera"
ObjectObj_Floor = "Obj_Floor"
ObjectObj_KelThuzad = "Obj_KelThuzad"
ObjectObj_Archimonde = "Obj_Archimonde"
ObjectObj_ObeliskBase1 = "Obj_ObeliskBase1"
ObjectObj_ObeliskBody1 = "Obj_ObeliskBody1"
ObjectObj_ObeliskBase2 = "Obj_ObeliskBase2"
ObjectObj_ObeliskBody2 = "Obj_ObeliskBody2"
ObjectObj_ObeliskBase3 = "Obj_ObeliskBase3"
ObjectObj_ObeliskBody3 = "Obj_ObeliskBody3"
ObjectObj_ObeliskBase4 = "Obj_ObeliskBase4"
ObjectObj_ObeliskBody4 = "Obj_ObeliskBody4"
ObjectObj_FloorOuter = "Obj_FloorOuter"
ObjectObj_Arthas = "Obj_Arthas"
ObjectObj_Tichondrius = "Obj_Tichondrius"
ObjectObj_TichFrontCam = "Obj_TichFrontCam"
ObjectObj_ArthBackCam = "Obj_ArthBackCam"
ObjectObj_ArchFrontCam = "Obj_ArchFrontCam"
ObjectObj_KTBackCam = "Obj_KTBackCam"

-------------------------------------------------
-- Create all of the scene's objects.
-------------------------------------------------
CreateObject( ObjectObj_Camera, "Sphere", 1)
CreateObject( ObjectObj_Floor, "Cube", 20, 1, 20)
CreateObject( ObjectObj_KelThuzad, "Cube", 5, 10, 1)
CreateObject( ObjectObj_Archimonde, "Cube", 5, 12, 1)
CreateObject( ObjectObj_ObeliskBase1, "Cube", 2, 0.5, 2)
CreateObject( ObjectObj_ObeliskBody1, "Cube", 1.5, 7, 1.5)
CreateObject( ObjectObj_ObeliskBase2, "Cube", 2, 0.5, 2)
CreateObject( ObjectObj_ObeliskBody2, "Cube", 1.5, 7, 1.5)
CreateObject( ObjectObj_ObeliskBase3, "Cube", 2, 0.5, 2)
CreateObject( ObjectObj_ObeliskBody3, "Cube", 1.5, 7, 1.5)
CreateObject( ObjectObj_ObeliskBase4, "Cube", 2, 0.5, 2)
CreateObject( ObjectObj_ObeliskBody4, "Cube", 1.5, 7, 1.5)
CreateObject( ObjectObj_FloorOuter, "Cube", 70, 0.5, 70)
CreateObject( ObjectObj_Arthas, "Cube", 5, 6, 8)
CreateObject( ObjectObj_Tichondrius, "Cube", 7, 6, 1)
CreateObject( ObjectObj_TichFrontCam, "Cube", 6, 7, 1)
CreateObject( ObjectObj_ArthBackCam, "Cube", 5, 6, 8)
CreateObject( ObjectObj_ArchFrontCam, "Cube", 5, 12, 1)
CreateObject( ObjectObj_KTBackCam, "Cube", 5, 10, 1)

-------------------------------------------------
-- Start of the scene's actions.
-------------------------------------------------
-- Frame 0
SetFrame( IncrementFrame())

PositionObjectAt( ObjectObj_Camera, "Act_InitialPosition", 0, 1, 0)
SetObjectVisibility( ObjectObj_Camera, "Act_InitialVisibility", true)
LinkCamThirdPerson( ObjectObj_Camera, "Act_LookAtSummon", "Obj_Archimonde")
SetObjectVisibility( ObjectObj_Floor, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_Floor, "Act_InitialPosition", 0, 0, 0)
SetObjectVisibility( ObjectObj_KelThuzad, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_KelThuzad, "Act_InitialPosition", 0, 5, 0)
PositionObjectAt( ObjectObj_Archimonde, "Act_InitialPosition", 0, 6, 0)
SetObjectVisibility( ObjectObj_Archimonde, "Act_InitialVisibility", false)
SetObjectVisibility( ObjectObj_ObeliskBase1, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_ObeliskBase1, "Act_InitialPosition", 9, 0.5, 9)
SetObjectVisibility( ObjectObj_ObeliskBody1, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_ObeliskBody1, "Act_InitialPosition", 9, 4, 9)
SetObjectVisibility( ObjectObj_ObeliskBase2, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_ObeliskBase2, "Act_InitialPosition", -9, 0.5, 9)
SetObjectVisibility( ObjectObj_ObeliskBody2, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_ObeliskBody2, "Act_InitialPosition", -9, 4, 9)
SetObjectVisibility( ObjectObj_ObeliskBase3, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_ObeliskBase3, "Act_InitialPosition", -9, 0.5, -9)
SetObjectVisibility( ObjectObj_ObeliskBody3, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_ObeliskBody3, "Act_InitialPosition", -9, 4, -9)
SetObjectVisibility( ObjectObj_ObeliskBase4, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_ObeliskBase4, "Act_InitialPosition", 9, 0.5, -9)
SetObjectVisibility( ObjectObj_ObeliskBody4, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_ObeliskBody4, "Act_InitialPosition", 9, 4, -9)
SetObjectVisibility( ObjectObj_FloorOuter, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_FloorOuter, "Act_InitialPosition", 0, -1, 0)
SetObjectVisibility( ObjectObj_Arthas, "Act_InitialVisibility", true)
PositionObjectAt( ObjectObj_Arthas, "Act_InitialPosition", 18, 2, 18)
MoveObjectTo( ObjectObj_Arthas, "Act_MoveStart", 20, 2, 16)
RotateObjectTo( ObjectObj_Arthas, "Act_RotateStart", 0, -20, 0)
PositionObjectAt( ObjectObj_Tichondrius, "Act_InitialPosition", 17, 2, 4)
RotateObjectTo( ObjectObj_Tichondrius, "Act_RotateStartT", 0, -100, 0)
SetObjectVisibility( ObjectObj_Tichondrius, "Act_InitialVisibility", false)
PositionObjectAt( ObjectObj_TichFrontCam, "Act_InitialPosition", 17, 2, 4)
RotateObjectTo( ObjectObj_TichFrontCam, "Act_RotateTFrontCam", 0, 80, 0)
SetObjectVisibility( ObjectObj_TichFrontCam, "Act_InitialVisibility", false)
PositionObjectAt( ObjectObj_ArthBackCam, "Act_InitialPosition", 20, 2, 16)
RotateObjectTo( ObjectObj_ArthBackCam, "Act_RotStart", 0, -130, 0)
SetObjectVisibility( ObjectObj_ArthBackCam, "Act_InitialVisibility", false)
PositionObjectAt( ObjectObj_ArchFrontCam, "Act_InitialPosition", 0, 6, 0)
RotateObjectTo( ObjectObj_ArchFrontCam, "Act_TurnArchFrontCam", 0, 80, 0)
SetObjectVisibility( ObjectObj_ArchFrontCam, "Act_InitialVisibility", false)
PositionObjectAt( ObjectObj_KTBackCam, "Act_InitialPosition", 9, 4, 18)
RotateObjectTo( ObjectObj_KTBackCam, "Act_RotateKTBackCam", 0, 50, 0)
SetObjectVisibility( ObjectObj_KTBackCam, "Act_InitialVisibility", false)

-- Frame 1
SetFrame( IncrementFrame())


-- Frame 2
SetFrame( IncrementFrame())


-- Frame 3
SetFrame( IncrementFrame())


-- Frame 4
SetFrame( IncrementFrame())


-- Frame 5
SetFrame( IncrementFrame())


-- Frame 6
SetFrame( IncrementFrame())


-- Frame 7
SetFrame( IncrementFrame())


-- Frame 8
SetFrame( IncrementFrame())


-- Frame 9
SetFrame( IncrementFrame())


-- Frame 10
SetFrame( IncrementFrame())


-- Frame 11
SetFrame( IncrementFrame())


-- Frame 12
SetFrame( IncrementFrame())


-- Frame 13
SetFrame( IncrementFrame())


-- Frame 14
SetFrame( IncrementFrame())


-- Frame 15
SetFrame( IncrementFrame())


-- Frame 16
SetFrame( IncrementFrame())


-- Frame 17
SetFrame( IncrementFrame())


-- Frame 18
SetFrame( IncrementFrame())


-- Frame 19
SetFrame( IncrementFrame())


-- Frame 20
SetFrame( IncrementFrame())


-- Frame 21
SetFrame( IncrementFrame())


-- Frame 22
SetFrame( IncrementFrame())


-- Frame 23
SetFrame( IncrementFrame())


-- Frame 24
SetFrame( IncrementFrame())


-- Frame 25
SetFrame( IncrementFrame())


-- Frame 26
SetFrame( IncrementFrame())


-- Frame 27
SetFrame( IncrementFrame())


-- Frame 28
SetFrame( IncrementFrame())


-- Frame 29
SetFrame( IncrementFrame())


-- Frame 30
SetFrame( IncrementFrame())


-- Frame 31
SetFrame( IncrementFrame())


-- Frame 32
SetFrame( IncrementFrame())


-- Frame 33
SetFrame( IncrementFrame())


-- Frame 34
SetFrame( IncrementFrame())


-- Frame 35
SetFrame( IncrementFrame())


-- Frame 36
SetFrame( IncrementFrame())


-- Frame 37
SetFrame( IncrementFrame())


-- Frame 38
SetFrame( IncrementFrame())


-- Frame 39
SetFrame( IncrementFrame())


-- Frame 40
SetFrame( IncrementFrame())


-- Frame 41
SetFrame( IncrementFrame())


-- Frame 42
SetFrame( IncrementFrame())


-- Frame 43
SetFrame( IncrementFrame())


-- Frame 44
SetFrame( IncrementFrame())


-- Frame 45
SetFrame( IncrementFrame())


-- Frame 46
SetFrame( IncrementFrame())


-- Frame 47
SetFrame( IncrementFrame())


-- Frame 48
SetFrame( IncrementFrame())


-- Frame 49
SetFrame( IncrementFrame())


-- Frame 50
SetFrame( IncrementFrame())

RotateObjectTo( ObjectObj_Arthas, "Act_RotateToArchimonde", 0, 70, 0)

-- Frame 51
SetFrame( IncrementFrame())


-- Frame 52
SetFrame( IncrementFrame())


-- Frame 53
SetFrame( IncrementFrame())


-- Frame 54
SetFrame( IncrementFrame())


-- Frame 55
SetFrame( IncrementFrame())


-- Frame 56
SetFrame( IncrementFrame())


-- Frame 57
SetFrame( IncrementFrame())


-- Frame 58
SetFrame( IncrementFrame())


-- Frame 59
SetFrame( IncrementFrame())


-- Frame 60
SetFrame( IncrementFrame())


-- Frame 61
SetFrame( IncrementFrame())


-- Frame 62
SetFrame( IncrementFrame())


-- Frame 63
SetFrame( IncrementFrame())


-- Frame 64
SetFrame( IncrementFrame())


-- Frame 65
SetFrame( IncrementFrame())


-- Frame 66
SetFrame( IncrementFrame())


-- Frame 67
SetFrame( IncrementFrame())


-- Frame 68
SetFrame( IncrementFrame())


-- Frame 69
SetFrame( IncrementFrame())


-- Frame 70
SetFrame( IncrementFrame())


-- Frame 71
SetFrame( IncrementFrame())


-- Frame 72
SetFrame( IncrementFrame())


-- Frame 73
SetFrame( IncrementFrame())


-- Frame 74
SetFrame( IncrementFrame())


-- Frame 75
SetFrame( IncrementFrame())


-- Frame 76
SetFrame( IncrementFrame())


-- Frame 77
SetFrame( IncrementFrame())


-- Frame 78
SetFrame( IncrementFrame())


-- Frame 79
SetFrame( IncrementFrame())


-- Frame 80
SetFrame( IncrementFrame())


-- Frame 81
SetFrame( IncrementFrame())


-- Frame 82
SetFrame( IncrementFrame())


-- Frame 83
SetFrame( IncrementFrame())


-- Frame 84
SetFrame( IncrementFrame())


-- Frame 85
SetFrame( IncrementFrame())


-- Frame 86
SetFrame( IncrementFrame())


-- Frame 87
SetFrame( IncrementFrame())


-- Frame 88
SetFrame( IncrementFrame())


-- Frame 89
SetFrame( IncrementFrame())


-- Frame 90
SetFrame( IncrementFrame())

MoveObjectTo( ObjectObj_KelThuzad, "Act_MoveBack", 9, 4, 18)
RotateObjectTo( ObjectObj_KelThuzad, "Act_RotateWhieMovingBack", 0, 200, 0)

-- Frame 91
SetFrame( IncrementFrame())


-- Frame 92
SetFrame( IncrementFrame())


-- Frame 93
SetFrame( IncrementFrame())


-- Frame 94
SetFrame( IncrementFrame())


-- Frame 95
SetFrame( IncrementFrame())


-- Frame 96
SetFrame( IncrementFrame())


-- Frame 97
SetFrame( IncrementFrame())


-- Frame 98
SetFrame( IncrementFrame())


-- Frame 99
SetFrame( IncrementFrame())


-- Frame 100
SetFrame( IncrementFrame())


-- Frame 101
SetFrame( IncrementFrame())


-- Frame 102
SetFrame( IncrementFrame())


-- Frame 103
SetFrame( IncrementFrame())


-- Frame 104
SetFrame( IncrementFrame())


-- Frame 105
SetFrame( IncrementFrame())


-- Frame 106
SetFrame( IncrementFrame())


-- Frame 107
SetFrame( IncrementFrame())


-- Frame 108
SetFrame( IncrementFrame())


-- Frame 109
SetFrame( IncrementFrame())


-- Frame 110
SetFrame( IncrementFrame())


-- Frame 111
SetFrame( IncrementFrame())


-- Frame 112
SetFrame( IncrementFrame())


-- Frame 113
SetFrame( IncrementFrame())


-- Frame 114
SetFrame( IncrementFrame())


-- Frame 115
SetFrame( IncrementFrame())


-- Frame 116
SetFrame( IncrementFrame())


-- Frame 117
SetFrame( IncrementFrame())


-- Frame 118
SetFrame( IncrementFrame())


-- Frame 119
SetFrame( IncrementFrame())


-- Frame 120
SetFrame( IncrementFrame())


-- Frame 121
SetFrame( IncrementFrame())


-- Frame 122
SetFrame( IncrementFrame())


-- Frame 123
SetFrame( IncrementFrame())


-- Frame 124
SetFrame( IncrementFrame())


-- Frame 125
SetFrame( IncrementFrame())


-- Frame 126
SetFrame( IncrementFrame())


-- Frame 127
SetFrame( IncrementFrame())


-- Frame 128
SetFrame( IncrementFrame())


-- Frame 129
SetFrame( IncrementFrame())


-- Frame 130
SetFrame( IncrementFrame())


-- Frame 131
SetFrame( IncrementFrame())


-- Frame 132
SetFrame( IncrementFrame())


-- Frame 133
SetFrame( IncrementFrame())


-- Frame 134
SetFrame( IncrementFrame())


-- Frame 135
SetFrame( IncrementFrame())


-- Frame 136
SetFrame( IncrementFrame())


-- Frame 137
SetFrame( IncrementFrame())


-- Frame 138
SetFrame( IncrementFrame())


-- Frame 139
SetFrame( IncrementFrame())


-- Frame 140
SetFrame( IncrementFrame())


-- Frame 141
SetFrame( IncrementFrame())


-- Frame 142
SetFrame( IncrementFrame())


-- Frame 143
SetFrame( IncrementFrame())


-- Frame 144
SetFrame( IncrementFrame())


-- Frame 145
SetFrame( IncrementFrame())


-- Frame 146
SetFrame( IncrementFrame())


-- Frame 147
SetFrame( IncrementFrame())


-- Frame 148
SetFrame( IncrementFrame())


-- Frame 149
SetFrame( IncrementFrame())


-- Frame 150
SetFrame( IncrementFrame())


-- Frame 151
SetFrame( IncrementFrame())


-- Frame 152
SetFrame( IncrementFrame())


-- Frame 153
SetFrame( IncrementFrame())


-- Frame 154
SetFrame( IncrementFrame())


-- Frame 155
SetFrame( IncrementFrame())


-- Frame 156
SetFrame( IncrementFrame())


-- Frame 157
SetFrame( IncrementFrame())


-- Frame 158
SetFrame( IncrementFrame())


-- Frame 159
SetFrame( IncrementFrame())


-- Frame 160
SetFrame( IncrementFrame())


-- Frame 161
SetFrame( IncrementFrame())


-- Frame 162
SetFrame( IncrementFrame())


-- Frame 163
SetFrame( IncrementFrame())


-- Frame 164
SetFrame( IncrementFrame())


-- Frame 165
SetFrame( IncrementFrame())


-- Frame 166
SetFrame( IncrementFrame())


-- Frame 167
SetFrame( IncrementFrame())


-- Frame 168
SetFrame( IncrementFrame())


-- Frame 169
SetFrame( IncrementFrame())


-- Frame 170
SetFrame( IncrementFrame())


-- Frame 171
SetFrame( IncrementFrame())


-- Frame 172
SetFrame( IncrementFrame())


-- Frame 173
SetFrame( IncrementFrame())


-- Frame 174
SetFrame( IncrementFrame())


-- Frame 175
SetFrame( IncrementFrame())


-- Frame 176
SetFrame( IncrementFrame())


-- Frame 177
SetFrame( IncrementFrame())


-- Frame 178
SetFrame( IncrementFrame())


-- Frame 179
SetFrame( IncrementFrame())


-- Frame 180
SetFrame( IncrementFrame())


-- Frame 181
SetFrame( IncrementFrame())


-- Frame 182
SetFrame( IncrementFrame())


-- Frame 183
SetFrame( IncrementFrame())


-- Frame 184
SetFrame( IncrementFrame())


-- Frame 185
SetFrame( IncrementFrame())


-- Frame 186
SetFrame( IncrementFrame())


-- Frame 187
SetFrame( IncrementFrame())


-- Frame 188
SetFrame( IncrementFrame())


-- Frame 189
SetFrame( IncrementFrame())


-- Frame 190
SetFrame( IncrementFrame())


-- Frame 191
SetFrame( IncrementFrame())


-- Frame 192
SetFrame( IncrementFrame())


-- Frame 193
SetFrame( IncrementFrame())


-- Frame 194
SetFrame( IncrementFrame())


-- Frame 195
SetFrame( IncrementFrame())


-- Frame 196
SetFrame( IncrementFrame())


-- Frame 197
SetFrame( IncrementFrame())


-- Frame 198
SetFrame( IncrementFrame())


-- Frame 199
SetFrame( IncrementFrame())


-- Frame 200
SetFrame( IncrementFrame())


-- Frame 201
SetFrame( IncrementFrame())


-- Frame 202
SetFrame( IncrementFrame())


-- Frame 203
SetFrame( IncrementFrame())


-- Frame 204
SetFrame( IncrementFrame())


-- Frame 205
SetFrame( IncrementFrame())


-- Frame 206
SetFrame( IncrementFrame())


-- Frame 207
SetFrame( IncrementFrame())


-- Frame 208
SetFrame( IncrementFrame())


-- Frame 209
SetFrame( IncrementFrame())


-- Frame 210
SetFrame( IncrementFrame())


-- Frame 211
SetFrame( IncrementFrame())


-- Frame 212
SetFrame( IncrementFrame())


-- Frame 213
SetFrame( IncrementFrame())


-- Frame 214
SetFrame( IncrementFrame())


-- Frame 215
SetFrame( IncrementFrame())


-- Frame 216
SetFrame( IncrementFrame())


-- Frame 217
SetFrame( IncrementFrame())


-- Frame 218
SetFrame( IncrementFrame())


-- Frame 219
SetFrame( IncrementFrame())


-- Frame 220
SetFrame( IncrementFrame())


-- Frame 221
SetFrame( IncrementFrame())


-- Frame 222
SetFrame( IncrementFrame())


-- Frame 223
SetFrame( IncrementFrame())


-- Frame 224
SetFrame( IncrementFrame())


-- Frame 225
SetFrame( IncrementFrame())


-- Frame 226
SetFrame( IncrementFrame())


-- Frame 227
SetFrame( IncrementFrame())


-- Frame 228
SetFrame( IncrementFrame())


-- Frame 229
SetFrame( IncrementFrame())


-- Frame 230
SetFrame( IncrementFrame())


-- Frame 231
SetFrame( IncrementFrame())


-- Frame 232
SetFrame( IncrementFrame())


-- Frame 233
SetFrame( IncrementFrame())


-- Frame 234
SetFrame( IncrementFrame())


-- Frame 235
SetFrame( IncrementFrame())


-- Frame 236
SetFrame( IncrementFrame())


-- Frame 237
SetFrame( IncrementFrame())


-- Frame 238
SetFrame( IncrementFrame())


-- Frame 239
SetFrame( IncrementFrame())


-- Frame 240
SetFrame( IncrementFrame())


-- Frame 241
SetFrame( IncrementFrame())


-- Frame 242
SetFrame( IncrementFrame())


-- Frame 243
SetFrame( IncrementFrame())


-- Frame 244
SetFrame( IncrementFrame())


-- Frame 245
SetFrame( IncrementFrame())


-- Frame 246
SetFrame( IncrementFrame())


-- Frame 247
SetFrame( IncrementFrame())


-- Frame 248
SetFrame( IncrementFrame())


-- Frame 249
SetFrame( IncrementFrame())


-- Frame 250
SetFrame( IncrementFrame())


-- Frame 251
SetFrame( IncrementFrame())


-- Frame 252
SetFrame( IncrementFrame())


-- Frame 253
SetFrame( IncrementFrame())


-- Frame 254
SetFrame( IncrementFrame())


-- Frame 255
SetFrame( IncrementFrame())


-- Frame 256
SetFrame( IncrementFrame())


-- Frame 257
SetFrame( IncrementFrame())


-- Frame 258
SetFrame( IncrementFrame())


-- Frame 259
SetFrame( IncrementFrame())


-- Frame 260
SetFrame( IncrementFrame())


-- Frame 261
SetFrame( IncrementFrame())


-- Frame 262
SetFrame( IncrementFrame())


-- Frame 263
SetFrame( IncrementFrame())


-- Frame 264
SetFrame( IncrementFrame())


-- Frame 265
SetFrame( IncrementFrame())


-- Frame 266
SetFrame( IncrementFrame())


-- Frame 267
SetFrame( IncrementFrame())


-- Frame 268
SetFrame( IncrementFrame())


-- Frame 269
SetFrame( IncrementFrame())


-- Frame 270
SetFrame( IncrementFrame())


-- Frame 271
SetFrame( IncrementFrame())


-- Frame 272
SetFrame( IncrementFrame())


-- Frame 273
SetFrame( IncrementFrame())


-- Frame 274
SetFrame( IncrementFrame())


-- Frame 275
SetFrame( IncrementFrame())


-- Frame 276
SetFrame( IncrementFrame())


-- Frame 277
SetFrame( IncrementFrame())


-- Frame 278
SetFrame( IncrementFrame())


-- Frame 279
SetFrame( IncrementFrame())


-- Frame 280
SetFrame( IncrementFrame())


-- Frame 281
SetFrame( IncrementFrame())


-- Frame 282
SetFrame( IncrementFrame())


-- Frame 283
SetFrame( IncrementFrame())


-- Frame 284
SetFrame( IncrementFrame())


-- Frame 285
SetFrame( IncrementFrame())


-- Frame 286
SetFrame( IncrementFrame())


-- Frame 287
SetFrame( IncrementFrame())


-- Frame 288
SetFrame( IncrementFrame())


-- Frame 289
SetFrame( IncrementFrame())


-- Frame 290
SetFrame( IncrementFrame())


-- Frame 291
SetFrame( IncrementFrame())


-- Frame 292
SetFrame( IncrementFrame())


-- Frame 293
SetFrame( IncrementFrame())


-- Frame 294
SetFrame( IncrementFrame())


-- Frame 295
SetFrame( IncrementFrame())


-- Frame 296
SetFrame( IncrementFrame())


-- Frame 297
SetFrame( IncrementFrame())


-- Frame 298
SetFrame( IncrementFrame())


-- Frame 299
SetFrame( IncrementFrame())


-- Frame 300
SetFrame( IncrementFrame())

PlayAudio( ObjectObj_KelThuzad, "Act_Tension", "Resources/WarCraftSounds/CS_Music_Tension.mp3", 0.1)

-- Frame 301
SetFrame( IncrementFrame())


-- Frame 302
SetFrame( IncrementFrame())


-- Frame 303
SetFrame( IncrementFrame())


-- Frame 304
SetFrame( IncrementFrame())


-- Frame 305
SetFrame( IncrementFrame())


-- Frame 306
SetFrame( IncrementFrame())


-- Frame 307
SetFrame( IncrementFrame())


-- Frame 308
SetFrame( IncrementFrame())


-- Frame 309
SetFrame( IncrementFrame())


-- Frame 310
SetFrame( IncrementFrame())


-- Frame 311
SetFrame( IncrementFrame())


-- Frame 312
SetFrame( IncrementFrame())


-- Frame 313
SetFrame( IncrementFrame())


-- Frame 314
SetFrame( IncrementFrame())


-- Frame 315
SetFrame( IncrementFrame())


-- Frame 316
SetFrame( IncrementFrame())


-- Frame 317
SetFrame( IncrementFrame())


-- Frame 318
SetFrame( IncrementFrame())


-- Frame 319
SetFrame( IncrementFrame())


-- Frame 320
SetFrame( IncrementFrame())


-- Frame 321
SetFrame( IncrementFrame())


-- Frame 322
SetFrame( IncrementFrame())


-- Frame 323
SetFrame( IncrementFrame())


-- Frame 324
SetFrame( IncrementFrame())


-- Frame 325
SetFrame( IncrementFrame())


-- Frame 326
SetFrame( IncrementFrame())


-- Frame 327
SetFrame( IncrementFrame())


-- Frame 328
SetFrame( IncrementFrame())


-- Frame 329
SetFrame( IncrementFrame())


-- Frame 330
SetFrame( IncrementFrame())

RotateObjectTo( ObjectObj_KelThuzad, "Act_TurnBackToAltar", 0, 190, 0)
PlayAudio( ObjectObj_KelThuzad, "Act_SpeechSummon", "Resources/WarCraftSounds/CS_01_KelThuzad.mp3", 1)

-- Frame 331
SetFrame( IncrementFrame())


-- Frame 332
SetFrame( IncrementFrame())


-- Frame 333
SetFrame( IncrementFrame())


-- Frame 334
SetFrame( IncrementFrame())


-- Frame 335
SetFrame( IncrementFrame())


-- Frame 336
SetFrame( IncrementFrame())


-- Frame 337
SetFrame( IncrementFrame())


-- Frame 338
SetFrame( IncrementFrame())


-- Frame 339
SetFrame( IncrementFrame())


-- Frame 340
SetFrame( IncrementFrame())


-- Frame 341
SetFrame( IncrementFrame())


-- Frame 342
SetFrame( IncrementFrame())


-- Frame 343
SetFrame( IncrementFrame())


-- Frame 344
SetFrame( IncrementFrame())


-- Frame 345
SetFrame( IncrementFrame())


-- Frame 346
SetFrame( IncrementFrame())


-- Frame 347
SetFrame( IncrementFrame())


-- Frame 348
SetFrame( IncrementFrame())


-- Frame 349
SetFrame( IncrementFrame())


-- Frame 350
SetFrame( IncrementFrame())


-- Frame 351
SetFrame( IncrementFrame())


-- Frame 352
SetFrame( IncrementFrame())


-- Frame 353
SetFrame( IncrementFrame())


-- Frame 354
SetFrame( IncrementFrame())


-- Frame 355
SetFrame( IncrementFrame())


-- Frame 356
SetFrame( IncrementFrame())


-- Frame 357
SetFrame( IncrementFrame())


-- Frame 358
SetFrame( IncrementFrame())


-- Frame 359
SetFrame( IncrementFrame())


-- Frame 360
SetFrame( IncrementFrame())


-- Frame 361
SetFrame( IncrementFrame())


-- Frame 362
SetFrame( IncrementFrame())


-- Frame 363
SetFrame( IncrementFrame())


-- Frame 364
SetFrame( IncrementFrame())


-- Frame 365
SetFrame( IncrementFrame())


-- Frame 366
SetFrame( IncrementFrame())


-- Frame 367
SetFrame( IncrementFrame())


-- Frame 368
SetFrame( IncrementFrame())


-- Frame 369
SetFrame( IncrementFrame())


-- Frame 370
SetFrame( IncrementFrame())


-- Frame 371
SetFrame( IncrementFrame())


-- Frame 372
SetFrame( IncrementFrame())


-- Frame 373
SetFrame( IncrementFrame())


-- Frame 374
SetFrame( IncrementFrame())


-- Frame 375
SetFrame( IncrementFrame())


-- Frame 376
SetFrame( IncrementFrame())


-- Frame 377
SetFrame( IncrementFrame())


-- Frame 378
SetFrame( IncrementFrame())


-- Frame 379
SetFrame( IncrementFrame())


-- Frame 380
SetFrame( IncrementFrame())


-- Frame 381
SetFrame( IncrementFrame())


-- Frame 382
SetFrame( IncrementFrame())


-- Frame 383
SetFrame( IncrementFrame())


-- Frame 384
SetFrame( IncrementFrame())


-- Frame 385
SetFrame( IncrementFrame())


-- Frame 386
SetFrame( IncrementFrame())


-- Frame 387
SetFrame( IncrementFrame())


-- Frame 388
SetFrame( IncrementFrame())


-- Frame 389
SetFrame( IncrementFrame())


-- Frame 390
SetFrame( IncrementFrame())


-- Frame 391
SetFrame( IncrementFrame())


-- Frame 392
SetFrame( IncrementFrame())


-- Frame 393
SetFrame( IncrementFrame())


-- Frame 394
SetFrame( IncrementFrame())


-- Frame 395
SetFrame( IncrementFrame())


-- Frame 396
SetFrame( IncrementFrame())


-- Frame 397
SetFrame( IncrementFrame())


-- Frame 398
SetFrame( IncrementFrame())


-- Frame 399
SetFrame( IncrementFrame())


-- Frame 400
SetFrame( IncrementFrame())


-- Frame 401
SetFrame( IncrementFrame())


-- Frame 402
SetFrame( IncrementFrame())


-- Frame 403
SetFrame( IncrementFrame())


-- Frame 404
SetFrame( IncrementFrame())


-- Frame 405
SetFrame( IncrementFrame())


-- Frame 406
SetFrame( IncrementFrame())


-- Frame 407
SetFrame( IncrementFrame())


-- Frame 408
SetFrame( IncrementFrame())


-- Frame 409
SetFrame( IncrementFrame())


-- Frame 410
SetFrame( IncrementFrame())


-- Frame 411
SetFrame( IncrementFrame())


-- Frame 412
SetFrame( IncrementFrame())


-- Frame 413
SetFrame( IncrementFrame())


-- Frame 414
SetFrame( IncrementFrame())


-- Frame 415
SetFrame( IncrementFrame())


-- Frame 416
SetFrame( IncrementFrame())


-- Frame 417
SetFrame( IncrementFrame())


-- Frame 418
SetFrame( IncrementFrame())


-- Frame 419
SetFrame( IncrementFrame())


-- Frame 420
SetFrame( IncrementFrame())


-- Frame 421
SetFrame( IncrementFrame())


-- Frame 422
SetFrame( IncrementFrame())


-- Frame 423
SetFrame( IncrementFrame())


-- Frame 424
SetFrame( IncrementFrame())


-- Frame 425
SetFrame( IncrementFrame())


-- Frame 426
SetFrame( IncrementFrame())


-- Frame 427
SetFrame( IncrementFrame())


-- Frame 428
SetFrame( IncrementFrame())


-- Frame 429
SetFrame( IncrementFrame())


-- Frame 430
SetFrame( IncrementFrame())


-- Frame 431
SetFrame( IncrementFrame())


-- Frame 432
SetFrame( IncrementFrame())


-- Frame 433
SetFrame( IncrementFrame())


-- Frame 434
SetFrame( IncrementFrame())


-- Frame 435
SetFrame( IncrementFrame())


-- Frame 436
SetFrame( IncrementFrame())


-- Frame 437
SetFrame( IncrementFrame())


-- Frame 438
SetFrame( IncrementFrame())


-- Frame 439
SetFrame( IncrementFrame())


-- Frame 440
SetFrame( IncrementFrame())


-- Frame 441
SetFrame( IncrementFrame())


-- Frame 442
SetFrame( IncrementFrame())


-- Frame 443
SetFrame( IncrementFrame())


-- Frame 444
SetFrame( IncrementFrame())


-- Frame 445
SetFrame( IncrementFrame())


-- Frame 446
SetFrame( IncrementFrame())


-- Frame 447
SetFrame( IncrementFrame())


-- Frame 448
SetFrame( IncrementFrame())


-- Frame 449
SetFrame( IncrementFrame())


-- Frame 450
SetFrame( IncrementFrame())


-- Frame 451
SetFrame( IncrementFrame())


-- Frame 452
SetFrame( IncrementFrame())


-- Frame 453
SetFrame( IncrementFrame())


-- Frame 454
SetFrame( IncrementFrame())


-- Frame 455
SetFrame( IncrementFrame())


-- Frame 456
SetFrame( IncrementFrame())


-- Frame 457
SetFrame( IncrementFrame())


-- Frame 458
SetFrame( IncrementFrame())


-- Frame 459
SetFrame( IncrementFrame())


-- Frame 460
SetFrame( IncrementFrame())


-- Frame 461
SetFrame( IncrementFrame())


-- Frame 462
SetFrame( IncrementFrame())


-- Frame 463
SetFrame( IncrementFrame())


-- Frame 464
SetFrame( IncrementFrame())


-- Frame 465
SetFrame( IncrementFrame())


-- Frame 466
SetFrame( IncrementFrame())


-- Frame 467
SetFrame( IncrementFrame())


-- Frame 468
SetFrame( IncrementFrame())


-- Frame 469
SetFrame( IncrementFrame())


-- Frame 470
SetFrame( IncrementFrame())


-- Frame 471
SetFrame( IncrementFrame())


-- Frame 472
SetFrame( IncrementFrame())


-- Frame 473
SetFrame( IncrementFrame())


-- Frame 474
SetFrame( IncrementFrame())


-- Frame 475
SetFrame( IncrementFrame())


-- Frame 476
SetFrame( IncrementFrame())


-- Frame 477
SetFrame( IncrementFrame())


-- Frame 478
SetFrame( IncrementFrame())


-- Frame 479
SetFrame( IncrementFrame())


-- Frame 480
SetFrame( IncrementFrame())


-- Frame 481
SetFrame( IncrementFrame())


-- Frame 482
SetFrame( IncrementFrame())


-- Frame 483
SetFrame( IncrementFrame())


-- Frame 484
SetFrame( IncrementFrame())


-- Frame 485
SetFrame( IncrementFrame())


-- Frame 486
SetFrame( IncrementFrame())


-- Frame 487
SetFrame( IncrementFrame())


-- Frame 488
SetFrame( IncrementFrame())


-- Frame 489
SetFrame( IncrementFrame())


-- Frame 490
SetFrame( IncrementFrame())


-- Frame 491
SetFrame( IncrementFrame())


-- Frame 492
SetFrame( IncrementFrame())


-- Frame 493
SetFrame( IncrementFrame())


-- Frame 494
SetFrame( IncrementFrame())


-- Frame 495
SetFrame( IncrementFrame())


-- Frame 496
SetFrame( IncrementFrame())


-- Frame 497
SetFrame( IncrementFrame())


-- Frame 498
SetFrame( IncrementFrame())


-- Frame 499
SetFrame( IncrementFrame())


-- Frame 500
SetFrame( IncrementFrame())


-- Frame 501
SetFrame( IncrementFrame())


-- Frame 502
SetFrame( IncrementFrame())


-- Frame 503
SetFrame( IncrementFrame())


-- Frame 504
SetFrame( IncrementFrame())


-- Frame 505
SetFrame( IncrementFrame())


-- Frame 506
SetFrame( IncrementFrame())


-- Frame 507
SetFrame( IncrementFrame())


-- Frame 508
SetFrame( IncrementFrame())


-- Frame 509
SetFrame( IncrementFrame())


-- Frame 510
SetFrame( IncrementFrame())


-- Frame 511
SetFrame( IncrementFrame())


-- Frame 512
SetFrame( IncrementFrame())


-- Frame 513
SetFrame( IncrementFrame())


-- Frame 514
SetFrame( IncrementFrame())


-- Frame 515
SetFrame( IncrementFrame())


-- Frame 516
SetFrame( IncrementFrame())


-- Frame 517
SetFrame( IncrementFrame())


-- Frame 518
SetFrame( IncrementFrame())


-- Frame 519
SetFrame( IncrementFrame())


-- Frame 520
SetFrame( IncrementFrame())


-- Frame 521
SetFrame( IncrementFrame())


-- Frame 522
SetFrame( IncrementFrame())


-- Frame 523
SetFrame( IncrementFrame())


-- Frame 524
SetFrame( IncrementFrame())


-- Frame 525
SetFrame( IncrementFrame())


-- Frame 526
SetFrame( IncrementFrame())


-- Frame 527
SetFrame( IncrementFrame())


-- Frame 528
SetFrame( IncrementFrame())


-- Frame 529
SetFrame( IncrementFrame())


-- Frame 530
SetFrame( IncrementFrame())


-- Frame 531
SetFrame( IncrementFrame())


-- Frame 532
SetFrame( IncrementFrame())


-- Frame 533
SetFrame( IncrementFrame())


-- Frame 534
SetFrame( IncrementFrame())


-- Frame 535
SetFrame( IncrementFrame())


-- Frame 536
SetFrame( IncrementFrame())


-- Frame 537
SetFrame( IncrementFrame())


-- Frame 538
SetFrame( IncrementFrame())


-- Frame 539
SetFrame( IncrementFrame())


-- Frame 540
SetFrame( IncrementFrame())


-- Frame 541
SetFrame( IncrementFrame())


-- Frame 542
SetFrame( IncrementFrame())


-- Frame 543
SetFrame( IncrementFrame())


-- Frame 544
SetFrame( IncrementFrame())


-- Frame 545
SetFrame( IncrementFrame())


-- Frame 546
SetFrame( IncrementFrame())


-- Frame 547
SetFrame( IncrementFrame())


-- Frame 548
SetFrame( IncrementFrame())


-- Frame 549
SetFrame( IncrementFrame())


-- Frame 550
SetFrame( IncrementFrame())


-- Frame 551
SetFrame( IncrementFrame())


-- Frame 552
SetFrame( IncrementFrame())


-- Frame 553
SetFrame( IncrementFrame())


-- Frame 554
SetFrame( IncrementFrame())


-- Frame 555
SetFrame( IncrementFrame())


-- Frame 556
SetFrame( IncrementFrame())


-- Frame 557
SetFrame( IncrementFrame())


-- Frame 558
SetFrame( IncrementFrame())


-- Frame 559
SetFrame( IncrementFrame())


-- Frame 560
SetFrame( IncrementFrame())


-- Frame 561
SetFrame( IncrementFrame())


-- Frame 562
SetFrame( IncrementFrame())


-- Frame 563
SetFrame( IncrementFrame())


-- Frame 564
SetFrame( IncrementFrame())


-- Frame 565
SetFrame( IncrementFrame())


-- Frame 566
SetFrame( IncrementFrame())


-- Frame 567
SetFrame( IncrementFrame())


-- Frame 568
SetFrame( IncrementFrame())


-- Frame 569
SetFrame( IncrementFrame())


-- Frame 570
SetFrame( IncrementFrame())


-- Frame 571
SetFrame( IncrementFrame())


-- Frame 572
SetFrame( IncrementFrame())


-- Frame 573
SetFrame( IncrementFrame())


-- Frame 574
SetFrame( IncrementFrame())


-- Frame 575
SetFrame( IncrementFrame())


-- Frame 576
SetFrame( IncrementFrame())


-- Frame 577
SetFrame( IncrementFrame())


-- Frame 578
SetFrame( IncrementFrame())


-- Frame 579
SetFrame( IncrementFrame())


-- Frame 580
SetFrame( IncrementFrame())


-- Frame 581
SetFrame( IncrementFrame())


-- Frame 582
SetFrame( IncrementFrame())


-- Frame 583
SetFrame( IncrementFrame())


-- Frame 584
SetFrame( IncrementFrame())


-- Frame 585
SetFrame( IncrementFrame())


-- Frame 586
SetFrame( IncrementFrame())


-- Frame 587
SetFrame( IncrementFrame())


-- Frame 588
SetFrame( IncrementFrame())


-- Frame 589
SetFrame( IncrementFrame())


-- Frame 590
SetFrame( IncrementFrame())


-- Frame 591
SetFrame( IncrementFrame())


-- Frame 592
SetFrame( IncrementFrame())


-- Frame 593
SetFrame( IncrementFrame())


-- Frame 594
SetFrame( IncrementFrame())


-- Frame 595
SetFrame( IncrementFrame())


-- Frame 596
SetFrame( IncrementFrame())


-- Frame 597
SetFrame( IncrementFrame())


-- Frame 598
SetFrame( IncrementFrame())


-- Frame 599
SetFrame( IncrementFrame())


-- Frame 600
SetFrame( IncrementFrame())


-- Frame 601
SetFrame( IncrementFrame())


-- Frame 602
SetFrame( IncrementFrame())


-- Frame 603
SetFrame( IncrementFrame())


-- Frame 604
SetFrame( IncrementFrame())


-- Frame 605
SetFrame( IncrementFrame())


-- Frame 606
SetFrame( IncrementFrame())


-- Frame 607
SetFrame( IncrementFrame())


-- Frame 608
SetFrame( IncrementFrame())


-- Frame 609
SetFrame( IncrementFrame())


-- Frame 610
SetFrame( IncrementFrame())


-- Frame 611
SetFrame( IncrementFrame())


-- Frame 612
SetFrame( IncrementFrame())


-- Frame 613
SetFrame( IncrementFrame())


-- Frame 614
SetFrame( IncrementFrame())


-- Frame 615
SetFrame( IncrementFrame())


-- Frame 616
SetFrame( IncrementFrame())


-- Frame 617
SetFrame( IncrementFrame())


-- Frame 618
SetFrame( IncrementFrame())


-- Frame 619
SetFrame( IncrementFrame())


-- Frame 620
SetFrame( IncrementFrame())


-- Frame 621
SetFrame( IncrementFrame())


-- Frame 622
SetFrame( IncrementFrame())


-- Frame 623
SetFrame( IncrementFrame())


-- Frame 624
SetFrame( IncrementFrame())


-- Frame 625
SetFrame( IncrementFrame())


-- Frame 626
SetFrame( IncrementFrame())


-- Frame 627
SetFrame( IncrementFrame())


-- Frame 628
SetFrame( IncrementFrame())


-- Frame 629
SetFrame( IncrementFrame())


-- Frame 630
SetFrame( IncrementFrame())


-- Frame 631
SetFrame( IncrementFrame())


-- Frame 632
SetFrame( IncrementFrame())


-- Frame 633
SetFrame( IncrementFrame())


-- Frame 634
SetFrame( IncrementFrame())


-- Frame 635
SetFrame( IncrementFrame())


-- Frame 636
SetFrame( IncrementFrame())


-- Frame 637
SetFrame( IncrementFrame())


-- Frame 638
SetFrame( IncrementFrame())


-- Frame 639
SetFrame( IncrementFrame())


-- Frame 640
SetFrame( IncrementFrame())


-- Frame 641
SetFrame( IncrementFrame())


-- Frame 642
SetFrame( IncrementFrame())


-- Frame 643
SetFrame( IncrementFrame())


-- Frame 644
SetFrame( IncrementFrame())


-- Frame 645
SetFrame( IncrementFrame())


-- Frame 646
SetFrame( IncrementFrame())


-- Frame 647
SetFrame( IncrementFrame())


-- Frame 648
SetFrame( IncrementFrame())


-- Frame 649
SetFrame( IncrementFrame())


-- Frame 650
SetFrame( IncrementFrame())


-- Frame 651
SetFrame( IncrementFrame())


-- Frame 652
SetFrame( IncrementFrame())


-- Frame 653
SetFrame( IncrementFrame())


-- Frame 654
SetFrame( IncrementFrame())


-- Frame 655
SetFrame( IncrementFrame())


-- Frame 656
SetFrame( IncrementFrame())


-- Frame 657
SetFrame( IncrementFrame())


-- Frame 658
SetFrame( IncrementFrame())


-- Frame 659
SetFrame( IncrementFrame())


-- Frame 660
SetFrame( IncrementFrame())


-- Frame 661
SetFrame( IncrementFrame())


-- Frame 662
SetFrame( IncrementFrame())


-- Frame 663
SetFrame( IncrementFrame())


-- Frame 664
SetFrame( IncrementFrame())


-- Frame 665
SetFrame( IncrementFrame())


-- Frame 666
SetFrame( IncrementFrame())


-- Frame 667
SetFrame( IncrementFrame())


-- Frame 668
SetFrame( IncrementFrame())


-- Frame 669
SetFrame( IncrementFrame())


-- Frame 670
SetFrame( IncrementFrame())


-- Frame 671
SetFrame( IncrementFrame())


-- Frame 672
SetFrame( IncrementFrame())


-- Frame 673
SetFrame( IncrementFrame())


-- Frame 674
SetFrame( IncrementFrame())


-- Frame 675
SetFrame( IncrementFrame())


-- Frame 676
SetFrame( IncrementFrame())


-- Frame 677
SetFrame( IncrementFrame())


-- Frame 678
SetFrame( IncrementFrame())


-- Frame 679
SetFrame( IncrementFrame())


-- Frame 680
SetFrame( IncrementFrame())


-- Frame 681
SetFrame( IncrementFrame())


-- Frame 682
SetFrame( IncrementFrame())


-- Frame 683
SetFrame( IncrementFrame())


-- Frame 684
SetFrame( IncrementFrame())


-- Frame 685
SetFrame( IncrementFrame())


-- Frame 686
SetFrame( IncrementFrame())


-- Frame 687
SetFrame( IncrementFrame())


-- Frame 688
SetFrame( IncrementFrame())


-- Frame 689
SetFrame( IncrementFrame())


-- Frame 690
SetFrame( IncrementFrame())


-- Frame 691
SetFrame( IncrementFrame())


-- Frame 692
SetFrame( IncrementFrame())


-- Frame 693
SetFrame( IncrementFrame())


-- Frame 694
SetFrame( IncrementFrame())


-- Frame 695
SetFrame( IncrementFrame())


-- Frame 696
SetFrame( IncrementFrame())


-- Frame 697
SetFrame( IncrementFrame())


-- Frame 698
SetFrame( IncrementFrame())


-- Frame 699
SetFrame( IncrementFrame())


-- Frame 700
SetFrame( IncrementFrame())


-- Frame 701
SetFrame( IncrementFrame())


-- Frame 702
SetFrame( IncrementFrame())


-- Frame 703
SetFrame( IncrementFrame())


-- Frame 704
SetFrame( IncrementFrame())


-- Frame 705
SetFrame( IncrementFrame())


-- Frame 706
SetFrame( IncrementFrame())


-- Frame 707
SetFrame( IncrementFrame())


-- Frame 708
SetFrame( IncrementFrame())


-- Frame 709
SetFrame( IncrementFrame())


-- Frame 710
SetFrame( IncrementFrame())


-- Frame 711
SetFrame( IncrementFrame())


-- Frame 712
SetFrame( IncrementFrame())


-- Frame 713
SetFrame( IncrementFrame())


-- Frame 714
SetFrame( IncrementFrame())


-- Frame 715
SetFrame( IncrementFrame())


-- Frame 716
SetFrame( IncrementFrame())


-- Frame 717
SetFrame( IncrementFrame())


-- Frame 718
SetFrame( IncrementFrame())


-- Frame 719
SetFrame( IncrementFrame())


-- Frame 720
SetFrame( IncrementFrame())


-- Frame 721
SetFrame( IncrementFrame())


-- Frame 722
SetFrame( IncrementFrame())


-- Frame 723
SetFrame( IncrementFrame())


-- Frame 724
SetFrame( IncrementFrame())


-- Frame 725
SetFrame( IncrementFrame())


-- Frame 726
SetFrame( IncrementFrame())


-- Frame 727
SetFrame( IncrementFrame())


-- Frame 728
SetFrame( IncrementFrame())


-- Frame 729
SetFrame( IncrementFrame())


-- Frame 730
SetFrame( IncrementFrame())


-- Frame 731
SetFrame( IncrementFrame())


-- Frame 732
SetFrame( IncrementFrame())


-- Frame 733
SetFrame( IncrementFrame())


-- Frame 734
SetFrame( IncrementFrame())


-- Frame 735
SetFrame( IncrementFrame())


-- Frame 736
SetFrame( IncrementFrame())


-- Frame 737
SetFrame( IncrementFrame())


-- Frame 738
SetFrame( IncrementFrame())


-- Frame 739
SetFrame( IncrementFrame())


-- Frame 740
SetFrame( IncrementFrame())


-- Frame 741
SetFrame( IncrementFrame())


-- Frame 742
SetFrame( IncrementFrame())


-- Frame 743
SetFrame( IncrementFrame())


-- Frame 744
SetFrame( IncrementFrame())


-- Frame 745
SetFrame( IncrementFrame())


-- Frame 746
SetFrame( IncrementFrame())


-- Frame 747
SetFrame( IncrementFrame())


-- Frame 748
SetFrame( IncrementFrame())


-- Frame 749
SetFrame( IncrementFrame())


-- Frame 750
SetFrame( IncrementFrame())


-- Frame 751
SetFrame( IncrementFrame())


-- Frame 752
SetFrame( IncrementFrame())


-- Frame 753
SetFrame( IncrementFrame())


-- Frame 754
SetFrame( IncrementFrame())


-- Frame 755
SetFrame( IncrementFrame())


-- Frame 756
SetFrame( IncrementFrame())


-- Frame 757
SetFrame( IncrementFrame())


-- Frame 758
SetFrame( IncrementFrame())


-- Frame 759
SetFrame( IncrementFrame())


-- Frame 760
SetFrame( IncrementFrame())


-- Frame 761
SetFrame( IncrementFrame())


-- Frame 762
SetFrame( IncrementFrame())


-- Frame 763
SetFrame( IncrementFrame())


-- Frame 764
SetFrame( IncrementFrame())


-- Frame 765
SetFrame( IncrementFrame())


-- Frame 766
SetFrame( IncrementFrame())


-- Frame 767
SetFrame( IncrementFrame())


-- Frame 768
SetFrame( IncrementFrame())


-- Frame 769
SetFrame( IncrementFrame())


-- Frame 770
SetFrame( IncrementFrame())


-- Frame 771
SetFrame( IncrementFrame())


-- Frame 772
SetFrame( IncrementFrame())


-- Frame 773
SetFrame( IncrementFrame())


-- Frame 774
SetFrame( IncrementFrame())


-- Frame 775
SetFrame( IncrementFrame())


-- Frame 776
SetFrame( IncrementFrame())


-- Frame 777
SetFrame( IncrementFrame())


-- Frame 778
SetFrame( IncrementFrame())


-- Frame 779
SetFrame( IncrementFrame())


-- Frame 780
SetFrame( IncrementFrame())


-- Frame 781
SetFrame( IncrementFrame())


-- Frame 782
SetFrame( IncrementFrame())


-- Frame 783
SetFrame( IncrementFrame())


-- Frame 784
SetFrame( IncrementFrame())


-- Frame 785
SetFrame( IncrementFrame())


-- Frame 786
SetFrame( IncrementFrame())


-- Frame 787
SetFrame( IncrementFrame())


-- Frame 788
SetFrame( IncrementFrame())


-- Frame 789
SetFrame( IncrementFrame())


-- Frame 790
SetFrame( IncrementFrame())


-- Frame 791
SetFrame( IncrementFrame())


-- Frame 792
SetFrame( IncrementFrame())


-- Frame 793
SetFrame( IncrementFrame())


-- Frame 794
SetFrame( IncrementFrame())


-- Frame 795
SetFrame( IncrementFrame())


-- Frame 796
SetFrame( IncrementFrame())


-- Frame 797
SetFrame( IncrementFrame())


-- Frame 798
SetFrame( IncrementFrame())


-- Frame 799
SetFrame( IncrementFrame())


-- Frame 800
SetFrame( IncrementFrame())


-- Frame 801
SetFrame( IncrementFrame())


-- Frame 802
SetFrame( IncrementFrame())


-- Frame 803
SetFrame( IncrementFrame())


-- Frame 804
SetFrame( IncrementFrame())


-- Frame 805
SetFrame( IncrementFrame())


-- Frame 806
SetFrame( IncrementFrame())


-- Frame 807
SetFrame( IncrementFrame())


-- Frame 808
SetFrame( IncrementFrame())


-- Frame 809
SetFrame( IncrementFrame())


-- Frame 810
SetFrame( IncrementFrame())


-- Frame 811
SetFrame( IncrementFrame())


-- Frame 812
SetFrame( IncrementFrame())


-- Frame 813
SetFrame( IncrementFrame())


-- Frame 814
SetFrame( IncrementFrame())


-- Frame 815
SetFrame( IncrementFrame())


-- Frame 816
SetFrame( IncrementFrame())


-- Frame 817
SetFrame( IncrementFrame())


-- Frame 818
SetFrame( IncrementFrame())


-- Frame 819
SetFrame( IncrementFrame())


-- Frame 820
SetFrame( IncrementFrame())


-- Frame 821
SetFrame( IncrementFrame())


-- Frame 822
SetFrame( IncrementFrame())


-- Frame 823
SetFrame( IncrementFrame())


-- Frame 824
SetFrame( IncrementFrame())


-- Frame 825
SetFrame( IncrementFrame())


-- Frame 826
SetFrame( IncrementFrame())


-- Frame 827
SetFrame( IncrementFrame())


-- Frame 828
SetFrame( IncrementFrame())


-- Frame 829
SetFrame( IncrementFrame())


-- Frame 830
SetFrame( IncrementFrame())


-- Frame 831
SetFrame( IncrementFrame())


-- Frame 832
SetFrame( IncrementFrame())


-- Frame 833
SetFrame( IncrementFrame())


-- Frame 834
SetFrame( IncrementFrame())


-- Frame 835
SetFrame( IncrementFrame())


-- Frame 836
SetFrame( IncrementFrame())


-- Frame 837
SetFrame( IncrementFrame())


-- Frame 838
SetFrame( IncrementFrame())


-- Frame 839
SetFrame( IncrementFrame())


-- Frame 840
SetFrame( IncrementFrame())


-- Frame 841
SetFrame( IncrementFrame())


-- Frame 842
SetFrame( IncrementFrame())


-- Frame 843
SetFrame( IncrementFrame())


-- Frame 844
SetFrame( IncrementFrame())


-- Frame 845
SetFrame( IncrementFrame())


-- Frame 846
SetFrame( IncrementFrame())


-- Frame 847
SetFrame( IncrementFrame())


-- Frame 848
SetFrame( IncrementFrame())


-- Frame 849
SetFrame( IncrementFrame())


-- Frame 850
SetFrame( IncrementFrame())


-- Frame 851
SetFrame( IncrementFrame())


-- Frame 852
SetFrame( IncrementFrame())


-- Frame 853
SetFrame( IncrementFrame())


-- Frame 854
SetFrame( IncrementFrame())


-- Frame 855
SetFrame( IncrementFrame())


-- Frame 856
SetFrame( IncrementFrame())


-- Frame 857
SetFrame( IncrementFrame())


-- Frame 858
SetFrame( IncrementFrame())


-- Frame 859
SetFrame( IncrementFrame())


-- Frame 860
SetFrame( IncrementFrame())

SetObjectVisibility( ObjectObj_Archimonde, "Act_Appear", true)

-- Frame 861
SetFrame( IncrementFrame())


-- Frame 862
SetFrame( IncrementFrame())


-- Frame 863
SetFrame( IncrementFrame())


-- Frame 864
SetFrame( IncrementFrame())


-- Frame 865
SetFrame( IncrementFrame())


-- Frame 866
SetFrame( IncrementFrame())


-- Frame 867
SetFrame( IncrementFrame())


-- Frame 868
SetFrame( IncrementFrame())


-- Frame 869
SetFrame( IncrementFrame())


-- Frame 870
SetFrame( IncrementFrame())


-- Frame 871
SetFrame( IncrementFrame())


-- Frame 872
SetFrame( IncrementFrame())


-- Frame 873
SetFrame( IncrementFrame())


-- Frame 874
SetFrame( IncrementFrame())


-- Frame 875
SetFrame( IncrementFrame())


-- Frame 876
SetFrame( IncrementFrame())


-- Frame 877
SetFrame( IncrementFrame())


-- Frame 878
SetFrame( IncrementFrame())


-- Frame 879
SetFrame( IncrementFrame())


-- Frame 880
SetFrame( IncrementFrame())


-- Frame 881
SetFrame( IncrementFrame())


-- Frame 882
SetFrame( IncrementFrame())


-- Frame 883
SetFrame( IncrementFrame())


-- Frame 884
SetFrame( IncrementFrame())


-- Frame 885
SetFrame( IncrementFrame())


-- Frame 886
SetFrame( IncrementFrame())


-- Frame 887
SetFrame( IncrementFrame())


-- Frame 888
SetFrame( IncrementFrame())


-- Frame 889
SetFrame( IncrementFrame())


-- Frame 890
SetFrame( IncrementFrame())


-- Frame 891
SetFrame( IncrementFrame())


-- Frame 892
SetFrame( IncrementFrame())


-- Frame 893
SetFrame( IncrementFrame())


-- Frame 894
SetFrame( IncrementFrame())


-- Frame 895
SetFrame( IncrementFrame())


-- Frame 896
SetFrame( IncrementFrame())


-- Frame 897
SetFrame( IncrementFrame())


-- Frame 898
SetFrame( IncrementFrame())


-- Frame 899
SetFrame( IncrementFrame())


-- Frame 900
SetFrame( IncrementFrame())

PlayAudio( ObjectObj_Archimonde, "Act_TrembleMortals", "Resources/WarCraftSounds/CS_02_Archimonde.mp3", 1)

-- Frame 901
SetFrame( IncrementFrame())


-- Frame 902
SetFrame( IncrementFrame())


-- Frame 903
SetFrame( IncrementFrame())


-- Frame 904
SetFrame( IncrementFrame())


-- Frame 905
SetFrame( IncrementFrame())


-- Frame 906
SetFrame( IncrementFrame())


-- Frame 907
SetFrame( IncrementFrame())


-- Frame 908
SetFrame( IncrementFrame())


-- Frame 909
SetFrame( IncrementFrame())


-- Frame 910
SetFrame( IncrementFrame())


-- Frame 911
SetFrame( IncrementFrame())


-- Frame 912
SetFrame( IncrementFrame())


-- Frame 913
SetFrame( IncrementFrame())


-- Frame 914
SetFrame( IncrementFrame())


-- Frame 915
SetFrame( IncrementFrame())


-- Frame 916
SetFrame( IncrementFrame())


-- Frame 917
SetFrame( IncrementFrame())


-- Frame 918
SetFrame( IncrementFrame())


-- Frame 919
SetFrame( IncrementFrame())


-- Frame 920
SetFrame( IncrementFrame())


-- Frame 921
SetFrame( IncrementFrame())


-- Frame 922
SetFrame( IncrementFrame())


-- Frame 923
SetFrame( IncrementFrame())


-- Frame 924
SetFrame( IncrementFrame())


-- Frame 925
SetFrame( IncrementFrame())


-- Frame 926
SetFrame( IncrementFrame())


-- Frame 927
SetFrame( IncrementFrame())


-- Frame 928
SetFrame( IncrementFrame())


-- Frame 929
SetFrame( IncrementFrame())


-- Frame 930
SetFrame( IncrementFrame())


-- Frame 931
SetFrame( IncrementFrame())


-- Frame 932
SetFrame( IncrementFrame())


-- Frame 933
SetFrame( IncrementFrame())


-- Frame 934
SetFrame( IncrementFrame())


-- Frame 935
SetFrame( IncrementFrame())


-- Frame 936
SetFrame( IncrementFrame())


-- Frame 937
SetFrame( IncrementFrame())


-- Frame 938
SetFrame( IncrementFrame())


-- Frame 939
SetFrame( IncrementFrame())


-- Frame 940
SetFrame( IncrementFrame())


-- Frame 941
SetFrame( IncrementFrame())


-- Frame 942
SetFrame( IncrementFrame())


-- Frame 943
SetFrame( IncrementFrame())


-- Frame 944
SetFrame( IncrementFrame())


-- Frame 945
SetFrame( IncrementFrame())


-- Frame 946
SetFrame( IncrementFrame())


-- Frame 947
SetFrame( IncrementFrame())


-- Frame 948
SetFrame( IncrementFrame())


-- Frame 949
SetFrame( IncrementFrame())


-- Frame 950
SetFrame( IncrementFrame())


-- Frame 951
SetFrame( IncrementFrame())


-- Frame 952
SetFrame( IncrementFrame())


-- Frame 953
SetFrame( IncrementFrame())


-- Frame 954
SetFrame( IncrementFrame())


-- Frame 955
SetFrame( IncrementFrame())


-- Frame 956
SetFrame( IncrementFrame())


-- Frame 957
SetFrame( IncrementFrame())


-- Frame 958
SetFrame( IncrementFrame())


-- Frame 959
SetFrame( IncrementFrame())


-- Frame 960
SetFrame( IncrementFrame())


-- Frame 961
SetFrame( IncrementFrame())


-- Frame 962
SetFrame( IncrementFrame())


-- Frame 963
SetFrame( IncrementFrame())


-- Frame 964
SetFrame( IncrementFrame())


-- Frame 965
SetFrame( IncrementFrame())


-- Frame 966
SetFrame( IncrementFrame())


-- Frame 967
SetFrame( IncrementFrame())


-- Frame 968
SetFrame( IncrementFrame())


-- Frame 969
SetFrame( IncrementFrame())


-- Frame 970
SetFrame( IncrementFrame())


-- Frame 971
SetFrame( IncrementFrame())


-- Frame 972
SetFrame( IncrementFrame())


-- Frame 973
SetFrame( IncrementFrame())


-- Frame 974
SetFrame( IncrementFrame())


-- Frame 975
SetFrame( IncrementFrame())


-- Frame 976
SetFrame( IncrementFrame())


-- Frame 977
SetFrame( IncrementFrame())


-- Frame 978
SetFrame( IncrementFrame())


-- Frame 979
SetFrame( IncrementFrame())


-- Frame 980
SetFrame( IncrementFrame())


-- Frame 981
SetFrame( IncrementFrame())


-- Frame 982
SetFrame( IncrementFrame())


-- Frame 983
SetFrame( IncrementFrame())


-- Frame 984
SetFrame( IncrementFrame())


-- Frame 985
SetFrame( IncrementFrame())


-- Frame 986
SetFrame( IncrementFrame())


-- Frame 987
SetFrame( IncrementFrame())


-- Frame 988
SetFrame( IncrementFrame())


-- Frame 989
SetFrame( IncrementFrame())


-- Frame 990
SetFrame( IncrementFrame())


-- Frame 991
SetFrame( IncrementFrame())


-- Frame 992
SetFrame( IncrementFrame())


-- Frame 993
SetFrame( IncrementFrame())


-- Frame 994
SetFrame( IncrementFrame())


-- Frame 995
SetFrame( IncrementFrame())


-- Frame 996
SetFrame( IncrementFrame())


-- Frame 997
SetFrame( IncrementFrame())


-- Frame 998
SetFrame( IncrementFrame())


-- Frame 999
SetFrame( IncrementFrame())


-- Frame 1000
SetFrame( IncrementFrame())


-- Frame 1001
SetFrame( IncrementFrame())


-- Frame 1002
SetFrame( IncrementFrame())


-- Frame 1003
SetFrame( IncrementFrame())


-- Frame 1004
SetFrame( IncrementFrame())


-- Frame 1005
SetFrame( IncrementFrame())


-- Frame 1006
SetFrame( IncrementFrame())


-- Frame 1007
SetFrame( IncrementFrame())


-- Frame 1008
SetFrame( IncrementFrame())


-- Frame 1009
SetFrame( IncrementFrame())


-- Frame 1010
SetFrame( IncrementFrame())


-- Frame 1011
SetFrame( IncrementFrame())


-- Frame 1012
SetFrame( IncrementFrame())


-- Frame 1013
SetFrame( IncrementFrame())


-- Frame 1014
SetFrame( IncrementFrame())


-- Frame 1015
SetFrame( IncrementFrame())


-- Frame 1016
SetFrame( IncrementFrame())


-- Frame 1017
SetFrame( IncrementFrame())


-- Frame 1018
SetFrame( IncrementFrame())


-- Frame 1019
SetFrame( IncrementFrame())


-- Frame 1020
SetFrame( IncrementFrame())


-- Frame 1021
SetFrame( IncrementFrame())


-- Frame 1022
SetFrame( IncrementFrame())


-- Frame 1023
SetFrame( IncrementFrame())


-- Frame 1024
SetFrame( IncrementFrame())


-- Frame 1025
SetFrame( IncrementFrame())


-- Frame 1026
SetFrame( IncrementFrame())


-- Frame 1027
SetFrame( IncrementFrame())


-- Frame 1028
SetFrame( IncrementFrame())


-- Frame 1029
SetFrame( IncrementFrame())


-- Frame 1030
SetFrame( IncrementFrame())


-- Frame 1031
SetFrame( IncrementFrame())


-- Frame 1032
SetFrame( IncrementFrame())


-- Frame 1033
SetFrame( IncrementFrame())


-- Frame 1034
SetFrame( IncrementFrame())


-- Frame 1035
SetFrame( IncrementFrame())


-- Frame 1036
SetFrame( IncrementFrame())


-- Frame 1037
SetFrame( IncrementFrame())


-- Frame 1038
SetFrame( IncrementFrame())


-- Frame 1039
SetFrame( IncrementFrame())


-- Frame 1040
SetFrame( IncrementFrame())


-- Frame 1041
SetFrame( IncrementFrame())


-- Frame 1042
SetFrame( IncrementFrame())


-- Frame 1043
SetFrame( IncrementFrame())


-- Frame 1044
SetFrame( IncrementFrame())


-- Frame 1045
SetFrame( IncrementFrame())


-- Frame 1046
SetFrame( IncrementFrame())


-- Frame 1047
SetFrame( IncrementFrame())


-- Frame 1048
SetFrame( IncrementFrame())


-- Frame 1049
SetFrame( IncrementFrame())


-- Frame 1050
SetFrame( IncrementFrame())


-- Frame 1051
SetFrame( IncrementFrame())


-- Frame 1052
SetFrame( IncrementFrame())


-- Frame 1053
SetFrame( IncrementFrame())


-- Frame 1054
SetFrame( IncrementFrame())


-- Frame 1055
SetFrame( IncrementFrame())


-- Frame 1056
SetFrame( IncrementFrame())


-- Frame 1057
SetFrame( IncrementFrame())


-- Frame 1058
SetFrame( IncrementFrame())


-- Frame 1059
SetFrame( IncrementFrame())


-- Frame 1060
SetFrame( IncrementFrame())


-- Frame 1061
SetFrame( IncrementFrame())


-- Frame 1062
SetFrame( IncrementFrame())


-- Frame 1063
SetFrame( IncrementFrame())


-- Frame 1064
SetFrame( IncrementFrame())


-- Frame 1065
SetFrame( IncrementFrame())


-- Frame 1066
SetFrame( IncrementFrame())


-- Frame 1067
SetFrame( IncrementFrame())


-- Frame 1068
SetFrame( IncrementFrame())


-- Frame 1069
SetFrame( IncrementFrame())


-- Frame 1070
SetFrame( IncrementFrame())


-- Frame 1071
SetFrame( IncrementFrame())


-- Frame 1072
SetFrame( IncrementFrame())


-- Frame 1073
SetFrame( IncrementFrame())


-- Frame 1074
SetFrame( IncrementFrame())


-- Frame 1075
SetFrame( IncrementFrame())


-- Frame 1076
SetFrame( IncrementFrame())


-- Frame 1077
SetFrame( IncrementFrame())


-- Frame 1078
SetFrame( IncrementFrame())


-- Frame 1079
SetFrame( IncrementFrame())


-- Frame 1080
SetFrame( IncrementFrame())


-- Frame 1081
SetFrame( IncrementFrame())


-- Frame 1082
SetFrame( IncrementFrame())


-- Frame 1083
SetFrame( IncrementFrame())


-- Frame 1084
SetFrame( IncrementFrame())


-- Frame 1085
SetFrame( IncrementFrame())


-- Frame 1086
SetFrame( IncrementFrame())


-- Frame 1087
SetFrame( IncrementFrame())


-- Frame 1088
SetFrame( IncrementFrame())


-- Frame 1089
SetFrame( IncrementFrame())


-- Frame 1090
SetFrame( IncrementFrame())


-- Frame 1091
SetFrame( IncrementFrame())


-- Frame 1092
SetFrame( IncrementFrame())


-- Frame 1093
SetFrame( IncrementFrame())


-- Frame 1094
SetFrame( IncrementFrame())


-- Frame 1095
SetFrame( IncrementFrame())


-- Frame 1096
SetFrame( IncrementFrame())


-- Frame 1097
SetFrame( IncrementFrame())


-- Frame 1098
SetFrame( IncrementFrame())


-- Frame 1099
SetFrame( IncrementFrame())


-- Frame 1100
SetFrame( IncrementFrame())


-- Frame 1101
SetFrame( IncrementFrame())


-- Frame 1102
SetFrame( IncrementFrame())


-- Frame 1103
SetFrame( IncrementFrame())


-- Frame 1104
SetFrame( IncrementFrame())


-- Frame 1105
SetFrame( IncrementFrame())


-- Frame 1106
SetFrame( IncrementFrame())


-- Frame 1107
SetFrame( IncrementFrame())


-- Frame 1108
SetFrame( IncrementFrame())


-- Frame 1109
SetFrame( IncrementFrame())


-- Frame 1110
SetFrame( IncrementFrame())


-- Frame 1111
SetFrame( IncrementFrame())


-- Frame 1112
SetFrame( IncrementFrame())


-- Frame 1113
SetFrame( IncrementFrame())


-- Frame 1114
SetFrame( IncrementFrame())


-- Frame 1115
SetFrame( IncrementFrame())


-- Frame 1116
SetFrame( IncrementFrame())


-- Frame 1117
SetFrame( IncrementFrame())


-- Frame 1118
SetFrame( IncrementFrame())


-- Frame 1119
SetFrame( IncrementFrame())


-- Frame 1120
SetFrame( IncrementFrame())


-- Frame 1121
SetFrame( IncrementFrame())


-- Frame 1122
SetFrame( IncrementFrame())


-- Frame 1123
SetFrame( IncrementFrame())


-- Frame 1124
SetFrame( IncrementFrame())


-- Frame 1125
SetFrame( IncrementFrame())


-- Frame 1126
SetFrame( IncrementFrame())


-- Frame 1127
SetFrame( IncrementFrame())


-- Frame 1128
SetFrame( IncrementFrame())


-- Frame 1129
SetFrame( IncrementFrame())


-- Frame 1130
SetFrame( IncrementFrame())


-- Frame 1131
SetFrame( IncrementFrame())


-- Frame 1132
SetFrame( IncrementFrame())


-- Frame 1133
SetFrame( IncrementFrame())


-- Frame 1134
SetFrame( IncrementFrame())


-- Frame 1135
SetFrame( IncrementFrame())


-- Frame 1136
SetFrame( IncrementFrame())


-- Frame 1137
SetFrame( IncrementFrame())


-- Frame 1138
SetFrame( IncrementFrame())


-- Frame 1139
SetFrame( IncrementFrame())


-- Frame 1140
SetFrame( IncrementFrame())


-- Frame 1141
SetFrame( IncrementFrame())


-- Frame 1142
SetFrame( IncrementFrame())


-- Frame 1143
SetFrame( IncrementFrame())


-- Frame 1144
SetFrame( IncrementFrame())


-- Frame 1145
SetFrame( IncrementFrame())


-- Frame 1146
SetFrame( IncrementFrame())


-- Frame 1147
SetFrame( IncrementFrame())


-- Frame 1148
SetFrame( IncrementFrame())


-- Frame 1149
SetFrame( IncrementFrame())


-- Frame 1150
SetFrame( IncrementFrame())


-- Frame 1151
SetFrame( IncrementFrame())


-- Frame 1152
SetFrame( IncrementFrame())


-- Frame 1153
SetFrame( IncrementFrame())


-- Frame 1154
SetFrame( IncrementFrame())


-- Frame 1155
SetFrame( IncrementFrame())


-- Frame 1156
SetFrame( IncrementFrame())


-- Frame 1157
SetFrame( IncrementFrame())


-- Frame 1158
SetFrame( IncrementFrame())


-- Frame 1159
SetFrame( IncrementFrame())


-- Frame 1160
SetFrame( IncrementFrame())


-- Frame 1161
SetFrame( IncrementFrame())


-- Frame 1162
SetFrame( IncrementFrame())


-- Frame 1163
SetFrame( IncrementFrame())


-- Frame 1164
SetFrame( IncrementFrame())


-- Frame 1165
SetFrame( IncrementFrame())


-- Frame 1166
SetFrame( IncrementFrame())


-- Frame 1167
SetFrame( IncrementFrame())


-- Frame 1168
SetFrame( IncrementFrame())


-- Frame 1169
SetFrame( IncrementFrame())


-- Frame 1170
SetFrame( IncrementFrame())


-- Frame 1171
SetFrame( IncrementFrame())


-- Frame 1172
SetFrame( IncrementFrame())


-- Frame 1173
SetFrame( IncrementFrame())


-- Frame 1174
SetFrame( IncrementFrame())


-- Frame 1175
SetFrame( IncrementFrame())


-- Frame 1176
SetFrame( IncrementFrame())


-- Frame 1177
SetFrame( IncrementFrame())


-- Frame 1178
SetFrame( IncrementFrame())


-- Frame 1179
SetFrame( IncrementFrame())


-- Frame 1180
SetFrame( IncrementFrame())


-- Frame 1181
SetFrame( IncrementFrame())


-- Frame 1182
SetFrame( IncrementFrame())


-- Frame 1183
SetFrame( IncrementFrame())


-- Frame 1184
SetFrame( IncrementFrame())


-- Frame 1185
SetFrame( IncrementFrame())


-- Frame 1186
SetFrame( IncrementFrame())


-- Frame 1187
SetFrame( IncrementFrame())


-- Frame 1188
SetFrame( IncrementFrame())


-- Frame 1189
SetFrame( IncrementFrame())


-- Frame 1190
SetFrame( IncrementFrame())


-- Frame 1191
SetFrame( IncrementFrame())


-- Frame 1192
SetFrame( IncrementFrame())


-- Frame 1193
SetFrame( IncrementFrame())


-- Frame 1194
SetFrame( IncrementFrame())


-- Frame 1195
SetFrame( IncrementFrame())


-- Frame 1196
SetFrame( IncrementFrame())


-- Frame 1197
SetFrame( IncrementFrame())


-- Frame 1198
SetFrame( IncrementFrame())


-- Frame 1199
SetFrame( IncrementFrame())


-- Frame 1200
SetFrame( IncrementFrame())


-- Frame 1201
SetFrame( IncrementFrame())


-- Frame 1202
SetFrame( IncrementFrame())


-- Frame 1203
SetFrame( IncrementFrame())


-- Frame 1204
SetFrame( IncrementFrame())


-- Frame 1205
SetFrame( IncrementFrame())


-- Frame 1206
SetFrame( IncrementFrame())


-- Frame 1207
SetFrame( IncrementFrame())


-- Frame 1208
SetFrame( IncrementFrame())


-- Frame 1209
SetFrame( IncrementFrame())


-- Frame 1210
SetFrame( IncrementFrame())


-- Frame 1211
SetFrame( IncrementFrame())


-- Frame 1212
SetFrame( IncrementFrame())


-- Frame 1213
SetFrame( IncrementFrame())


-- Frame 1214
SetFrame( IncrementFrame())


-- Frame 1215
SetFrame( IncrementFrame())


-- Frame 1216
SetFrame( IncrementFrame())


-- Frame 1217
SetFrame( IncrementFrame())


-- Frame 1218
SetFrame( IncrementFrame())


-- Frame 1219
SetFrame( IncrementFrame())


-- Frame 1220
SetFrame( IncrementFrame())


-- Frame 1221
SetFrame( IncrementFrame())


-- Frame 1222
SetFrame( IncrementFrame())


-- Frame 1223
SetFrame( IncrementFrame())


-- Frame 1224
SetFrame( IncrementFrame())


-- Frame 1225
SetFrame( IncrementFrame())


-- Frame 1226
SetFrame( IncrementFrame())


-- Frame 1227
SetFrame( IncrementFrame())


-- Frame 1228
SetFrame( IncrementFrame())


-- Frame 1229
SetFrame( IncrementFrame())


-- Frame 1230
SetFrame( IncrementFrame())


-- Frame 1231
SetFrame( IncrementFrame())


-- Frame 1232
SetFrame( IncrementFrame())


-- Frame 1233
SetFrame( IncrementFrame())


-- Frame 1234
SetFrame( IncrementFrame())


-- Frame 1235
SetFrame( IncrementFrame())


-- Frame 1236
SetFrame( IncrementFrame())


-- Frame 1237
SetFrame( IncrementFrame())


-- Frame 1238
SetFrame( IncrementFrame())


-- Frame 1239
SetFrame( IncrementFrame())


-- Frame 1240
SetFrame( IncrementFrame())


-- Frame 1241
SetFrame( IncrementFrame())


-- Frame 1242
SetFrame( IncrementFrame())


-- Frame 1243
SetFrame( IncrementFrame())


-- Frame 1244
SetFrame( IncrementFrame())


-- Frame 1245
SetFrame( IncrementFrame())


-- Frame 1246
SetFrame( IncrementFrame())


-- Frame 1247
SetFrame( IncrementFrame())


-- Frame 1248
SetFrame( IncrementFrame())


-- Frame 1249
SetFrame( IncrementFrame())


-- Frame 1250
SetFrame( IncrementFrame())


-- Frame 1251
SetFrame( IncrementFrame())


-- Frame 1252
SetFrame( IncrementFrame())


-- Frame 1253
SetFrame( IncrementFrame())


-- Frame 1254
SetFrame( IncrementFrame())


-- Frame 1255
SetFrame( IncrementFrame())


-- Frame 1256
SetFrame( IncrementFrame())


-- Frame 1257
SetFrame( IncrementFrame())


-- Frame 1258
SetFrame( IncrementFrame())


-- Frame 1259
SetFrame( IncrementFrame())


-- Frame 1260
SetFrame( IncrementFrame())


-- Frame 1261
SetFrame( IncrementFrame())


-- Frame 1262
SetFrame( IncrementFrame())


-- Frame 1263
SetFrame( IncrementFrame())


-- Frame 1264
SetFrame( IncrementFrame())


-- Frame 1265
SetFrame( IncrementFrame())


-- Frame 1266
SetFrame( IncrementFrame())


-- Frame 1267
SetFrame( IncrementFrame())


-- Frame 1268
SetFrame( IncrementFrame())


-- Frame 1269
SetFrame( IncrementFrame())


-- Frame 1270
SetFrame( IncrementFrame())


-- Frame 1271
SetFrame( IncrementFrame())


-- Frame 1272
SetFrame( IncrementFrame())


-- Frame 1273
SetFrame( IncrementFrame())


-- Frame 1274
SetFrame( IncrementFrame())


-- Frame 1275
SetFrame( IncrementFrame())


-- Frame 1276
SetFrame( IncrementFrame())


-- Frame 1277
SetFrame( IncrementFrame())


-- Frame 1278
SetFrame( IncrementFrame())


-- Frame 1279
SetFrame( IncrementFrame())


-- Frame 1280
SetFrame( IncrementFrame())


-- Frame 1281
SetFrame( IncrementFrame())


-- Frame 1282
SetFrame( IncrementFrame())


-- Frame 1283
SetFrame( IncrementFrame())


-- Frame 1284
SetFrame( IncrementFrame())


-- Frame 1285
SetFrame( IncrementFrame())


-- Frame 1286
SetFrame( IncrementFrame())


-- Frame 1287
SetFrame( IncrementFrame())


-- Frame 1288
SetFrame( IncrementFrame())


-- Frame 1289
SetFrame( IncrementFrame())


-- Frame 1290
SetFrame( IncrementFrame())


-- Frame 1291
SetFrame( IncrementFrame())


-- Frame 1292
SetFrame( IncrementFrame())


-- Frame 1293
SetFrame( IncrementFrame())


-- Frame 1294
SetFrame( IncrementFrame())


-- Frame 1295
SetFrame( IncrementFrame())


-- Frame 1296
SetFrame( IncrementFrame())


-- Frame 1297
SetFrame( IncrementFrame())


-- Frame 1298
SetFrame( IncrementFrame())


-- Frame 1299
SetFrame( IncrementFrame())


-- Frame 1300
SetFrame( IncrementFrame())


-- Frame 1301
SetFrame( IncrementFrame())


-- Frame 1302
SetFrame( IncrementFrame())


-- Frame 1303
SetFrame( IncrementFrame())


-- Frame 1304
SetFrame( IncrementFrame())


-- Frame 1305
SetFrame( IncrementFrame())


-- Frame 1306
SetFrame( IncrementFrame())


-- Frame 1307
SetFrame( IncrementFrame())


-- Frame 1308
SetFrame( IncrementFrame())


-- Frame 1309
SetFrame( IncrementFrame())


-- Frame 1310
SetFrame( IncrementFrame())


-- Frame 1311
SetFrame( IncrementFrame())


-- Frame 1312
SetFrame( IncrementFrame())


-- Frame 1313
SetFrame( IncrementFrame())


-- Frame 1314
SetFrame( IncrementFrame())


-- Frame 1315
SetFrame( IncrementFrame())


-- Frame 1316
SetFrame( IncrementFrame())


-- Frame 1317
SetFrame( IncrementFrame())


-- Frame 1318
SetFrame( IncrementFrame())


-- Frame 1319
SetFrame( IncrementFrame())


-- Frame 1320
SetFrame( IncrementFrame())


-- Frame 1321
SetFrame( IncrementFrame())


-- Frame 1322
SetFrame( IncrementFrame())


-- Frame 1323
SetFrame( IncrementFrame())


-- Frame 1324
SetFrame( IncrementFrame())


-- Frame 1325
SetFrame( IncrementFrame())


-- Frame 1326
SetFrame( IncrementFrame())


-- Frame 1327
SetFrame( IncrementFrame())


-- Frame 1328
SetFrame( IncrementFrame())


-- Frame 1329
SetFrame( IncrementFrame())


-- Frame 1330
SetFrame( IncrementFrame())


-- Frame 1331
SetFrame( IncrementFrame())


-- Frame 1332
SetFrame( IncrementFrame())


-- Frame 1333
SetFrame( IncrementFrame())


-- Frame 1334
SetFrame( IncrementFrame())


-- Frame 1335
SetFrame( IncrementFrame())


-- Frame 1336
SetFrame( IncrementFrame())


-- Frame 1337
SetFrame( IncrementFrame())


-- Frame 1338
SetFrame( IncrementFrame())


-- Frame 1339
SetFrame( IncrementFrame())


-- Frame 1340
SetFrame( IncrementFrame())


-- Frame 1341
SetFrame( IncrementFrame())


-- Frame 1342
SetFrame( IncrementFrame())


-- Frame 1343
SetFrame( IncrementFrame())


-- Frame 1344
SetFrame( IncrementFrame())


-- Frame 1345
SetFrame( IncrementFrame())


-- Frame 1346
SetFrame( IncrementFrame())


-- Frame 1347
SetFrame( IncrementFrame())


-- Frame 1348
SetFrame( IncrementFrame())


-- Frame 1349
SetFrame( IncrementFrame())


-- Frame 1350
SetFrame( IncrementFrame())


-- Frame 1351
SetFrame( IncrementFrame())


-- Frame 1352
SetFrame( IncrementFrame())


-- Frame 1353
SetFrame( IncrementFrame())


-- Frame 1354
SetFrame( IncrementFrame())


-- Frame 1355
SetFrame( IncrementFrame())


-- Frame 1356
SetFrame( IncrementFrame())


-- Frame 1357
SetFrame( IncrementFrame())


-- Frame 1358
SetFrame( IncrementFrame())


-- Frame 1359
SetFrame( IncrementFrame())


-- Frame 1360
SetFrame( IncrementFrame())


-- Frame 1361
SetFrame( IncrementFrame())


-- Frame 1362
SetFrame( IncrementFrame())


-- Frame 1363
SetFrame( IncrementFrame())


-- Frame 1364
SetFrame( IncrementFrame())


-- Frame 1365
SetFrame( IncrementFrame())


-- Frame 1366
SetFrame( IncrementFrame())


-- Frame 1367
SetFrame( IncrementFrame())


-- Frame 1368
SetFrame( IncrementFrame())


-- Frame 1369
SetFrame( IncrementFrame())


-- Frame 1370
SetFrame( IncrementFrame())


-- Frame 1371
SetFrame( IncrementFrame())


-- Frame 1372
SetFrame( IncrementFrame())


-- Frame 1373
SetFrame( IncrementFrame())


-- Frame 1374
SetFrame( IncrementFrame())


-- Frame 1375
SetFrame( IncrementFrame())


-- Frame 1376
SetFrame( IncrementFrame())


-- Frame 1377
SetFrame( IncrementFrame())

LinkCamThirdPerson( ObjectObj_Camera, "Act_BehindA", "Obj_ArthBackCam")
RotateObjectTo( ObjectObj_Archimonde, "Act_TurnToKT", 0, -150, 0)
PlayAudio( ObjectObj_Archimonde, "Act_DoneWell", "Resources/WarCraftSounds/CS_03_Archimonde.mp3", 1)
PlayAudio( ObjectObj_Archimonde, "Act_Doom", "Resources/WarCraftSounds/CS_Music_Doom.mp3", 0.2)

-- Frame 1378
SetFrame( IncrementFrame())


-- Frame 1379
SetFrame( IncrementFrame())


-- Frame 1380
SetFrame( IncrementFrame())


-- Frame 1381
SetFrame( IncrementFrame())


-- Frame 1382
SetFrame( IncrementFrame())


-- Frame 1383
SetFrame( IncrementFrame())


-- Frame 1384
SetFrame( IncrementFrame())


-- Frame 1385
SetFrame( IncrementFrame())


-- Frame 1386
SetFrame( IncrementFrame())


-- Frame 1387
SetFrame( IncrementFrame())


-- Frame 1388
SetFrame( IncrementFrame())


-- Frame 1389
SetFrame( IncrementFrame())


-- Frame 1390
SetFrame( IncrementFrame())


-- Frame 1391
SetFrame( IncrementFrame())


-- Frame 1392
SetFrame( IncrementFrame())


-- Frame 1393
SetFrame( IncrementFrame())


-- Frame 1394
SetFrame( IncrementFrame())


-- Frame 1395
SetFrame( IncrementFrame())


-- Frame 1396
SetFrame( IncrementFrame())


-- Frame 1397
SetFrame( IncrementFrame())


-- Frame 1398
SetFrame( IncrementFrame())


-- Frame 1399
SetFrame( IncrementFrame())


-- Frame 1400
SetFrame( IncrementFrame())


-- Frame 1401
SetFrame( IncrementFrame())


-- Frame 1402
SetFrame( IncrementFrame())


-- Frame 1403
SetFrame( IncrementFrame())


-- Frame 1404
SetFrame( IncrementFrame())


-- Frame 1405
SetFrame( IncrementFrame())


-- Frame 1406
SetFrame( IncrementFrame())


-- Frame 1407
SetFrame( IncrementFrame())


-- Frame 1408
SetFrame( IncrementFrame())


-- Frame 1409
SetFrame( IncrementFrame())


-- Frame 1410
SetFrame( IncrementFrame())


-- Frame 1411
SetFrame( IncrementFrame())


-- Frame 1412
SetFrame( IncrementFrame())


-- Frame 1413
SetFrame( IncrementFrame())


-- Frame 1414
SetFrame( IncrementFrame())


-- Frame 1415
SetFrame( IncrementFrame())


-- Frame 1416
SetFrame( IncrementFrame())


-- Frame 1417
SetFrame( IncrementFrame())


-- Frame 1418
SetFrame( IncrementFrame())


-- Frame 1419
SetFrame( IncrementFrame())


-- Frame 1420
SetFrame( IncrementFrame())


-- Frame 1421
SetFrame( IncrementFrame())


-- Frame 1422
SetFrame( IncrementFrame())


-- Frame 1423
SetFrame( IncrementFrame())


-- Frame 1424
SetFrame( IncrementFrame())


-- Frame 1425
SetFrame( IncrementFrame())


-- Frame 1426
SetFrame( IncrementFrame())


-- Frame 1427
SetFrame( IncrementFrame())


-- Frame 1428
SetFrame( IncrementFrame())


-- Frame 1429
SetFrame( IncrementFrame())


-- Frame 1430
SetFrame( IncrementFrame())


-- Frame 1431
SetFrame( IncrementFrame())


-- Frame 1432
SetFrame( IncrementFrame())


-- Frame 1433
SetFrame( IncrementFrame())


-- Frame 1434
SetFrame( IncrementFrame())


-- Frame 1435
SetFrame( IncrementFrame())


-- Frame 1436
SetFrame( IncrementFrame())


-- Frame 1437
SetFrame( IncrementFrame())


-- Frame 1438
SetFrame( IncrementFrame())


-- Frame 1439
SetFrame( IncrementFrame())


-- Frame 1440
SetFrame( IncrementFrame())


-- Frame 1441
SetFrame( IncrementFrame())


-- Frame 1442
SetFrame( IncrementFrame())


-- Frame 1443
SetFrame( IncrementFrame())


-- Frame 1444
SetFrame( IncrementFrame())


-- Frame 1445
SetFrame( IncrementFrame())


-- Frame 1446
SetFrame( IncrementFrame())


-- Frame 1447
SetFrame( IncrementFrame())


-- Frame 1448
SetFrame( IncrementFrame())


-- Frame 1449
SetFrame( IncrementFrame())


-- Frame 1450
SetFrame( IncrementFrame())


-- Frame 1451
SetFrame( IncrementFrame())


-- Frame 1452
SetFrame( IncrementFrame())


-- Frame 1453
SetFrame( IncrementFrame())


-- Frame 1454
SetFrame( IncrementFrame())


-- Frame 1455
SetFrame( IncrementFrame())


-- Frame 1456
SetFrame( IncrementFrame())


-- Frame 1457
SetFrame( IncrementFrame())


-- Frame 1458
SetFrame( IncrementFrame())


-- Frame 1459
SetFrame( IncrementFrame())


-- Frame 1460
SetFrame( IncrementFrame())


-- Frame 1461
SetFrame( IncrementFrame())


-- Frame 1462
SetFrame( IncrementFrame())


-- Frame 1463
SetFrame( IncrementFrame())


-- Frame 1464
SetFrame( IncrementFrame())


-- Frame 1465
SetFrame( IncrementFrame())


-- Frame 1466
SetFrame( IncrementFrame())


-- Frame 1467
SetFrame( IncrementFrame())


-- Frame 1468
SetFrame( IncrementFrame())


-- Frame 1469
SetFrame( IncrementFrame())


-- Frame 1470
SetFrame( IncrementFrame())


-- Frame 1471
SetFrame( IncrementFrame())


-- Frame 1472
SetFrame( IncrementFrame())


-- Frame 1473
SetFrame( IncrementFrame())


-- Frame 1474
SetFrame( IncrementFrame())


-- Frame 1475
SetFrame( IncrementFrame())


-- Frame 1476
SetFrame( IncrementFrame())


-- Frame 1477
SetFrame( IncrementFrame())


-- Frame 1478
SetFrame( IncrementFrame())


-- Frame 1479
SetFrame( IncrementFrame())


-- Frame 1480
SetFrame( IncrementFrame())


-- Frame 1481
SetFrame( IncrementFrame())


-- Frame 1482
SetFrame( IncrementFrame())


-- Frame 1483
SetFrame( IncrementFrame())


-- Frame 1484
SetFrame( IncrementFrame())


-- Frame 1485
SetFrame( IncrementFrame())


-- Frame 1486
SetFrame( IncrementFrame())


-- Frame 1487
SetFrame( IncrementFrame())


-- Frame 1488
SetFrame( IncrementFrame())


-- Frame 1489
SetFrame( IncrementFrame())


-- Frame 1490
SetFrame( IncrementFrame())


-- Frame 1491
SetFrame( IncrementFrame())


-- Frame 1492
SetFrame( IncrementFrame())


-- Frame 1493
SetFrame( IncrementFrame())


-- Frame 1494
SetFrame( IncrementFrame())


-- Frame 1495
SetFrame( IncrementFrame())


-- Frame 1496
SetFrame( IncrementFrame())


-- Frame 1497
SetFrame( IncrementFrame())


-- Frame 1498
SetFrame( IncrementFrame())


-- Frame 1499
SetFrame( IncrementFrame())


-- Frame 1500
SetFrame( IncrementFrame())


-- Frame 1501
SetFrame( IncrementFrame())


-- Frame 1502
SetFrame( IncrementFrame())


-- Frame 1503
SetFrame( IncrementFrame())


-- Frame 1504
SetFrame( IncrementFrame())


-- Frame 1505
SetFrame( IncrementFrame())


-- Frame 1506
SetFrame( IncrementFrame())


-- Frame 1507
SetFrame( IncrementFrame())


-- Frame 1508
SetFrame( IncrementFrame())


-- Frame 1509
SetFrame( IncrementFrame())


-- Frame 1510
SetFrame( IncrementFrame())


-- Frame 1511
SetFrame( IncrementFrame())


-- Frame 1512
SetFrame( IncrementFrame())


-- Frame 1513
SetFrame( IncrementFrame())


-- Frame 1514
SetFrame( IncrementFrame())


-- Frame 1515
SetFrame( IncrementFrame())


-- Frame 1516
SetFrame( IncrementFrame())


-- Frame 1517
SetFrame( IncrementFrame())


-- Frame 1518
SetFrame( IncrementFrame())


-- Frame 1519
SetFrame( IncrementFrame())


-- Frame 1520
SetFrame( IncrementFrame())


-- Frame 1521
SetFrame( IncrementFrame())


-- Frame 1522
SetFrame( IncrementFrame())


-- Frame 1523
SetFrame( IncrementFrame())


-- Frame 1524
SetFrame( IncrementFrame())


-- Frame 1525
SetFrame( IncrementFrame())


-- Frame 1526
SetFrame( IncrementFrame())


-- Frame 1527
SetFrame( IncrementFrame())


-- Frame 1528
SetFrame( IncrementFrame())


-- Frame 1529
SetFrame( IncrementFrame())


-- Frame 1530
SetFrame( IncrementFrame())


-- Frame 1531
SetFrame( IncrementFrame())


-- Frame 1532
SetFrame( IncrementFrame())


-- Frame 1533
SetFrame( IncrementFrame())


-- Frame 1534
SetFrame( IncrementFrame())


-- Frame 1535
SetFrame( IncrementFrame())


-- Frame 1536
SetFrame( IncrementFrame())


-- Frame 1537
SetFrame( IncrementFrame())


-- Frame 1538
SetFrame( IncrementFrame())


-- Frame 1539
SetFrame( IncrementFrame())


-- Frame 1540
SetFrame( IncrementFrame())


-- Frame 1541
SetFrame( IncrementFrame())


-- Frame 1542
SetFrame( IncrementFrame())


-- Frame 1543
SetFrame( IncrementFrame())


-- Frame 1544
SetFrame( IncrementFrame())


-- Frame 1545
SetFrame( IncrementFrame())


-- Frame 1546
SetFrame( IncrementFrame())


-- Frame 1547
SetFrame( IncrementFrame())


-- Frame 1548
SetFrame( IncrementFrame())


-- Frame 1549
SetFrame( IncrementFrame())


-- Frame 1550
SetFrame( IncrementFrame())


-- Frame 1551
SetFrame( IncrementFrame())


-- Frame 1552
SetFrame( IncrementFrame())


-- Frame 1553
SetFrame( IncrementFrame())


-- Frame 1554
SetFrame( IncrementFrame())


-- Frame 1555
SetFrame( IncrementFrame())


-- Frame 1556
SetFrame( IncrementFrame())


-- Frame 1557
SetFrame( IncrementFrame())


-- Frame 1558
SetFrame( IncrementFrame())


-- Frame 1559
SetFrame( IncrementFrame())


-- Frame 1560
SetFrame( IncrementFrame())


-- Frame 1561
SetFrame( IncrementFrame())


-- Frame 1562
SetFrame( IncrementFrame())


-- Frame 1563
SetFrame( IncrementFrame())


-- Frame 1564
SetFrame( IncrementFrame())


-- Frame 1565
SetFrame( IncrementFrame())


-- Frame 1566
SetFrame( IncrementFrame())


-- Frame 1567
SetFrame( IncrementFrame())


-- Frame 1568
SetFrame( IncrementFrame())


-- Frame 1569
SetFrame( IncrementFrame())


-- Frame 1570
SetFrame( IncrementFrame())


-- Frame 1571
SetFrame( IncrementFrame())


-- Frame 1572
SetFrame( IncrementFrame())


-- Frame 1573
SetFrame( IncrementFrame())


-- Frame 1574
SetFrame( IncrementFrame())


-- Frame 1575
SetFrame( IncrementFrame())


-- Frame 1576
SetFrame( IncrementFrame())


-- Frame 1577
SetFrame( IncrementFrame())


-- Frame 1578
SetFrame( IncrementFrame())


-- Frame 1579
SetFrame( IncrementFrame())


-- Frame 1580
SetFrame( IncrementFrame())


-- Frame 1581
SetFrame( IncrementFrame())


-- Frame 1582
SetFrame( IncrementFrame())


-- Frame 1583
SetFrame( IncrementFrame())


-- Frame 1584
SetFrame( IncrementFrame())


-- Frame 1585
SetFrame( IncrementFrame())


-- Frame 1586
SetFrame( IncrementFrame())


-- Frame 1587
SetFrame( IncrementFrame())


-- Frame 1588
SetFrame( IncrementFrame())


-- Frame 1589
SetFrame( IncrementFrame())


-- Frame 1590
SetFrame( IncrementFrame())


-- Frame 1591
SetFrame( IncrementFrame())


-- Frame 1592
SetFrame( IncrementFrame())


-- Frame 1593
SetFrame( IncrementFrame())


-- Frame 1594
SetFrame( IncrementFrame())


-- Frame 1595
SetFrame( IncrementFrame())


-- Frame 1596
SetFrame( IncrementFrame())


-- Frame 1597
SetFrame( IncrementFrame())


-- Frame 1598
SetFrame( IncrementFrame())


-- Frame 1599
SetFrame( IncrementFrame())


-- Frame 1600
SetFrame( IncrementFrame())


-- Frame 1601
SetFrame( IncrementFrame())


-- Frame 1602
SetFrame( IncrementFrame())


-- Frame 1603
SetFrame( IncrementFrame())


-- Frame 1604
SetFrame( IncrementFrame())


-- Frame 1605
SetFrame( IncrementFrame())


-- Frame 1606
SetFrame( IncrementFrame())


-- Frame 1607
SetFrame( IncrementFrame())


-- Frame 1608
SetFrame( IncrementFrame())


-- Frame 1609
SetFrame( IncrementFrame())


-- Frame 1610
SetFrame( IncrementFrame())


-- Frame 1611
SetFrame( IncrementFrame())


-- Frame 1612
SetFrame( IncrementFrame())


-- Frame 1613
SetFrame( IncrementFrame())


-- Frame 1614
SetFrame( IncrementFrame())


-- Frame 1615
SetFrame( IncrementFrame())


-- Frame 1616
SetFrame( IncrementFrame())


-- Frame 1617
SetFrame( IncrementFrame())


-- Frame 1618
SetFrame( IncrementFrame())


-- Frame 1619
SetFrame( IncrementFrame())


-- Frame 1620
SetFrame( IncrementFrame())


-- Frame 1621
SetFrame( IncrementFrame())


-- Frame 1622
SetFrame( IncrementFrame())


-- Frame 1623
SetFrame( IncrementFrame())


-- Frame 1624
SetFrame( IncrementFrame())


-- Frame 1625
SetFrame( IncrementFrame())


-- Frame 1626
SetFrame( IncrementFrame())


-- Frame 1627
SetFrame( IncrementFrame())


-- Frame 1628
SetFrame( IncrementFrame())


-- Frame 1629
SetFrame( IncrementFrame())


-- Frame 1630
SetFrame( IncrementFrame())


-- Frame 1631
SetFrame( IncrementFrame())


-- Frame 1632
SetFrame( IncrementFrame())


-- Frame 1633
SetFrame( IncrementFrame())


-- Frame 1634
SetFrame( IncrementFrame())


-- Frame 1635
SetFrame( IncrementFrame())


-- Frame 1636
SetFrame( IncrementFrame())


-- Frame 1637
SetFrame( IncrementFrame())


-- Frame 1638
SetFrame( IncrementFrame())


-- Frame 1639
SetFrame( IncrementFrame())


-- Frame 1640
SetFrame( IncrementFrame())


-- Frame 1641
SetFrame( IncrementFrame())


-- Frame 1642
SetFrame( IncrementFrame())


-- Frame 1643
SetFrame( IncrementFrame())


-- Frame 1644
SetFrame( IncrementFrame())


-- Frame 1645
SetFrame( IncrementFrame())


-- Frame 1646
SetFrame( IncrementFrame())


-- Frame 1647
SetFrame( IncrementFrame())


-- Frame 1648
SetFrame( IncrementFrame())


-- Frame 1649
SetFrame( IncrementFrame())


-- Frame 1650
SetFrame( IncrementFrame())


-- Frame 1651
SetFrame( IncrementFrame())


-- Frame 1652
SetFrame( IncrementFrame())


-- Frame 1653
SetFrame( IncrementFrame())


-- Frame 1654
SetFrame( IncrementFrame())


-- Frame 1655
SetFrame( IncrementFrame())


-- Frame 1656
SetFrame( IncrementFrame())


-- Frame 1657
SetFrame( IncrementFrame())


-- Frame 1658
SetFrame( IncrementFrame())


-- Frame 1659
SetFrame( IncrementFrame())


-- Frame 1660
SetFrame( IncrementFrame())


-- Frame 1661
SetFrame( IncrementFrame())


-- Frame 1662
SetFrame( IncrementFrame())


-- Frame 1663
SetFrame( IncrementFrame())


-- Frame 1664
SetFrame( IncrementFrame())


-- Frame 1665
SetFrame( IncrementFrame())


-- Frame 1666
SetFrame( IncrementFrame())


-- Frame 1667
SetFrame( IncrementFrame())


-- Frame 1668
SetFrame( IncrementFrame())


-- Frame 1669
SetFrame( IncrementFrame())


-- Frame 1670
SetFrame( IncrementFrame())


-- Frame 1671
SetFrame( IncrementFrame())


-- Frame 1672
SetFrame( IncrementFrame())


-- Frame 1673
SetFrame( IncrementFrame())


-- Frame 1674
SetFrame( IncrementFrame())


-- Frame 1675
SetFrame( IncrementFrame())


-- Frame 1676
SetFrame( IncrementFrame())


-- Frame 1677
SetFrame( IncrementFrame())


-- Frame 1678
SetFrame( IncrementFrame())


-- Frame 1679
SetFrame( IncrementFrame())


-- Frame 1680
SetFrame( IncrementFrame())


-- Frame 1681
SetFrame( IncrementFrame())


-- Frame 1682
SetFrame( IncrementFrame())


-- Frame 1683
SetFrame( IncrementFrame())


-- Frame 1684
SetFrame( IncrementFrame())


-- Frame 1685
SetFrame( IncrementFrame())


-- Frame 1686
SetFrame( IncrementFrame())


-- Frame 1687
SetFrame( IncrementFrame())


-- Frame 1688
SetFrame( IncrementFrame())


-- Frame 1689
SetFrame( IncrementFrame())


-- Frame 1690
SetFrame( IncrementFrame())


-- Frame 1691
SetFrame( IncrementFrame())


-- Frame 1692
SetFrame( IncrementFrame())


-- Frame 1693
SetFrame( IncrementFrame())


-- Frame 1694
SetFrame( IncrementFrame())


-- Frame 1695
SetFrame( IncrementFrame())


-- Frame 1696
SetFrame( IncrementFrame())


-- Frame 1697
SetFrame( IncrementFrame())


-- Frame 1698
SetFrame( IncrementFrame())


-- Frame 1699
SetFrame( IncrementFrame())


-- Frame 1700
SetFrame( IncrementFrame())


-- Frame 1701
SetFrame( IncrementFrame())


-- Frame 1702
SetFrame( IncrementFrame())

SetObjectVisibility( ObjectObj_Tichondrius, "Act_AppearT", true)

-- Frame 1703
SetFrame( IncrementFrame())


-- Frame 1704
SetFrame( IncrementFrame())


-- Frame 1705
SetFrame( IncrementFrame())


-- Frame 1706
SetFrame( IncrementFrame())


-- Frame 1707
SetFrame( IncrementFrame())


-- Frame 1708
SetFrame( IncrementFrame())


-- Frame 1709
SetFrame( IncrementFrame())


-- Frame 1710
SetFrame( IncrementFrame())


-- Frame 1711
SetFrame( IncrementFrame())


-- Frame 1712
SetFrame( IncrementFrame())


-- Frame 1713
SetFrame( IncrementFrame())


-- Frame 1714
SetFrame( IncrementFrame())


-- Frame 1715
SetFrame( IncrementFrame())


-- Frame 1716
SetFrame( IncrementFrame())


-- Frame 1717
SetFrame( IncrementFrame())


-- Frame 1718
SetFrame( IncrementFrame())


-- Frame 1719
SetFrame( IncrementFrame())


-- Frame 1720
SetFrame( IncrementFrame())


-- Frame 1721
SetFrame( IncrementFrame())


-- Frame 1722
SetFrame( IncrementFrame())


-- Frame 1723
SetFrame( IncrementFrame())


-- Frame 1724
SetFrame( IncrementFrame())


-- Frame 1725
SetFrame( IncrementFrame())


-- Frame 1726
SetFrame( IncrementFrame())


-- Frame 1727
SetFrame( IncrementFrame())


-- Frame 1728
SetFrame( IncrementFrame())


-- Frame 1729
SetFrame( IncrementFrame())


-- Frame 1730
SetFrame( IncrementFrame())


-- Frame 1731
SetFrame( IncrementFrame())


-- Frame 1732
SetFrame( IncrementFrame())


-- Frame 1733
SetFrame( IncrementFrame())


-- Frame 1734
SetFrame( IncrementFrame())


-- Frame 1735
SetFrame( IncrementFrame())


-- Frame 1736
SetFrame( IncrementFrame())


-- Frame 1737
SetFrame( IncrementFrame())


-- Frame 1738
SetFrame( IncrementFrame())


-- Frame 1739
SetFrame( IncrementFrame())


-- Frame 1740
SetFrame( IncrementFrame())


-- Frame 1741
SetFrame( IncrementFrame())


-- Frame 1742
SetFrame( IncrementFrame())


-- Frame 1743
SetFrame( IncrementFrame())


-- Frame 1744
SetFrame( IncrementFrame())


-- Frame 1745
SetFrame( IncrementFrame())


-- Frame 1746
SetFrame( IncrementFrame())


-- Frame 1747
SetFrame( IncrementFrame())


-- Frame 1748
SetFrame( IncrementFrame())


-- Frame 1749
SetFrame( IncrementFrame())


-- Frame 1750
SetFrame( IncrementFrame())


-- Frame 1751
SetFrame( IncrementFrame())


-- Frame 1752
SetFrame( IncrementFrame())


-- Frame 1753
SetFrame( IncrementFrame())


-- Frame 1754
SetFrame( IncrementFrame())


-- Frame 1755
SetFrame( IncrementFrame())


-- Frame 1756
SetFrame( IncrementFrame())


-- Frame 1757
SetFrame( IncrementFrame())


-- Frame 1758
SetFrame( IncrementFrame())


-- Frame 1759
SetFrame( IncrementFrame())


-- Frame 1760
SetFrame( IncrementFrame())


-- Frame 1761
SetFrame( IncrementFrame())


-- Frame 1762
SetFrame( IncrementFrame())


-- Frame 1763
SetFrame( IncrementFrame())


-- Frame 1764
SetFrame( IncrementFrame())


-- Frame 1765
SetFrame( IncrementFrame())


-- Frame 1766
SetFrame( IncrementFrame())


-- Frame 1767
SetFrame( IncrementFrame())


-- Frame 1768
SetFrame( IncrementFrame())


-- Frame 1769
SetFrame( IncrementFrame())


-- Frame 1770
SetFrame( IncrementFrame())


-- Frame 1771
SetFrame( IncrementFrame())


-- Frame 1772
SetFrame( IncrementFrame())


-- Frame 1773
SetFrame( IncrementFrame())


-- Frame 1774
SetFrame( IncrementFrame())


-- Frame 1775
SetFrame( IncrementFrame())


-- Frame 1776
SetFrame( IncrementFrame())


-- Frame 1777
SetFrame( IncrementFrame())


-- Frame 1778
SetFrame( IncrementFrame())


-- Frame 1779
SetFrame( IncrementFrame())


-- Frame 1780
SetFrame( IncrementFrame())


-- Frame 1781
SetFrame( IncrementFrame())


-- Frame 1782
SetFrame( IncrementFrame())


-- Frame 1783
SetFrame( IncrementFrame())


-- Frame 1784
SetFrame( IncrementFrame())


-- Frame 1785
SetFrame( IncrementFrame())


-- Frame 1786
SetFrame( IncrementFrame())


-- Frame 1787
SetFrame( IncrementFrame())


-- Frame 1788
SetFrame( IncrementFrame())


-- Frame 1789
SetFrame( IncrementFrame())


-- Frame 1790
SetFrame( IncrementFrame())


-- Frame 1791
SetFrame( IncrementFrame())


-- Frame 1792
SetFrame( IncrementFrame())


-- Frame 1793
SetFrame( IncrementFrame())


-- Frame 1794
SetFrame( IncrementFrame())


-- Frame 1795
SetFrame( IncrementFrame())


-- Frame 1796
SetFrame( IncrementFrame())


-- Frame 1797
SetFrame( IncrementFrame())


-- Frame 1798
SetFrame( IncrementFrame())


-- Frame 1799
SetFrame( IncrementFrame())


-- Frame 1800
SetFrame( IncrementFrame())


-- Frame 1801
SetFrame( IncrementFrame())


-- Frame 1802
SetFrame( IncrementFrame())


-- Frame 1803
SetFrame( IncrementFrame())


-- Frame 1804
SetFrame( IncrementFrame())


-- Frame 1805
SetFrame( IncrementFrame())


-- Frame 1806
SetFrame( IncrementFrame())


-- Frame 1807
SetFrame( IncrementFrame())


-- Frame 1808
SetFrame( IncrementFrame())


-- Frame 1809
SetFrame( IncrementFrame())


-- Frame 1810
SetFrame( IncrementFrame())


-- Frame 1811
SetFrame( IncrementFrame())


-- Frame 1812
SetFrame( IncrementFrame())


-- Frame 1813
SetFrame( IncrementFrame())


-- Frame 1814
SetFrame( IncrementFrame())


-- Frame 1815
SetFrame( IncrementFrame())


-- Frame 1816
SetFrame( IncrementFrame())


-- Frame 1817
SetFrame( IncrementFrame())


-- Frame 1818
SetFrame( IncrementFrame())


-- Frame 1819
SetFrame( IncrementFrame())


-- Frame 1820
SetFrame( IncrementFrame())


-- Frame 1821
SetFrame( IncrementFrame())


-- Frame 1822
SetFrame( IncrementFrame())


-- Frame 1823
SetFrame( IncrementFrame())


-- Frame 1824
SetFrame( IncrementFrame())


-- Frame 1825
SetFrame( IncrementFrame())


-- Frame 1826
SetFrame( IncrementFrame())


-- Frame 1827
SetFrame( IncrementFrame())


-- Frame 1828
SetFrame( IncrementFrame())


-- Frame 1829
SetFrame( IncrementFrame())


-- Frame 1830
SetFrame( IncrementFrame())


-- Frame 1831
SetFrame( IncrementFrame())


-- Frame 1832
SetFrame( IncrementFrame())


-- Frame 1833
SetFrame( IncrementFrame())


-- Frame 1834
SetFrame( IncrementFrame())


-- Frame 1835
SetFrame( IncrementFrame())


-- Frame 1836
SetFrame( IncrementFrame())


-- Frame 1837
SetFrame( IncrementFrame())


-- Frame 1838
SetFrame( IncrementFrame())


-- Frame 1839
SetFrame( IncrementFrame())


-- Frame 1840
SetFrame( IncrementFrame())


-- Frame 1841
SetFrame( IncrementFrame())


-- Frame 1842
SetFrame( IncrementFrame())


-- Frame 1843
SetFrame( IncrementFrame())


-- Frame 1844
SetFrame( IncrementFrame())


-- Frame 1845
SetFrame( IncrementFrame())


-- Frame 1846
SetFrame( IncrementFrame())


-- Frame 1847
SetFrame( IncrementFrame())


-- Frame 1848
SetFrame( IncrementFrame())


-- Frame 1849
SetFrame( IncrementFrame())


-- Frame 1850
SetFrame( IncrementFrame())


-- Frame 1851
SetFrame( IncrementFrame())


-- Frame 1852
SetFrame( IncrementFrame())


-- Frame 1853
SetFrame( IncrementFrame())


-- Frame 1854
SetFrame( IncrementFrame())


-- Frame 1855
SetFrame( IncrementFrame())


-- Frame 1856
SetFrame( IncrementFrame())


-- Frame 1857
SetFrame( IncrementFrame())


-- Frame 1858
SetFrame( IncrementFrame())


-- Frame 1859
SetFrame( IncrementFrame())


-- Frame 1860
SetFrame( IncrementFrame())


-- Frame 1861
SetFrame( IncrementFrame())


-- Frame 1862
SetFrame( IncrementFrame())


-- Frame 1863
SetFrame( IncrementFrame())


-- Frame 1864
SetFrame( IncrementFrame())


-- Frame 1865
SetFrame( IncrementFrame())


-- Frame 1866
SetFrame( IncrementFrame())


-- Frame 1867
SetFrame( IncrementFrame())


-- Frame 1868
SetFrame( IncrementFrame())


-- Frame 1869
SetFrame( IncrementFrame())


-- Frame 1870
SetFrame( IncrementFrame())


-- Frame 1871
SetFrame( IncrementFrame())


-- Frame 1872
SetFrame( IncrementFrame())


-- Frame 1873
SetFrame( IncrementFrame())


-- Frame 1874
SetFrame( IncrementFrame())


-- Frame 1875
SetFrame( IncrementFrame())


-- Frame 1876
SetFrame( IncrementFrame())


-- Frame 1877
SetFrame( IncrementFrame())


-- Frame 1878
SetFrame( IncrementFrame())


-- Frame 1879
SetFrame( IncrementFrame())


-- Frame 1880
SetFrame( IncrementFrame())

LinkCamThirdPerson( ObjectObj_Camera, "Act_FrontT", "Obj_TichFrontCam")
RotateObjectTo( ObjectObj_Archimonde, "Act_TurnToTich", 0, 50, 0)
PlayAudio( ObjectObj_Tichondrius, "Act_Preperations", "Resources/WarCraftSounds/CS_04_Tichondrius.mp3", 1)

-- Frame 1881
SetFrame( IncrementFrame())


-- Frame 1882
SetFrame( IncrementFrame())


-- Frame 1883
SetFrame( IncrementFrame())


-- Frame 1884
SetFrame( IncrementFrame())


-- Frame 1885
SetFrame( IncrementFrame())


-- Frame 1886
SetFrame( IncrementFrame())


-- Frame 1887
SetFrame( IncrementFrame())


-- Frame 1888
SetFrame( IncrementFrame())


-- Frame 1889
SetFrame( IncrementFrame())


-- Frame 1890
SetFrame( IncrementFrame())


-- Frame 1891
SetFrame( IncrementFrame())


-- Frame 1892
SetFrame( IncrementFrame())


-- Frame 1893
SetFrame( IncrementFrame())


-- Frame 1894
SetFrame( IncrementFrame())


-- Frame 1895
SetFrame( IncrementFrame())


-- Frame 1896
SetFrame( IncrementFrame())


-- Frame 1897
SetFrame( IncrementFrame())


-- Frame 1898
SetFrame( IncrementFrame())


-- Frame 1899
SetFrame( IncrementFrame())


-- Frame 1900
SetFrame( IncrementFrame())


-- Frame 1901
SetFrame( IncrementFrame())


-- Frame 1902
SetFrame( IncrementFrame())


-- Frame 1903
SetFrame( IncrementFrame())


-- Frame 1904
SetFrame( IncrementFrame())


-- Frame 1905
SetFrame( IncrementFrame())


-- Frame 1906
SetFrame( IncrementFrame())


-- Frame 1907
SetFrame( IncrementFrame())


-- Frame 1908
SetFrame( IncrementFrame())


-- Frame 1909
SetFrame( IncrementFrame())


-- Frame 1910
SetFrame( IncrementFrame())


-- Frame 1911
SetFrame( IncrementFrame())


-- Frame 1912
SetFrame( IncrementFrame())


-- Frame 1913
SetFrame( IncrementFrame())


-- Frame 1914
SetFrame( IncrementFrame())


-- Frame 1915
SetFrame( IncrementFrame())


-- Frame 1916
SetFrame( IncrementFrame())


-- Frame 1917
SetFrame( IncrementFrame())


-- Frame 1918
SetFrame( IncrementFrame())


-- Frame 1919
SetFrame( IncrementFrame())


-- Frame 1920
SetFrame( IncrementFrame())


-- Frame 1921
SetFrame( IncrementFrame())


-- Frame 1922
SetFrame( IncrementFrame())


-- Frame 1923
SetFrame( IncrementFrame())


-- Frame 1924
SetFrame( IncrementFrame())


-- Frame 1925
SetFrame( IncrementFrame())


-- Frame 1926
SetFrame( IncrementFrame())


-- Frame 1927
SetFrame( IncrementFrame())


-- Frame 1928
SetFrame( IncrementFrame())


-- Frame 1929
SetFrame( IncrementFrame())


-- Frame 1930
SetFrame( IncrementFrame())


-- Frame 1931
SetFrame( IncrementFrame())


-- Frame 1932
SetFrame( IncrementFrame())


-- Frame 1933
SetFrame( IncrementFrame())


-- Frame 1934
SetFrame( IncrementFrame())


-- Frame 1935
SetFrame( IncrementFrame())


-- Frame 1936
SetFrame( IncrementFrame())


-- Frame 1937
SetFrame( IncrementFrame())


-- Frame 1938
SetFrame( IncrementFrame())


-- Frame 1939
SetFrame( IncrementFrame())


-- Frame 1940
SetFrame( IncrementFrame())


-- Frame 1941
SetFrame( IncrementFrame())


-- Frame 1942
SetFrame( IncrementFrame())


-- Frame 1943
SetFrame( IncrementFrame())


-- Frame 1944
SetFrame( IncrementFrame())


-- Frame 1945
SetFrame( IncrementFrame())


-- Frame 1946
SetFrame( IncrementFrame())


-- Frame 1947
SetFrame( IncrementFrame())


-- Frame 1948
SetFrame( IncrementFrame())


-- Frame 1949
SetFrame( IncrementFrame())


-- Frame 1950
SetFrame( IncrementFrame())


-- Frame 1951
SetFrame( IncrementFrame())


-- Frame 1952
SetFrame( IncrementFrame())


-- Frame 1953
SetFrame( IncrementFrame())


-- Frame 1954
SetFrame( IncrementFrame())


-- Frame 1955
SetFrame( IncrementFrame())


-- Frame 1956
SetFrame( IncrementFrame())


-- Frame 1957
SetFrame( IncrementFrame())


-- Frame 1958
SetFrame( IncrementFrame())


-- Frame 1959
SetFrame( IncrementFrame())


-- Frame 1960
SetFrame( IncrementFrame())


-- Frame 1961
SetFrame( IncrementFrame())


-- Frame 1962
SetFrame( IncrementFrame())


-- Frame 1963
SetFrame( IncrementFrame())


-- Frame 1964
SetFrame( IncrementFrame())


-- Frame 1965
SetFrame( IncrementFrame())


-- Frame 1966
SetFrame( IncrementFrame())


-- Frame 1967
SetFrame( IncrementFrame())


-- Frame 1968
SetFrame( IncrementFrame())


-- Frame 1969
SetFrame( IncrementFrame())


-- Frame 1970
SetFrame( IncrementFrame())


-- Frame 1971
SetFrame( IncrementFrame())


-- Frame 1972
SetFrame( IncrementFrame())


-- Frame 1973
SetFrame( IncrementFrame())


-- Frame 1974
SetFrame( IncrementFrame())


-- Frame 1975
SetFrame( IncrementFrame())


-- Frame 1976
SetFrame( IncrementFrame())


-- Frame 1977
SetFrame( IncrementFrame())


-- Frame 1978
SetFrame( IncrementFrame())


-- Frame 1979
SetFrame( IncrementFrame())


-- Frame 1980
SetFrame( IncrementFrame())


-- Frame 1981
SetFrame( IncrementFrame())


-- Frame 1982
SetFrame( IncrementFrame())


-- Frame 1983
SetFrame( IncrementFrame())


-- Frame 1984
SetFrame( IncrementFrame())


-- Frame 1985
SetFrame( IncrementFrame())


-- Frame 1986
SetFrame( IncrementFrame())


-- Frame 1987
SetFrame( IncrementFrame())


-- Frame 1988
SetFrame( IncrementFrame())


-- Frame 1989
SetFrame( IncrementFrame())


-- Frame 1990
SetFrame( IncrementFrame())


-- Frame 1991
SetFrame( IncrementFrame())


-- Frame 1992
SetFrame( IncrementFrame())


-- Frame 1993
SetFrame( IncrementFrame())


-- Frame 1994
SetFrame( IncrementFrame())


-- Frame 1995
SetFrame( IncrementFrame())


-- Frame 1996
SetFrame( IncrementFrame())


-- Frame 1997
SetFrame( IncrementFrame())


-- Frame 1998
SetFrame( IncrementFrame())


-- Frame 1999
SetFrame( IncrementFrame())


-- Frame 2000
SetFrame( IncrementFrame())


-- Frame 2001
SetFrame( IncrementFrame())


-- Frame 2002
SetFrame( IncrementFrame())


-- Frame 2003
SetFrame( IncrementFrame())


-- Frame 2004
SetFrame( IncrementFrame())


-- Frame 2005
SetFrame( IncrementFrame())


-- Frame 2006
SetFrame( IncrementFrame())


-- Frame 2007
SetFrame( IncrementFrame())


-- Frame 2008
SetFrame( IncrementFrame())


-- Frame 2009
SetFrame( IncrementFrame())


-- Frame 2010
SetFrame( IncrementFrame())


-- Frame 2011
SetFrame( IncrementFrame())


-- Frame 2012
SetFrame( IncrementFrame())


-- Frame 2013
SetFrame( IncrementFrame())


-- Frame 2014
SetFrame( IncrementFrame())


-- Frame 2015
SetFrame( IncrementFrame())


-- Frame 2016
SetFrame( IncrementFrame())


-- Frame 2017
SetFrame( IncrementFrame())


-- Frame 2018
SetFrame( IncrementFrame())


-- Frame 2019
SetFrame( IncrementFrame())


-- Frame 2020
SetFrame( IncrementFrame())


-- Frame 2021
SetFrame( IncrementFrame())


-- Frame 2022
SetFrame( IncrementFrame())


-- Frame 2023
SetFrame( IncrementFrame())


-- Frame 2024
SetFrame( IncrementFrame())


-- Frame 2025
SetFrame( IncrementFrame())


-- Frame 2026
SetFrame( IncrementFrame())


-- Frame 2027
SetFrame( IncrementFrame())


-- Frame 2028
SetFrame( IncrementFrame())


-- Frame 2029
SetFrame( IncrementFrame())


-- Frame 2030
SetFrame( IncrementFrame())


-- Frame 2031
SetFrame( IncrementFrame())


-- Frame 2032
SetFrame( IncrementFrame())


-- Frame 2033
SetFrame( IncrementFrame())


-- Frame 2034
SetFrame( IncrementFrame())


-- Frame 2035
SetFrame( IncrementFrame())


-- Frame 2036
SetFrame( IncrementFrame())


-- Frame 2037
SetFrame( IncrementFrame())


-- Frame 2038
SetFrame( IncrementFrame())


-- Frame 2039
SetFrame( IncrementFrame())


-- Frame 2040
SetFrame( IncrementFrame())


-- Frame 2041
SetFrame( IncrementFrame())


-- Frame 2042
SetFrame( IncrementFrame())


-- Frame 2043
SetFrame( IncrementFrame())


-- Frame 2044
SetFrame( IncrementFrame())


-- Frame 2045
SetFrame( IncrementFrame())


-- Frame 2046
SetFrame( IncrementFrame())


-- Frame 2047
SetFrame( IncrementFrame())


-- Frame 2048
SetFrame( IncrementFrame())


-- Frame 2049
SetFrame( IncrementFrame())


-- Frame 2050
SetFrame( IncrementFrame())


-- Frame 2051
SetFrame( IncrementFrame())


-- Frame 2052
SetFrame( IncrementFrame())


-- Frame 2053
SetFrame( IncrementFrame())


-- Frame 2054
SetFrame( IncrementFrame())


-- Frame 2055
SetFrame( IncrementFrame())


-- Frame 2056
SetFrame( IncrementFrame())


-- Frame 2057
SetFrame( IncrementFrame())


-- Frame 2058
SetFrame( IncrementFrame())


-- Frame 2059
SetFrame( IncrementFrame())


-- Frame 2060
SetFrame( IncrementFrame())


-- Frame 2061
SetFrame( IncrementFrame())


-- Frame 2062
SetFrame( IncrementFrame())


-- Frame 2063
SetFrame( IncrementFrame())


-- Frame 2064
SetFrame( IncrementFrame())


-- Frame 2065
SetFrame( IncrementFrame())


-- Frame 2066
SetFrame( IncrementFrame())


-- Frame 2067
SetFrame( IncrementFrame())


-- Frame 2068
SetFrame( IncrementFrame())


-- Frame 2069
SetFrame( IncrementFrame())


-- Frame 2070
SetFrame( IncrementFrame())


-- Frame 2071
SetFrame( IncrementFrame())


-- Frame 2072
SetFrame( IncrementFrame())


-- Frame 2073
SetFrame( IncrementFrame())


-- Frame 2074
SetFrame( IncrementFrame())


-- Frame 2075
SetFrame( IncrementFrame())


-- Frame 2076
SetFrame( IncrementFrame())


-- Frame 2077
SetFrame( IncrementFrame())


-- Frame 2078
SetFrame( IncrementFrame())


-- Frame 2079
SetFrame( IncrementFrame())


-- Frame 2080
SetFrame( IncrementFrame())


-- Frame 2081
SetFrame( IncrementFrame())


-- Frame 2082
SetFrame( IncrementFrame())


-- Frame 2083
SetFrame( IncrementFrame())


-- Frame 2084
SetFrame( IncrementFrame())


-- Frame 2085
SetFrame( IncrementFrame())


-- Frame 2086
SetFrame( IncrementFrame())


-- Frame 2087
SetFrame( IncrementFrame())


-- Frame 2088
SetFrame( IncrementFrame())


-- Frame 2089
SetFrame( IncrementFrame())


-- Frame 2090
SetFrame( IncrementFrame())


-- Frame 2091
SetFrame( IncrementFrame())


-- Frame 2092
SetFrame( IncrementFrame())


-- Frame 2093
SetFrame( IncrementFrame())


-- Frame 2094
SetFrame( IncrementFrame())


-- Frame 2095
SetFrame( IncrementFrame())


-- Frame 2096
SetFrame( IncrementFrame())


-- Frame 2097
SetFrame( IncrementFrame())


-- Frame 2098
SetFrame( IncrementFrame())


-- Frame 2099
SetFrame( IncrementFrame())


-- Frame 2100
SetFrame( IncrementFrame())


-- Frame 2101
SetFrame( IncrementFrame())


-- Frame 2102
SetFrame( IncrementFrame())


-- Frame 2103
SetFrame( IncrementFrame())


-- Frame 2104
SetFrame( IncrementFrame())


-- Frame 2105
SetFrame( IncrementFrame())


-- Frame 2106
SetFrame( IncrementFrame())


-- Frame 2107
SetFrame( IncrementFrame())


-- Frame 2108
SetFrame( IncrementFrame())


-- Frame 2109
SetFrame( IncrementFrame())


-- Frame 2110
SetFrame( IncrementFrame())


-- Frame 2111
SetFrame( IncrementFrame())


-- Frame 2112
SetFrame( IncrementFrame())


-- Frame 2113
SetFrame( IncrementFrame())


-- Frame 2114
SetFrame( IncrementFrame())


-- Frame 2115
SetFrame( IncrementFrame())


-- Frame 2116
SetFrame( IncrementFrame())


-- Frame 2117
SetFrame( IncrementFrame())


-- Frame 2118
SetFrame( IncrementFrame())


-- Frame 2119
SetFrame( IncrementFrame())


-- Frame 2120
SetFrame( IncrementFrame())


-- Frame 2121
SetFrame( IncrementFrame())


-- Frame 2122
SetFrame( IncrementFrame())


-- Frame 2123
SetFrame( IncrementFrame())


-- Frame 2124
SetFrame( IncrementFrame())


-- Frame 2125
SetFrame( IncrementFrame())


-- Frame 2126
SetFrame( IncrementFrame())


-- Frame 2127
SetFrame( IncrementFrame())


-- Frame 2128
SetFrame( IncrementFrame())


-- Frame 2129
SetFrame( IncrementFrame())


-- Frame 2130
SetFrame( IncrementFrame())


-- Frame 2131
SetFrame( IncrementFrame())


-- Frame 2132
SetFrame( IncrementFrame())


-- Frame 2133
SetFrame( IncrementFrame())


-- Frame 2134
SetFrame( IncrementFrame())


-- Frame 2135
SetFrame( IncrementFrame())


-- Frame 2136
SetFrame( IncrementFrame())


-- Frame 2137
SetFrame( IncrementFrame())


-- Frame 2138
SetFrame( IncrementFrame())


-- Frame 2139
SetFrame( IncrementFrame())


-- Frame 2140
SetFrame( IncrementFrame())


-- Frame 2141
SetFrame( IncrementFrame())


-- Frame 2142
SetFrame( IncrementFrame())


-- Frame 2143
SetFrame( IncrementFrame())


-- Frame 2144
SetFrame( IncrementFrame())


-- Frame 2145
SetFrame( IncrementFrame())


-- Frame 2146
SetFrame( IncrementFrame())


-- Frame 2147
SetFrame( IncrementFrame())


-- Frame 2148
SetFrame( IncrementFrame())


-- Frame 2149
SetFrame( IncrementFrame())


-- Frame 2150
SetFrame( IncrementFrame())


-- Frame 2151
SetFrame( IncrementFrame())


-- Frame 2152
SetFrame( IncrementFrame())


-- Frame 2153
SetFrame( IncrementFrame())


-- Frame 2154
SetFrame( IncrementFrame())


-- Frame 2155
SetFrame( IncrementFrame())


-- Frame 2156
SetFrame( IncrementFrame())


-- Frame 2157
SetFrame( IncrementFrame())


-- Frame 2158
SetFrame( IncrementFrame())


-- Frame 2159
SetFrame( IncrementFrame())


-- Frame 2160
SetFrame( IncrementFrame())


-- Frame 2161
SetFrame( IncrementFrame())


-- Frame 2162
SetFrame( IncrementFrame())


-- Frame 2163
SetFrame( IncrementFrame())


-- Frame 2164
SetFrame( IncrementFrame())


-- Frame 2165
SetFrame( IncrementFrame())


-- Frame 2166
SetFrame( IncrementFrame())


-- Frame 2167
SetFrame( IncrementFrame())


-- Frame 2168
SetFrame( IncrementFrame())


-- Frame 2169
SetFrame( IncrementFrame())


-- Frame 2170
SetFrame( IncrementFrame())


-- Frame 2171
SetFrame( IncrementFrame())


-- Frame 2172
SetFrame( IncrementFrame())


-- Frame 2173
SetFrame( IncrementFrame())


-- Frame 2174
SetFrame( IncrementFrame())


-- Frame 2175
SetFrame( IncrementFrame())


-- Frame 2176
SetFrame( IncrementFrame())


-- Frame 2177
SetFrame( IncrementFrame())


-- Frame 2178
SetFrame( IncrementFrame())


-- Frame 2179
SetFrame( IncrementFrame())


-- Frame 2180
SetFrame( IncrementFrame())


-- Frame 2181
SetFrame( IncrementFrame())


-- Frame 2182
SetFrame( IncrementFrame())


-- Frame 2183
SetFrame( IncrementFrame())


-- Frame 2184
SetFrame( IncrementFrame())


-- Frame 2185
SetFrame( IncrementFrame())


-- Frame 2186
SetFrame( IncrementFrame())


-- Frame 2187
SetFrame( IncrementFrame())


-- Frame 2188
SetFrame( IncrementFrame())


-- Frame 2189
SetFrame( IncrementFrame())


-- Frame 2190
SetFrame( IncrementFrame())

LinkCamThirdPerson( ObjectObj_Camera, "Act_BehindT", "Obj_Tichondrius")
PlayAudio( ObjectObj_Archimonde, "Act_VeryWellT", "Resources/WarCraftSounds/CS_05_Archimonde.mp3", 1)

-- Frame 2191
SetFrame( IncrementFrame())


-- Frame 2192
SetFrame( IncrementFrame())


-- Frame 2193
SetFrame( IncrementFrame())


-- Frame 2194
SetFrame( IncrementFrame())


-- Frame 2195
SetFrame( IncrementFrame())


-- Frame 2196
SetFrame( IncrementFrame())


-- Frame 2197
SetFrame( IncrementFrame())


-- Frame 2198
SetFrame( IncrementFrame())


-- Frame 2199
SetFrame( IncrementFrame())


-- Frame 2200
SetFrame( IncrementFrame())


-- Frame 2201
SetFrame( IncrementFrame())


-- Frame 2202
SetFrame( IncrementFrame())


-- Frame 2203
SetFrame( IncrementFrame())


-- Frame 2204
SetFrame( IncrementFrame())


-- Frame 2205
SetFrame( IncrementFrame())


-- Frame 2206
SetFrame( IncrementFrame())


-- Frame 2207
SetFrame( IncrementFrame())


-- Frame 2208
SetFrame( IncrementFrame())


-- Frame 2209
SetFrame( IncrementFrame())


-- Frame 2210
SetFrame( IncrementFrame())


-- Frame 2211
SetFrame( IncrementFrame())


-- Frame 2212
SetFrame( IncrementFrame())


-- Frame 2213
SetFrame( IncrementFrame())


-- Frame 2214
SetFrame( IncrementFrame())


-- Frame 2215
SetFrame( IncrementFrame())


-- Frame 2216
SetFrame( IncrementFrame())


-- Frame 2217
SetFrame( IncrementFrame())


-- Frame 2218
SetFrame( IncrementFrame())


-- Frame 2219
SetFrame( IncrementFrame())


-- Frame 2220
SetFrame( IncrementFrame())


-- Frame 2221
SetFrame( IncrementFrame())


-- Frame 2222
SetFrame( IncrementFrame())


-- Frame 2223
SetFrame( IncrementFrame())


-- Frame 2224
SetFrame( IncrementFrame())


-- Frame 2225
SetFrame( IncrementFrame())


-- Frame 2226
SetFrame( IncrementFrame())


-- Frame 2227
SetFrame( IncrementFrame())


-- Frame 2228
SetFrame( IncrementFrame())


-- Frame 2229
SetFrame( IncrementFrame())


-- Frame 2230
SetFrame( IncrementFrame())


-- Frame 2231
SetFrame( IncrementFrame())


-- Frame 2232
SetFrame( IncrementFrame())


-- Frame 2233
SetFrame( IncrementFrame())


-- Frame 2234
SetFrame( IncrementFrame())


-- Frame 2235
SetFrame( IncrementFrame())


-- Frame 2236
SetFrame( IncrementFrame())


-- Frame 2237
SetFrame( IncrementFrame())


-- Frame 2238
SetFrame( IncrementFrame())


-- Frame 2239
SetFrame( IncrementFrame())


-- Frame 2240
SetFrame( IncrementFrame())


-- Frame 2241
SetFrame( IncrementFrame())


-- Frame 2242
SetFrame( IncrementFrame())


-- Frame 2243
SetFrame( IncrementFrame())


-- Frame 2244
SetFrame( IncrementFrame())


-- Frame 2245
SetFrame( IncrementFrame())


-- Frame 2246
SetFrame( IncrementFrame())


-- Frame 2247
SetFrame( IncrementFrame())


-- Frame 2248
SetFrame( IncrementFrame())


-- Frame 2249
SetFrame( IncrementFrame())


-- Frame 2250
SetFrame( IncrementFrame())


-- Frame 2251
SetFrame( IncrementFrame())


-- Frame 2252
SetFrame( IncrementFrame())


-- Frame 2253
SetFrame( IncrementFrame())


-- Frame 2254
SetFrame( IncrementFrame())


-- Frame 2255
SetFrame( IncrementFrame())


-- Frame 2256
SetFrame( IncrementFrame())


-- Frame 2257
SetFrame( IncrementFrame())


-- Frame 2258
SetFrame( IncrementFrame())


-- Frame 2259
SetFrame( IncrementFrame())


-- Frame 2260
SetFrame( IncrementFrame())


-- Frame 2261
SetFrame( IncrementFrame())


-- Frame 2262
SetFrame( IncrementFrame())


-- Frame 2263
SetFrame( IncrementFrame())


-- Frame 2264
SetFrame( IncrementFrame())


-- Frame 2265
SetFrame( IncrementFrame())


-- Frame 2266
SetFrame( IncrementFrame())


-- Frame 2267
SetFrame( IncrementFrame())


-- Frame 2268
SetFrame( IncrementFrame())


-- Frame 2269
SetFrame( IncrementFrame())


-- Frame 2270
SetFrame( IncrementFrame())


-- Frame 2271
SetFrame( IncrementFrame())


-- Frame 2272
SetFrame( IncrementFrame())


-- Frame 2273
SetFrame( IncrementFrame())


-- Frame 2274
SetFrame( IncrementFrame())


-- Frame 2275
SetFrame( IncrementFrame())


-- Frame 2276
SetFrame( IncrementFrame())


-- Frame 2277
SetFrame( IncrementFrame())


-- Frame 2278
SetFrame( IncrementFrame())


-- Frame 2279
SetFrame( IncrementFrame())


-- Frame 2280
SetFrame( IncrementFrame())


-- Frame 2281
SetFrame( IncrementFrame())


-- Frame 2282
SetFrame( IncrementFrame())


-- Frame 2283
SetFrame( IncrementFrame())


-- Frame 2284
SetFrame( IncrementFrame())


-- Frame 2285
SetFrame( IncrementFrame())


-- Frame 2286
SetFrame( IncrementFrame())


-- Frame 2287
SetFrame( IncrementFrame())


-- Frame 2288
SetFrame( IncrementFrame())


-- Frame 2289
SetFrame( IncrementFrame())


-- Frame 2290
SetFrame( IncrementFrame())


-- Frame 2291
SetFrame( IncrementFrame())


-- Frame 2292
SetFrame( IncrementFrame())


-- Frame 2293
SetFrame( IncrementFrame())


-- Frame 2294
SetFrame( IncrementFrame())


-- Frame 2295
SetFrame( IncrementFrame())


-- Frame 2296
SetFrame( IncrementFrame())


-- Frame 2297
SetFrame( IncrementFrame())


-- Frame 2298
SetFrame( IncrementFrame())


-- Frame 2299
SetFrame( IncrementFrame())


-- Frame 2300
SetFrame( IncrementFrame())


-- Frame 2301
SetFrame( IncrementFrame())


-- Frame 2302
SetFrame( IncrementFrame())


-- Frame 2303
SetFrame( IncrementFrame())


-- Frame 2304
SetFrame( IncrementFrame())


-- Frame 2305
SetFrame( IncrementFrame())


-- Frame 2306
SetFrame( IncrementFrame())


-- Frame 2307
SetFrame( IncrementFrame())


-- Frame 2308
SetFrame( IncrementFrame())


-- Frame 2309
SetFrame( IncrementFrame())


-- Frame 2310
SetFrame( IncrementFrame())


-- Frame 2311
SetFrame( IncrementFrame())


-- Frame 2312
SetFrame( IncrementFrame())


-- Frame 2313
SetFrame( IncrementFrame())


-- Frame 2314
SetFrame( IncrementFrame())


-- Frame 2315
SetFrame( IncrementFrame())


-- Frame 2316
SetFrame( IncrementFrame())


-- Frame 2317
SetFrame( IncrementFrame())


-- Frame 2318
SetFrame( IncrementFrame())


-- Frame 2319
SetFrame( IncrementFrame())


-- Frame 2320
SetFrame( IncrementFrame())


-- Frame 2321
SetFrame( IncrementFrame())


-- Frame 2322
SetFrame( IncrementFrame())


-- Frame 2323
SetFrame( IncrementFrame())


-- Frame 2324
SetFrame( IncrementFrame())


-- Frame 2325
SetFrame( IncrementFrame())


-- Frame 2326
SetFrame( IncrementFrame())


-- Frame 2327
SetFrame( IncrementFrame())


-- Frame 2328
SetFrame( IncrementFrame())


-- Frame 2329
SetFrame( IncrementFrame())


-- Frame 2330
SetFrame( IncrementFrame())


-- Frame 2331
SetFrame( IncrementFrame())


-- Frame 2332
SetFrame( IncrementFrame())


-- Frame 2333
SetFrame( IncrementFrame())


-- Frame 2334
SetFrame( IncrementFrame())


-- Frame 2335
SetFrame( IncrementFrame())


-- Frame 2336
SetFrame( IncrementFrame())


-- Frame 2337
SetFrame( IncrementFrame())


-- Frame 2338
SetFrame( IncrementFrame())


-- Frame 2339
SetFrame( IncrementFrame())


-- Frame 2340
SetFrame( IncrementFrame())


-- Frame 2341
SetFrame( IncrementFrame())


-- Frame 2342
SetFrame( IncrementFrame())


-- Frame 2343
SetFrame( IncrementFrame())


-- Frame 2344
SetFrame( IncrementFrame())


-- Frame 2345
SetFrame( IncrementFrame())


-- Frame 2346
SetFrame( IncrementFrame())


-- Frame 2347
SetFrame( IncrementFrame())


-- Frame 2348
SetFrame( IncrementFrame())


-- Frame 2349
SetFrame( IncrementFrame())


-- Frame 2350
SetFrame( IncrementFrame())


-- Frame 2351
SetFrame( IncrementFrame())


-- Frame 2352
SetFrame( IncrementFrame())


-- Frame 2353
SetFrame( IncrementFrame())


-- Frame 2354
SetFrame( IncrementFrame())


-- Frame 2355
SetFrame( IncrementFrame())


-- Frame 2356
SetFrame( IncrementFrame())


-- Frame 2357
SetFrame( IncrementFrame())


-- Frame 2358
SetFrame( IncrementFrame())


-- Frame 2359
SetFrame( IncrementFrame())


-- Frame 2360
SetFrame( IncrementFrame())


-- Frame 2361
SetFrame( IncrementFrame())


-- Frame 2362
SetFrame( IncrementFrame())


-- Frame 2363
SetFrame( IncrementFrame())


-- Frame 2364
SetFrame( IncrementFrame())


-- Frame 2365
SetFrame( IncrementFrame())


-- Frame 2366
SetFrame( IncrementFrame())


-- Frame 2367
SetFrame( IncrementFrame())


-- Frame 2368
SetFrame( IncrementFrame())


-- Frame 2369
SetFrame( IncrementFrame())


-- Frame 2370
SetFrame( IncrementFrame())


-- Frame 2371
SetFrame( IncrementFrame())


-- Frame 2372
SetFrame( IncrementFrame())


-- Frame 2373
SetFrame( IncrementFrame())


-- Frame 2374
SetFrame( IncrementFrame())


-- Frame 2375
SetFrame( IncrementFrame())


-- Frame 2376
SetFrame( IncrementFrame())


-- Frame 2377
SetFrame( IncrementFrame())


-- Frame 2378
SetFrame( IncrementFrame())


-- Frame 2379
SetFrame( IncrementFrame())


-- Frame 2380
SetFrame( IncrementFrame())


-- Frame 2381
SetFrame( IncrementFrame())


-- Frame 2382
SetFrame( IncrementFrame())


-- Frame 2383
SetFrame( IncrementFrame())


-- Frame 2384
SetFrame( IncrementFrame())


-- Frame 2385
SetFrame( IncrementFrame())


-- Frame 2386
SetFrame( IncrementFrame())


-- Frame 2387
SetFrame( IncrementFrame())


-- Frame 2388
SetFrame( IncrementFrame())


-- Frame 2389
SetFrame( IncrementFrame())


-- Frame 2390
SetFrame( IncrementFrame())


-- Frame 2391
SetFrame( IncrementFrame())


-- Frame 2392
SetFrame( IncrementFrame())


-- Frame 2393
SetFrame( IncrementFrame())


-- Frame 2394
SetFrame( IncrementFrame())


-- Frame 2395
SetFrame( IncrementFrame())


-- Frame 2396
SetFrame( IncrementFrame())


-- Frame 2397
SetFrame( IncrementFrame())


-- Frame 2398
SetFrame( IncrementFrame())


-- Frame 2399
SetFrame( IncrementFrame())


-- Frame 2400
SetFrame( IncrementFrame())


-- Frame 2401
SetFrame( IncrementFrame())


-- Frame 2402
SetFrame( IncrementFrame())


-- Frame 2403
SetFrame( IncrementFrame())


-- Frame 2404
SetFrame( IncrementFrame())


-- Frame 2405
SetFrame( IncrementFrame())


-- Frame 2406
SetFrame( IncrementFrame())


-- Frame 2407
SetFrame( IncrementFrame())


-- Frame 2408
SetFrame( IncrementFrame())


-- Frame 2409
SetFrame( IncrementFrame())


-- Frame 2410
SetFrame( IncrementFrame())


-- Frame 2411
SetFrame( IncrementFrame())


-- Frame 2412
SetFrame( IncrementFrame())


-- Frame 2413
SetFrame( IncrementFrame())


-- Frame 2414
SetFrame( IncrementFrame())


-- Frame 2415
SetFrame( IncrementFrame())


-- Frame 2416
SetFrame( IncrementFrame())


-- Frame 2417
SetFrame( IncrementFrame())


-- Frame 2418
SetFrame( IncrementFrame())


-- Frame 2419
SetFrame( IncrementFrame())


-- Frame 2420
SetFrame( IncrementFrame())


-- Frame 2421
SetFrame( IncrementFrame())


-- Frame 2422
SetFrame( IncrementFrame())


-- Frame 2423
SetFrame( IncrementFrame())


-- Frame 2424
SetFrame( IncrementFrame())


-- Frame 2425
SetFrame( IncrementFrame())


-- Frame 2426
SetFrame( IncrementFrame())


-- Frame 2427
SetFrame( IncrementFrame())


-- Frame 2428
SetFrame( IncrementFrame())


-- Frame 2429
SetFrame( IncrementFrame())


-- Frame 2430
SetFrame( IncrementFrame())


-- Frame 2431
SetFrame( IncrementFrame())


-- Frame 2432
SetFrame( IncrementFrame())


-- Frame 2433
SetFrame( IncrementFrame())


-- Frame 2434
SetFrame( IncrementFrame())


-- Frame 2435
SetFrame( IncrementFrame())


-- Frame 2436
SetFrame( IncrementFrame())


-- Frame 2437
SetFrame( IncrementFrame())


-- Frame 2438
SetFrame( IncrementFrame())


-- Frame 2439
SetFrame( IncrementFrame())


-- Frame 2440
SetFrame( IncrementFrame())


-- Frame 2441
SetFrame( IncrementFrame())


-- Frame 2442
SetFrame( IncrementFrame())


-- Frame 2443
SetFrame( IncrementFrame())


-- Frame 2444
SetFrame( IncrementFrame())


-- Frame 2445
SetFrame( IncrementFrame())


-- Frame 2446
SetFrame( IncrementFrame())


-- Frame 2447
SetFrame( IncrementFrame())


-- Frame 2448
SetFrame( IncrementFrame())


-- Frame 2449
SetFrame( IncrementFrame())


-- Frame 2450
SetFrame( IncrementFrame())


-- Frame 2451
SetFrame( IncrementFrame())


-- Frame 2452
SetFrame( IncrementFrame())


-- Frame 2453
SetFrame( IncrementFrame())


-- Frame 2454
SetFrame( IncrementFrame())


-- Frame 2455
SetFrame( IncrementFrame())


-- Frame 2456
SetFrame( IncrementFrame())


-- Frame 2457
SetFrame( IncrementFrame())


-- Frame 2458
SetFrame( IncrementFrame())


-- Frame 2459
SetFrame( IncrementFrame())


-- Frame 2460
SetFrame( IncrementFrame())


-- Frame 2461
SetFrame( IncrementFrame())


-- Frame 2462
SetFrame( IncrementFrame())


-- Frame 2463
SetFrame( IncrementFrame())


-- Frame 2464
SetFrame( IncrementFrame())


-- Frame 2465
SetFrame( IncrementFrame())


-- Frame 2466
SetFrame( IncrementFrame())


-- Frame 2467
SetFrame( IncrementFrame())


-- Frame 2468
SetFrame( IncrementFrame())


-- Frame 2469
SetFrame( IncrementFrame())


-- Frame 2470
SetFrame( IncrementFrame())


-- Frame 2471
SetFrame( IncrementFrame())


-- Frame 2472
SetFrame( IncrementFrame())


-- Frame 2473
SetFrame( IncrementFrame())


-- Frame 2474
SetFrame( IncrementFrame())


-- Frame 2475
SetFrame( IncrementFrame())


-- Frame 2476
SetFrame( IncrementFrame())


-- Frame 2477
SetFrame( IncrementFrame())


-- Frame 2478
SetFrame( IncrementFrame())


-- Frame 2479
SetFrame( IncrementFrame())


-- Frame 2480
SetFrame( IncrementFrame())


-- Frame 2481
SetFrame( IncrementFrame())


-- Frame 2482
SetFrame( IncrementFrame())


-- Frame 2483
SetFrame( IncrementFrame())


-- Frame 2484
SetFrame( IncrementFrame())


-- Frame 2485
SetFrame( IncrementFrame())


-- Frame 2486
SetFrame( IncrementFrame())


-- Frame 2487
SetFrame( IncrementFrame())


-- Frame 2488
SetFrame( IncrementFrame())


-- Frame 2489
SetFrame( IncrementFrame())


-- Frame 2490
SetFrame( IncrementFrame())


-- Frame 2491
SetFrame( IncrementFrame())


-- Frame 2492
SetFrame( IncrementFrame())


-- Frame 2493
SetFrame( IncrementFrame())


-- Frame 2494
SetFrame( IncrementFrame())


-- Frame 2495
SetFrame( IncrementFrame())


-- Frame 2496
SetFrame( IncrementFrame())


-- Frame 2497
SetFrame( IncrementFrame())


-- Frame 2498
SetFrame( IncrementFrame())


-- Frame 2499
SetFrame( IncrementFrame())


-- Frame 2500
SetFrame( IncrementFrame())


-- Frame 2501
SetFrame( IncrementFrame())


-- Frame 2502
SetFrame( IncrementFrame())


-- Frame 2503
SetFrame( IncrementFrame())


-- Frame 2504
SetFrame( IncrementFrame())


-- Frame 2505
SetFrame( IncrementFrame())


-- Frame 2506
SetFrame( IncrementFrame())


-- Frame 2507
SetFrame( IncrementFrame())


-- Frame 2508
SetFrame( IncrementFrame())


-- Frame 2509
SetFrame( IncrementFrame())


-- Frame 2510
SetFrame( IncrementFrame())


-- Frame 2511
SetFrame( IncrementFrame())


-- Frame 2512
SetFrame( IncrementFrame())


-- Frame 2513
SetFrame( IncrementFrame())


-- Frame 2514
SetFrame( IncrementFrame())


-- Frame 2515
SetFrame( IncrementFrame())


-- Frame 2516
SetFrame( IncrementFrame())


-- Frame 2517
SetFrame( IncrementFrame())


-- Frame 2518
SetFrame( IncrementFrame())


-- Frame 2519
SetFrame( IncrementFrame())


-- Frame 2520
SetFrame( IncrementFrame())


-- Frame 2521
SetFrame( IncrementFrame())


-- Frame 2522
SetFrame( IncrementFrame())


-- Frame 2523
SetFrame( IncrementFrame())


-- Frame 2524
SetFrame( IncrementFrame())


-- Frame 2525
SetFrame( IncrementFrame())


-- Frame 2526
SetFrame( IncrementFrame())


-- Frame 2527
SetFrame( IncrementFrame())


-- Frame 2528
SetFrame( IncrementFrame())


-- Frame 2529
SetFrame( IncrementFrame())


-- Frame 2530
SetFrame( IncrementFrame())


-- Frame 2531
SetFrame( IncrementFrame())


-- Frame 2532
SetFrame( IncrementFrame())


-- Frame 2533
SetFrame( IncrementFrame())


-- Frame 2534
SetFrame( IncrementFrame())


-- Frame 2535
SetFrame( IncrementFrame())


-- Frame 2536
SetFrame( IncrementFrame())


-- Frame 2537
SetFrame( IncrementFrame())


-- Frame 2538
SetFrame( IncrementFrame())


-- Frame 2539
SetFrame( IncrementFrame())


-- Frame 2540
SetFrame( IncrementFrame())


-- Frame 2541
SetFrame( IncrementFrame())


-- Frame 2542
SetFrame( IncrementFrame())


-- Frame 2543
SetFrame( IncrementFrame())


-- Frame 2544
SetFrame( IncrementFrame())


-- Frame 2545
SetFrame( IncrementFrame())


-- Frame 2546
SetFrame( IncrementFrame())


-- Frame 2547
SetFrame( IncrementFrame())


-- Frame 2548
SetFrame( IncrementFrame())


-- Frame 2549
SetFrame( IncrementFrame())


-- Frame 2550
SetFrame( IncrementFrame())


-- Frame 2551
SetFrame( IncrementFrame())


-- Frame 2552
SetFrame( IncrementFrame())


-- Frame 2553
SetFrame( IncrementFrame())


-- Frame 2554
SetFrame( IncrementFrame())


-- Frame 2555
SetFrame( IncrementFrame())


-- Frame 2556
SetFrame( IncrementFrame())


-- Frame 2557
SetFrame( IncrementFrame())


-- Frame 2558
SetFrame( IncrementFrame())


-- Frame 2559
SetFrame( IncrementFrame())


-- Frame 2560
SetFrame( IncrementFrame())


-- Frame 2561
SetFrame( IncrementFrame())


-- Frame 2562
SetFrame( IncrementFrame())


-- Frame 2563
SetFrame( IncrementFrame())


-- Frame 2564
SetFrame( IncrementFrame())


-- Frame 2565
SetFrame( IncrementFrame())


-- Frame 2566
SetFrame( IncrementFrame())


-- Frame 2567
SetFrame( IncrementFrame())


-- Frame 2568
SetFrame( IncrementFrame())


-- Frame 2569
SetFrame( IncrementFrame())


-- Frame 2570
SetFrame( IncrementFrame())


-- Frame 2571
SetFrame( IncrementFrame())


-- Frame 2572
SetFrame( IncrementFrame())


-- Frame 2573
SetFrame( IncrementFrame())


-- Frame 2574
SetFrame( IncrementFrame())


-- Frame 2575
SetFrame( IncrementFrame())


-- Frame 2576
SetFrame( IncrementFrame())


-- Frame 2577
SetFrame( IncrementFrame())


-- Frame 2578
SetFrame( IncrementFrame())


-- Frame 2579
SetFrame( IncrementFrame())


-- Frame 2580
SetFrame( IncrementFrame())


-- Frame 2581
SetFrame( IncrementFrame())


-- Frame 2582
SetFrame( IncrementFrame())


-- Frame 2583
SetFrame( IncrementFrame())


-- Frame 2584
SetFrame( IncrementFrame())


-- Frame 2585
SetFrame( IncrementFrame())


-- Frame 2586
SetFrame( IncrementFrame())


-- Frame 2587
SetFrame( IncrementFrame())


-- Frame 2588
SetFrame( IncrementFrame())


-- Frame 2589
SetFrame( IncrementFrame())


-- Frame 2590
SetFrame( IncrementFrame())


-- Frame 2591
SetFrame( IncrementFrame())


-- Frame 2592
SetFrame( IncrementFrame())


-- Frame 2593
SetFrame( IncrementFrame())


-- Frame 2594
SetFrame( IncrementFrame())


-- Frame 2595
SetFrame( IncrementFrame())


-- Frame 2596
SetFrame( IncrementFrame())


-- Frame 2597
SetFrame( IncrementFrame())


-- Frame 2598
SetFrame( IncrementFrame())


-- Frame 2599
SetFrame( IncrementFrame())


-- Frame 2600
SetFrame( IncrementFrame())


-- Frame 2601
SetFrame( IncrementFrame())


-- Frame 2602
SetFrame( IncrementFrame())


-- Frame 2603
SetFrame( IncrementFrame())


-- Frame 2604
SetFrame( IncrementFrame())


-- Frame 2605
SetFrame( IncrementFrame())


-- Frame 2606
SetFrame( IncrementFrame())


-- Frame 2607
SetFrame( IncrementFrame())


-- Frame 2608
SetFrame( IncrementFrame())


-- Frame 2609
SetFrame( IncrementFrame())


-- Frame 2610
SetFrame( IncrementFrame())


-- Frame 2611
SetFrame( IncrementFrame())


-- Frame 2612
SetFrame( IncrementFrame())


-- Frame 2613
SetFrame( IncrementFrame())


-- Frame 2614
SetFrame( IncrementFrame())


-- Frame 2615
SetFrame( IncrementFrame())


-- Frame 2616
SetFrame( IncrementFrame())


-- Frame 2617
SetFrame( IncrementFrame())


-- Frame 2618
SetFrame( IncrementFrame())


-- Frame 2619
SetFrame( IncrementFrame())


-- Frame 2620
SetFrame( IncrementFrame())


-- Frame 2621
SetFrame( IncrementFrame())


-- Frame 2622
SetFrame( IncrementFrame())


-- Frame 2623
SetFrame( IncrementFrame())


-- Frame 2624
SetFrame( IncrementFrame())


-- Frame 2625
SetFrame( IncrementFrame())


-- Frame 2626
SetFrame( IncrementFrame())


-- Frame 2627
SetFrame( IncrementFrame())


-- Frame 2628
SetFrame( IncrementFrame())


-- Frame 2629
SetFrame( IncrementFrame())


-- Frame 2630
SetFrame( IncrementFrame())


-- Frame 2631
SetFrame( IncrementFrame())


-- Frame 2632
SetFrame( IncrementFrame())


-- Frame 2633
SetFrame( IncrementFrame())


-- Frame 2634
SetFrame( IncrementFrame())


-- Frame 2635
SetFrame( IncrementFrame())


-- Frame 2636
SetFrame( IncrementFrame())


-- Frame 2637
SetFrame( IncrementFrame())


-- Frame 2638
SetFrame( IncrementFrame())


-- Frame 2639
SetFrame( IncrementFrame())


-- Frame 2640
SetFrame( IncrementFrame())


-- Frame 2641
SetFrame( IncrementFrame())


-- Frame 2642
SetFrame( IncrementFrame())


-- Frame 2643
SetFrame( IncrementFrame())


-- Frame 2644
SetFrame( IncrementFrame())


-- Frame 2645
SetFrame( IncrementFrame())


-- Frame 2646
SetFrame( IncrementFrame())


-- Frame 2647
SetFrame( IncrementFrame())


-- Frame 2648
SetFrame( IncrementFrame())


-- Frame 2649
SetFrame( IncrementFrame())


-- Frame 2650
SetFrame( IncrementFrame())


-- Frame 2651
SetFrame( IncrementFrame())


-- Frame 2652
SetFrame( IncrementFrame())


-- Frame 2653
SetFrame( IncrementFrame())


-- Frame 2654
SetFrame( IncrementFrame())


-- Frame 2655
SetFrame( IncrementFrame())


-- Frame 2656
SetFrame( IncrementFrame())


-- Frame 2657
SetFrame( IncrementFrame())


-- Frame 2658
SetFrame( IncrementFrame())


-- Frame 2659
SetFrame( IncrementFrame())


-- Frame 2660
SetFrame( IncrementFrame())


-- Frame 2661
SetFrame( IncrementFrame())


-- Frame 2662
SetFrame( IncrementFrame())


-- Frame 2663
SetFrame( IncrementFrame())


-- Frame 2664
SetFrame( IncrementFrame())


-- Frame 2665
SetFrame( IncrementFrame())


-- Frame 2666
SetFrame( IncrementFrame())


-- Frame 2667
SetFrame( IncrementFrame())


-- Frame 2668
SetFrame( IncrementFrame())


-- Frame 2669
SetFrame( IncrementFrame())


-- Frame 2670
SetFrame( IncrementFrame())


-- Frame 2671
SetFrame( IncrementFrame())


-- Frame 2672
SetFrame( IncrementFrame())


-- Frame 2673
SetFrame( IncrementFrame())


-- Frame 2674
SetFrame( IncrementFrame())


-- Frame 2675
SetFrame( IncrementFrame())


-- Frame 2676
SetFrame( IncrementFrame())


-- Frame 2677
SetFrame( IncrementFrame())


-- Frame 2678
SetFrame( IncrementFrame())


-- Frame 2679
SetFrame( IncrementFrame())


-- Frame 2680
SetFrame( IncrementFrame())


-- Frame 2681
SetFrame( IncrementFrame())


-- Frame 2682
SetFrame( IncrementFrame())


-- Frame 2683
SetFrame( IncrementFrame())


-- Frame 2684
SetFrame( IncrementFrame())


-- Frame 2685
SetFrame( IncrementFrame())


-- Frame 2686
SetFrame( IncrementFrame())


-- Frame 2687
SetFrame( IncrementFrame())


-- Frame 2688
SetFrame( IncrementFrame())


-- Frame 2689
SetFrame( IncrementFrame())


-- Frame 2690
SetFrame( IncrementFrame())


-- Frame 2691
SetFrame( IncrementFrame())


-- Frame 2692
SetFrame( IncrementFrame())


-- Frame 2693
SetFrame( IncrementFrame())


-- Frame 2694
SetFrame( IncrementFrame())


-- Frame 2695
SetFrame( IncrementFrame())


-- Frame 2696
SetFrame( IncrementFrame())


-- Frame 2697
SetFrame( IncrementFrame())


-- Frame 2698
SetFrame( IncrementFrame())


-- Frame 2699
SetFrame( IncrementFrame())


-- Frame 2700
SetFrame( IncrementFrame())


-- Frame 2701
SetFrame( IncrementFrame())


-- Frame 2702
SetFrame( IncrementFrame())


-- Frame 2703
SetFrame( IncrementFrame())


-- Frame 2704
SetFrame( IncrementFrame())


-- Frame 2705
SetFrame( IncrementFrame())


-- Frame 2706
SetFrame( IncrementFrame())


-- Frame 2707
SetFrame( IncrementFrame())


-- Frame 2708
SetFrame( IncrementFrame())


-- Frame 2709
SetFrame( IncrementFrame())


-- Frame 2710
SetFrame( IncrementFrame())


-- Frame 2711
SetFrame( IncrementFrame())


-- Frame 2712
SetFrame( IncrementFrame())


-- Frame 2713
SetFrame( IncrementFrame())


-- Frame 2714
SetFrame( IncrementFrame())


-- Frame 2715
SetFrame( IncrementFrame())


-- Frame 2716
SetFrame( IncrementFrame())


-- Frame 2717
SetFrame( IncrementFrame())


-- Frame 2718
SetFrame( IncrementFrame())


-- Frame 2719
SetFrame( IncrementFrame())


-- Frame 2720
SetFrame( IncrementFrame())


-- Frame 2721
SetFrame( IncrementFrame())


-- Frame 2722
SetFrame( IncrementFrame())


-- Frame 2723
SetFrame( IncrementFrame())


-- Frame 2724
SetFrame( IncrementFrame())


-- Frame 2725
SetFrame( IncrementFrame())


-- Frame 2726
SetFrame( IncrementFrame())


-- Frame 2727
SetFrame( IncrementFrame())


-- Frame 2728
SetFrame( IncrementFrame())


-- Frame 2729
SetFrame( IncrementFrame())


-- Frame 2730
SetFrame( IncrementFrame())


-- Frame 2731
SetFrame( IncrementFrame())


-- Frame 2732
SetFrame( IncrementFrame())


-- Frame 2733
SetFrame( IncrementFrame())


-- Frame 2734
SetFrame( IncrementFrame())


-- Frame 2735
SetFrame( IncrementFrame())


-- Frame 2736
SetFrame( IncrementFrame())


-- Frame 2737
SetFrame( IncrementFrame())


-- Frame 2738
SetFrame( IncrementFrame())


-- Frame 2739
SetFrame( IncrementFrame())


-- Frame 2740
SetFrame( IncrementFrame())


-- Frame 2741
SetFrame( IncrementFrame())


-- Frame 2742
SetFrame( IncrementFrame())


-- Frame 2743
SetFrame( IncrementFrame())


-- Frame 2744
SetFrame( IncrementFrame())


-- Frame 2745
SetFrame( IncrementFrame())


-- Frame 2746
SetFrame( IncrementFrame())


-- Frame 2747
SetFrame( IncrementFrame())


-- Frame 2748
SetFrame( IncrementFrame())


-- Frame 2749
SetFrame( IncrementFrame())


-- Frame 2750
SetFrame( IncrementFrame())


-- Frame 2751
SetFrame( IncrementFrame())


-- Frame 2752
SetFrame( IncrementFrame())


-- Frame 2753
SetFrame( IncrementFrame())


-- Frame 2754
SetFrame( IncrementFrame())


-- Frame 2755
SetFrame( IncrementFrame())


-- Frame 2756
SetFrame( IncrementFrame())


-- Frame 2757
SetFrame( IncrementFrame())


-- Frame 2758
SetFrame( IncrementFrame())


-- Frame 2759
SetFrame( IncrementFrame())


-- Frame 2760
SetFrame( IncrementFrame())


-- Frame 2761
SetFrame( IncrementFrame())


-- Frame 2762
SetFrame( IncrementFrame())


-- Frame 2763
SetFrame( IncrementFrame())


-- Frame 2764
SetFrame( IncrementFrame())


-- Frame 2765
SetFrame( IncrementFrame())


-- Frame 2766
SetFrame( IncrementFrame())


-- Frame 2767
SetFrame( IncrementFrame())


-- Frame 2768
SetFrame( IncrementFrame())


-- Frame 2769
SetFrame( IncrementFrame())


-- Frame 2770
SetFrame( IncrementFrame())


-- Frame 2771
SetFrame( IncrementFrame())


-- Frame 2772
SetFrame( IncrementFrame())


-- Frame 2773
SetFrame( IncrementFrame())


-- Frame 2774
SetFrame( IncrementFrame())


-- Frame 2775
SetFrame( IncrementFrame())


-- Frame 2776
SetFrame( IncrementFrame())


-- Frame 2777
SetFrame( IncrementFrame())


-- Frame 2778
SetFrame( IncrementFrame())


-- Frame 2779
SetFrame( IncrementFrame())


-- Frame 2780
SetFrame( IncrementFrame())


-- Frame 2781
SetFrame( IncrementFrame())


-- Frame 2782
SetFrame( IncrementFrame())


-- Frame 2783
SetFrame( IncrementFrame())


-- Frame 2784
SetFrame( IncrementFrame())


-- Frame 2785
SetFrame( IncrementFrame())


-- Frame 2786
SetFrame( IncrementFrame())


-- Frame 2787
SetFrame( IncrementFrame())


-- Frame 2788
SetFrame( IncrementFrame())


-- Frame 2789
SetFrame( IncrementFrame())


-- Frame 2790
SetFrame( IncrementFrame())


-- Frame 2791
SetFrame( IncrementFrame())


-- Frame 2792
SetFrame( IncrementFrame())


-- Frame 2793
SetFrame( IncrementFrame())


-- Frame 2794
SetFrame( IncrementFrame())


-- Frame 2795
SetFrame( IncrementFrame())


-- Frame 2796
SetFrame( IncrementFrame())


-- Frame 2797
SetFrame( IncrementFrame())


-- Frame 2798
SetFrame( IncrementFrame())


-- Frame 2799
SetFrame( IncrementFrame())


-- Frame 2800
SetFrame( IncrementFrame())


-- Frame 2801
SetFrame( IncrementFrame())


-- Frame 2802
SetFrame( IncrementFrame())


-- Frame 2803
SetFrame( IncrementFrame())


-- Frame 2804
SetFrame( IncrementFrame())


-- Frame 2805
SetFrame( IncrementFrame())


-- Frame 2806
SetFrame( IncrementFrame())


-- Frame 2807
SetFrame( IncrementFrame())


-- Frame 2808
SetFrame( IncrementFrame())


-- Frame 2809
SetFrame( IncrementFrame())


-- Frame 2810
SetFrame( IncrementFrame())


-- Frame 2811
SetFrame( IncrementFrame())


-- Frame 2812
SetFrame( IncrementFrame())


-- Frame 2813
SetFrame( IncrementFrame())


-- Frame 2814
SetFrame( IncrementFrame())


-- Frame 2815
SetFrame( IncrementFrame())


-- Frame 2816
SetFrame( IncrementFrame())


-- Frame 2817
SetFrame( IncrementFrame())


-- Frame 2818
SetFrame( IncrementFrame())


-- Frame 2819
SetFrame( IncrementFrame())


-- Frame 2820
SetFrame( IncrementFrame())


-- Frame 2821
SetFrame( IncrementFrame())


-- Frame 2822
SetFrame( IncrementFrame())


-- Frame 2823
SetFrame( IncrementFrame())


-- Frame 2824
SetFrame( IncrementFrame())


-- Frame 2825
SetFrame( IncrementFrame())


-- Frame 2826
SetFrame( IncrementFrame())


-- Frame 2827
SetFrame( IncrementFrame())


-- Frame 2828
SetFrame( IncrementFrame())


-- Frame 2829
SetFrame( IncrementFrame())


-- Frame 2830
SetFrame( IncrementFrame())


-- Frame 2831
SetFrame( IncrementFrame())


-- Frame 2832
SetFrame( IncrementFrame())


-- Frame 2833
SetFrame( IncrementFrame())


-- Frame 2834
SetFrame( IncrementFrame())


-- Frame 2835
SetFrame( IncrementFrame())


-- Frame 2836
SetFrame( IncrementFrame())


-- Frame 2837
SetFrame( IncrementFrame())


-- Frame 2838
SetFrame( IncrementFrame())


-- Frame 2839
SetFrame( IncrementFrame())


-- Frame 2840
SetFrame( IncrementFrame())


-- Frame 2841
SetFrame( IncrementFrame())


-- Frame 2842
SetFrame( IncrementFrame())


-- Frame 2843
SetFrame( IncrementFrame())


-- Frame 2844
SetFrame( IncrementFrame())


-- Frame 2845
SetFrame( IncrementFrame())


-- Frame 2846
SetFrame( IncrementFrame())


-- Frame 2847
SetFrame( IncrementFrame())


-- Frame 2848
SetFrame( IncrementFrame())


-- Frame 2849
SetFrame( IncrementFrame())


-- Frame 2850
SetFrame( IncrementFrame())


-- Frame 2851
SetFrame( IncrementFrame())


-- Frame 2852
SetFrame( IncrementFrame())


-- Frame 2853
SetFrame( IncrementFrame())


-- Frame 2854
SetFrame( IncrementFrame())


-- Frame 2855
SetFrame( IncrementFrame())


-- Frame 2856
SetFrame( IncrementFrame())


-- Frame 2857
SetFrame( IncrementFrame())


-- Frame 2858
SetFrame( IncrementFrame())


-- Frame 2859
SetFrame( IncrementFrame())


-- Frame 2860
SetFrame( IncrementFrame())


-- Frame 2861
SetFrame( IncrementFrame())


-- Frame 2862
SetFrame( IncrementFrame())


-- Frame 2863
SetFrame( IncrementFrame())


-- Frame 2864
SetFrame( IncrementFrame())


-- Frame 2865
SetFrame( IncrementFrame())


-- Frame 2866
SetFrame( IncrementFrame())


-- Frame 2867
SetFrame( IncrementFrame())


-- Frame 2868
SetFrame( IncrementFrame())


-- Frame 2869
SetFrame( IncrementFrame())


-- Frame 2870
SetFrame( IncrementFrame())


-- Frame 2871
SetFrame( IncrementFrame())


-- Frame 2872
SetFrame( IncrementFrame())


-- Frame 2873
SetFrame( IncrementFrame())


-- Frame 2874
SetFrame( IncrementFrame())


-- Frame 2875
SetFrame( IncrementFrame())


-- Frame 2876
SetFrame( IncrementFrame())


-- Frame 2877
SetFrame( IncrementFrame())


-- Frame 2878
SetFrame( IncrementFrame())


-- Frame 2879
SetFrame( IncrementFrame())


-- Frame 2880
SetFrame( IncrementFrame())


-- Frame 2881
SetFrame( IncrementFrame())


-- Frame 2882
SetFrame( IncrementFrame())


-- Frame 2883
SetFrame( IncrementFrame())


-- Frame 2884
SetFrame( IncrementFrame())


-- Frame 2885
SetFrame( IncrementFrame())


-- Frame 2886
SetFrame( IncrementFrame())


-- Frame 2887
SetFrame( IncrementFrame())


-- Frame 2888
SetFrame( IncrementFrame())


-- Frame 2889
SetFrame( IncrementFrame())


-- Frame 2890
SetFrame( IncrementFrame())

PlayAudio( ObjectObj_Tichondrius, "Act_AsYouWish", "Resources/WarCraftSounds/CS_06_Tichondrius.mp3", 1)

-- Frame 2891
SetFrame( IncrementFrame())


-- Frame 2892
SetFrame( IncrementFrame())


-- Frame 2893
SetFrame( IncrementFrame())


-- Frame 2894
SetFrame( IncrementFrame())


-- Frame 2895
SetFrame( IncrementFrame())


-- Frame 2896
SetFrame( IncrementFrame())


-- Frame 2897
SetFrame( IncrementFrame())


-- Frame 2898
SetFrame( IncrementFrame())


-- Frame 2899
SetFrame( IncrementFrame())


-- Frame 2900
SetFrame( IncrementFrame())


-- Frame 2901
SetFrame( IncrementFrame())


-- Frame 2902
SetFrame( IncrementFrame())


-- Frame 2903
SetFrame( IncrementFrame())


-- Frame 2904
SetFrame( IncrementFrame())


-- Frame 2905
SetFrame( IncrementFrame())


-- Frame 2906
SetFrame( IncrementFrame())


-- Frame 2907
SetFrame( IncrementFrame())


-- Frame 2908
SetFrame( IncrementFrame())


-- Frame 2909
SetFrame( IncrementFrame())


-- Frame 2910
SetFrame( IncrementFrame())


-- Frame 2911
SetFrame( IncrementFrame())


-- Frame 2912
SetFrame( IncrementFrame())


-- Frame 2913
SetFrame( IncrementFrame())


-- Frame 2914
SetFrame( IncrementFrame())


-- Frame 2915
SetFrame( IncrementFrame())


-- Frame 2916
SetFrame( IncrementFrame())


-- Frame 2917
SetFrame( IncrementFrame())


-- Frame 2918
SetFrame( IncrementFrame())


-- Frame 2919
SetFrame( IncrementFrame())


-- Frame 2920
SetFrame( IncrementFrame())


-- Frame 2921
SetFrame( IncrementFrame())


-- Frame 2922
SetFrame( IncrementFrame())


-- Frame 2923
SetFrame( IncrementFrame())


-- Frame 2924
SetFrame( IncrementFrame())


-- Frame 2925
SetFrame( IncrementFrame())


-- Frame 2926
SetFrame( IncrementFrame())


-- Frame 2927
SetFrame( IncrementFrame())


-- Frame 2928
SetFrame( IncrementFrame())


-- Frame 2929
SetFrame( IncrementFrame())


-- Frame 2930
SetFrame( IncrementFrame())


-- Frame 2931
SetFrame( IncrementFrame())


-- Frame 2932
SetFrame( IncrementFrame())


-- Frame 2933
SetFrame( IncrementFrame())


-- Frame 2934
SetFrame( IncrementFrame())


-- Frame 2935
SetFrame( IncrementFrame())


-- Frame 2936
SetFrame( IncrementFrame())


-- Frame 2937
SetFrame( IncrementFrame())


-- Frame 2938
SetFrame( IncrementFrame())


-- Frame 2939
SetFrame( IncrementFrame())


-- Frame 2940
SetFrame( IncrementFrame())


-- Frame 2941
SetFrame( IncrementFrame())


-- Frame 2942
SetFrame( IncrementFrame())


-- Frame 2943
SetFrame( IncrementFrame())


-- Frame 2944
SetFrame( IncrementFrame())


-- Frame 2945
SetFrame( IncrementFrame())


-- Frame 2946
SetFrame( IncrementFrame())


-- Frame 2947
SetFrame( IncrementFrame())


-- Frame 2948
SetFrame( IncrementFrame())


-- Frame 2949
SetFrame( IncrementFrame())


-- Frame 2950
SetFrame( IncrementFrame())


-- Frame 2951
SetFrame( IncrementFrame())


-- Frame 2952
SetFrame( IncrementFrame())


-- Frame 2953
SetFrame( IncrementFrame())


-- Frame 2954
SetFrame( IncrementFrame())


-- Frame 2955
SetFrame( IncrementFrame())


-- Frame 2956
SetFrame( IncrementFrame())


-- Frame 2957
SetFrame( IncrementFrame())


-- Frame 2958
SetFrame( IncrementFrame())


-- Frame 2959
SetFrame( IncrementFrame())


-- Frame 2960
SetFrame( IncrementFrame())


-- Frame 2961
SetFrame( IncrementFrame())


-- Frame 2962
SetFrame( IncrementFrame())


-- Frame 2963
SetFrame( IncrementFrame())


-- Frame 2964
SetFrame( IncrementFrame())


-- Frame 2965
SetFrame( IncrementFrame())


-- Frame 2966
SetFrame( IncrementFrame())


-- Frame 2967
SetFrame( IncrementFrame())


-- Frame 2968
SetFrame( IncrementFrame())


-- Frame 2969
SetFrame( IncrementFrame())


-- Frame 2970
SetFrame( IncrementFrame())


-- Frame 2971
SetFrame( IncrementFrame())


-- Frame 2972
SetFrame( IncrementFrame())


-- Frame 2973
SetFrame( IncrementFrame())


-- Frame 2974
SetFrame( IncrementFrame())


-- Frame 2975
SetFrame( IncrementFrame())


-- Frame 2976
SetFrame( IncrementFrame())


-- Frame 2977
SetFrame( IncrementFrame())


-- Frame 2978
SetFrame( IncrementFrame())


-- Frame 2979
SetFrame( IncrementFrame())


-- Frame 2980
SetFrame( IncrementFrame())


-- Frame 2981
SetFrame( IncrementFrame())


-- Frame 2982
SetFrame( IncrementFrame())


-- Frame 2983
SetFrame( IncrementFrame())


-- Frame 2984
SetFrame( IncrementFrame())


-- Frame 2985
SetFrame( IncrementFrame())


-- Frame 2986
SetFrame( IncrementFrame())


-- Frame 2987
SetFrame( IncrementFrame())


-- Frame 2988
SetFrame( IncrementFrame())


-- Frame 2989
SetFrame( IncrementFrame())


-- Frame 2990
SetFrame( IncrementFrame())


-- Frame 2991
SetFrame( IncrementFrame())


-- Frame 2992
SetFrame( IncrementFrame())


-- Frame 2993
SetFrame( IncrementFrame())


-- Frame 2994
SetFrame( IncrementFrame())


-- Frame 2995
SetFrame( IncrementFrame())


-- Frame 2996
SetFrame( IncrementFrame())


-- Frame 2997
SetFrame( IncrementFrame())


-- Frame 2998
SetFrame( IncrementFrame())


-- Frame 2999
SetFrame( IncrementFrame())


-- Frame 3000
SetFrame( IncrementFrame())


-- Frame 3001
SetFrame( IncrementFrame())


-- Frame 3002
SetFrame( IncrementFrame())


-- Frame 3003
SetFrame( IncrementFrame())


-- Frame 3004
SetFrame( IncrementFrame())


-- Frame 3005
SetFrame( IncrementFrame())


-- Frame 3006
SetFrame( IncrementFrame())


-- Frame 3007
SetFrame( IncrementFrame())


-- Frame 3008
SetFrame( IncrementFrame())


-- Frame 3009
SetFrame( IncrementFrame())


-- Frame 3010
SetFrame( IncrementFrame())


-- Frame 3011
SetFrame( IncrementFrame())


-- Frame 3012
SetFrame( IncrementFrame())


-- Frame 3013
SetFrame( IncrementFrame())


-- Frame 3014
SetFrame( IncrementFrame())


-- Frame 3015
SetFrame( IncrementFrame())


-- Frame 3016
SetFrame( IncrementFrame())


-- Frame 3017
SetFrame( IncrementFrame())


-- Frame 3018
SetFrame( IncrementFrame())


-- Frame 3019
SetFrame( IncrementFrame())


-- Frame 3020
SetFrame( IncrementFrame())


-- Frame 3021
SetFrame( IncrementFrame())


-- Frame 3022
SetFrame( IncrementFrame())


-- Frame 3023
SetFrame( IncrementFrame())


-- Frame 3024
SetFrame( IncrementFrame())


-- Frame 3025
SetFrame( IncrementFrame())


-- Frame 3026
SetFrame( IncrementFrame())


-- Frame 3027
SetFrame( IncrementFrame())


-- Frame 3028
SetFrame( IncrementFrame())


-- Frame 3029
SetFrame( IncrementFrame())


-- Frame 3030
SetFrame( IncrementFrame())


-- Frame 3031
SetFrame( IncrementFrame())


-- Frame 3032
SetFrame( IncrementFrame())


-- Frame 3033
SetFrame( IncrementFrame())


-- Frame 3034
SetFrame( IncrementFrame())


-- Frame 3035
SetFrame( IncrementFrame())


-- Frame 3036
SetFrame( IncrementFrame())


-- Frame 3037
SetFrame( IncrementFrame())


-- Frame 3038
SetFrame( IncrementFrame())


-- Frame 3039
SetFrame( IncrementFrame())


-- Frame 3040
SetFrame( IncrementFrame())


-- Frame 3041
SetFrame( IncrementFrame())


-- Frame 3042
SetFrame( IncrementFrame())


-- Frame 3043
SetFrame( IncrementFrame())


-- Frame 3044
SetFrame( IncrementFrame())


-- Frame 3045
SetFrame( IncrementFrame())


-- Frame 3046
SetFrame( IncrementFrame())


-- Frame 3047
SetFrame( IncrementFrame())


-- Frame 3048
SetFrame( IncrementFrame())


-- Frame 3049
SetFrame( IncrementFrame())


-- Frame 3050
SetFrame( IncrementFrame())


-- Frame 3051
SetFrame( IncrementFrame())


-- Frame 3052
SetFrame( IncrementFrame())


-- Frame 3053
SetFrame( IncrementFrame())


-- Frame 3054
SetFrame( IncrementFrame())


-- Frame 3055
SetFrame( IncrementFrame())


-- Frame 3056
SetFrame( IncrementFrame())


-- Frame 3057
SetFrame( IncrementFrame())


-- Frame 3058
SetFrame( IncrementFrame())


-- Frame 3059
SetFrame( IncrementFrame())


-- Frame 3060
SetFrame( IncrementFrame())


-- Frame 3061
SetFrame( IncrementFrame())


-- Frame 3062
SetFrame( IncrementFrame())


-- Frame 3063
SetFrame( IncrementFrame())


-- Frame 3064
SetFrame( IncrementFrame())


-- Frame 3065
SetFrame( IncrementFrame())


-- Frame 3066
SetFrame( IncrementFrame())


-- Frame 3067
SetFrame( IncrementFrame())


-- Frame 3068
SetFrame( IncrementFrame())


-- Frame 3069
SetFrame( IncrementFrame())


-- Frame 3070
SetFrame( IncrementFrame())


-- Frame 3071
SetFrame( IncrementFrame())


-- Frame 3072
SetFrame( IncrementFrame())


-- Frame 3073
SetFrame( IncrementFrame())


-- Frame 3074
SetFrame( IncrementFrame())


-- Frame 3075
SetFrame( IncrementFrame())


-- Frame 3076
SetFrame( IncrementFrame())


-- Frame 3077
SetFrame( IncrementFrame())


-- Frame 3078
SetFrame( IncrementFrame())


-- Frame 3079
SetFrame( IncrementFrame())


-- Frame 3080
SetFrame( IncrementFrame())


-- Frame 3081
SetFrame( IncrementFrame())


-- Frame 3082
SetFrame( IncrementFrame())


-- Frame 3083
SetFrame( IncrementFrame())


-- Frame 3084
SetFrame( IncrementFrame())


-- Frame 3085
SetFrame( IncrementFrame())


-- Frame 3086
SetFrame( IncrementFrame())


-- Frame 3087
SetFrame( IncrementFrame())


-- Frame 3088
SetFrame( IncrementFrame())


-- Frame 3089
SetFrame( IncrementFrame())


-- Frame 3090
SetFrame( IncrementFrame())


-- Frame 3091
SetFrame( IncrementFrame())


-- Frame 3092
SetFrame( IncrementFrame())


-- Frame 3093
SetFrame( IncrementFrame())


-- Frame 3094
SetFrame( IncrementFrame())


-- Frame 3095
SetFrame( IncrementFrame())


-- Frame 3096
SetFrame( IncrementFrame())


-- Frame 3097
SetFrame( IncrementFrame())


-- Frame 3098
SetFrame( IncrementFrame())


-- Frame 3099
SetFrame( IncrementFrame())


-- Frame 3100
SetFrame( IncrementFrame())

LinkCamThirdPerson( ObjectObj_Camera, "Act_BehindArchimonde", "Obj_ArchFrontCam")
PlayAudio( ObjectObj_Archimonde, "Act_SoonInvasion", "Resources/WarCraftSounds/CS_07_Archimonde.mp3", 1)

-- Frame 3101
SetFrame( IncrementFrame())


-- Frame 3102
SetFrame( IncrementFrame())


-- Frame 3103
SetFrame( IncrementFrame())


-- Frame 3104
SetFrame( IncrementFrame())


-- Frame 3105
SetFrame( IncrementFrame())


-- Frame 3106
SetFrame( IncrementFrame())


-- Frame 3107
SetFrame( IncrementFrame())


-- Frame 3108
SetFrame( IncrementFrame())


-- Frame 3109
SetFrame( IncrementFrame())


-- Frame 3110
SetFrame( IncrementFrame())


-- Frame 3111
SetFrame( IncrementFrame())


-- Frame 3112
SetFrame( IncrementFrame())


-- Frame 3113
SetFrame( IncrementFrame())


-- Frame 3114
SetFrame( IncrementFrame())


-- Frame 3115
SetFrame( IncrementFrame())


-- Frame 3116
SetFrame( IncrementFrame())


-- Frame 3117
SetFrame( IncrementFrame())


-- Frame 3118
SetFrame( IncrementFrame())


-- Frame 3119
SetFrame( IncrementFrame())


-- Frame 3120
SetFrame( IncrementFrame())


-- Frame 3121
SetFrame( IncrementFrame())


-- Frame 3122
SetFrame( IncrementFrame())


-- Frame 3123
SetFrame( IncrementFrame())


-- Frame 3124
SetFrame( IncrementFrame())


-- Frame 3125
SetFrame( IncrementFrame())


-- Frame 3126
SetFrame( IncrementFrame())


-- Frame 3127
SetFrame( IncrementFrame())


-- Frame 3128
SetFrame( IncrementFrame())


-- Frame 3129
SetFrame( IncrementFrame())


-- Frame 3130
SetFrame( IncrementFrame())


-- Frame 3131
SetFrame( IncrementFrame())


-- Frame 3132
SetFrame( IncrementFrame())


-- Frame 3133
SetFrame( IncrementFrame())


-- Frame 3134
SetFrame( IncrementFrame())


-- Frame 3135
SetFrame( IncrementFrame())


-- Frame 3136
SetFrame( IncrementFrame())


-- Frame 3137
SetFrame( IncrementFrame())


-- Frame 3138
SetFrame( IncrementFrame())


-- Frame 3139
SetFrame( IncrementFrame())


-- Frame 3140
SetFrame( IncrementFrame())


-- Frame 3141
SetFrame( IncrementFrame())


-- Frame 3142
SetFrame( IncrementFrame())


-- Frame 3143
SetFrame( IncrementFrame())


-- Frame 3144
SetFrame( IncrementFrame())


-- Frame 3145
SetFrame( IncrementFrame())


-- Frame 3146
SetFrame( IncrementFrame())


-- Frame 3147
SetFrame( IncrementFrame())


-- Frame 3148
SetFrame( IncrementFrame())


-- Frame 3149
SetFrame( IncrementFrame())


-- Frame 3150
SetFrame( IncrementFrame())


-- Frame 3151
SetFrame( IncrementFrame())


-- Frame 3152
SetFrame( IncrementFrame())


-- Frame 3153
SetFrame( IncrementFrame())


-- Frame 3154
SetFrame( IncrementFrame())


-- Frame 3155
SetFrame( IncrementFrame())


-- Frame 3156
SetFrame( IncrementFrame())


-- Frame 3157
SetFrame( IncrementFrame())


-- Frame 3158
SetFrame( IncrementFrame())


-- Frame 3159
SetFrame( IncrementFrame())


-- Frame 3160
SetFrame( IncrementFrame())


-- Frame 3161
SetFrame( IncrementFrame())


-- Frame 3162
SetFrame( IncrementFrame())


-- Frame 3163
SetFrame( IncrementFrame())


-- Frame 3164
SetFrame( IncrementFrame())


-- Frame 3165
SetFrame( IncrementFrame())


-- Frame 3166
SetFrame( IncrementFrame())


-- Frame 3167
SetFrame( IncrementFrame())


-- Frame 3168
SetFrame( IncrementFrame())


-- Frame 3169
SetFrame( IncrementFrame())


-- Frame 3170
SetFrame( IncrementFrame())


-- Frame 3171
SetFrame( IncrementFrame())


-- Frame 3172
SetFrame( IncrementFrame())


-- Frame 3173
SetFrame( IncrementFrame())


-- Frame 3174
SetFrame( IncrementFrame())


-- Frame 3175
SetFrame( IncrementFrame())


-- Frame 3176
SetFrame( IncrementFrame())


-- Frame 3177
SetFrame( IncrementFrame())


-- Frame 3178
SetFrame( IncrementFrame())


-- Frame 3179
SetFrame( IncrementFrame())


-- Frame 3180
SetFrame( IncrementFrame())


-- Frame 3181
SetFrame( IncrementFrame())


-- Frame 3182
SetFrame( IncrementFrame())


-- Frame 3183
SetFrame( IncrementFrame())


-- Frame 3184
SetFrame( IncrementFrame())


-- Frame 3185
SetFrame( IncrementFrame())


-- Frame 3186
SetFrame( IncrementFrame())


-- Frame 3187
SetFrame( IncrementFrame())


-- Frame 3188
SetFrame( IncrementFrame())


-- Frame 3189
SetFrame( IncrementFrame())


-- Frame 3190
SetFrame( IncrementFrame())


-- Frame 3191
SetFrame( IncrementFrame())


-- Frame 3192
SetFrame( IncrementFrame())


-- Frame 3193
SetFrame( IncrementFrame())


-- Frame 3194
SetFrame( IncrementFrame())


-- Frame 3195
SetFrame( IncrementFrame())


-- Frame 3196
SetFrame( IncrementFrame())


-- Frame 3197
SetFrame( IncrementFrame())


-- Frame 3198
SetFrame( IncrementFrame())


-- Frame 3199
SetFrame( IncrementFrame())


-- Frame 3200
SetFrame( IncrementFrame())


-- Frame 3201
SetFrame( IncrementFrame())


-- Frame 3202
SetFrame( IncrementFrame())


-- Frame 3203
SetFrame( IncrementFrame())


-- Frame 3204
SetFrame( IncrementFrame())


-- Frame 3205
SetFrame( IncrementFrame())


-- Frame 3206
SetFrame( IncrementFrame())


-- Frame 3207
SetFrame( IncrementFrame())


-- Frame 3208
SetFrame( IncrementFrame())


-- Frame 3209
SetFrame( IncrementFrame())


-- Frame 3210
SetFrame( IncrementFrame())


-- Frame 3211
SetFrame( IncrementFrame())


-- Frame 3212
SetFrame( IncrementFrame())


-- Frame 3213
SetFrame( IncrementFrame())


-- Frame 3214
SetFrame( IncrementFrame())


-- Frame 3215
SetFrame( IncrementFrame())


-- Frame 3216
SetFrame( IncrementFrame())


-- Frame 3217
SetFrame( IncrementFrame())


-- Frame 3218
SetFrame( IncrementFrame())


-- Frame 3219
SetFrame( IncrementFrame())


-- Frame 3220
SetFrame( IncrementFrame())


-- Frame 3221
SetFrame( IncrementFrame())


-- Frame 3222
SetFrame( IncrementFrame())


-- Frame 3223
SetFrame( IncrementFrame())


-- Frame 3224
SetFrame( IncrementFrame())


-- Frame 3225
SetFrame( IncrementFrame())


-- Frame 3226
SetFrame( IncrementFrame())


-- Frame 3227
SetFrame( IncrementFrame())


-- Frame 3228
SetFrame( IncrementFrame())


-- Frame 3229
SetFrame( IncrementFrame())


-- Frame 3230
SetFrame( IncrementFrame())


-- Frame 3231
SetFrame( IncrementFrame())


-- Frame 3232
SetFrame( IncrementFrame())


-- Frame 3233
SetFrame( IncrementFrame())


-- Frame 3234
SetFrame( IncrementFrame())


-- Frame 3235
SetFrame( IncrementFrame())


-- Frame 3236
SetFrame( IncrementFrame())


-- Frame 3237
SetFrame( IncrementFrame())


-- Frame 3238
SetFrame( IncrementFrame())


-- Frame 3239
SetFrame( IncrementFrame())


-- Frame 3240
SetFrame( IncrementFrame())


-- Frame 3241
SetFrame( IncrementFrame())


-- Frame 3242
SetFrame( IncrementFrame())


-- Frame 3243
SetFrame( IncrementFrame())


-- Frame 3244
SetFrame( IncrementFrame())


-- Frame 3245
SetFrame( IncrementFrame())


-- Frame 3246
SetFrame( IncrementFrame())


-- Frame 3247
SetFrame( IncrementFrame())


-- Frame 3248
SetFrame( IncrementFrame())


-- Frame 3249
SetFrame( IncrementFrame())


-- Frame 3250
SetFrame( IncrementFrame())


-- Frame 3251
SetFrame( IncrementFrame())


-- Frame 3252
SetFrame( IncrementFrame())


-- Frame 3253
SetFrame( IncrementFrame())


-- Frame 3254
SetFrame( IncrementFrame())


-- Frame 3255
SetFrame( IncrementFrame())


-- Frame 3256
SetFrame( IncrementFrame())


-- Frame 3257
SetFrame( IncrementFrame())


-- Frame 3258
SetFrame( IncrementFrame())


-- Frame 3259
SetFrame( IncrementFrame())


-- Frame 3260
SetFrame( IncrementFrame())


-- Frame 3261
SetFrame( IncrementFrame())


-- Frame 3262
SetFrame( IncrementFrame())


-- Frame 3263
SetFrame( IncrementFrame())


-- Frame 3264
SetFrame( IncrementFrame())


-- Frame 3265
SetFrame( IncrementFrame())


-- Frame 3266
SetFrame( IncrementFrame())


-- Frame 3267
SetFrame( IncrementFrame())


-- Frame 3268
SetFrame( IncrementFrame())


-- Frame 3269
SetFrame( IncrementFrame())


-- Frame 3270
SetFrame( IncrementFrame())


-- Frame 3271
SetFrame( IncrementFrame())


-- Frame 3272
SetFrame( IncrementFrame())


-- Frame 3273
SetFrame( IncrementFrame())


-- Frame 3274
SetFrame( IncrementFrame())


-- Frame 3275
SetFrame( IncrementFrame())


-- Frame 3276
SetFrame( IncrementFrame())


-- Frame 3277
SetFrame( IncrementFrame())


-- Frame 3278
SetFrame( IncrementFrame())


-- Frame 3279
SetFrame( IncrementFrame())


-- Frame 3280
SetFrame( IncrementFrame())


-- Frame 3281
SetFrame( IncrementFrame())


-- Frame 3282
SetFrame( IncrementFrame())


-- Frame 3283
SetFrame( IncrementFrame())


-- Frame 3284
SetFrame( IncrementFrame())


-- Frame 3285
SetFrame( IncrementFrame())


-- Frame 3286
SetFrame( IncrementFrame())


-- Frame 3287
SetFrame( IncrementFrame())


-- Frame 3288
SetFrame( IncrementFrame())


-- Frame 3289
SetFrame( IncrementFrame())


-- Frame 3290
SetFrame( IncrementFrame())


-- Frame 3291
SetFrame( IncrementFrame())


-- Frame 3292
SetFrame( IncrementFrame())


-- Frame 3293
SetFrame( IncrementFrame())


-- Frame 3294
SetFrame( IncrementFrame())


-- Frame 3295
SetFrame( IncrementFrame())


-- Frame 3296
SetFrame( IncrementFrame())


-- Frame 3297
SetFrame( IncrementFrame())


-- Frame 3298
SetFrame( IncrementFrame())


-- Frame 3299
SetFrame( IncrementFrame())


-- Frame 3300
SetFrame( IncrementFrame())


-- Frame 3301
SetFrame( IncrementFrame())


-- Frame 3302
SetFrame( IncrementFrame())


-- Frame 3303
SetFrame( IncrementFrame())


-- Frame 3304
SetFrame( IncrementFrame())


-- Frame 3305
SetFrame( IncrementFrame())


-- Frame 3306
SetFrame( IncrementFrame())


-- Frame 3307
SetFrame( IncrementFrame())


-- Frame 3308
SetFrame( IncrementFrame())


-- Frame 3309
SetFrame( IncrementFrame())


-- Frame 3310
SetFrame( IncrementFrame())


-- Frame 3311
SetFrame( IncrementFrame())


-- Frame 3312
SetFrame( IncrementFrame())


-- Frame 3313
SetFrame( IncrementFrame())


-- Frame 3314
SetFrame( IncrementFrame())


-- Frame 3315
SetFrame( IncrementFrame())


-- Frame 3316
SetFrame( IncrementFrame())


-- Frame 3317
SetFrame( IncrementFrame())


-- Frame 3318
SetFrame( IncrementFrame())


-- Frame 3319
SetFrame( IncrementFrame())


-- Frame 3320
SetFrame( IncrementFrame())


-- Frame 3321
SetFrame( IncrementFrame())


-- Frame 3322
SetFrame( IncrementFrame())


-- Frame 3323
SetFrame( IncrementFrame())


-- Frame 3324
SetFrame( IncrementFrame())


-- Frame 3325
SetFrame( IncrementFrame())


-- Frame 3326
SetFrame( IncrementFrame())


-- Frame 3327
SetFrame( IncrementFrame())


-- Frame 3328
SetFrame( IncrementFrame())


-- Frame 3329
SetFrame( IncrementFrame())


-- Frame 3330
SetFrame( IncrementFrame())


-- Frame 3331
SetFrame( IncrementFrame())


-- Frame 3332
SetFrame( IncrementFrame())


-- Frame 3333
SetFrame( IncrementFrame())


-- Frame 3334
SetFrame( IncrementFrame())


-- Frame 3335
SetFrame( IncrementFrame())


-- Frame 3336
SetFrame( IncrementFrame())


-- Frame 3337
SetFrame( IncrementFrame())


-- Frame 3338
SetFrame( IncrementFrame())


-- Frame 3339
SetFrame( IncrementFrame())


-- Frame 3340
SetFrame( IncrementFrame())


-- Frame 3341
SetFrame( IncrementFrame())


-- Frame 3342
SetFrame( IncrementFrame())


-- Frame 3343
SetFrame( IncrementFrame())


-- Frame 3344
SetFrame( IncrementFrame())


-- Frame 3345
SetFrame( IncrementFrame())


-- Frame 3346
SetFrame( IncrementFrame())


-- Frame 3347
SetFrame( IncrementFrame())


-- Frame 3348
SetFrame( IncrementFrame())


-- Frame 3349
SetFrame( IncrementFrame())


-- Frame 3350
SetFrame( IncrementFrame())


-- Frame 3351
SetFrame( IncrementFrame())


-- Frame 3352
SetFrame( IncrementFrame())


-- Frame 3353
SetFrame( IncrementFrame())


-- Frame 3354
SetFrame( IncrementFrame())


-- Frame 3355
SetFrame( IncrementFrame())


-- Frame 3356
SetFrame( IncrementFrame())


-- Frame 3357
SetFrame( IncrementFrame())


-- Frame 3358
SetFrame( IncrementFrame())


-- Frame 3359
SetFrame( IncrementFrame())


-- Frame 3360
SetFrame( IncrementFrame())


-- Frame 3361
SetFrame( IncrementFrame())


-- Frame 3362
SetFrame( IncrementFrame())


-- Frame 3363
SetFrame( IncrementFrame())


-- Frame 3364
SetFrame( IncrementFrame())


-- Frame 3365
SetFrame( IncrementFrame())


-- Frame 3366
SetFrame( IncrementFrame())


-- Frame 3367
SetFrame( IncrementFrame())


-- Frame 3368
SetFrame( IncrementFrame())


-- Frame 3369
SetFrame( IncrementFrame())


-- Frame 3370
SetFrame( IncrementFrame())


-- Frame 3371
SetFrame( IncrementFrame())


-- Frame 3372
SetFrame( IncrementFrame())


-- Frame 3373
SetFrame( IncrementFrame())


-- Frame 3374
SetFrame( IncrementFrame())


-- Frame 3375
SetFrame( IncrementFrame())


-- Frame 3376
SetFrame( IncrementFrame())


-- Frame 3377
SetFrame( IncrementFrame())


-- Frame 3378
SetFrame( IncrementFrame())


-- Frame 3379
SetFrame( IncrementFrame())


-- Frame 3380
SetFrame( IncrementFrame())


-- Frame 3381
SetFrame( IncrementFrame())


-- Frame 3382
SetFrame( IncrementFrame())


-- Frame 3383
SetFrame( IncrementFrame())


-- Frame 3384
SetFrame( IncrementFrame())


-- Frame 3385
SetFrame( IncrementFrame())


-- Frame 3386
SetFrame( IncrementFrame())


-- Frame 3387
SetFrame( IncrementFrame())


-- Frame 3388
SetFrame( IncrementFrame())


-- Frame 3389
SetFrame( IncrementFrame())


-- Frame 3390
SetFrame( IncrementFrame())


-- Frame 3391
SetFrame( IncrementFrame())


-- Frame 3392
SetFrame( IncrementFrame())


-- Frame 3393
SetFrame( IncrementFrame())


-- Frame 3394
SetFrame( IncrementFrame())


-- Frame 3395
SetFrame( IncrementFrame())


-- Frame 3396
SetFrame( IncrementFrame())


-- Frame 3397
SetFrame( IncrementFrame())


-- Frame 3398
SetFrame( IncrementFrame())


-- Frame 3399
SetFrame( IncrementFrame())


-- Frame 3400
SetFrame( IncrementFrame())


-- Frame 3401
SetFrame( IncrementFrame())


-- Frame 3402
SetFrame( IncrementFrame())


-- Frame 3403
SetFrame( IncrementFrame())


-- Frame 3404
SetFrame( IncrementFrame())


-- Frame 3405
SetFrame( IncrementFrame())


-- Frame 3406
SetFrame( IncrementFrame())


-- Frame 3407
SetFrame( IncrementFrame())


-- Frame 3408
SetFrame( IncrementFrame())


-- Frame 3409
SetFrame( IncrementFrame())


-- Frame 3410
SetFrame( IncrementFrame())


-- Frame 3411
SetFrame( IncrementFrame())


-- Frame 3412
SetFrame( IncrementFrame())


-- Frame 3413
SetFrame( IncrementFrame())


-- Frame 3414
SetFrame( IncrementFrame())


-- Frame 3415
SetFrame( IncrementFrame())


-- Frame 3416
SetFrame( IncrementFrame())


-- Frame 3417
SetFrame( IncrementFrame())


-- Frame 3418
SetFrame( IncrementFrame())


-- Frame 3419
SetFrame( IncrementFrame())


-- Frame 3420
SetFrame( IncrementFrame())


-- Frame 3421
SetFrame( IncrementFrame())


-- Frame 3422
SetFrame( IncrementFrame())


-- Frame 3423
SetFrame( IncrementFrame())


-- Frame 3424
SetFrame( IncrementFrame())


-- Frame 3425
SetFrame( IncrementFrame())


-- Frame 3426
SetFrame( IncrementFrame())


-- Frame 3427
SetFrame( IncrementFrame())


-- Frame 3428
SetFrame( IncrementFrame())


-- Frame 3429
SetFrame( IncrementFrame())


-- Frame 3430
SetFrame( IncrementFrame())


-- Frame 3431
SetFrame( IncrementFrame())


-- Frame 3432
SetFrame( IncrementFrame())


-- Frame 3433
SetFrame( IncrementFrame())


-- Frame 3434
SetFrame( IncrementFrame())


-- Frame 3435
SetFrame( IncrementFrame())


-- Frame 3436
SetFrame( IncrementFrame())


-- Frame 3437
SetFrame( IncrementFrame())


-- Frame 3438
SetFrame( IncrementFrame())


-- Frame 3439
SetFrame( IncrementFrame())


-- Frame 3440
SetFrame( IncrementFrame())


-- Frame 3441
SetFrame( IncrementFrame())


-- Frame 3442
SetFrame( IncrementFrame())


-- Frame 3443
SetFrame( IncrementFrame())


-- Frame 3444
SetFrame( IncrementFrame())


-- Frame 3445
SetFrame( IncrementFrame())


-- Frame 3446
SetFrame( IncrementFrame())


-- Frame 3447
SetFrame( IncrementFrame())


-- Frame 3448
SetFrame( IncrementFrame())


-- Frame 3449
SetFrame( IncrementFrame())


-- Frame 3450
SetFrame( IncrementFrame())


-- Frame 3451
SetFrame( IncrementFrame())


-- Frame 3452
SetFrame( IncrementFrame())


-- Frame 3453
SetFrame( IncrementFrame())


-- Frame 3454
SetFrame( IncrementFrame())


-- Frame 3455
SetFrame( IncrementFrame())


-- Frame 3456
SetFrame( IncrementFrame())


-- Frame 3457
SetFrame( IncrementFrame())


-- Frame 3458
SetFrame( IncrementFrame())


-- Frame 3459
SetFrame( IncrementFrame())


-- Frame 3460
SetFrame( IncrementFrame())


-- Frame 3461
SetFrame( IncrementFrame())


-- Frame 3462
SetFrame( IncrementFrame())


-- Frame 3463
SetFrame( IncrementFrame())


-- Frame 3464
SetFrame( IncrementFrame())


-- Frame 3465
SetFrame( IncrementFrame())


-- Frame 3466
SetFrame( IncrementFrame())


-- Frame 3467
SetFrame( IncrementFrame())


-- Frame 3468
SetFrame( IncrementFrame())


-- Frame 3469
SetFrame( IncrementFrame())


-- Frame 3470
SetFrame( IncrementFrame())


-- Frame 3471
SetFrame( IncrementFrame())


-- Frame 3472
SetFrame( IncrementFrame())


-- Frame 3473
SetFrame( IncrementFrame())


-- Frame 3474
SetFrame( IncrementFrame())


-- Frame 3475
SetFrame( IncrementFrame())


-- Frame 3476
SetFrame( IncrementFrame())


-- Frame 3477
SetFrame( IncrementFrame())


-- Frame 3478
SetFrame( IncrementFrame())


-- Frame 3479
SetFrame( IncrementFrame())


-- Frame 3480
SetFrame( IncrementFrame())


-- Frame 3481
SetFrame( IncrementFrame())


-- Frame 3482
SetFrame( IncrementFrame())


-- Frame 3483
SetFrame( IncrementFrame())


-- Frame 3484
SetFrame( IncrementFrame())


-- Frame 3485
SetFrame( IncrementFrame())


-- Frame 3486
SetFrame( IncrementFrame())


-- Frame 3487
SetFrame( IncrementFrame())


-- Frame 3488
SetFrame( IncrementFrame())


-- Frame 3489
SetFrame( IncrementFrame())


-- Frame 3490
SetFrame( IncrementFrame())


-- Frame 3491
SetFrame( IncrementFrame())


-- Frame 3492
SetFrame( IncrementFrame())


-- Frame 3493
SetFrame( IncrementFrame())


-- Frame 3494
SetFrame( IncrementFrame())


-- Frame 3495
SetFrame( IncrementFrame())


-- Frame 3496
SetFrame( IncrementFrame())


-- Frame 3497
SetFrame( IncrementFrame())


-- Frame 3498
SetFrame( IncrementFrame())


-- Frame 3499
SetFrame( IncrementFrame())


-- Frame 3500
SetFrame( IncrementFrame())


-- Frame 3501
SetFrame( IncrementFrame())


-- Frame 3502
SetFrame( IncrementFrame())


-- Frame 3503
SetFrame( IncrementFrame())


-- Frame 3504
SetFrame( IncrementFrame())


-- Frame 3505
SetFrame( IncrementFrame())


-- Frame 3506
SetFrame( IncrementFrame())


-- Frame 3507
SetFrame( IncrementFrame())


-- Frame 3508
SetFrame( IncrementFrame())


-- Frame 3509
SetFrame( IncrementFrame())


-- Frame 3510
SetFrame( IncrementFrame())


-- Frame 3511
SetFrame( IncrementFrame())


-- Frame 3512
SetFrame( IncrementFrame())


-- Frame 3513
SetFrame( IncrementFrame())


-- Frame 3514
SetFrame( IncrementFrame())


-- Frame 3515
SetFrame( IncrementFrame())


-- Frame 3516
SetFrame( IncrementFrame())


-- Frame 3517
SetFrame( IncrementFrame())


-- Frame 3518
SetFrame( IncrementFrame())


-- Frame 3519
SetFrame( IncrementFrame())


-- Frame 3520
SetFrame( IncrementFrame())


-- Frame 3521
SetFrame( IncrementFrame())


-- Frame 3522
SetFrame( IncrementFrame())


-- Frame 3523
SetFrame( IncrementFrame())


-- Frame 3524
SetFrame( IncrementFrame())


-- Frame 3525
SetFrame( IncrementFrame())


-- Frame 3526
SetFrame( IncrementFrame())


-- Frame 3527
SetFrame( IncrementFrame())


-- Frame 3528
SetFrame( IncrementFrame())


-- Frame 3529
SetFrame( IncrementFrame())


-- Frame 3530
SetFrame( IncrementFrame())


-- Frame 3531
SetFrame( IncrementFrame())


-- Frame 3532
SetFrame( IncrementFrame())


-- Frame 3533
SetFrame( IncrementFrame())


-- Frame 3534
SetFrame( IncrementFrame())


-- Frame 3535
SetFrame( IncrementFrame())


-- Frame 3536
SetFrame( IncrementFrame())


-- Frame 3537
SetFrame( IncrementFrame())


-- Frame 3538
SetFrame( IncrementFrame())


-- Frame 3539
SetFrame( IncrementFrame())


-- Frame 3540
SetFrame( IncrementFrame())


-- Frame 3541
SetFrame( IncrementFrame())


-- Frame 3542
SetFrame( IncrementFrame())


-- Frame 3543
SetFrame( IncrementFrame())


-- Frame 3544
SetFrame( IncrementFrame())


-- Frame 3545
SetFrame( IncrementFrame())


-- Frame 3546
SetFrame( IncrementFrame())


-- Frame 3547
SetFrame( IncrementFrame())


-- Frame 3548
SetFrame( IncrementFrame())


-- Frame 3549
SetFrame( IncrementFrame())


-- Frame 3550
SetFrame( IncrementFrame())


-- Frame 3551
SetFrame( IncrementFrame())


-- Frame 3552
SetFrame( IncrementFrame())


-- Frame 3553
SetFrame( IncrementFrame())


-- Frame 3554
SetFrame( IncrementFrame())


-- Frame 3555
SetFrame( IncrementFrame())


-- Frame 3556
SetFrame( IncrementFrame())


-- Frame 3557
SetFrame( IncrementFrame())


-- Frame 3558
SetFrame( IncrementFrame())


-- Frame 3559
SetFrame( IncrementFrame())


-- Frame 3560
SetFrame( IncrementFrame())


-- Frame 3561
SetFrame( IncrementFrame())


-- Frame 3562
SetFrame( IncrementFrame())


-- Frame 3563
SetFrame( IncrementFrame())


-- Frame 3564
SetFrame( IncrementFrame())


-- Frame 3565
SetFrame( IncrementFrame())


-- Frame 3566
SetFrame( IncrementFrame())


-- Frame 3567
SetFrame( IncrementFrame())


-- Frame 3568
SetFrame( IncrementFrame())


-- Frame 3569
SetFrame( IncrementFrame())


-- Frame 3570
SetFrame( IncrementFrame())


-- Frame 3571
SetFrame( IncrementFrame())


-- Frame 3572
SetFrame( IncrementFrame())


-- Frame 3573
SetFrame( IncrementFrame())


-- Frame 3574
SetFrame( IncrementFrame())


-- Frame 3575
SetFrame( IncrementFrame())


-- Frame 3576
SetFrame( IncrementFrame())


-- Frame 3577
SetFrame( IncrementFrame())


-- Frame 3578
SetFrame( IncrementFrame())


-- Frame 3579
SetFrame( IncrementFrame())


-- Frame 3580
SetFrame( IncrementFrame())


-- Frame 3581
SetFrame( IncrementFrame())


-- Frame 3582
SetFrame( IncrementFrame())


-- Frame 3583
SetFrame( IncrementFrame())


-- Frame 3584
SetFrame( IncrementFrame())


-- Frame 3585
SetFrame( IncrementFrame())


-- Frame 3586
SetFrame( IncrementFrame())


-- Frame 3587
SetFrame( IncrementFrame())


-- Frame 3588
SetFrame( IncrementFrame())


-- Frame 3589
SetFrame( IncrementFrame())


-- Frame 3590
SetFrame( IncrementFrame())


-- Frame 3591
SetFrame( IncrementFrame())


-- Frame 3592
SetFrame( IncrementFrame())


-- Frame 3593
SetFrame( IncrementFrame())


-- Frame 3594
SetFrame( IncrementFrame())


-- Frame 3595
SetFrame( IncrementFrame())


-- Frame 3596
SetFrame( IncrementFrame())


-- Frame 3597
SetFrame( IncrementFrame())


-- Frame 3598
SetFrame( IncrementFrame())


-- Frame 3599
SetFrame( IncrementFrame())


-- Frame 3600
SetFrame( IncrementFrame())


-- Frame 3601
SetFrame( IncrementFrame())


-- Frame 3602
SetFrame( IncrementFrame())


-- Frame 3603
SetFrame( IncrementFrame())


-- Frame 3604
SetFrame( IncrementFrame())


-- Frame 3605
SetFrame( IncrementFrame())


-- Frame 3606
SetFrame( IncrementFrame())


-- Frame 3607
SetFrame( IncrementFrame())


-- Frame 3608
SetFrame( IncrementFrame())


-- Frame 3609
SetFrame( IncrementFrame())


-- Frame 3610
SetFrame( IncrementFrame())


-- Frame 3611
SetFrame( IncrementFrame())


-- Frame 3612
SetFrame( IncrementFrame())


-- Frame 3613
SetFrame( IncrementFrame())


-- Frame 3614
SetFrame( IncrementFrame())


-- Frame 3615
SetFrame( IncrementFrame())


-- Frame 3616
SetFrame( IncrementFrame())


-- Frame 3617
SetFrame( IncrementFrame())


-- Frame 3618
SetFrame( IncrementFrame())


-- Frame 3619
SetFrame( IncrementFrame())


-- Frame 3620
SetFrame( IncrementFrame())


-- Frame 3621
SetFrame( IncrementFrame())


-- Frame 3622
SetFrame( IncrementFrame())


-- Frame 3623
SetFrame( IncrementFrame())


-- Frame 3624
SetFrame( IncrementFrame())


-- Frame 3625
SetFrame( IncrementFrame())


-- Frame 3626
SetFrame( IncrementFrame())


-- Frame 3627
SetFrame( IncrementFrame())


-- Frame 3628
SetFrame( IncrementFrame())


-- Frame 3629
SetFrame( IncrementFrame())


-- Frame 3630
SetFrame( IncrementFrame())


-- Frame 3631
SetFrame( IncrementFrame())


-- Frame 3632
SetFrame( IncrementFrame())


-- Frame 3633
SetFrame( IncrementFrame())


-- Frame 3634
SetFrame( IncrementFrame())


-- Frame 3635
SetFrame( IncrementFrame())


-- Frame 3636
SetFrame( IncrementFrame())


-- Frame 3637
SetFrame( IncrementFrame())


-- Frame 3638
SetFrame( IncrementFrame())


-- Frame 3639
SetFrame( IncrementFrame())


-- Frame 3640
SetFrame( IncrementFrame())


-- Frame 3641
SetFrame( IncrementFrame())


-- Frame 3642
SetFrame( IncrementFrame())


-- Frame 3643
SetFrame( IncrementFrame())


-- Frame 3644
SetFrame( IncrementFrame())


-- Frame 3645
SetFrame( IncrementFrame())


-- Frame 3646
SetFrame( IncrementFrame())


-- Frame 3647
SetFrame( IncrementFrame())


-- Frame 3648
SetFrame( IncrementFrame())


-- Frame 3649
SetFrame( IncrementFrame())


-- Frame 3650
SetFrame( IncrementFrame())


-- Frame 3651
SetFrame( IncrementFrame())


-- Frame 3652
SetFrame( IncrementFrame())


-- Frame 3653
SetFrame( IncrementFrame())


-- Frame 3654
SetFrame( IncrementFrame())


-- Frame 3655
SetFrame( IncrementFrame())


-- Frame 3656
SetFrame( IncrementFrame())


-- Frame 3657
SetFrame( IncrementFrame())


-- Frame 3658
SetFrame( IncrementFrame())


-- Frame 3659
SetFrame( IncrementFrame())


-- Frame 3660
SetFrame( IncrementFrame())


-- Frame 3661
SetFrame( IncrementFrame())


-- Frame 3662
SetFrame( IncrementFrame())


-- Frame 3663
SetFrame( IncrementFrame())


-- Frame 3664
SetFrame( IncrementFrame())


-- Frame 3665
SetFrame( IncrementFrame())


-- Frame 3666
SetFrame( IncrementFrame())


-- Frame 3667
SetFrame( IncrementFrame())


-- Frame 3668
SetFrame( IncrementFrame())


-- Frame 3669
SetFrame( IncrementFrame())


-- Frame 3670
SetFrame( IncrementFrame())


-- Frame 3671
SetFrame( IncrementFrame())


-- Frame 3672
SetFrame( IncrementFrame())


-- Frame 3673
SetFrame( IncrementFrame())


-- Frame 3674
SetFrame( IncrementFrame())


-- Frame 3675
SetFrame( IncrementFrame())


-- Frame 3676
SetFrame( IncrementFrame())


-- Frame 3677
SetFrame( IncrementFrame())


-- Frame 3678
SetFrame( IncrementFrame())


-- Frame 3679
SetFrame( IncrementFrame())


-- Frame 3680
SetFrame( IncrementFrame())


-- Frame 3681
SetFrame( IncrementFrame())


-- Frame 3682
SetFrame( IncrementFrame())


-- Frame 3683
SetFrame( IncrementFrame())


-- Frame 3684
SetFrame( IncrementFrame())


-- Frame 3685
SetFrame( IncrementFrame())


-- Frame 3686
SetFrame( IncrementFrame())


-- Frame 3687
SetFrame( IncrementFrame())


-- Frame 3688
SetFrame( IncrementFrame())


-- Frame 3689
SetFrame( IncrementFrame())


-- Frame 3690
SetFrame( IncrementFrame())


-- Frame 3691
SetFrame( IncrementFrame())


-- Frame 3692
SetFrame( IncrementFrame())


-- Frame 3693
SetFrame( IncrementFrame())


-- Frame 3694
SetFrame( IncrementFrame())


-- Frame 3695
SetFrame( IncrementFrame())


-- Frame 3696
SetFrame( IncrementFrame())


-- Frame 3697
SetFrame( IncrementFrame())


-- Frame 3698
SetFrame( IncrementFrame())


-- Frame 3699
SetFrame( IncrementFrame())


-- Frame 3700
SetFrame( IncrementFrame())


-- Frame 3701
SetFrame( IncrementFrame())


-- Frame 3702
SetFrame( IncrementFrame())


-- Frame 3703
SetFrame( IncrementFrame())


-- Frame 3704
SetFrame( IncrementFrame())


-- Frame 3705
SetFrame( IncrementFrame())


-- Frame 3706
SetFrame( IncrementFrame())


-- Frame 3707
SetFrame( IncrementFrame())


-- Frame 3708
SetFrame( IncrementFrame())


-- Frame 3709
SetFrame( IncrementFrame())


-- Frame 3710
SetFrame( IncrementFrame())


-- Frame 3711
SetFrame( IncrementFrame())


-- Frame 3712
SetFrame( IncrementFrame())


-- Frame 3713
SetFrame( IncrementFrame())


-- Frame 3714
SetFrame( IncrementFrame())


-- Frame 3715
SetFrame( IncrementFrame())


-- Frame 3716
SetFrame( IncrementFrame())


-- Frame 3717
SetFrame( IncrementFrame())


-- Frame 3718
SetFrame( IncrementFrame())


-- Frame 3719
SetFrame( IncrementFrame())


-- Frame 3720
SetFrame( IncrementFrame())


-- Frame 3721
SetFrame( IncrementFrame())


-- Frame 3722
SetFrame( IncrementFrame())


-- Frame 3723
SetFrame( IncrementFrame())


-- Frame 3724
SetFrame( IncrementFrame())


-- Frame 3725
SetFrame( IncrementFrame())


-- Frame 3726
SetFrame( IncrementFrame())


-- Frame 3727
SetFrame( IncrementFrame())


-- Frame 3728
SetFrame( IncrementFrame())


-- Frame 3729
SetFrame( IncrementFrame())


-- Frame 3730
SetFrame( IncrementFrame())


-- Frame 3731
SetFrame( IncrementFrame())


-- Frame 3732
SetFrame( IncrementFrame())


-- Frame 3733
SetFrame( IncrementFrame())


-- Frame 3734
SetFrame( IncrementFrame())


-- Frame 3735
SetFrame( IncrementFrame())


-- Frame 3736
SetFrame( IncrementFrame())


-- Frame 3737
SetFrame( IncrementFrame())


-- Frame 3738
SetFrame( IncrementFrame())


-- Frame 3739
SetFrame( IncrementFrame())


-- Frame 3740
SetFrame( IncrementFrame())


-- Frame 3741
SetFrame( IncrementFrame())


-- Frame 3742
SetFrame( IncrementFrame())


-- Frame 3743
SetFrame( IncrementFrame())


-- Frame 3744
SetFrame( IncrementFrame())


-- Frame 3745
SetFrame( IncrementFrame())


-- Frame 3746
SetFrame( IncrementFrame())


-- Frame 3747
SetFrame( IncrementFrame())


-- Frame 3748
SetFrame( IncrementFrame())


-- Frame 3749
SetFrame( IncrementFrame())


-- Frame 3750
SetFrame( IncrementFrame())


-- Frame 3751
SetFrame( IncrementFrame())


-- Frame 3752
SetFrame( IncrementFrame())


-- Frame 3753
SetFrame( IncrementFrame())


-- Frame 3754
SetFrame( IncrementFrame())


-- Frame 3755
SetFrame( IncrementFrame())


-- Frame 3756
SetFrame( IncrementFrame())


-- Frame 3757
SetFrame( IncrementFrame())


-- Frame 3758
SetFrame( IncrementFrame())


-- Frame 3759
SetFrame( IncrementFrame())


-- Frame 3760
SetFrame( IncrementFrame())


-- Frame 3761
SetFrame( IncrementFrame())


-- Frame 3762
SetFrame( IncrementFrame())


-- Frame 3763
SetFrame( IncrementFrame())


-- Frame 3764
SetFrame( IncrementFrame())


-- Frame 3765
SetFrame( IncrementFrame())


-- Frame 3766
SetFrame( IncrementFrame())


-- Frame 3767
SetFrame( IncrementFrame())


-- Frame 3768
SetFrame( IncrementFrame())


-- Frame 3769
SetFrame( IncrementFrame())


-- Frame 3770
SetFrame( IncrementFrame())


-- Frame 3771
SetFrame( IncrementFrame())


-- Frame 3772
SetFrame( IncrementFrame())


-- Frame 3773
SetFrame( IncrementFrame())


-- Frame 3774
SetFrame( IncrementFrame())


-- Frame 3775
SetFrame( IncrementFrame())


-- Frame 3776
SetFrame( IncrementFrame())


-- Frame 3777
SetFrame( IncrementFrame())


-- Frame 3778
SetFrame( IncrementFrame())


-- Frame 3779
SetFrame( IncrementFrame())


-- Frame 3780
SetFrame( IncrementFrame())


-- Frame 3781
SetFrame( IncrementFrame())


-- Frame 3782
SetFrame( IncrementFrame())


-- Frame 3783
SetFrame( IncrementFrame())


-- Frame 3784
SetFrame( IncrementFrame())


-- Frame 3785
SetFrame( IncrementFrame())


-- Frame 3786
SetFrame( IncrementFrame())


-- Frame 3787
SetFrame( IncrementFrame())


-- Frame 3788
SetFrame( IncrementFrame())


-- Frame 3789
SetFrame( IncrementFrame())


-- Frame 3790
SetFrame( IncrementFrame())


-- Frame 3791
SetFrame( IncrementFrame())


-- Frame 3792
SetFrame( IncrementFrame())


-- Frame 3793
SetFrame( IncrementFrame())


-- Frame 3794
SetFrame( IncrementFrame())


-- Frame 3795
SetFrame( IncrementFrame())


-- Frame 3796
SetFrame( IncrementFrame())


-- Frame 3797
SetFrame( IncrementFrame())


-- Frame 3798
SetFrame( IncrementFrame())


-- Frame 3799
SetFrame( IncrementFrame())


-- Frame 3800
SetFrame( IncrementFrame())


-- Frame 3801
SetFrame( IncrementFrame())


-- Frame 3802
SetFrame( IncrementFrame())


-- Frame 3803
SetFrame( IncrementFrame())


-- Frame 3804
SetFrame( IncrementFrame())


-- Frame 3805
SetFrame( IncrementFrame())


-- Frame 3806
SetFrame( IncrementFrame())


-- Frame 3807
SetFrame( IncrementFrame())


-- Frame 3808
SetFrame( IncrementFrame())


-- Frame 3809
SetFrame( IncrementFrame())


-- Frame 3810
SetFrame( IncrementFrame())


-- Frame 3811
SetFrame( IncrementFrame())


-- Frame 3812
SetFrame( IncrementFrame())


-- Frame 3813
SetFrame( IncrementFrame())


-- Frame 3814
SetFrame( IncrementFrame())


-- Frame 3815
SetFrame( IncrementFrame())


-- Frame 3816
SetFrame( IncrementFrame())


-- Frame 3817
SetFrame( IncrementFrame())


-- Frame 3818
SetFrame( IncrementFrame())


-- Frame 3819
SetFrame( IncrementFrame())


-- Frame 3820
SetFrame( IncrementFrame())


-- Frame 3821
SetFrame( IncrementFrame())


-- Frame 3822
SetFrame( IncrementFrame())


-- Frame 3823
SetFrame( IncrementFrame())


-- Frame 3824
SetFrame( IncrementFrame())


-- Frame 3825
SetFrame( IncrementFrame())


-- Frame 3826
SetFrame( IncrementFrame())


-- Frame 3827
SetFrame( IncrementFrame())


-- Frame 3828
SetFrame( IncrementFrame())


-- Frame 3829
SetFrame( IncrementFrame())


-- Frame 3830
SetFrame( IncrementFrame())


-- Frame 3831
SetFrame( IncrementFrame())


-- Frame 3832
SetFrame( IncrementFrame())


-- Frame 3833
SetFrame( IncrementFrame())


-- Frame 3834
SetFrame( IncrementFrame())


-- Frame 3835
SetFrame( IncrementFrame())


-- Frame 3836
SetFrame( IncrementFrame())


-- Frame 3837
SetFrame( IncrementFrame())


-- Frame 3838
SetFrame( IncrementFrame())


-- Frame 3839
SetFrame( IncrementFrame())


-- Frame 3840
SetFrame( IncrementFrame())


-- Frame 3841
SetFrame( IncrementFrame())


-- Frame 3842
SetFrame( IncrementFrame())


-- Frame 3843
SetFrame( IncrementFrame())


-- Frame 3844
SetFrame( IncrementFrame())


-- Frame 3845
SetFrame( IncrementFrame())


-- Frame 3846
SetFrame( IncrementFrame())


-- Frame 3847
SetFrame( IncrementFrame())


-- Frame 3848
SetFrame( IncrementFrame())


-- Frame 3849
SetFrame( IncrementFrame())


-- Frame 3850
SetFrame( IncrementFrame())


-- Frame 3851
SetFrame( IncrementFrame())


-- Frame 3852
SetFrame( IncrementFrame())


-- Frame 3853
SetFrame( IncrementFrame())


-- Frame 3854
SetFrame( IncrementFrame())


-- Frame 3855
SetFrame( IncrementFrame())


-- Frame 3856
SetFrame( IncrementFrame())


-- Frame 3857
SetFrame( IncrementFrame())


-- Frame 3858
SetFrame( IncrementFrame())


-- Frame 3859
SetFrame( IncrementFrame())


-- Frame 3860
SetFrame( IncrementFrame())


-- Frame 3861
SetFrame( IncrementFrame())


-- Frame 3862
SetFrame( IncrementFrame())


-- Frame 3863
SetFrame( IncrementFrame())


-- Frame 3864
SetFrame( IncrementFrame())


-- Frame 3865
SetFrame( IncrementFrame())


-- Frame 3866
SetFrame( IncrementFrame())


-- Frame 3867
SetFrame( IncrementFrame())


-- Frame 3868
SetFrame( IncrementFrame())


-- Frame 3869
SetFrame( IncrementFrame())


-- Frame 3870
SetFrame( IncrementFrame())


-- Frame 3871
SetFrame( IncrementFrame())


-- Frame 3872
SetFrame( IncrementFrame())


-- Frame 3873
SetFrame( IncrementFrame())


-- Frame 3874
SetFrame( IncrementFrame())


-- Frame 3875
SetFrame( IncrementFrame())


-- Frame 3876
SetFrame( IncrementFrame())


-- Frame 3877
SetFrame( IncrementFrame())


-- Frame 3878
SetFrame( IncrementFrame())


-- Frame 3879
SetFrame( IncrementFrame())


-- Frame 3880
SetFrame( IncrementFrame())


-- Frame 3881
SetFrame( IncrementFrame())


-- Frame 3882
SetFrame( IncrementFrame())


-- Frame 3883
SetFrame( IncrementFrame())


-- Frame 3884
SetFrame( IncrementFrame())


-- Frame 3885
SetFrame( IncrementFrame())


-- Frame 3886
SetFrame( IncrementFrame())


-- Frame 3887
SetFrame( IncrementFrame())


-- Frame 3888
SetFrame( IncrementFrame())


-- Frame 3889
SetFrame( IncrementFrame())


-- Frame 3890
SetFrame( IncrementFrame())


-- Frame 3891
SetFrame( IncrementFrame())


-- Frame 3892
SetFrame( IncrementFrame())


-- Frame 3893
SetFrame( IncrementFrame())


-- Frame 3894
SetFrame( IncrementFrame())


-- Frame 3895
SetFrame( IncrementFrame())


-- Frame 3896
SetFrame( IncrementFrame())


-- Frame 3897
SetFrame( IncrementFrame())


-- Frame 3898
SetFrame( IncrementFrame())


-- Frame 3899
SetFrame( IncrementFrame())


-- Frame 3900
SetFrame( IncrementFrame())


-- Frame 3901
SetFrame( IncrementFrame())


-- Frame 3902
SetFrame( IncrementFrame())


-- Frame 3903
SetFrame( IncrementFrame())


-- Frame 3904
SetFrame( IncrementFrame())


-- Frame 3905
SetFrame( IncrementFrame())


-- Frame 3906
SetFrame( IncrementFrame())


-- Frame 3907
SetFrame( IncrementFrame())


-- Frame 3908
SetFrame( IncrementFrame())


-- Frame 3909
SetFrame( IncrementFrame())


-- Frame 3910
SetFrame( IncrementFrame())


-- Frame 3911
SetFrame( IncrementFrame())


-- Frame 3912
SetFrame( IncrementFrame())


-- Frame 3913
SetFrame( IncrementFrame())


-- Frame 3914
SetFrame( IncrementFrame())


-- Frame 3915
SetFrame( IncrementFrame())


-- Frame 3916
SetFrame( IncrementFrame())


-- Frame 3917
SetFrame( IncrementFrame())


-- Frame 3918
SetFrame( IncrementFrame())


-- Frame 3919
SetFrame( IncrementFrame())


-- Frame 3920
SetFrame( IncrementFrame())


-- Frame 3921
SetFrame( IncrementFrame())


-- Frame 3922
SetFrame( IncrementFrame())


-- Frame 3923
SetFrame( IncrementFrame())


-- Frame 3924
SetFrame( IncrementFrame())


-- Frame 3925
SetFrame( IncrementFrame())


-- Frame 3926
SetFrame( IncrementFrame())


-- Frame 3927
SetFrame( IncrementFrame())


-- Frame 3928
SetFrame( IncrementFrame())


-- Frame 3929
SetFrame( IncrementFrame())


-- Frame 3930
SetFrame( IncrementFrame())


-- Frame 3931
SetFrame( IncrementFrame())


-- Frame 3932
SetFrame( IncrementFrame())


-- Frame 3933
SetFrame( IncrementFrame())


-- Frame 3934
SetFrame( IncrementFrame())


-- Frame 3935
SetFrame( IncrementFrame())


-- Frame 3936
SetFrame( IncrementFrame())


-- Frame 3937
SetFrame( IncrementFrame())


-- Frame 3938
SetFrame( IncrementFrame())


-- Frame 3939
SetFrame( IncrementFrame())


-- Frame 3940
SetFrame( IncrementFrame())


-- Frame 3941
SetFrame( IncrementFrame())


-- Frame 3942
SetFrame( IncrementFrame())


-- Frame 3943
SetFrame( IncrementFrame())


-- Frame 3944
SetFrame( IncrementFrame())


-- Frame 3945
SetFrame( IncrementFrame())


-- Frame 3946
SetFrame( IncrementFrame())


-- Frame 3947
SetFrame( IncrementFrame())


-- Frame 3948
SetFrame( IncrementFrame())


-- Frame 3949
SetFrame( IncrementFrame())


-- Frame 3950
SetFrame( IncrementFrame())


-- Frame 3951
SetFrame( IncrementFrame())


-- Frame 3952
SetFrame( IncrementFrame())


-- Frame 3953
SetFrame( IncrementFrame())


-- Frame 3954
SetFrame( IncrementFrame())


-- Frame 3955
SetFrame( IncrementFrame())


-- Frame 3956
SetFrame( IncrementFrame())


-- Frame 3957
SetFrame( IncrementFrame())


-- Frame 3958
SetFrame( IncrementFrame())


-- Frame 3959
SetFrame( IncrementFrame())


-- Frame 3960
SetFrame( IncrementFrame())


-- Frame 3961
SetFrame( IncrementFrame())


-- Frame 3962
SetFrame( IncrementFrame())


-- Frame 3963
SetFrame( IncrementFrame())


-- Frame 3964
SetFrame( IncrementFrame())


-- Frame 3965
SetFrame( IncrementFrame())


-- Frame 3966
SetFrame( IncrementFrame())


-- Frame 3967
SetFrame( IncrementFrame())


-- Frame 3968
SetFrame( IncrementFrame())


-- Frame 3969
SetFrame( IncrementFrame())


-- Frame 3970
SetFrame( IncrementFrame())


-- Frame 3971
SetFrame( IncrementFrame())


-- Frame 3972
SetFrame( IncrementFrame())


-- Frame 3973
SetFrame( IncrementFrame())


-- Frame 3974
SetFrame( IncrementFrame())


-- Frame 3975
SetFrame( IncrementFrame())


-- Frame 3976
SetFrame( IncrementFrame())


-- Frame 3977
SetFrame( IncrementFrame())


-- Frame 3978
SetFrame( IncrementFrame())


-- Frame 3979
SetFrame( IncrementFrame())


-- Frame 3980
SetFrame( IncrementFrame())


-- Frame 3981
SetFrame( IncrementFrame())


-- Frame 3982
SetFrame( IncrementFrame())


-- Frame 3983
SetFrame( IncrementFrame())


-- Frame 3984
SetFrame( IncrementFrame())


-- Frame 3985
SetFrame( IncrementFrame())


-- Frame 3986
SetFrame( IncrementFrame())


-- Frame 3987
SetFrame( IncrementFrame())


-- Frame 3988
SetFrame( IncrementFrame())


-- Frame 3989
SetFrame( IncrementFrame())


-- Frame 3990
SetFrame( IncrementFrame())


-- Frame 3991
SetFrame( IncrementFrame())


-- Frame 3992
SetFrame( IncrementFrame())


-- Frame 3993
SetFrame( IncrementFrame())


-- Frame 3994
SetFrame( IncrementFrame())


-- Frame 3995
SetFrame( IncrementFrame())


-- Frame 3996
SetFrame( IncrementFrame())


-- Frame 3997
SetFrame( IncrementFrame())


-- Frame 3998
SetFrame( IncrementFrame())


-- Frame 3999
SetFrame( IncrementFrame())


-- Frame 4000
SetFrame( IncrementFrame())


-- Frame 4001
SetFrame( IncrementFrame())


-- Frame 4002
SetFrame( IncrementFrame())


-- Frame 4003
SetFrame( IncrementFrame())


-- Frame 4004
SetFrame( IncrementFrame())


-- Frame 4005
SetFrame( IncrementFrame())


-- Frame 4006
SetFrame( IncrementFrame())


-- Frame 4007
SetFrame( IncrementFrame())


-- Frame 4008
SetFrame( IncrementFrame())


-- Frame 4009
SetFrame( IncrementFrame())


-- Frame 4010
SetFrame( IncrementFrame())


-- Frame 4011
SetFrame( IncrementFrame())


-- Frame 4012
SetFrame( IncrementFrame())


-- Frame 4013
SetFrame( IncrementFrame())


-- Frame 4014
SetFrame( IncrementFrame())


-- Frame 4015
SetFrame( IncrementFrame())


-- Frame 4016
SetFrame( IncrementFrame())


-- Frame 4017
SetFrame( IncrementFrame())


-- Frame 4018
SetFrame( IncrementFrame())


-- Frame 4019
SetFrame( IncrementFrame())


-- Frame 4020
SetFrame( IncrementFrame())


-- Frame 4021
SetFrame( IncrementFrame())


-- Frame 4022
SetFrame( IncrementFrame())


-- Frame 4023
SetFrame( IncrementFrame())


-- Frame 4024
SetFrame( IncrementFrame())


-- Frame 4025
SetFrame( IncrementFrame())


-- Frame 4026
SetFrame( IncrementFrame())


-- Frame 4027
SetFrame( IncrementFrame())


-- Frame 4028
SetFrame( IncrementFrame())


-- Frame 4029
SetFrame( IncrementFrame())


-- Frame 4030
SetFrame( IncrementFrame())


-- Frame 4031
SetFrame( IncrementFrame())


-- Frame 4032
SetFrame( IncrementFrame())


-- Frame 4033
SetFrame( IncrementFrame())


-- Frame 4034
SetFrame( IncrementFrame())


-- Frame 4035
SetFrame( IncrementFrame())


-- Frame 4036
SetFrame( IncrementFrame())


-- Frame 4037
SetFrame( IncrementFrame())


-- Frame 4038
SetFrame( IncrementFrame())


-- Frame 4039
SetFrame( IncrementFrame())


-- Frame 4040
SetFrame( IncrementFrame())


-- Frame 4041
SetFrame( IncrementFrame())


-- Frame 4042
SetFrame( IncrementFrame())


-- Frame 4043
SetFrame( IncrementFrame())


-- Frame 4044
SetFrame( IncrementFrame())


-- Frame 4045
SetFrame( IncrementFrame())


-- Frame 4046
SetFrame( IncrementFrame())


-- Frame 4047
SetFrame( IncrementFrame())


-- Frame 4048
SetFrame( IncrementFrame())


-- Frame 4049
SetFrame( IncrementFrame())


-- Frame 4050
SetFrame( IncrementFrame())


-- Frame 4051
SetFrame( IncrementFrame())


-- Frame 4052
SetFrame( IncrementFrame())


-- Frame 4053
SetFrame( IncrementFrame())


-- Frame 4054
SetFrame( IncrementFrame())


-- Frame 4055
SetFrame( IncrementFrame())


-- Frame 4056
SetFrame( IncrementFrame())


-- Frame 4057
SetFrame( IncrementFrame())


-- Frame 4058
SetFrame( IncrementFrame())


-- Frame 4059
SetFrame( IncrementFrame())


-- Frame 4060
SetFrame( IncrementFrame())


-- Frame 4061
SetFrame( IncrementFrame())


-- Frame 4062
SetFrame( IncrementFrame())


-- Frame 4063
SetFrame( IncrementFrame())


-- Frame 4064
SetFrame( IncrementFrame())


-- Frame 4065
SetFrame( IncrementFrame())


-- Frame 4066
SetFrame( IncrementFrame())


-- Frame 4067
SetFrame( IncrementFrame())


-- Frame 4068
SetFrame( IncrementFrame())


-- Frame 4069
SetFrame( IncrementFrame())


-- Frame 4070
SetFrame( IncrementFrame())


-- Frame 4071
SetFrame( IncrementFrame())


-- Frame 4072
SetFrame( IncrementFrame())


-- Frame 4073
SetFrame( IncrementFrame())


-- Frame 4074
SetFrame( IncrementFrame())


-- Frame 4075
SetFrame( IncrementFrame())


-- Frame 4076
SetFrame( IncrementFrame())


-- Frame 4077
SetFrame( IncrementFrame())


-- Frame 4078
SetFrame( IncrementFrame())


-- Frame 4079
SetFrame( IncrementFrame())


-- Frame 4080
SetFrame( IncrementFrame())


-- Frame 4081
SetFrame( IncrementFrame())


-- Frame 4082
SetFrame( IncrementFrame())


-- Frame 4083
SetFrame( IncrementFrame())


-- Frame 4084
SetFrame( IncrementFrame())


-- Frame 4085
SetFrame( IncrementFrame())


-- Frame 4086
SetFrame( IncrementFrame())


-- Frame 4087
SetFrame( IncrementFrame())


-- Frame 4088
SetFrame( IncrementFrame())


-- Frame 4089
SetFrame( IncrementFrame())


-- Frame 4090
SetFrame( IncrementFrame())


-- Frame 4091
SetFrame( IncrementFrame())


-- Frame 4092
SetFrame( IncrementFrame())


-- Frame 4093
SetFrame( IncrementFrame())


-- Frame 4094
SetFrame( IncrementFrame())


-- Frame 4095
SetFrame( IncrementFrame())


-- Frame 4096
SetFrame( IncrementFrame())


-- Frame 4097
SetFrame( IncrementFrame())


-- Frame 4098
SetFrame( IncrementFrame())


-- Frame 4099
SetFrame( IncrementFrame())


-- Frame 4100
SetFrame( IncrementFrame())

MoveObjectTo( ObjectObj_Archimonde, "Act_MoveAwayArch", -30, 6, 0)
RotateObjectTo( ObjectObj_Archimonde, "Act_RotateAwayArch", 0, -160, 0)
MoveObjectTo( ObjectObj_Tichondrius, "Act_MoveAwayTich", -20, 2, 0)

-- Frame 4101
SetFrame( IncrementFrame())


-- Frame 4102
SetFrame( IncrementFrame())


-- Frame 4103
SetFrame( IncrementFrame())


-- Frame 4104
SetFrame( IncrementFrame())


-- Frame 4105
SetFrame( IncrementFrame())


-- Frame 4106
SetFrame( IncrementFrame())


-- Frame 4107
SetFrame( IncrementFrame())


-- Frame 4108
SetFrame( IncrementFrame())


-- Frame 4109
SetFrame( IncrementFrame())


-- Frame 4110
SetFrame( IncrementFrame())


-- Frame 4111
SetFrame( IncrementFrame())


-- Frame 4112
SetFrame( IncrementFrame())


-- Frame 4113
SetFrame( IncrementFrame())


-- Frame 4114
SetFrame( IncrementFrame())


-- Frame 4115
SetFrame( IncrementFrame())


-- Frame 4116
SetFrame( IncrementFrame())


-- Frame 4117
SetFrame( IncrementFrame())


-- Frame 4118
SetFrame( IncrementFrame())


-- Frame 4119
SetFrame( IncrementFrame())


-- Frame 4120
SetFrame( IncrementFrame())


-- Frame 4121
SetFrame( IncrementFrame())


-- Frame 4122
SetFrame( IncrementFrame())


-- Frame 4123
SetFrame( IncrementFrame())


-- Frame 4124
SetFrame( IncrementFrame())


-- Frame 4125
SetFrame( IncrementFrame())


-- Frame 4126
SetFrame( IncrementFrame())


-- Frame 4127
SetFrame( IncrementFrame())


-- Frame 4128
SetFrame( IncrementFrame())


-- Frame 4129
SetFrame( IncrementFrame())


-- Frame 4130
SetFrame( IncrementFrame())


-- Frame 4131
SetFrame( IncrementFrame())


-- Frame 4132
SetFrame( IncrementFrame())


-- Frame 4133
SetFrame( IncrementFrame())


-- Frame 4134
SetFrame( IncrementFrame())


-- Frame 4135
SetFrame( IncrementFrame())


-- Frame 4136
SetFrame( IncrementFrame())


-- Frame 4137
SetFrame( IncrementFrame())


-- Frame 4138
SetFrame( IncrementFrame())


-- Frame 4139
SetFrame( IncrementFrame())


-- Frame 4140
SetFrame( IncrementFrame())


-- Frame 4141
SetFrame( IncrementFrame())


-- Frame 4142
SetFrame( IncrementFrame())


-- Frame 4143
SetFrame( IncrementFrame())


-- Frame 4144
SetFrame( IncrementFrame())


-- Frame 4145
SetFrame( IncrementFrame())


-- Frame 4146
SetFrame( IncrementFrame())


-- Frame 4147
SetFrame( IncrementFrame())


-- Frame 4148
SetFrame( IncrementFrame())


-- Frame 4149
SetFrame( IncrementFrame())


-- Frame 4150
SetFrame( IncrementFrame())


-- Frame 4151
SetFrame( IncrementFrame())


-- Frame 4152
SetFrame( IncrementFrame())


-- Frame 4153
SetFrame( IncrementFrame())


-- Frame 4154
SetFrame( IncrementFrame())


-- Frame 4155
SetFrame( IncrementFrame())


-- Frame 4156
SetFrame( IncrementFrame())


-- Frame 4157
SetFrame( IncrementFrame())


-- Frame 4158
SetFrame( IncrementFrame())


-- Frame 4159
SetFrame( IncrementFrame())


-- Frame 4160
SetFrame( IncrementFrame())


-- Frame 4161
SetFrame( IncrementFrame())


-- Frame 4162
SetFrame( IncrementFrame())


-- Frame 4163
SetFrame( IncrementFrame())


-- Frame 4164
SetFrame( IncrementFrame())


-- Frame 4165
SetFrame( IncrementFrame())


-- Frame 4166
SetFrame( IncrementFrame())


-- Frame 4167
SetFrame( IncrementFrame())


-- Frame 4168
SetFrame( IncrementFrame())


-- Frame 4169
SetFrame( IncrementFrame())


-- Frame 4170
SetFrame( IncrementFrame())


-- Frame 4171
SetFrame( IncrementFrame())


-- Frame 4172
SetFrame( IncrementFrame())


-- Frame 4173
SetFrame( IncrementFrame())

RotateObjectTo( ObjectObj_Tichondrius, "Act_RotateAwayTich", 0, 10, 0)

-- Frame 4174
SetFrame( IncrementFrame())


-- Frame 4175
SetFrame( IncrementFrame())


-- Frame 4176
SetFrame( IncrementFrame())


-- Frame 4177
SetFrame( IncrementFrame())


-- Frame 4178
SetFrame( IncrementFrame())


-- Frame 4179
SetFrame( IncrementFrame())


-- Frame 4180
SetFrame( IncrementFrame())


-- Frame 4181
SetFrame( IncrementFrame())


-- Frame 4182
SetFrame( IncrementFrame())


-- Frame 4183
SetFrame( IncrementFrame())


-- Frame 4184
SetFrame( IncrementFrame())


-- Frame 4185
SetFrame( IncrementFrame())


-- Frame 4186
SetFrame( IncrementFrame())


-- Frame 4187
SetFrame( IncrementFrame())


-- Frame 4188
SetFrame( IncrementFrame())


-- Frame 4189
SetFrame( IncrementFrame())


-- Frame 4190
SetFrame( IncrementFrame())


-- Frame 4191
SetFrame( IncrementFrame())


-- Frame 4192
SetFrame( IncrementFrame())


-- Frame 4193
SetFrame( IncrementFrame())


-- Frame 4194
SetFrame( IncrementFrame())


-- Frame 4195
SetFrame( IncrementFrame())


-- Frame 4196
SetFrame( IncrementFrame())


-- Frame 4197
SetFrame( IncrementFrame())


-- Frame 4198
SetFrame( IncrementFrame())


-- Frame 4199
SetFrame( IncrementFrame())


-- Frame 4200
SetFrame( IncrementFrame())


-- Frame 4201
SetFrame( IncrementFrame())


-- Frame 4202
SetFrame( IncrementFrame())


-- Frame 4203
SetFrame( IncrementFrame())


-- Frame 4204
SetFrame( IncrementFrame())


-- Frame 4205
SetFrame( IncrementFrame())


-- Frame 4206
SetFrame( IncrementFrame())


-- Frame 4207
SetFrame( IncrementFrame())


-- Frame 4208
SetFrame( IncrementFrame())


-- Frame 4209
SetFrame( IncrementFrame())


-- Frame 4210
SetFrame( IncrementFrame())


-- Frame 4211
SetFrame( IncrementFrame())


-- Frame 4212
SetFrame( IncrementFrame())


-- Frame 4213
SetFrame( IncrementFrame())


-- Frame 4214
SetFrame( IncrementFrame())


-- Frame 4215
SetFrame( IncrementFrame())


-- Frame 4216
SetFrame( IncrementFrame())


-- Frame 4217
SetFrame( IncrementFrame())


-- Frame 4218
SetFrame( IncrementFrame())


-- Frame 4219
SetFrame( IncrementFrame())


-- Frame 4220
SetFrame( IncrementFrame())


-- Frame 4221
SetFrame( IncrementFrame())


-- Frame 4222
SetFrame( IncrementFrame())


-- Frame 4223
SetFrame( IncrementFrame())


-- Frame 4224
SetFrame( IncrementFrame())


-- Frame 4225
SetFrame( IncrementFrame())


-- Frame 4226
SetFrame( IncrementFrame())


-- Frame 4227
SetFrame( IncrementFrame())


-- Frame 4228
SetFrame( IncrementFrame())


-- Frame 4229
SetFrame( IncrementFrame())


-- Frame 4230
SetFrame( IncrementFrame())


-- Frame 4231
SetFrame( IncrementFrame())


-- Frame 4232
SetFrame( IncrementFrame())


-- Frame 4233
SetFrame( IncrementFrame())


-- Frame 4234
SetFrame( IncrementFrame())


-- Frame 4235
SetFrame( IncrementFrame())


-- Frame 4236
SetFrame( IncrementFrame())


-- Frame 4237
SetFrame( IncrementFrame())


-- Frame 4238
SetFrame( IncrementFrame())


-- Frame 4239
SetFrame( IncrementFrame())


-- Frame 4240
SetFrame( IncrementFrame())


-- Frame 4241
SetFrame( IncrementFrame())


-- Frame 4242
SetFrame( IncrementFrame())


-- Frame 4243
SetFrame( IncrementFrame())


-- Frame 4244
SetFrame( IncrementFrame())


-- Frame 4245
SetFrame( IncrementFrame())


-- Frame 4246
SetFrame( IncrementFrame())


-- Frame 4247
SetFrame( IncrementFrame())


-- Frame 4248
SetFrame( IncrementFrame())

LinkCamThirdPerson( ObjectObj_Camera, "Act_BehindKT", "Obj_KTBackCam")
RotateObjectTo( ObjectObj_KelThuzad, "Act_TurnToArthas", 0, -110, 0)
PlayAudio( ObjectObj_Arthas, "Act_Joke", "Resources/WarCraftSounds/CS_08_Arthas.mp3", 1)
RotateObjectTo( ObjectObj_Arthas, "Act_RotateToKT", 0, 50, 0)
RotateObjectTo( ObjectObj_ArthBackCam, "Act_RotateArthBackCam", 0, 50, 0)
RotateObjectTo( ObjectObj_KTBackCam, "Act_CamTurnToArthas", 0, 50, 0)

-- Frame 4249
SetFrame( IncrementFrame())


-- Frame 4250
SetFrame( IncrementFrame())


-- Frame 4251
SetFrame( IncrementFrame())


-- Frame 4252
SetFrame( IncrementFrame())


-- Frame 4253
SetFrame( IncrementFrame())


-- Frame 4254
SetFrame( IncrementFrame())


-- Frame 4255
SetFrame( IncrementFrame())


-- Frame 4256
SetFrame( IncrementFrame())


-- Frame 4257
SetFrame( IncrementFrame())


-- Frame 4258
SetFrame( IncrementFrame())


-- Frame 4259
SetFrame( IncrementFrame())


-- Frame 4260
SetFrame( IncrementFrame())


-- Frame 4261
SetFrame( IncrementFrame())


-- Frame 4262
SetFrame( IncrementFrame())


-- Frame 4263
SetFrame( IncrementFrame())


-- Frame 4264
SetFrame( IncrementFrame())


-- Frame 4265
SetFrame( IncrementFrame())


-- Frame 4266
SetFrame( IncrementFrame())


-- Frame 4267
SetFrame( IncrementFrame())


-- Frame 4268
SetFrame( IncrementFrame())


-- Frame 4269
SetFrame( IncrementFrame())


-- Frame 4270
SetFrame( IncrementFrame())


-- Frame 4271
SetFrame( IncrementFrame())


-- Frame 4272
SetFrame( IncrementFrame())


-- Frame 4273
SetFrame( IncrementFrame())


-- Frame 4274
SetFrame( IncrementFrame())


-- Frame 4275
SetFrame( IncrementFrame())


-- Frame 4276
SetFrame( IncrementFrame())


-- Frame 4277
SetFrame( IncrementFrame())


-- Frame 4278
SetFrame( IncrementFrame())


-- Frame 4279
SetFrame( IncrementFrame())


-- Frame 4280
SetFrame( IncrementFrame())


-- Frame 4281
SetFrame( IncrementFrame())


-- Frame 4282
SetFrame( IncrementFrame())


-- Frame 4283
SetFrame( IncrementFrame())


-- Frame 4284
SetFrame( IncrementFrame())


-- Frame 4285
SetFrame( IncrementFrame())


-- Frame 4286
SetFrame( IncrementFrame())


-- Frame 4287
SetFrame( IncrementFrame())


-- Frame 4288
SetFrame( IncrementFrame())


-- Frame 4289
SetFrame( IncrementFrame())


-- Frame 4290
SetFrame( IncrementFrame())


-- Frame 4291
SetFrame( IncrementFrame())


-- Frame 4292
SetFrame( IncrementFrame())


-- Frame 4293
SetFrame( IncrementFrame())


-- Frame 4294
SetFrame( IncrementFrame())


-- Frame 4295
SetFrame( IncrementFrame())


-- Frame 4296
SetFrame( IncrementFrame())


-- Frame 4297
SetFrame( IncrementFrame())


-- Frame 4298
SetFrame( IncrementFrame())


-- Frame 4299
SetFrame( IncrementFrame())


-- Frame 4300
SetFrame( IncrementFrame())


-- Frame 4301
SetFrame( IncrementFrame())


-- Frame 4302
SetFrame( IncrementFrame())


-- Frame 4303
SetFrame( IncrementFrame())


-- Frame 4304
SetFrame( IncrementFrame())


-- Frame 4305
SetFrame( IncrementFrame())


-- Frame 4306
SetFrame( IncrementFrame())


-- Frame 4307
SetFrame( IncrementFrame())


-- Frame 4308
SetFrame( IncrementFrame())


-- Frame 4309
SetFrame( IncrementFrame())


-- Frame 4310
SetFrame( IncrementFrame())


-- Frame 4311
SetFrame( IncrementFrame())


-- Frame 4312
SetFrame( IncrementFrame())


-- Frame 4313
SetFrame( IncrementFrame())


-- Frame 4314
SetFrame( IncrementFrame())


-- Frame 4315
SetFrame( IncrementFrame())


-- Frame 4316
SetFrame( IncrementFrame())


-- Frame 4317
SetFrame( IncrementFrame())


-- Frame 4318
SetFrame( IncrementFrame())


-- Frame 4319
SetFrame( IncrementFrame())


-- Frame 4320
SetFrame( IncrementFrame())


-- Frame 4321
SetFrame( IncrementFrame())


-- Frame 4322
SetFrame( IncrementFrame())


-- Frame 4323
SetFrame( IncrementFrame())


-- Frame 4324
SetFrame( IncrementFrame())


-- Frame 4325
SetFrame( IncrementFrame())


-- Frame 4326
SetFrame( IncrementFrame())


-- Frame 4327
SetFrame( IncrementFrame())


-- Frame 4328
SetFrame( IncrementFrame())


-- Frame 4329
SetFrame( IncrementFrame())


-- Frame 4330
SetFrame( IncrementFrame())


-- Frame 4331
SetFrame( IncrementFrame())


-- Frame 4332
SetFrame( IncrementFrame())


-- Frame 4333
SetFrame( IncrementFrame())


-- Frame 4334
SetFrame( IncrementFrame())


-- Frame 4335
SetFrame( IncrementFrame())


-- Frame 4336
SetFrame( IncrementFrame())


-- Frame 4337
SetFrame( IncrementFrame())


-- Frame 4338
SetFrame( IncrementFrame())


-- Frame 4339
SetFrame( IncrementFrame())


-- Frame 4340
SetFrame( IncrementFrame())


-- Frame 4341
SetFrame( IncrementFrame())


-- Frame 4342
SetFrame( IncrementFrame())


-- Frame 4343
SetFrame( IncrementFrame())


-- Frame 4344
SetFrame( IncrementFrame())


-- Frame 4345
SetFrame( IncrementFrame())


-- Frame 4346
SetFrame( IncrementFrame())


-- Frame 4347
SetFrame( IncrementFrame())


-- Frame 4348
SetFrame( IncrementFrame())


-- Frame 4349
SetFrame( IncrementFrame())


-- Frame 4350
SetFrame( IncrementFrame())


-- Frame 4351
SetFrame( IncrementFrame())


-- Frame 4352
SetFrame( IncrementFrame())


-- Frame 4353
SetFrame( IncrementFrame())


-- Frame 4354
SetFrame( IncrementFrame())


-- Frame 4355
SetFrame( IncrementFrame())


-- Frame 4356
SetFrame( IncrementFrame())


-- Frame 4357
SetFrame( IncrementFrame())


-- Frame 4358
SetFrame( IncrementFrame())


-- Frame 4359
SetFrame( IncrementFrame())


-- Frame 4360
SetFrame( IncrementFrame())


-- Frame 4361
SetFrame( IncrementFrame())


-- Frame 4362
SetFrame( IncrementFrame())


-- Frame 4363
SetFrame( IncrementFrame())


-- Frame 4364
SetFrame( IncrementFrame())


-- Frame 4365
SetFrame( IncrementFrame())


-- Frame 4366
SetFrame( IncrementFrame())


-- Frame 4367
SetFrame( IncrementFrame())


-- Frame 4368
SetFrame( IncrementFrame())


-- Frame 4369
SetFrame( IncrementFrame())


-- Frame 4370
SetFrame( IncrementFrame())


-- Frame 4371
SetFrame( IncrementFrame())


-- Frame 4372
SetFrame( IncrementFrame())


-- Frame 4373
SetFrame( IncrementFrame())


-- Frame 4374
SetFrame( IncrementFrame())


-- Frame 4375
SetFrame( IncrementFrame())


-- Frame 4376
SetFrame( IncrementFrame())


-- Frame 4377
SetFrame( IncrementFrame())


-- Frame 4378
SetFrame( IncrementFrame())


-- Frame 4379
SetFrame( IncrementFrame())


-- Frame 4380
SetFrame( IncrementFrame())


-- Frame 4381
SetFrame( IncrementFrame())


-- Frame 4382
SetFrame( IncrementFrame())


-- Frame 4383
SetFrame( IncrementFrame())


-- Frame 4384
SetFrame( IncrementFrame())


-- Frame 4385
SetFrame( IncrementFrame())


-- Frame 4386
SetFrame( IncrementFrame())


-- Frame 4387
SetFrame( IncrementFrame())


-- Frame 4388
SetFrame( IncrementFrame())


-- Frame 4389
SetFrame( IncrementFrame())


-- Frame 4390
SetFrame( IncrementFrame())


-- Frame 4391
SetFrame( IncrementFrame())


-- Frame 4392
SetFrame( IncrementFrame())


-- Frame 4393
SetFrame( IncrementFrame())


-- Frame 4394
SetFrame( IncrementFrame())


-- Frame 4395
SetFrame( IncrementFrame())


-- Frame 4396
SetFrame( IncrementFrame())


-- Frame 4397
SetFrame( IncrementFrame())


-- Frame 4398
SetFrame( IncrementFrame())


-- Frame 4399
SetFrame( IncrementFrame())


-- Frame 4400
SetFrame( IncrementFrame())


-- Frame 4401
SetFrame( IncrementFrame())


-- Frame 4402
SetFrame( IncrementFrame())


-- Frame 4403
SetFrame( IncrementFrame())


-- Frame 4404
SetFrame( IncrementFrame())


-- Frame 4405
SetFrame( IncrementFrame())


-- Frame 4406
SetFrame( IncrementFrame())


-- Frame 4407
SetFrame( IncrementFrame())


-- Frame 4408
SetFrame( IncrementFrame())


-- Frame 4409
SetFrame( IncrementFrame())


-- Frame 4410
SetFrame( IncrementFrame())


-- Frame 4411
SetFrame( IncrementFrame())


-- Frame 4412
SetFrame( IncrementFrame())


-- Frame 4413
SetFrame( IncrementFrame())


-- Frame 4414
SetFrame( IncrementFrame())


-- Frame 4415
SetFrame( IncrementFrame())


-- Frame 4416
SetFrame( IncrementFrame())


-- Frame 4417
SetFrame( IncrementFrame())


-- Frame 4418
SetFrame( IncrementFrame())


-- Frame 4419
SetFrame( IncrementFrame())


-- Frame 4420
SetFrame( IncrementFrame())


-- Frame 4421
SetFrame( IncrementFrame())


-- Frame 4422
SetFrame( IncrementFrame())


-- Frame 4423
SetFrame( IncrementFrame())


-- Frame 4424
SetFrame( IncrementFrame())


-- Frame 4425
SetFrame( IncrementFrame())


-- Frame 4426
SetFrame( IncrementFrame())


-- Frame 4427
SetFrame( IncrementFrame())


-- Frame 4428
SetFrame( IncrementFrame())


-- Frame 4429
SetFrame( IncrementFrame())


-- Frame 4430
SetFrame( IncrementFrame())


-- Frame 4431
SetFrame( IncrementFrame())


-- Frame 4432
SetFrame( IncrementFrame())


-- Frame 4433
SetFrame( IncrementFrame())


-- Frame 4434
SetFrame( IncrementFrame())


-- Frame 4435
SetFrame( IncrementFrame())


-- Frame 4436
SetFrame( IncrementFrame())


-- Frame 4437
SetFrame( IncrementFrame())


-- Frame 4438
SetFrame( IncrementFrame())


-- Frame 4439
SetFrame( IncrementFrame())


-- Frame 4440
SetFrame( IncrementFrame())


-- Frame 4441
SetFrame( IncrementFrame())


-- Frame 4442
SetFrame( IncrementFrame())


-- Frame 4443
SetFrame( IncrementFrame())


-- Frame 4444
SetFrame( IncrementFrame())


-- Frame 4445
SetFrame( IncrementFrame())


-- Frame 4446
SetFrame( IncrementFrame())


-- Frame 4447
SetFrame( IncrementFrame())


-- Frame 4448
SetFrame( IncrementFrame())


-- Frame 4449
SetFrame( IncrementFrame())


-- Frame 4450
SetFrame( IncrementFrame())

SetObjectVisibility( ObjectObj_Archimonde, "Act_DespawnAch", false)
SetObjectVisibility( ObjectObj_Tichondrius, "Act_DespawnTich", false)

-- Frame 4451
SetFrame( IncrementFrame())


-- Frame 4452
SetFrame( IncrementFrame())


-- Frame 4453
SetFrame( IncrementFrame())


-- Frame 4454
SetFrame( IncrementFrame())


-- Frame 4455
SetFrame( IncrementFrame())


-- Frame 4456
SetFrame( IncrementFrame())


-- Frame 4457
SetFrame( IncrementFrame())


-- Frame 4458
SetFrame( IncrementFrame())


-- Frame 4459
SetFrame( IncrementFrame())


-- Frame 4460
SetFrame( IncrementFrame())


-- Frame 4461
SetFrame( IncrementFrame())


-- Frame 4462
SetFrame( IncrementFrame())


-- Frame 4463
SetFrame( IncrementFrame())


-- Frame 4464
SetFrame( IncrementFrame())


-- Frame 4465
SetFrame( IncrementFrame())


-- Frame 4466
SetFrame( IncrementFrame())


-- Frame 4467
SetFrame( IncrementFrame())


-- Frame 4468
SetFrame( IncrementFrame())


-- Frame 4469
SetFrame( IncrementFrame())


-- Frame 4470
SetFrame( IncrementFrame())


-- Frame 4471
SetFrame( IncrementFrame())


-- Frame 4472
SetFrame( IncrementFrame())


-- Frame 4473
SetFrame( IncrementFrame())


-- Frame 4474
SetFrame( IncrementFrame())


-- Frame 4475
SetFrame( IncrementFrame())


-- Frame 4476
SetFrame( IncrementFrame())


-- Frame 4477
SetFrame( IncrementFrame())


-- Frame 4478
SetFrame( IncrementFrame())


-- Frame 4479
SetFrame( IncrementFrame())


-- Frame 4480
SetFrame( IncrementFrame())


-- Frame 4481
SetFrame( IncrementFrame())


-- Frame 4482
SetFrame( IncrementFrame())


-- Frame 4483
SetFrame( IncrementFrame())


-- Frame 4484
SetFrame( IncrementFrame())


-- Frame 4485
SetFrame( IncrementFrame())


-- Frame 4486
SetFrame( IncrementFrame())


-- Frame 4487
SetFrame( IncrementFrame())


-- Frame 4488
SetFrame( IncrementFrame())


-- Frame 4489
SetFrame( IncrementFrame())


-- Frame 4490
SetFrame( IncrementFrame())


-- Frame 4491
SetFrame( IncrementFrame())


-- Frame 4492
SetFrame( IncrementFrame())


-- Frame 4493
SetFrame( IncrementFrame())


-- Frame 4494
SetFrame( IncrementFrame())


-- Frame 4495
SetFrame( IncrementFrame())


-- Frame 4496
SetFrame( IncrementFrame())


-- Frame 4497
SetFrame( IncrementFrame())


-- Frame 4498
SetFrame( IncrementFrame())


-- Frame 4499
SetFrame( IncrementFrame())


-- Frame 4500
SetFrame( IncrementFrame())


-- Frame 4501
SetFrame( IncrementFrame())


-- Frame 4502
SetFrame( IncrementFrame())


-- Frame 4503
SetFrame( IncrementFrame())


-- Frame 4504
SetFrame( IncrementFrame())


-- Frame 4505
SetFrame( IncrementFrame())


-- Frame 4506
SetFrame( IncrementFrame())


-- Frame 4507
SetFrame( IncrementFrame())


-- Frame 4508
SetFrame( IncrementFrame())


-- Frame 4509
SetFrame( IncrementFrame())


-- Frame 4510
SetFrame( IncrementFrame())


-- Frame 4511
SetFrame( IncrementFrame())


-- Frame 4512
SetFrame( IncrementFrame())


-- Frame 4513
SetFrame( IncrementFrame())


-- Frame 4514
SetFrame( IncrementFrame())


-- Frame 4515
SetFrame( IncrementFrame())


-- Frame 4516
SetFrame( IncrementFrame())


-- Frame 4517
SetFrame( IncrementFrame())


-- Frame 4518
SetFrame( IncrementFrame())


-- Frame 4519
SetFrame( IncrementFrame())


-- Frame 4520
SetFrame( IncrementFrame())


-- Frame 4521
SetFrame( IncrementFrame())


-- Frame 4522
SetFrame( IncrementFrame())


-- Frame 4523
SetFrame( IncrementFrame())


-- Frame 4524
SetFrame( IncrementFrame())


-- Frame 4525
SetFrame( IncrementFrame())


-- Frame 4526
SetFrame( IncrementFrame())


-- Frame 4527
SetFrame( IncrementFrame())


-- Frame 4528
SetFrame( IncrementFrame())


-- Frame 4529
SetFrame( IncrementFrame())


-- Frame 4530
SetFrame( IncrementFrame())

LinkCamThirdPerson( ObjectObj_Camera, "Act_BehindA02", "Obj_ArthBackCam")
PlayAudio( ObjectObj_KelThuzad, "Act_BePatient", "Resources/WarCraftSounds/CS_09_KelThuzad.mp3", 1)

-- Frame 4531
SetFrame( IncrementFrame())


-- Frame 4532
SetFrame( IncrementFrame())


-- Frame 4533
SetFrame( IncrementFrame())


-- Frame 4534
SetFrame( IncrementFrame())


-- Frame 4535
SetFrame( IncrementFrame())


-- Frame 4536
SetFrame( IncrementFrame())


-- Frame 4537
SetFrame( IncrementFrame())


-- Frame 4538
SetFrame( IncrementFrame())


-- Frame 4539
SetFrame( IncrementFrame())


-- Frame 4540
SetFrame( IncrementFrame())


-- Frame 4541
SetFrame( IncrementFrame())


-- Frame 4542
SetFrame( IncrementFrame())


-- Frame 4543
SetFrame( IncrementFrame())


-- Frame 4544
SetFrame( IncrementFrame())


-- Frame 4545
SetFrame( IncrementFrame())


-- Frame 4546
SetFrame( IncrementFrame())


-- Frame 4547
SetFrame( IncrementFrame())


-- Frame 4548
SetFrame( IncrementFrame())


-- Frame 4549
SetFrame( IncrementFrame())


-- Frame 4550
SetFrame( IncrementFrame())


-- Frame 4551
SetFrame( IncrementFrame())


-- Frame 4552
SetFrame( IncrementFrame())


-- Frame 4553
SetFrame( IncrementFrame())


-- Frame 4554
SetFrame( IncrementFrame())


-- Frame 4555
SetFrame( IncrementFrame())


-- Frame 4556
SetFrame( IncrementFrame())


-- Frame 4557
SetFrame( IncrementFrame())


-- Frame 4558
SetFrame( IncrementFrame())


-- Frame 4559
SetFrame( IncrementFrame())


-- Frame 4560
SetFrame( IncrementFrame())


-- Frame 4561
SetFrame( IncrementFrame())


-- Frame 4562
SetFrame( IncrementFrame())


-- Frame 4563
SetFrame( IncrementFrame())


-- Frame 4564
SetFrame( IncrementFrame())


-- Frame 4565
SetFrame( IncrementFrame())


-- Frame 4566
SetFrame( IncrementFrame())


-- Frame 4567
SetFrame( IncrementFrame())


-- Frame 4568
SetFrame( IncrementFrame())


-- Frame 4569
SetFrame( IncrementFrame())


-- Frame 4570
SetFrame( IncrementFrame())


-- Frame 4571
SetFrame( IncrementFrame())


-- Frame 4572
SetFrame( IncrementFrame())


-- Frame 4573
SetFrame( IncrementFrame())


-- Frame 4574
SetFrame( IncrementFrame())


-- Frame 4575
SetFrame( IncrementFrame())


-- Frame 4576
SetFrame( IncrementFrame())


-- Frame 4577
SetFrame( IncrementFrame())


-- Frame 4578
SetFrame( IncrementFrame())


-- Frame 4579
SetFrame( IncrementFrame())


-- Frame 4580
SetFrame( IncrementFrame())


-- Frame 4581
SetFrame( IncrementFrame())


-- Frame 4582
SetFrame( IncrementFrame())


-- Frame 4583
SetFrame( IncrementFrame())


-- Frame 4584
SetFrame( IncrementFrame())


-- Frame 4585
SetFrame( IncrementFrame())


-- Frame 4586
SetFrame( IncrementFrame())


-- Frame 4587
SetFrame( IncrementFrame())


-- Frame 4588
SetFrame( IncrementFrame())


-- Frame 4589
SetFrame( IncrementFrame())


-- Frame 4590
SetFrame( IncrementFrame())


-- Frame 4591
SetFrame( IncrementFrame())


-- Frame 4592
SetFrame( IncrementFrame())


-- Frame 4593
SetFrame( IncrementFrame())


-- Frame 4594
SetFrame( IncrementFrame())


-- Frame 4595
SetFrame( IncrementFrame())


-- Frame 4596
SetFrame( IncrementFrame())


-- Frame 4597
SetFrame( IncrementFrame())


-- Frame 4598
SetFrame( IncrementFrame())


-- Frame 4599
SetFrame( IncrementFrame())


-- Frame 4600
SetFrame( IncrementFrame())


-- Frame 4601
SetFrame( IncrementFrame())


-- Frame 4602
SetFrame( IncrementFrame())


-- Frame 4603
SetFrame( IncrementFrame())


-- Frame 4604
SetFrame( IncrementFrame())


-- Frame 4605
SetFrame( IncrementFrame())


-- Frame 4606
SetFrame( IncrementFrame())


-- Frame 4607
SetFrame( IncrementFrame())


-- Frame 4608
SetFrame( IncrementFrame())


-- Frame 4609
SetFrame( IncrementFrame())


-- Frame 4610
SetFrame( IncrementFrame())


-- Frame 4611
SetFrame( IncrementFrame())


-- Frame 4612
SetFrame( IncrementFrame())


-- Frame 4613
SetFrame( IncrementFrame())


-- Frame 4614
SetFrame( IncrementFrame())


-- Frame 4615
SetFrame( IncrementFrame())


-- Frame 4616
SetFrame( IncrementFrame())


-- Frame 4617
SetFrame( IncrementFrame())


-- Frame 4618
SetFrame( IncrementFrame())


-- Frame 4619
SetFrame( IncrementFrame())


-- Frame 4620
SetFrame( IncrementFrame())


-- Frame 4621
SetFrame( IncrementFrame())


-- Frame 4622
SetFrame( IncrementFrame())


-- Frame 4623
SetFrame( IncrementFrame())


-- Frame 4624
SetFrame( IncrementFrame())


-- Frame 4625
SetFrame( IncrementFrame())


-- Frame 4626
SetFrame( IncrementFrame())


-- Frame 4627
SetFrame( IncrementFrame())


-- Frame 4628
SetFrame( IncrementFrame())


-- Frame 4629
SetFrame( IncrementFrame())


-- Frame 4630
SetFrame( IncrementFrame())


-- Frame 4631
SetFrame( IncrementFrame())


-- Frame 4632
SetFrame( IncrementFrame())


-- Frame 4633
SetFrame( IncrementFrame())


-- Frame 4634
SetFrame( IncrementFrame())


-- Frame 4635
SetFrame( IncrementFrame())


-- Frame 4636
SetFrame( IncrementFrame())


-- Frame 4637
SetFrame( IncrementFrame())


-- Frame 4638
SetFrame( IncrementFrame())


-- Frame 4639
SetFrame( IncrementFrame())


-- Frame 4640
SetFrame( IncrementFrame())


-- Frame 4641
SetFrame( IncrementFrame())


-- Frame 4642
SetFrame( IncrementFrame())


-- Frame 4643
SetFrame( IncrementFrame())


-- Frame 4644
SetFrame( IncrementFrame())


-- Frame 4645
SetFrame( IncrementFrame())


-- Frame 4646
SetFrame( IncrementFrame())


-- Frame 4647
SetFrame( IncrementFrame())


-- Frame 4648
SetFrame( IncrementFrame())


-- Frame 4649
SetFrame( IncrementFrame())


-- Frame 4650
SetFrame( IncrementFrame())


-- Frame 4651
SetFrame( IncrementFrame())


-- Frame 4652
SetFrame( IncrementFrame())


-- Frame 4653
SetFrame( IncrementFrame())


-- Frame 4654
SetFrame( IncrementFrame())


-- Frame 4655
SetFrame( IncrementFrame())


-- Frame 4656
SetFrame( IncrementFrame())


-- Frame 4657
SetFrame( IncrementFrame())


-- Frame 4658
SetFrame( IncrementFrame())


-- Frame 4659
SetFrame( IncrementFrame())


-- Frame 4660
SetFrame( IncrementFrame())


-- Frame 4661
SetFrame( IncrementFrame())


-- Frame 4662
SetFrame( IncrementFrame())


-- Frame 4663
SetFrame( IncrementFrame())


-- Frame 4664
SetFrame( IncrementFrame())


-- Frame 4665
SetFrame( IncrementFrame())


-- Frame 4666
SetFrame( IncrementFrame())


-- Frame 4667
SetFrame( IncrementFrame())


-- Frame 4668
SetFrame( IncrementFrame())


-- Frame 4669
SetFrame( IncrementFrame())


-- Frame 4670
SetFrame( IncrementFrame())


-- Frame 4671
SetFrame( IncrementFrame())


-- Frame 4672
SetFrame( IncrementFrame())


-- Frame 4673
SetFrame( IncrementFrame())


-- Frame 4674
SetFrame( IncrementFrame())


-- Frame 4675
SetFrame( IncrementFrame())


-- Frame 4676
SetFrame( IncrementFrame())


-- Frame 4677
SetFrame( IncrementFrame())


-- Frame 4678
SetFrame( IncrementFrame())


-- Frame 4679
SetFrame( IncrementFrame())


-- Frame 4680
SetFrame( IncrementFrame())


-- Frame 4681
SetFrame( IncrementFrame())


-- Frame 4682
SetFrame( IncrementFrame())


-- Frame 4683
SetFrame( IncrementFrame())


-- Frame 4684
SetFrame( IncrementFrame())


-- Frame 4685
SetFrame( IncrementFrame())


-- Frame 4686
SetFrame( IncrementFrame())


-- Frame 4687
SetFrame( IncrementFrame())


-- Frame 4688
SetFrame( IncrementFrame())


-- Frame 4689
SetFrame( IncrementFrame())


-- Frame 4690
SetFrame( IncrementFrame())


-- Frame 4691
SetFrame( IncrementFrame())


-- Frame 4692
SetFrame( IncrementFrame())


-- Frame 4693
SetFrame( IncrementFrame())


-- Frame 4694
SetFrame( IncrementFrame())


-- Frame 4695
SetFrame( IncrementFrame())


-- Frame 4696
SetFrame( IncrementFrame())


-- Frame 4697
SetFrame( IncrementFrame())


-- Frame 4698
SetFrame( IncrementFrame())


-- Frame 4699
SetFrame( IncrementFrame())


-- Frame 4700
SetFrame( IncrementFrame())


-- Frame 4701
SetFrame( IncrementFrame())


-- Frame 4702
SetFrame( IncrementFrame())


-- Frame 4703
SetFrame( IncrementFrame())


-- Frame 4704
SetFrame( IncrementFrame())


-- Frame 4705
SetFrame( IncrementFrame())


-- Frame 4706
SetFrame( IncrementFrame())


-- Frame 4707
SetFrame( IncrementFrame())


-- Frame 4708
SetFrame( IncrementFrame())


-- Frame 4709
SetFrame( IncrementFrame())


-- Frame 4710
SetFrame( IncrementFrame())


-- Frame 4711
SetFrame( IncrementFrame())


-- Frame 4712
SetFrame( IncrementFrame())


-- Frame 4713
SetFrame( IncrementFrame())


-- Frame 4714
SetFrame( IncrementFrame())


-- Frame 4715
SetFrame( IncrementFrame())


-- Frame 4716
SetFrame( IncrementFrame())


-- Frame 4717
SetFrame( IncrementFrame())


-- Frame 4718
SetFrame( IncrementFrame())


-- Frame 4719
SetFrame( IncrementFrame())


-- Frame 4720
SetFrame( IncrementFrame())


-- Frame 4721
SetFrame( IncrementFrame())


-- Frame 4722
SetFrame( IncrementFrame())


-- Frame 4723
SetFrame( IncrementFrame())


-- Frame 4724
SetFrame( IncrementFrame())


-- Frame 4725
SetFrame( IncrementFrame())


-- Frame 4726
SetFrame( IncrementFrame())


-- Frame 4727
SetFrame( IncrementFrame())


-- Frame 4728
SetFrame( IncrementFrame())


-- Frame 4729
SetFrame( IncrementFrame())


-- Frame 4730
SetFrame( IncrementFrame())


-- Frame 4731
SetFrame( IncrementFrame())


-- Frame 4732
SetFrame( IncrementFrame())


-- Frame 4733
SetFrame( IncrementFrame())


-- Frame 4734
SetFrame( IncrementFrame())


-- Frame 4735
SetFrame( IncrementFrame())


-- Frame 4736
SetFrame( IncrementFrame())


-- Frame 4737
SetFrame( IncrementFrame())


-- Frame 4738
SetFrame( IncrementFrame())


-- Frame 4739
SetFrame( IncrementFrame())


-- Frame 4740
SetFrame( IncrementFrame())


-- Frame 4741
SetFrame( IncrementFrame())


-- Frame 4742
SetFrame( IncrementFrame())


-- Frame 4743
SetFrame( IncrementFrame())


-- Frame 4744
SetFrame( IncrementFrame())


-- Frame 4745
SetFrame( IncrementFrame())


-- Frame 4746
SetFrame( IncrementFrame())


-- Frame 4747
SetFrame( IncrementFrame())


-- Frame 4748
SetFrame( IncrementFrame())


-- Frame 4749
SetFrame( IncrementFrame())


-- Frame 4750
SetFrame( IncrementFrame())


-- Frame 4751
SetFrame( IncrementFrame())


-- Frame 4752
SetFrame( IncrementFrame())


-- Frame 4753
SetFrame( IncrementFrame())


-- Frame 4754
SetFrame( IncrementFrame())


-- Frame 4755
SetFrame( IncrementFrame())


-- Frame 4756
SetFrame( IncrementFrame())


-- Frame 4757
SetFrame( IncrementFrame())


-- Frame 4758
SetFrame( IncrementFrame())


-- Frame 4759
SetFrame( IncrementFrame())


-- Frame 4760
SetFrame( IncrementFrame())


-- Frame 4761
SetFrame( IncrementFrame())


-- Frame 4762
SetFrame( IncrementFrame())


-- Frame 4763
SetFrame( IncrementFrame())


-- Frame 4764
SetFrame( IncrementFrame())


-- Frame 4765
SetFrame( IncrementFrame())


-- Frame 4766
SetFrame( IncrementFrame())


-- Frame 4767
SetFrame( IncrementFrame())


-- Frame 4768
SetFrame( IncrementFrame())


-- Frame 4769
SetFrame( IncrementFrame())


-- Frame 4770
SetFrame( IncrementFrame())


-- Frame 4771
SetFrame( IncrementFrame())


-- Frame 4772
SetFrame( IncrementFrame())


-- Frame 4773
SetFrame( IncrementFrame())


-- Frame 4774
SetFrame( IncrementFrame())


-- Frame 4775
SetFrame( IncrementFrame())


-- Frame 4776
SetFrame( IncrementFrame())


-- Frame 4777
SetFrame( IncrementFrame())


-- Frame 4778
SetFrame( IncrementFrame())


-- Frame 4779
SetFrame( IncrementFrame())


-- Frame 4780
SetFrame( IncrementFrame())


-- Frame 4781
SetFrame( IncrementFrame())


-- Frame 4782
SetFrame( IncrementFrame())


-- Frame 4783
SetFrame( IncrementFrame())


-- Frame 4784
SetFrame( IncrementFrame())


-- Frame 4785
SetFrame( IncrementFrame())


-- Frame 4786
SetFrame( IncrementFrame())


-- Frame 4787
SetFrame( IncrementFrame())


-- Frame 4788
SetFrame( IncrementFrame())


-- Frame 4789
SetFrame( IncrementFrame())


-- Frame 4790
SetFrame( IncrementFrame())


-- Frame 4791
SetFrame( IncrementFrame())


-- Frame 4792
SetFrame( IncrementFrame())


-- Frame 4793
SetFrame( IncrementFrame())


-- Frame 4794
SetFrame( IncrementFrame())


-- Frame 4795
SetFrame( IncrementFrame())


-- Frame 4796
SetFrame( IncrementFrame())


-- Frame 4797
SetFrame( IncrementFrame())


-- Frame 4798
SetFrame( IncrementFrame())


-- Frame 4799
SetFrame( IncrementFrame())


-- Frame 4800
SetFrame( IncrementFrame())


-- Frame 4801
SetFrame( IncrementFrame())


-- Frame 4802
SetFrame( IncrementFrame())


-- Frame 4803
SetFrame( IncrementFrame())


-- Frame 4804
SetFrame( IncrementFrame())


-- Frame 4805
SetFrame( IncrementFrame())


-- Frame 4806
SetFrame( IncrementFrame())


-- Frame 4807
SetFrame( IncrementFrame())


-- Frame 4808
SetFrame( IncrementFrame())


-- Frame 4809
SetFrame( IncrementFrame())


-- Frame 4810
SetFrame( IncrementFrame())


-- Frame 4811
SetFrame( IncrementFrame())


-- Frame 4812
SetFrame( IncrementFrame())


-- Frame 4813
SetFrame( IncrementFrame())


-- Frame 4814
SetFrame( IncrementFrame())


-- Frame 4815
SetFrame( IncrementFrame())


-- Frame 4816
SetFrame( IncrementFrame())


-- Frame 4817
SetFrame( IncrementFrame())


-- Frame 4818
SetFrame( IncrementFrame())


-- Frame 4819
SetFrame( IncrementFrame())


-- Frame 4820
SetFrame( IncrementFrame())


-- Frame 4821
SetFrame( IncrementFrame())


-- Frame 4822
SetFrame( IncrementFrame())


-- Frame 4823
SetFrame( IncrementFrame())


-- Frame 4824
SetFrame( IncrementFrame())


-- Frame 4825
SetFrame( IncrementFrame())


-- Frame 4826
SetFrame( IncrementFrame())


-- Frame 4827
SetFrame( IncrementFrame())


-- Frame 4828
SetFrame( IncrementFrame())


-- Frame 4829
SetFrame( IncrementFrame())


-- Frame 4830
SetFrame( IncrementFrame())


-- Frame 4831
SetFrame( IncrementFrame())


-- Frame 4832
SetFrame( IncrementFrame())


-- Frame 4833
SetFrame( IncrementFrame())


-- Frame 4834
SetFrame( IncrementFrame())


-- Frame 4835
SetFrame( IncrementFrame())


-- Frame 4836
SetFrame( IncrementFrame())


-- Frame 4837
SetFrame( IncrementFrame())


-- Frame 4838
SetFrame( IncrementFrame())


-- Frame 4839
SetFrame( IncrementFrame())


-- Frame 4840
SetFrame( IncrementFrame())


-- Frame 4841
SetFrame( IncrementFrame())


-- Frame 4842
SetFrame( IncrementFrame())


-- Frame 4843
SetFrame( IncrementFrame())


-- Frame 4844
SetFrame( IncrementFrame())


-- Frame 4845
SetFrame( IncrementFrame())


-- Frame 4846
SetFrame( IncrementFrame())


-- Frame 4847
SetFrame( IncrementFrame())


-- Frame 4848
SetFrame( IncrementFrame())


-- Frame 4849
SetFrame( IncrementFrame())


-- Frame 4850
SetFrame( IncrementFrame())


-- Frame 4851
SetFrame( IncrementFrame())


-- Frame 4852
SetFrame( IncrementFrame())


-- Frame 4853
SetFrame( IncrementFrame())


-- Frame 4854
SetFrame( IncrementFrame())


-- Frame 4855
SetFrame( IncrementFrame())


-- Frame 4856
SetFrame( IncrementFrame())


-- Frame 4857
SetFrame( IncrementFrame())


-- Frame 4858
SetFrame( IncrementFrame())


-- Frame 4859
SetFrame( IncrementFrame())


-- Frame 4860
SetFrame( IncrementFrame())


-- Frame 4861
SetFrame( IncrementFrame())


-- Frame 4862
SetFrame( IncrementFrame())


-- Frame 4863
SetFrame( IncrementFrame())


-- Frame 4864
SetFrame( IncrementFrame())


-- Frame 4865
SetFrame( IncrementFrame())


-- Frame 4866
SetFrame( IncrementFrame())


-- Frame 4867
SetFrame( IncrementFrame())


-- Frame 4868
SetFrame( IncrementFrame())


-- Frame 4869
SetFrame( IncrementFrame())


-- Frame 4870
SetFrame( IncrementFrame())


-- Frame 4871
SetFrame( IncrementFrame())


-- Frame 4872
SetFrame( IncrementFrame())


-- Frame 4873
SetFrame( IncrementFrame())


-- Frame 4874
SetFrame( IncrementFrame())


-- Frame 4875
SetFrame( IncrementFrame())


-- Frame 4876
SetFrame( IncrementFrame())


-- Frame 4877
SetFrame( IncrementFrame())


-- Frame 4878
SetFrame( IncrementFrame())


-- Frame 4879
SetFrame( IncrementFrame())


-- Frame 4880
SetFrame( IncrementFrame())


-- Frame 4881
SetFrame( IncrementFrame())


-- Frame 4882
SetFrame( IncrementFrame())


-- Frame 4883
SetFrame( IncrementFrame())


-- Frame 4884
SetFrame( IncrementFrame())


-- Frame 4885
SetFrame( IncrementFrame())


-- Frame 4886
SetFrame( IncrementFrame())


-- Frame 4887
SetFrame( IncrementFrame())


-- Frame 4888
SetFrame( IncrementFrame())


-- Frame 4889
SetFrame( IncrementFrame())


-- Frame 4890
SetFrame( IncrementFrame())


-- Frame 4891
SetFrame( IncrementFrame())


-- Frame 4892
SetFrame( IncrementFrame())


-- Frame 4893
SetFrame( IncrementFrame())


-- Frame 4894
SetFrame( IncrementFrame())


-- Frame 4895
SetFrame( IncrementFrame())


-- Frame 4896
SetFrame( IncrementFrame())


-- Frame 4897
SetFrame( IncrementFrame())


-- Frame 4898
SetFrame( IncrementFrame())


-- Frame 4899
SetFrame( IncrementFrame())


-- Frame 4900
SetFrame( IncrementFrame())


-- Frame 4901
SetFrame( IncrementFrame())


-- Frame 4902
SetFrame( IncrementFrame())


-- Frame 4903
SetFrame( IncrementFrame())


-- Frame 4904
SetFrame( IncrementFrame())


-- Frame 4905
SetFrame( IncrementFrame())


-- Frame 4906
SetFrame( IncrementFrame())


-- Frame 4907
SetFrame( IncrementFrame())


-- Frame 4908
SetFrame( IncrementFrame())


-- Frame 4909
SetFrame( IncrementFrame())


-- Frame 4910
SetFrame( IncrementFrame())


-- Frame 4911
SetFrame( IncrementFrame())


-- Frame 4912
SetFrame( IncrementFrame())


-- Frame 4913
SetFrame( IncrementFrame())


-- Frame 4914
SetFrame( IncrementFrame())


-- Frame 4915
SetFrame( IncrementFrame())


-- Frame 4916
SetFrame( IncrementFrame())


-- Frame 4917
SetFrame( IncrementFrame())


-- Frame 4918
SetFrame( IncrementFrame())


-- Frame 4919
SetFrame( IncrementFrame())


-- Frame 4920
SetFrame( IncrementFrame())


-- Frame 4921
SetFrame( IncrementFrame())


-- Frame 4922
SetFrame( IncrementFrame())


-- Frame 4923
SetFrame( IncrementFrame())


-- Frame 4924
SetFrame( IncrementFrame())


-- Frame 4925
SetFrame( IncrementFrame())


-- Frame 4926
SetFrame( IncrementFrame())


-- Frame 4927
SetFrame( IncrementFrame())


-- Frame 4928
SetFrame( IncrementFrame())


-- Frame 4929
SetFrame( IncrementFrame())


-- Frame 4930
SetFrame( IncrementFrame())


-- Frame 4931
SetFrame( IncrementFrame())


-- Frame 4932
SetFrame( IncrementFrame())


-- Frame 4933
SetFrame( IncrementFrame())


-- Frame 4934
SetFrame( IncrementFrame())


-- Frame 4935
SetFrame( IncrementFrame())


-- Frame 4936
SetFrame( IncrementFrame())


-- Frame 4937
SetFrame( IncrementFrame())


-- Frame 4938
SetFrame( IncrementFrame())


-- Frame 4939
SetFrame( IncrementFrame())


-- Frame 4940
SetFrame( IncrementFrame())


-- Frame 4941
SetFrame( IncrementFrame())


-- Frame 4942
SetFrame( IncrementFrame())


-- Frame 4943
SetFrame( IncrementFrame())


-- Frame 4944
SetFrame( IncrementFrame())


-- Frame 4945
SetFrame( IncrementFrame())


-- Frame 4946
SetFrame( IncrementFrame())


-- Frame 4947
SetFrame( IncrementFrame())


-- Frame 4948
SetFrame( IncrementFrame())


-- Frame 4949
SetFrame( IncrementFrame())


-- Frame 4950
SetFrame( IncrementFrame())


-- Frame 4951
SetFrame( IncrementFrame())


-- Frame 4952
SetFrame( IncrementFrame())


-- Frame 4953
SetFrame( IncrementFrame())


-- Frame 4954
SetFrame( IncrementFrame())


-- Frame 4955
SetFrame( IncrementFrame())


-- Frame 4956
SetFrame( IncrementFrame())


-- Frame 4957
SetFrame( IncrementFrame())


-- Frame 4958
SetFrame( IncrementFrame())


-- Frame 4959
SetFrame( IncrementFrame())


-- Frame 4960
SetFrame( IncrementFrame())


-- Frame 4961
SetFrame( IncrementFrame())


-- Frame 4962
SetFrame( IncrementFrame())


-- Frame 4963
SetFrame( IncrementFrame())


-- Frame 4964
SetFrame( IncrementFrame())


-- Frame 4965
SetFrame( IncrementFrame())


-- Frame 4966
SetFrame( IncrementFrame())


-- Frame 4967
SetFrame( IncrementFrame())


-- Frame 4968
SetFrame( IncrementFrame())


-- Frame 4969
SetFrame( IncrementFrame())


-- Frame 4970
SetFrame( IncrementFrame())


-- Frame 4971
SetFrame( IncrementFrame())


-- Frame 4972
SetFrame( IncrementFrame())


-- Frame 4973
SetFrame( IncrementFrame())


-- Frame 4974
SetFrame( IncrementFrame())


-- Frame 4975
SetFrame( IncrementFrame())


-- Frame 4976
SetFrame( IncrementFrame())


-- Frame 4977
SetFrame( IncrementFrame())


-- Frame 4978
SetFrame( IncrementFrame())


-- Frame 4979
SetFrame( IncrementFrame())


-- Frame 4980
SetFrame( IncrementFrame())


-- Frame 4981
SetFrame( IncrementFrame())


-- Frame 4982
SetFrame( IncrementFrame())


-- Frame 4983
SetFrame( IncrementFrame())


-- Frame 4984
SetFrame( IncrementFrame())


-- Frame 4985
SetFrame( IncrementFrame())


-- Frame 4986
SetFrame( IncrementFrame())


-- Frame 4987
SetFrame( IncrementFrame())


-- Frame 4988
SetFrame( IncrementFrame())


-- Frame 4989
SetFrame( IncrementFrame())


-- Frame 4990
SetFrame( IncrementFrame())


-- Frame 4991
SetFrame( IncrementFrame())


-- Frame 4992
SetFrame( IncrementFrame())


-- Frame 4993
SetFrame( IncrementFrame())


-- Frame 4994
SetFrame( IncrementFrame())


-- Frame 4995
SetFrame( IncrementFrame())


-- Frame 4996
SetFrame( IncrementFrame())


-- Frame 4997
SetFrame( IncrementFrame())


-- Frame 4998
SetFrame( IncrementFrame())


-- Frame 4999
SetFrame( IncrementFrame())


-- Frame 5000
SetFrame( IncrementFrame())


-- Frame 5001
SetFrame( IncrementFrame())


-- Frame 5002
SetFrame( IncrementFrame())


-- Frame 5003
SetFrame( IncrementFrame())


-- Frame 5004
SetFrame( IncrementFrame())


-- Frame 5005
SetFrame( IncrementFrame())


-- Frame 5006
SetFrame( IncrementFrame())


-- Frame 5007
SetFrame( IncrementFrame())


-- Frame 5008
SetFrame( IncrementFrame())


-- Frame 5009
SetFrame( IncrementFrame())


-- Frame 5010
SetFrame( IncrementFrame())


-- Frame 5011
SetFrame( IncrementFrame())


-- Frame 5012
SetFrame( IncrementFrame())


-- Frame 5013
SetFrame( IncrementFrame())


-- Frame 5014
SetFrame( IncrementFrame())


-- Frame 5015
SetFrame( IncrementFrame())


-- Frame 5016
SetFrame( IncrementFrame())


-- Frame 5017
SetFrame( IncrementFrame())


-- Frame 5018
SetFrame( IncrementFrame())


-- Frame 5019
SetFrame( IncrementFrame())


-- Frame 5020
SetFrame( IncrementFrame())


-- Frame 5021
SetFrame( IncrementFrame())


-- Frame 5022
SetFrame( IncrementFrame())


-- Frame 5023
SetFrame( IncrementFrame())


-- Frame 5024
SetFrame( IncrementFrame())


-- Frame 5025
SetFrame( IncrementFrame())


-- Frame 5026
SetFrame( IncrementFrame())


-- Frame 5027
SetFrame( IncrementFrame())


-- Frame 5028
SetFrame( IncrementFrame())


-- Frame 5029
SetFrame( IncrementFrame())


-- Frame 5030
SetFrame( IncrementFrame())


-- Frame 5031
SetFrame( IncrementFrame())


-- Frame 5032
SetFrame( IncrementFrame())


-- Frame 5033
SetFrame( IncrementFrame())


-- Frame 5034
SetFrame( IncrementFrame())


-- Frame 5035
SetFrame( IncrementFrame())


-- Frame 5036
SetFrame( IncrementFrame())


-- Frame 5037
SetFrame( IncrementFrame())


-- Frame 5038
SetFrame( IncrementFrame())


-- Frame 5039
SetFrame( IncrementFrame())


-- Frame 5040
SetFrame( IncrementFrame())


-- Frame 5041
SetFrame( IncrementFrame())


-- Frame 5042
SetFrame( IncrementFrame())


-- Frame 5043
SetFrame( IncrementFrame())


-- Frame 5044
SetFrame( IncrementFrame())


-- Frame 5045
SetFrame( IncrementFrame())


-- Frame 5046
SetFrame( IncrementFrame())


-- Frame 5047
SetFrame( IncrementFrame())


-- Frame 5048
SetFrame( IncrementFrame())


-- Frame 5049
SetFrame( IncrementFrame())


-- Frame 5050
SetFrame( IncrementFrame())


-- Frame 5051
SetFrame( IncrementFrame())


-- Frame 5052
SetFrame( IncrementFrame())


-- Frame 5053
SetFrame( IncrementFrame())


-- Frame 5054
SetFrame( IncrementFrame())


-- Frame 5055
SetFrame( IncrementFrame())


-- Frame 5056
SetFrame( IncrementFrame())


-- Frame 5057
SetFrame( IncrementFrame())


-- Frame 5058
SetFrame( IncrementFrame())


-- Frame 5059
SetFrame( IncrementFrame())


-- Frame 5060
SetFrame( IncrementFrame())


-- Frame 5061
SetFrame( IncrementFrame())


-- Frame 5062
SetFrame( IncrementFrame())


-- Frame 5063
SetFrame( IncrementFrame())


-- Frame 5064
SetFrame( IncrementFrame())


-- Frame 5065
SetFrame( IncrementFrame())


-- Frame 5066
SetFrame( IncrementFrame())


-- Frame 5067
SetFrame( IncrementFrame())


-- Frame 5068
SetFrame( IncrementFrame())


-- Frame 5069
SetFrame( IncrementFrame())


-- Frame 5070
SetFrame( IncrementFrame())


-- Frame 5071
SetFrame( IncrementFrame())


-- Frame 5072
SetFrame( IncrementFrame())


-- Frame 5073
SetFrame( IncrementFrame())


-- Frame 5074
SetFrame( IncrementFrame())


-- Frame 5075
SetFrame( IncrementFrame())


-- Frame 5076
SetFrame( IncrementFrame())


-- Frame 5077
SetFrame( IncrementFrame())


-- Frame 5078
SetFrame( IncrementFrame())


-- Frame 5079
SetFrame( IncrementFrame())


-- Frame 5080
SetFrame( IncrementFrame())


-- Frame 5081
SetFrame( IncrementFrame())


-- Frame 5082
SetFrame( IncrementFrame())


-- Frame 5083
SetFrame( IncrementFrame())


-- Frame 5084
SetFrame( IncrementFrame())


-- Frame 5085
SetFrame( IncrementFrame())


-- Frame 5086
SetFrame( IncrementFrame())


-- Frame 5087
SetFrame( IncrementFrame())


-- Frame 5088
SetFrame( IncrementFrame())


-- Frame 5089
SetFrame( IncrementFrame())


-- Frame 5090
SetFrame( IncrementFrame())


-- Frame 5091
SetFrame( IncrementFrame())


-- Frame 5092
SetFrame( IncrementFrame())


-- Frame 5093
SetFrame( IncrementFrame())


-- Frame 5094
SetFrame( IncrementFrame())


-- Frame 5095
SetFrame( IncrementFrame())


-- Frame 5096
SetFrame( IncrementFrame())


-- Frame 5097
SetFrame( IncrementFrame())


-- Frame 5098
SetFrame( IncrementFrame())


-- Frame 5099
SetFrame( IncrementFrame())


-- Frame 5100
SetFrame( IncrementFrame())


-- Frame 5101
SetFrame( IncrementFrame())


-- Frame 5102
SetFrame( IncrementFrame())


-- Frame 5103
SetFrame( IncrementFrame())


-- Frame 5104
SetFrame( IncrementFrame())


-- Frame 5105
SetFrame( IncrementFrame())


-- Frame 5106
SetFrame( IncrementFrame())


-- Frame 5107
SetFrame( IncrementFrame())


-- Frame 5108
SetFrame( IncrementFrame())


-- Frame 5109
SetFrame( IncrementFrame())


-- Frame 5110
SetFrame( IncrementFrame())


-- Frame 5111
SetFrame( IncrementFrame())


-- Frame 5112
SetFrame( IncrementFrame())


-- Frame 5113
SetFrame( IncrementFrame())


-- Frame 5114
SetFrame( IncrementFrame())


-- Frame 5115
SetFrame( IncrementFrame())


-- Frame 5116
SetFrame( IncrementFrame())


-- Frame 5117
SetFrame( IncrementFrame())


-- Frame 5118
SetFrame( IncrementFrame())


-- Frame 5119
SetFrame( IncrementFrame())


-- Frame 5120
SetFrame( IncrementFrame())


-- Frame 5121
SetFrame( IncrementFrame())


-- Frame 5122
SetFrame( IncrementFrame())


-- Frame 5123
SetFrame( IncrementFrame())


-- Frame 5124
SetFrame( IncrementFrame())


-- Frame 5125
SetFrame( IncrementFrame())


-- Frame 5126
SetFrame( IncrementFrame())


-- Frame 5127
SetFrame( IncrementFrame())


-- Frame 5128
SetFrame( IncrementFrame())


-- Frame 5129
SetFrame( IncrementFrame())


-- Frame 5130
SetFrame( IncrementFrame())


-- Frame 5131
SetFrame( IncrementFrame())


-- Frame 5132
SetFrame( IncrementFrame())


-- Frame 5133
SetFrame( IncrementFrame())


-- Frame 5134
SetFrame( IncrementFrame())


-- Frame 5135
SetFrame( IncrementFrame())


-- Frame 5136
SetFrame( IncrementFrame())


-- Frame 5137
SetFrame( IncrementFrame())


-- Frame 5138
SetFrame( IncrementFrame())


-- Frame 5139
SetFrame( IncrementFrame())


-- Frame 5140
SetFrame( IncrementFrame())


-- Frame 5141
SetFrame( IncrementFrame())


-- Frame 5142
SetFrame( IncrementFrame())


-- Frame 5143
SetFrame( IncrementFrame())


-- Frame 5144
SetFrame( IncrementFrame())


-- Frame 5145
SetFrame( IncrementFrame())


-- Frame 5146
SetFrame( IncrementFrame())


-- Frame 5147
SetFrame( IncrementFrame())


-- Frame 5148
SetFrame( IncrementFrame())


-- Frame 5149
SetFrame( IncrementFrame())


-- Frame 5150
SetFrame( IncrementFrame())


-- Frame 5151
SetFrame( IncrementFrame())


-- Frame 5152
SetFrame( IncrementFrame())


-- Frame 5153
SetFrame( IncrementFrame())


-- Frame 5154
SetFrame( IncrementFrame())


-- Frame 5155
SetFrame( IncrementFrame())


-- Frame 5156
SetFrame( IncrementFrame())


-- Frame 5157
SetFrame( IncrementFrame())


-- Frame 5158
SetFrame( IncrementFrame())


-- Frame 5159
SetFrame( IncrementFrame())


-- Frame 5160
SetFrame( IncrementFrame())


-- Frame 5161
SetFrame( IncrementFrame())


-- Frame 5162
SetFrame( IncrementFrame())


-- Frame 5163
SetFrame( IncrementFrame())


-- Frame 5164
SetFrame( IncrementFrame())


-- Frame 5165
SetFrame( IncrementFrame())


-- Frame 5166
SetFrame( IncrementFrame())


-- Frame 5167
SetFrame( IncrementFrame())


-- Frame 5168
SetFrame( IncrementFrame())


-- Frame 5169
SetFrame( IncrementFrame())


-- Frame 5170
SetFrame( IncrementFrame())


-- Frame 5171
SetFrame( IncrementFrame())


-- Frame 5172
SetFrame( IncrementFrame())


-- Frame 5173
SetFrame( IncrementFrame())


-- Frame 5174
SetFrame( IncrementFrame())


-- Frame 5175
SetFrame( IncrementFrame())


-- Frame 5176
SetFrame( IncrementFrame())


-- Frame 5177
SetFrame( IncrementFrame())


-- Frame 5178
SetFrame( IncrementFrame())


-- Frame 5179
SetFrame( IncrementFrame())


-- Frame 5180
SetFrame( IncrementFrame())


-- Frame 5181
SetFrame( IncrementFrame())


-- Frame 5182
SetFrame( IncrementFrame())


-- Frame 5183
SetFrame( IncrementFrame())


-- Frame 5184
SetFrame( IncrementFrame())


-- Frame 5185
SetFrame( IncrementFrame())


-- Frame 5186
SetFrame( IncrementFrame())


-- Frame 5187
SetFrame( IncrementFrame())


-- Frame 5188
SetFrame( IncrementFrame())


-- Frame 5189
SetFrame( IncrementFrame())


-- Frame 5190
SetFrame( IncrementFrame())


-- Frame 5191
SetFrame( IncrementFrame())


-- Frame 5192
SetFrame( IncrementFrame())


-- Frame 5193
SetFrame( IncrementFrame())


-- Frame 5194
SetFrame( IncrementFrame())


-- Frame 5195
SetFrame( IncrementFrame())


-- Frame 5196
SetFrame( IncrementFrame())


-- Frame 5197
SetFrame( IncrementFrame())


-- Frame 5198
SetFrame( IncrementFrame())


-- Frame 5199
SetFrame( IncrementFrame())


-- Frame 5200
SetFrame( IncrementFrame())


-- Frame 5201
SetFrame( IncrementFrame())


-- Frame 5202
SetFrame( IncrementFrame())


-- Frame 5203
SetFrame( IncrementFrame())


-- Frame 5204
SetFrame( IncrementFrame())


-- Frame 5205
SetFrame( IncrementFrame())


-- Frame 5206
SetFrame( IncrementFrame())


-- Frame 5207
SetFrame( IncrementFrame())


-- Frame 5208
SetFrame( IncrementFrame())


-- Frame 5209
SetFrame( IncrementFrame())


-- Frame 5210
SetFrame( IncrementFrame())


-- Frame 5211
SetFrame( IncrementFrame())


-- Frame 5212
SetFrame( IncrementFrame())


-- Frame 5213
SetFrame( IncrementFrame())


-- Frame 5214
SetFrame( IncrementFrame())


-- Frame 5215
SetFrame( IncrementFrame())


-- Frame 5216
SetFrame( IncrementFrame())


-- Frame 5217
SetFrame( IncrementFrame())


-- Frame 5218
SetFrame( IncrementFrame())


-- Frame 5219
SetFrame( IncrementFrame())


-- Frame 5220
SetFrame( IncrementFrame())


-- Frame 5221
SetFrame( IncrementFrame())


-- Frame 5222
SetFrame( IncrementFrame())


-- Frame 5223
SetFrame( IncrementFrame())


-- Frame 5224
SetFrame( IncrementFrame())


-- Frame 5225
SetFrame( IncrementFrame())


-- Frame 5226
SetFrame( IncrementFrame())


-- Frame 5227
SetFrame( IncrementFrame())


-- Frame 5228
SetFrame( IncrementFrame())


-- Frame 5229
SetFrame( IncrementFrame())


-- Frame 5230
SetFrame( IncrementFrame())


-- Frame 5231
SetFrame( IncrementFrame())


-- Frame 5232
SetFrame( IncrementFrame())


-- Frame 5233
SetFrame( IncrementFrame())


-- Frame 5234
SetFrame( IncrementFrame())


-- Frame 5235
SetFrame( IncrementFrame())


-- Frame 5236
SetFrame( IncrementFrame())


-- Frame 5237
SetFrame( IncrementFrame())


-- Frame 5238
SetFrame( IncrementFrame())


-- Frame 5239
SetFrame( IncrementFrame())


-- Frame 5240
SetFrame( IncrementFrame())


-- Frame 5241
SetFrame( IncrementFrame())


-- Frame 5242
SetFrame( IncrementFrame())


-- Frame 5243
SetFrame( IncrementFrame())


-- Frame 5244
SetFrame( IncrementFrame())


-- Frame 5245
SetFrame( IncrementFrame())


-- Frame 5246
SetFrame( IncrementFrame())


-- Frame 5247
SetFrame( IncrementFrame())


-- Frame 5248
SetFrame( IncrementFrame())


-- Frame 5249
SetFrame( IncrementFrame())


-- Frame 5250
SetFrame( IncrementFrame())


-- Frame 5251
SetFrame( IncrementFrame())


-- Frame 5252
SetFrame( IncrementFrame())


-- Frame 5253
SetFrame( IncrementFrame())


-- Frame 5254
SetFrame( IncrementFrame())


-- Frame 5255
SetFrame( IncrementFrame())


-- Frame 5256
SetFrame( IncrementFrame())


-- Frame 5257
SetFrame( IncrementFrame())


-- Frame 5258
SetFrame( IncrementFrame())


-- Frame 5259
SetFrame( IncrementFrame())


-- Frame 5260
SetFrame( IncrementFrame())


-- Frame 5261
SetFrame( IncrementFrame())


-- Frame 5262
SetFrame( IncrementFrame())


-- Frame 5263
SetFrame( IncrementFrame())


-- Frame 5264
SetFrame( IncrementFrame())


-- Frame 5265
SetFrame( IncrementFrame())


-- Frame 5266
SetFrame( IncrementFrame())


-- Frame 5267
SetFrame( IncrementFrame())


-- Frame 5268
SetFrame( IncrementFrame())


