﻿namespace NS_CutsceneCreatorToolset
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerRefresh = new System.Windows.Forms.Timer(this.components);
            this.menu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.containerAll = new System.Windows.Forms.SplitContainer();
            this.panelAssets = new System.Windows.Forms.Panel();
            this.lblTitleAssets = new System.Windows.Forms.Label();
            this.treeAssets = new System.Windows.Forms.TreeView();
            this.containerTopBottomRight = new System.Windows.Forms.SplitContainer();
            this.containerTopBottom = new System.Windows.Forms.SplitContainer();
            this.panelRender = new System.Windows.Forms.Panel();
            this.panelFrameSlider = new System.Windows.Forms.Panel();
            this.cbGlobalCam = new System.Windows.Forms.CheckBox();
            this.btnAction = new System.Windows.Forms.Button();
            this.btnSphere = new System.Windows.Forms.Button();
            this.btnBackEnd = new System.Windows.Forms.Button();
            this.btnForwardEnd = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.lblFrameCount = new System.Windows.Forms.Label();
            this.numFrame = new System.Windows.Forms.NumericUpDown();
            this.frameSlider = new System.Windows.Forms.TrackBar();
            this.btnReload = new System.Windows.Forms.Button();
            this.containerRight = new System.Windows.Forms.SplitContainer();
            this.panelSceneGraph = new System.Windows.Forms.Panel();
            this.treeSceneGraph = new System.Windows.Forms.TreeView();
            this.lblTitleSceneGraph = new System.Windows.Forms.Label();
            this.panelObjectActions = new System.Windows.Forms.Panel();
            this.listViewObjectActions = new System.Windows.Forms.ListView();
            this.lblTitleObjectActions = new System.Windows.Forms.Label();
            this.timerPlay = new System.Windows.Forms.Timer(this.components);
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.containerAll)).BeginInit();
            this.containerAll.Panel1.SuspendLayout();
            this.containerAll.Panel2.SuspendLayout();
            this.containerAll.SuspendLayout();
            this.panelAssets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.containerTopBottomRight)).BeginInit();
            this.containerTopBottomRight.Panel1.SuspendLayout();
            this.containerTopBottomRight.Panel2.SuspendLayout();
            this.containerTopBottomRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.containerTopBottom)).BeginInit();
            this.containerTopBottom.Panel1.SuspendLayout();
            this.containerTopBottom.Panel2.SuspendLayout();
            this.containerTopBottom.SuspendLayout();
            this.panelFrameSlider.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frameSlider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.containerRight)).BeginInit();
            this.containerRight.Panel1.SuspendLayout();
            this.containerRight.Panel2.SuspendLayout();
            this.containerRight.SuspendLayout();
            this.panelSceneGraph.SuspendLayout();
            this.panelObjectActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerRefresh
            // 
            this.timerRefresh.Interval = 10;
            this.timerRefresh.Tick += new System.EventHandler(this.timerRefresh_Tick);
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(780, 24);
            this.menu.TabIndex = 2;
            this.menu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // containerAll
            // 
            this.containerAll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.containerAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerAll.IsSplitterFixed = true;
            this.containerAll.Location = new System.Drawing.Point(0, 24);
            this.containerAll.Name = "containerAll";
            // 
            // containerAll.Panel1
            // 
            this.containerAll.Panel1.Controls.Add(this.panelAssets);
            // 
            // containerAll.Panel2
            // 
            this.containerAll.Panel2.Controls.Add(this.containerTopBottomRight);
            this.containerAll.Size = new System.Drawing.Size(780, 405);
            this.containerAll.SplitterDistance = 171;
            this.containerAll.TabIndex = 3;
            // 
            // panelAssets
            // 
            this.panelAssets.Controls.Add(this.lblTitleAssets);
            this.panelAssets.Controls.Add(this.treeAssets);
            this.panelAssets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAssets.Location = new System.Drawing.Point(0, 0);
            this.panelAssets.Name = "panelAssets";
            this.panelAssets.Size = new System.Drawing.Size(169, 403);
            this.panelAssets.TabIndex = 0;
            // 
            // lblTitleAssets
            // 
            this.lblTitleAssets.AutoSize = true;
            this.lblTitleAssets.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleAssets.Location = new System.Drawing.Point(3, 2);
            this.lblTitleAssets.Name = "lblTitleAssets";
            this.lblTitleAssets.Size = new System.Drawing.Size(48, 15);
            this.lblTitleAssets.TabIndex = 2;
            this.lblTitleAssets.Text = "Assets";
            // 
            // treeAssets
            // 
            this.treeAssets.HideSelection = false;
            this.treeAssets.Location = new System.Drawing.Point(-1, 20);
            this.treeAssets.Name = "treeAssets";
            this.treeAssets.Size = new System.Drawing.Size(170, 384);
            this.treeAssets.TabIndex = 1;
            this.treeAssets.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeAssets_NodeMouseClick);
            // 
            // containerTopBottomRight
            // 
            this.containerTopBottomRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.containerTopBottomRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerTopBottomRight.IsSplitterFixed = true;
            this.containerTopBottomRight.Location = new System.Drawing.Point(0, 0);
            this.containerTopBottomRight.Name = "containerTopBottomRight";
            // 
            // containerTopBottomRight.Panel1
            // 
            this.containerTopBottomRight.Panel1.Controls.Add(this.containerTopBottom);
            // 
            // containerTopBottomRight.Panel2
            // 
            this.containerTopBottomRight.Panel2.Controls.Add(this.containerRight);
            this.containerTopBottomRight.Size = new System.Drawing.Size(605, 405);
            this.containerTopBottomRight.SplitterDistance = 414;
            this.containerTopBottomRight.TabIndex = 0;
            // 
            // containerTopBottom
            // 
            this.containerTopBottom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.containerTopBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerTopBottom.Location = new System.Drawing.Point(0, 0);
            this.containerTopBottom.Name = "containerTopBottom";
            this.containerTopBottom.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // containerTopBottom.Panel1
            // 
            this.containerTopBottom.Panel1.Controls.Add(this.panelRender);
            // 
            // containerTopBottom.Panel2
            // 
            this.containerTopBottom.Panel2.Controls.Add(this.panelFrameSlider);
            this.containerTopBottom.Size = new System.Drawing.Size(414, 405);
            this.containerTopBottom.SplitterDistance = 186;
            this.containerTopBottom.TabIndex = 0;
            // 
            // panelRender
            // 
            this.panelRender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRender.Location = new System.Drawing.Point(0, 0);
            this.panelRender.Name = "panelRender";
            this.panelRender.Size = new System.Drawing.Size(412, 184);
            this.panelRender.TabIndex = 0;
            this.panelRender.MouseEnter += new System.EventHandler(this.panelRender_MouseEnter);
            this.panelRender.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelRender_MouseMove);
            // 
            // panelFrameSlider
            // 
            this.panelFrameSlider.Controls.Add(this.cbGlobalCam);
            this.panelFrameSlider.Controls.Add(this.btnAction);
            this.panelFrameSlider.Controls.Add(this.btnSphere);
            this.panelFrameSlider.Controls.Add(this.btnBackEnd);
            this.panelFrameSlider.Controls.Add(this.btnForwardEnd);
            this.panelFrameSlider.Controls.Add(this.btnBack);
            this.panelFrameSlider.Controls.Add(this.btnForward);
            this.panelFrameSlider.Controls.Add(this.btnPlay);
            this.panelFrameSlider.Controls.Add(this.lblFrameCount);
            this.panelFrameSlider.Controls.Add(this.numFrame);
            this.panelFrameSlider.Controls.Add(this.frameSlider);
            this.panelFrameSlider.Controls.Add(this.btnReload);
            this.panelFrameSlider.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFrameSlider.Location = new System.Drawing.Point(0, 0);
            this.panelFrameSlider.Name = "panelFrameSlider";
            this.panelFrameSlider.Size = new System.Drawing.Size(412, 213);
            this.panelFrameSlider.TabIndex = 0;
            // 
            // cbGlobalCam
            // 
            this.cbGlobalCam.AutoSize = true;
            this.cbGlobalCam.Checked = true;
            this.cbGlobalCam.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGlobalCam.Location = new System.Drawing.Point(198, 136);
            this.cbGlobalCam.Name = "cbGlobalCam";
            this.cbGlobalCam.Size = new System.Drawing.Size(123, 17);
            this.cbGlobalCam.TabIndex = 10;
            this.cbGlobalCam.Text = "Use Global Camera?";
            this.cbGlobalCam.UseVisualStyleBackColor = true;
            this.cbGlobalCam.CheckedChanged += new System.EventHandler(this.cbGlobalCam_CheckedChanged);
            // 
            // btnAction
            // 
            this.btnAction.Location = new System.Drawing.Point(4, 131);
            this.btnAction.Name = "btnAction";
            this.btnAction.Size = new System.Drawing.Size(92, 23);
            this.btnAction.TabIndex = 9;
            this.btnAction.Text = "Add Action";
            this.btnAction.UseVisualStyleBackColor = true;
            this.btnAction.Click += new System.EventHandler(this.btnAction_Click);
            // 
            // btnSphere
            // 
            this.btnSphere.Location = new System.Drawing.Point(4, 102);
            this.btnSphere.Name = "btnSphere";
            this.btnSphere.Size = new System.Drawing.Size(92, 23);
            this.btnSphere.TabIndex = 8;
            this.btnSphere.Text = "Create Sphere";
            this.btnSphere.UseVisualStyleBackColor = true;
            this.btnSphere.Click += new System.EventHandler(this.btnSphere_Click);
            // 
            // btnBackEnd
            // 
            this.btnBackEnd.Location = new System.Drawing.Point(172, 91);
            this.btnBackEnd.Name = "btnBackEnd";
            this.btnBackEnd.Size = new System.Drawing.Size(35, 23);
            this.btnBackEnd.TabIndex = 7;
            this.btnBackEnd.Text = "|<<";
            this.btnBackEnd.UseVisualStyleBackColor = true;
            this.btnBackEnd.Click += new System.EventHandler(this.btnBackEnd_Click);
            // 
            // btnForwardEnd
            // 
            this.btnForwardEnd.Location = new System.Drawing.Point(336, 91);
            this.btnForwardEnd.Name = "btnForwardEnd";
            this.btnForwardEnd.Size = new System.Drawing.Size(35, 23);
            this.btnForwardEnd.TabIndex = 6;
            this.btnForwardEnd.Text = ">>|";
            this.btnForwardEnd.UseVisualStyleBackColor = true;
            this.btnForwardEnd.Click += new System.EventHandler(this.btnForwardEnd_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(213, 91);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(35, 23);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "<";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnForward
            // 
            this.btnForward.Location = new System.Drawing.Point(295, 91);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(35, 23);
            this.btnForward.TabIndex = 4;
            this.btnForward.Text = ">";
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(254, 91);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(35, 23);
            this.btnPlay.TabIndex = 3;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // lblFrameCount
            // 
            this.lblFrameCount.AutoSize = true;
            this.lblFrameCount.Location = new System.Drawing.Point(195, 75);
            this.lblFrameCount.Name = "lblFrameCount";
            this.lblFrameCount.Size = new System.Drawing.Size(33, 13);
            this.lblFrameCount.TabIndex = 2;
            this.lblFrameCount.Text = "/ 100";
            // 
            // numFrame
            // 
            this.numFrame.Location = new System.Drawing.Point(139, 73);
            this.numFrame.Name = "numFrame";
            this.numFrame.Size = new System.Drawing.Size(50, 20);
            this.numFrame.TabIndex = 1;
            this.numFrame.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numFrame.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // frameSlider
            // 
            this.frameSlider.Location = new System.Drawing.Point(3, 21);
            this.frameSlider.Name = "frameSlider";
            this.frameSlider.Size = new System.Drawing.Size(406, 45);
            this.frameSlider.TabIndex = 0;
            // 
            // btnReload
            // 
            this.btnReload.Location = new System.Drawing.Point(3, 72);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(93, 23);
            this.btnReload.TabIndex = 0;
            this.btnReload.Text = "Reload DirectX";
            this.btnReload.UseVisualStyleBackColor = true;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // containerRight
            // 
            this.containerRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.containerRight.Location = new System.Drawing.Point(0, 0);
            this.containerRight.Name = "containerRight";
            this.containerRight.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // containerRight.Panel1
            // 
            this.containerRight.Panel1.Controls.Add(this.panelSceneGraph);
            // 
            // containerRight.Panel2
            // 
            this.containerRight.Panel2.Controls.Add(this.panelObjectActions);
            this.containerRight.Size = new System.Drawing.Size(185, 403);
            this.containerRight.SplitterDistance = 205;
            this.containerRight.TabIndex = 0;
            this.containerRight.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.containerRight_SplitterMoved);
            // 
            // panelSceneGraph
            // 
            this.panelSceneGraph.Controls.Add(this.treeSceneGraph);
            this.panelSceneGraph.Controls.Add(this.lblTitleSceneGraph);
            this.panelSceneGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSceneGraph.Location = new System.Drawing.Point(0, 0);
            this.panelSceneGraph.Name = "panelSceneGraph";
            this.panelSceneGraph.Size = new System.Drawing.Size(185, 205);
            this.panelSceneGraph.TabIndex = 0;
            // 
            // treeSceneGraph
            // 
            this.treeSceneGraph.HideSelection = false;
            this.treeSceneGraph.Location = new System.Drawing.Point(3, 20);
            this.treeSceneGraph.Name = "treeSceneGraph";
            this.treeSceneGraph.Size = new System.Drawing.Size(179, 182);
            this.treeSceneGraph.TabIndex = 0;
            this.treeSceneGraph.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeSceneGraph_NodeMouseClick);
            // 
            // lblTitleSceneGraph
            // 
            this.lblTitleSceneGraph.AutoSize = true;
            this.lblTitleSceneGraph.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleSceneGraph.Location = new System.Drawing.Point(3, 2);
            this.lblTitleSceneGraph.Name = "lblTitleSceneGraph";
            this.lblTitleSceneGraph.Size = new System.Drawing.Size(90, 15);
            this.lblTitleSceneGraph.TabIndex = 1;
            this.lblTitleSceneGraph.Text = "Scene Graph";
            // 
            // panelObjectActions
            // 
            this.panelObjectActions.Controls.Add(this.listViewObjectActions);
            this.panelObjectActions.Controls.Add(this.lblTitleObjectActions);
            this.panelObjectActions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelObjectActions.Location = new System.Drawing.Point(0, 0);
            this.panelObjectActions.Name = "panelObjectActions";
            this.panelObjectActions.Size = new System.Drawing.Size(185, 194);
            this.panelObjectActions.TabIndex = 0;
            // 
            // listViewObjectActions
            // 
            this.listViewObjectActions.HideSelection = false;
            this.listViewObjectActions.Location = new System.Drawing.Point(3, 20);
            this.listViewObjectActions.Name = "listViewObjectActions";
            this.listViewObjectActions.Size = new System.Drawing.Size(179, 171);
            this.listViewObjectActions.TabIndex = 3;
            this.listViewObjectActions.UseCompatibleStateImageBehavior = false;
            this.listViewObjectActions.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.listViewObjectActions_ColumnClick);
            this.listViewObjectActions.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listViewObjectActions_ItemSelectionChanged);
            this.listViewObjectActions.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listViewObjectActions_MouseClick);
            // 
            // lblTitleObjectActions
            // 
            this.lblTitleObjectActions.AutoSize = true;
            this.lblTitleObjectActions.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitleObjectActions.Location = new System.Drawing.Point(3, 2);
            this.lblTitleObjectActions.Name = "lblTitleObjectActions";
            this.lblTitleObjectActions.Size = new System.Drawing.Size(98, 15);
            this.lblTitleObjectActions.TabIndex = 2;
            this.lblTitleObjectActions.Text = "Object Actions";
            // 
            // timerPlay
            // 
            this.timerPlay.Interval = 10;
            this.timerPlay.Tick += new System.EventHandler(this.timerPlay_Tick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "Cut Scene files|*.cs";
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.Filter = "Cut Scene files|*.cs";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 429);
            this.Controls.Add(this.containerAll);
            this.Controls.Add(this.menu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menu;
            this.Name = "MainForm";
            this.Text = "Cutscene Creator Toolset";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.containerAll.Panel1.ResumeLayout(false);
            this.containerAll.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.containerAll)).EndInit();
            this.containerAll.ResumeLayout(false);
            this.panelAssets.ResumeLayout(false);
            this.panelAssets.PerformLayout();
            this.containerTopBottomRight.Panel1.ResumeLayout(false);
            this.containerTopBottomRight.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.containerTopBottomRight)).EndInit();
            this.containerTopBottomRight.ResumeLayout(false);
            this.containerTopBottom.Panel1.ResumeLayout(false);
            this.containerTopBottom.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.containerTopBottom)).EndInit();
            this.containerTopBottom.ResumeLayout(false);
            this.panelFrameSlider.ResumeLayout(false);
            this.panelFrameSlider.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numFrame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frameSlider)).EndInit();
            this.containerRight.Panel1.ResumeLayout(false);
            this.containerRight.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.containerRight)).EndInit();
            this.containerRight.ResumeLayout(false);
            this.panelSceneGraph.ResumeLayout(false);
            this.panelSceneGraph.PerformLayout();
            this.panelObjectActions.ResumeLayout(false);
            this.panelObjectActions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timerRefresh;
        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.SplitContainer containerAll;
        private System.Windows.Forms.SplitContainer containerTopBottomRight;
        private System.Windows.Forms.SplitContainer containerTopBottom;
        private System.Windows.Forms.Panel panelRender;
        private System.Windows.Forms.Panel panelAssets;
        private System.Windows.Forms.Panel panelFrameSlider;
        private System.Windows.Forms.Button btnReload;
        private System.Windows.Forms.TrackBar frameSlider;
        private System.Windows.Forms.Label lblTitleSceneGraph;
        private System.Windows.Forms.TreeView treeSceneGraph;
        private System.Windows.Forms.TreeView treeAssets;
        private System.Windows.Forms.Label lblTitleAssets;
        private System.Windows.Forms.Label lblFrameCount;
        private System.Windows.Forms.NumericUpDown numFrame;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Timer timerPlay;
        private System.Windows.Forms.Button btnBackEnd;
        private System.Windows.Forms.Button btnForwardEnd;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.Button btnSphere;
        private System.Windows.Forms.Button btnAction;
        private System.Windows.Forms.ListView listViewObjectActions;
        private System.Windows.Forms.Label lblTitleObjectActions;
        private System.Windows.Forms.SplitContainer containerRight;
        private System.Windows.Forms.Panel panelSceneGraph;
        private System.Windows.Forms.Panel panelObjectActions;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.CheckBox cbGlobalCam;

    }
}

