﻿namespace NS_CutsceneCreatorToolset
{
    partial class NewObjectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblParam1 = new System.Windows.Forms.Label();
            this.lblParam3 = new System.Windows.Forms.Label();
            this.lblParam2 = new System.Windows.Forms.Label();
            this.lblNameMesh = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbNameMesh = new System.Windows.Forms.TextBox();
            this.tbParam1 = new System.Windows.Forms.TextBox();
            this.tbParam2 = new System.Windows.Forms.TextBox();
            this.tbParam3 = new System.Windows.Forms.TextBox();
            this.lblPos = new System.Windows.Forms.Label();
            this.lblPosX = new System.Windows.Forms.Label();
            this.lblPosY = new System.Windows.Forms.Label();
            this.lblPosZ = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbPosX = new System.Windows.Forms.TextBox();
            this.tbPosY = new System.Windows.Forms.TextBox();
            this.tbPosZ = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 9);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(75, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Unique Name:";
            // 
            // lblParam1
            // 
            this.lblParam1.AutoSize = true;
            this.lblParam1.Location = new System.Drawing.Point(12, 139);
            this.lblParam1.Name = "lblParam1";
            this.lblParam1.Size = new System.Drawing.Size(42, 13);
            this.lblParam1.TabIndex = 2;
            this.lblParam1.Text = "param1";
            // 
            // lblParam3
            // 
            this.lblParam3.AutoSize = true;
            this.lblParam3.Location = new System.Drawing.Point(12, 191);
            this.lblParam3.Name = "lblParam3";
            this.lblParam3.Size = new System.Drawing.Size(42, 13);
            this.lblParam3.TabIndex = 4;
            this.lblParam3.Text = "param3";
            // 
            // lblParam2
            // 
            this.lblParam2.AutoSize = true;
            this.lblParam2.Location = new System.Drawing.Point(12, 165);
            this.lblParam2.Name = "lblParam2";
            this.lblParam2.Size = new System.Drawing.Size(42, 13);
            this.lblParam2.TabIndex = 3;
            this.lblParam2.Text = "param2";
            // 
            // lblNameMesh
            // 
            this.lblNameMesh.AutoSize = true;
            this.lblNameMesh.Location = new System.Drawing.Point(12, 35);
            this.lblNameMesh.Name = "lblNameMesh";
            this.lblNameMesh.Size = new System.Drawing.Size(67, 13);
            this.lblNameMesh.TabIndex = 9;
            this.lblNameMesh.Text = "Mesh Name:";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(93, 6);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(100, 20);
            this.tbName.TabIndex = 1;
            // 
            // tbNameMesh
            // 
            this.tbNameMesh.Location = new System.Drawing.Point(93, 32);
            this.tbNameMesh.Name = "tbNameMesh";
            this.tbNameMesh.Size = new System.Drawing.Size(100, 20);
            this.tbNameMesh.TabIndex = 2;
            // 
            // tbParam1
            // 
            this.tbParam1.Location = new System.Drawing.Point(93, 136);
            this.tbParam1.Name = "tbParam1";
            this.tbParam1.Size = new System.Drawing.Size(100, 20);
            this.tbParam1.TabIndex = 6;
            this.tbParam1.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // tbParam2
            // 
            this.tbParam2.Location = new System.Drawing.Point(93, 162);
            this.tbParam2.Name = "tbParam2";
            this.tbParam2.Size = new System.Drawing.Size(100, 20);
            this.tbParam2.TabIndex = 7;
            this.tbParam2.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // tbParam3
            // 
            this.tbParam3.Location = new System.Drawing.Point(93, 188);
            this.tbParam3.Name = "tbParam3";
            this.tbParam3.Size = new System.Drawing.Size(100, 20);
            this.tbParam3.TabIndex = 8;
            this.tbParam3.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // lblPos
            // 
            this.lblPos.AutoSize = true;
            this.lblPos.Location = new System.Drawing.Point(12, 86);
            this.lblPos.Name = "lblPos";
            this.lblPos.Size = new System.Drawing.Size(47, 13);
            this.lblPos.TabIndex = 19;
            this.lblPos.Text = "Position:";
            // 
            // lblPosX
            // 
            this.lblPosX.AutoSize = true;
            this.lblPosX.Location = new System.Drawing.Point(90, 86);
            this.lblPosX.Name = "lblPosX";
            this.lblPosX.Size = new System.Drawing.Size(17, 13);
            this.lblPosX.TabIndex = 20;
            this.lblPosX.Text = "X:";
            // 
            // lblPosY
            // 
            this.lblPosY.AutoSize = true;
            this.lblPosY.Location = new System.Drawing.Point(164, 86);
            this.lblPosY.Name = "lblPosY";
            this.lblPosY.Size = new System.Drawing.Size(17, 13);
            this.lblPosY.TabIndex = 22;
            this.lblPosY.Text = "Y:";
            // 
            // lblPosZ
            // 
            this.lblPosZ.AutoSize = true;
            this.lblPosZ.Location = new System.Drawing.Point(238, 86);
            this.lblPosZ.Name = "lblPosZ";
            this.lblPosZ.Size = new System.Drawing.Size(17, 13);
            this.lblPosZ.TabIndex = 24;
            this.lblPosZ.Text = "Z:";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(83, 218);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 9;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(164, 218);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 10;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tbPosX
            // 
            this.tbPosX.Location = new System.Drawing.Point(113, 83);
            this.tbPosX.Name = "tbPosX";
            this.tbPosX.Size = new System.Drawing.Size(45, 20);
            this.tbPosX.TabIndex = 3;
            this.tbPosX.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // tbPosY
            // 
            this.tbPosY.Location = new System.Drawing.Point(187, 83);
            this.tbPosY.Name = "tbPosY";
            this.tbPosY.Size = new System.Drawing.Size(45, 20);
            this.tbPosY.TabIndex = 4;
            this.tbPosY.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // tbPosZ
            // 
            this.tbPosZ.Location = new System.Drawing.Point(261, 83);
            this.tbPosZ.Name = "tbPosZ";
            this.tbPosZ.Size = new System.Drawing.Size(45, 20);
            this.tbPosZ.TabIndex = 5;
            this.tbPosZ.Validating += new System.ComponentModel.CancelEventHandler(this.FloatValidation);
            // 
            // NewObjectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 253);
            this.Controls.Add(this.tbPosZ);
            this.Controls.Add(this.tbPosY);
            this.Controls.Add(this.tbPosX);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.lblPosZ);
            this.Controls.Add(this.lblPosY);
            this.Controls.Add(this.lblPosX);
            this.Controls.Add(this.lblPos);
            this.Controls.Add(this.tbParam3);
            this.Controls.Add(this.tbParam2);
            this.Controls.Add(this.tbParam1);
            this.Controls.Add(this.tbNameMesh);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblNameMesh);
            this.Controls.Add(this.lblParam3);
            this.Controls.Add(this.lblParam2);
            this.Controls.Add(this.lblParam1);
            this.Controls.Add(this.lblName);
            this.Name = "NewObjectForm";
            this.Text = "Create New Object";
            this.Shown += new System.EventHandler(this.NewObjectForm_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblParam1;
        private System.Windows.Forms.Label lblParam3;
        private System.Windows.Forms.Label lblParam2;
        private System.Windows.Forms.Label lblNameMesh;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbNameMesh;
        private System.Windows.Forms.TextBox tbParam1;
        private System.Windows.Forms.TextBox tbParam2;
        private System.Windows.Forms.TextBox tbParam3;
        private System.Windows.Forms.Label lblPos;
        private System.Windows.Forms.Label lblPosX;
        private System.Windows.Forms.Label lblPosY;
        private System.Windows.Forms.Label lblPosZ;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox tbPosX;
        private System.Windows.Forms.TextBox tbPosY;
        private System.Windows.Forms.TextBox tbPosZ;
    }
}