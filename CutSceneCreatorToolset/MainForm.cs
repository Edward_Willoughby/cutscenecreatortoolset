﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;


namespace NS_CutsceneCreatorToolset
{
    public partial class MainForm : Form
    {
        static string[] version = {
            /// Version 3.2
            /// - Edited the 'NewAction' code so that it doesn't save the absolute path to audio
            ///     files.
            /// - Edited the 'WarCraftIII' example cut-scene so it doesn't use absolute paths to
            ///     audio files anymore.
            "v3.2",
            /// Version 3.1
            /// - None.
            "v3.1",
            /// Version 3.0
            /// - Implemented audio actions. Audio is only played in the toolset when the 'Play'
            ///     button is pressed.
            /// - Changed the 'ActionVisible' so it's now listed as taking 0 frames instead of 1.
            /// - Audio actions can now be saved and loaded to a script.
            /// - Implemented a camera so the scene can be viewed from a dynamic camera that
            ///     follows objects in the cut scene.
            /// - Fixed a bug when the user clicked 'New Scene' because the cut scene camera was
            ///     being deleted and not re-created.
            /// - Fixed a bug where the last selected object wasn't being stored unless the user
            ///     right-clicked it.
            /// - Fixed a bug where the scene camera wasn't being linked to the 'Obj_Camera' when
            ///     a file was opened.
            /// - Stopped the global camera from being manipulated when the scene is being viewed
            ///     from the scene camera.
            /// - Fixed a bug which stopped the audio from re-starting if the scene was played,
            ///     then paused, then played again without moving the slider.
            "v3.0",
            /// Version 2.2
            /// - Implemented position (that doesn't have a travel time), rotation, and visibility
            ///     actions.
            /// - The 'Visible?' checkbox now starts with the current value of the object's 
            ///     visibility.
            /// - Implemented functionality to zoom the camera in and out in the DirectX panel.
            /// - Implemented functionality to orbit the camera in the DirectX panel.
            /// - Actions can now be right-clicked and deleted.
            /// - Actions can now be right-clicked and edited.
            /// - Cut scenes can now be saved to a file and loaded in.
            /// - Fixed a bug that prevented rotations from working correctly.
            "v2.2",
            /// Version 2.1
            /// - Implemented functionality to the user can now right-click on an asset and add it
            ///     to the scene after entering the details into a window.
            /// - The user can add additional actions to the object by right-clicking the object in
            ///     the scene graph and clicking 'Add Action'.
            /// - The only action currently implemented is 'MoveTo'.
            /// - I've attempted to try and make the 'NewAction' window C++ driven, but I may end
            ///     up just hard-coding it into the C# code to get it working quickly.
            /// - The list of actions of a selected object are shown in the bottom right corner and
            ///     can be ordered by any column.
            /// - Set the 'NewObject' form to have a default size for all parameter dimensions of
            ///     5.0f instead of 0.0f.
            "v2.1",
            /// Version 2.0
            /// - Implemented two buttons that add a sphere to the scene and then add a movement
            ///     action to it.
            /// - The DirectX functions are now accessed through the 'DirectXWrapper' class instead
            ///     of free functions because putting the wrapper into a 'ref class' means I can
            ///     now pass text back to C# from C++.
            "v2.0",
            /// Version 1.2
            /// - Implemented 'Play', 'Forward', 'Back', 'Start' and 'End' buttons for moving
            ///     through the frames.
            /// - Added a frame rate limit (will be acquired from DirectX at a later stage).
            /// - Made the container splitters fixed in the code so they'll be fixed from the
            ///     user's perspective, but I can change them at will in the designer view (to help
            ///     with initial object placement).
            "v1.2",
            /// Version 1.1
            /// - Added a menu bar (no functionality yet).
            /// - Implemented a layout for the toolset.
            /// - Added an 'OnLoad' function for the form to prevent certain bugs.
            /// - Implemented the frame tracker and linked the slider to the 'NumericUpDown'.
            /// - Implemented functions to globally change the frame position and maximum frame.
            ///     position.
            /// - Made the layout dynamic so it SHOULD look pretty much identical regardless of the
            ///     user's screen resolution.
            "v1.1",
            /// Version 1.0
            /// - First version.
            "v1.0",
            };

        /**********************************************************************/
        // Variables to aid with debugging/testing.
        const int FrameMax = 60;
        const int FrameBuffer = 30;
        /**********************************************************************/

        static DirectXWrapper wrapper = new DirectXWrapper();

        // Hold the asset and scene graph lists and selection.
        static ContextMenuStrip optionsAssets = new ContextMenuStrip();
        static ContextMenuStrip optionsSceneGraph = new ContextMenuStrip();
        static ContextMenuStrip optionsActionList = new ContextMenuStrip();

        static TreeNode lastSelectedAsset = null;
        static TreeNode lastSelectedObject = null;
        static string lastSelectedAction = null;

        // Holds the possible action types.
        private List<string> actionTypes;

        // For sorting object actions.
        private ListViewColumnSorter listViewSorter;

        // For manipulating the DirectX view.
        private Point mousePosition = new Point();

        // For saving/loading.
        const string DefaultFileName = "NewScene";
        private string formTitle = null;
        private string fileName;
        private bool previouslySaved;
        private bool sceneHasBeenModified;

        /// <summary>
        /// OBSOLETE - I've realised that I only need to refresh DirectX upon certain actions.
        /// Event handler to refresh the DirectX panel when the application is idle.
        /// </summary>
        public void ApplicationIdle(object sender, EventArgs e)
        {
            //this.DirectXFuncSucceeded(_RefreshFrame());
        }

        /// <summary>
        /// Constructor for the form.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();

            // Set the panel splitters to the correct positions. It has to be based on the
            // container and NOT the program dimensions because the 'SplitterDistance' is relative
            // to the container.
            containerAll.SplitterDistance = containerAll.Width / 5;
            containerTopBottomRight.SplitterDistance = (containerTopBottomRight.Width / 4) * 3;
            // Ensure that the bottom splitter leaves enough space for the slider and its controls.
            int required = containerTopBottom.Height - (frameSlider.Height + numFrame.Height + btnPlay.Height);
            containerTopBottom.SplitterDistance = (containerTopBottom.Height / 6) * 4;
            if (containerTopBottom.SplitterDistance > required)
                containerTopBottom.SplitterDistance = required;
            containerRight.SplitterDistance = (containerRight.Height / 3) * 2;

            // Lock the splitters. I've unlocked them in the design view so I can position things,
            // but when the application is running I don't want them to be manipulatable.
            containerAll.IsSplitterFixed = true;
            containerTopBottomRight.IsSplitterFixed = true;
            containerTopBottom.IsSplitterFixed = true;
            // I'm not locking this one so the user can determine what size is best (and it doesn't
            // affect the DirectX panel).
            //containerRight.IsSplitterFixed = true;

            // Set the border design. I've got this in here because it's easier to change this one
            // variable than manually going through changing them all in the design view.
            BorderStyle design = BorderStyle.Fixed3D;
            containerAll.BorderStyle = design;
            containerTopBottomRight.BorderStyle = design;
            containerTopBottom.BorderStyle = design;
            containerRight.BorderStyle = design;

            // Create the options list for when a user attempts to add a model.
            ToolStripMenuItem optionAdd = new ToolStripMenuItem( "Add");
            optionAdd.Click += new EventHandler(AddSelection);
            optionsAssets.Items.AddRange(new ToolStripMenuItem[] { optionAdd });
            optionsAssets.TopLevel = true;
            // Create the options list for when a user attempts to delete an object.
            ToolStripMenuItem optionAddAction = new ToolStripMenuItem("Add Action");
            optionAddAction.Click += new EventHandler(AddActionSelection);
            ToolStripMenuItem optionDelete = new ToolStripMenuItem("Delete");
            optionDelete.Click += new EventHandler(DeleteSelection);
            optionsSceneGraph.Items.AddRange(new ToolStripItem[] { optionAddAction, optionDelete });
            optionsSceneGraph.TopLevel = true;
            // Create the options list for when a user right-clicks on an action.
            ToolStripMenuItem optionEditAction = new ToolStripMenuItem("Edit Action");
            optionEditAction.Click += new EventHandler(EditActionSelection);
            ToolStripMenuItem optionDeleteAction = new ToolStripMenuItem("Delete Action");
            optionDeleteAction.Click += new EventHandler(DeleteActionSelection);
            optionsActionList.Items.AddRange(new ToolStripMenuItem[] { optionEditAction, optionDeleteAction });
            optionsActionList.TopLevel = true;

            // Set up the open and save file dialog box objects.
            saveFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            // Turn the debugging buttons invisible.
            btnReload.Visible = false;
            btnSphere.Visible = false;
            btnAction.Visible = false;
        }

        /// <summary>
        /// This is called when the form first opens and contains all the initialisation for the
        /// toolset. I've had to put the initialisation in here instead of in the constructor
        /// because otherwise a couple of bugs occur. The 'bugs' are because the program is
        /// uncertain about user-specific values, such as screen size.
        /// </summary>
        private void MainForm_Shown(object sender, EventArgs e)
        {
            // This line of code basically just adds the version number to the name of the window
            // so it automatically updates when I change the version number instead of having to
            // manually do it. To view the version numbers and changes, see the top of this file.
            formTitle = this.Text + " " + version[0];

            this.SetFormTitle(DefaultFileName, false);

            // Format the window correctly as the values that are given to it in the design view
            // aren't based on the user's values (screen size etc.), whereas these will be.
            // The '-6' that's seen everywhere is the 'padding' to keep everything away from the
            // edges of the panels when setting them to the correct width.
            // Frame slider.
            frameSlider.Width = panelFrameSlider.Width - 6;
            numFrame.Location = new Point((panelFrameSlider.Width / 2) - (numFrame.Width), numFrame.Location.Y);
            lblFrameCount.Location = new Point((panelFrameSlider.Width / 2), lblFrameCount.Location.Y);
            btnPlay.Location = new Point((panelFrameSlider.Width / 2) - (btnPlay.Width / 2), numFrame.Location.Y + btnPlay.Height + 3);
            btnForward.Location = new Point(btnPlay.Location.X + btnPlay.Width + 3, btnPlay.Location.Y);
            btnForwardEnd.Location = new Point(btnForward.Location.X + btnForward.Width + 3, btnForward.Location.Y);
            btnBack.Location = new Point(btnPlay.Location.X - btnPlay.Width - 3, btnPlay.Location.Y);
            btnBackEnd.Location = new Point(btnBack.Location.X - btnBack.Width - 3, btnBack.Location.Y);
            this.SetFrameLimit(FrameMax);
            this.SetFrame(0);
            // Scene graph & object actions (this function is also called when the splitter's moved).
            containerRight_SplitterMoved(null, null);
            listViewObjectActions.View = View.Details;
            listViewObjectActions.GridLines = true;
            listViewObjectActions.FullRowSelect = true;
            //listViewObjectActions.HeaderStyle = ColumnHeaderStyle.Nonclickable;
            listViewSorter = new ListViewColumnSorter();
            listViewSorter.SortColumn = 1;
            listViewObjectActions.ListViewItemSorter = listViewSorter;
            listViewObjectActions.Columns.Add("Action", listViewObjectActions.Width - 90 - 5, HorizontalAlignment.Left);
            listViewObjectActions.Columns.Add("Start", 45, HorizontalAlignment.Left);
            listViewObjectActions.Columns.Add("End", 45, HorizontalAlignment.Left);
            //// Populate object actions for testing.
            //listViewObjectActions.Items.Add(new ListViewItem(new string[] { "Move", "10", "35" }));
            //listViewObjectActions.Items.Add(new ListViewItem(new string[] { "Talk", "40", "42" }));
            //listViewObjectActions.Items.Add(new ListViewItem(new string[] { "Rotate", "35", "39" }));
            //listViewObjectActions_ColumnClick(listViewObjectActions, new ColumnClickEventArgs(listViewSorter.SortColumn));
            // Assets.
            lblTitleAssets.Location = new Point(3, 3);
            lblTitleAssets.Width = panelAssets.Width - 6;
            treeAssets.Location = new Point(0, lblTitleAssets.Height + 6);
            treeAssets.Width = panelAssets.Width;
            treeAssets.Height = panelAssets.Height - (lblTitleAssets.Height + 6);
            treeAssets.Nodes.Add("Default", "Default");
            treeAssets.Nodes.Add("Custom", "Custom");

            // Initialise the DirectX application that'll render to the scene panel.
            this.DirectXReload();

            // Position and initialise the global camera check box.
            cbGlobalCam.Location = new Point(panelFrameSlider.Width - cbGlobalCam.Size.Width - 6, frameSlider.Location.Y + frameSlider.Size.Height + cbGlobalCam.Size.Height + 6);
            cbGlobalCam.Checked = true;

            // The valid model names for the assets window MUST be retrieved AFTER DirectX has been
            // initialised because it's getting the names from the C++ code.
            int index = 0;
            string name = "";
            TreeNode defModels = treeAssets.Nodes.Find("Default", false)[0];

            this.DirectXFuncSucceeded(wrapper._GetName(out name, index));
            // Keep acquiring and adding more names until the name becomes invalid.
            while (name != "")
            {
                defModels.Nodes.Add( name, name);
                // Get the next name.
                ++index;
                this.DirectXFuncSucceeded(wrapper._GetName(out name, index));
            }
            treeAssets.ExpandAll();

            // Get all the names of valid action types.
            index = 0;
            actionTypes = new List<string>();
            this.DirectXFuncSucceeded( wrapper._GetActionTypeName( out name, index));
            while( name != "")
            {
                actionTypes.Add( name);
                // Get the name of the next action type.
                ++index;
                this.DirectXFuncSucceeded(wrapper._GetActionTypeName(out name, index));
            }

            // Add these events AFTER DirectX has been initialised, otherwise it bugs because it
            // can't refresh DirectX.
            frameSlider.ValueChanged += frameTracker_ValueChanged;
            numFrame.ValueChanged += frameTracker_ValueChanged;

            // Disable audio being played.
            this.DirectXFuncSucceeded(wrapper._EnableAudio(false));

            // Use the global camera.
            cbGlobalCam.Checked = true;
            this.cbGlobalCam_CheckedChanged(null, null);

            this.RepopulateScenegraph();
        }

        /// <summary>
        /// The destructor/shutdown function for the application.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sceneHasBeenModified)
            {
                DialogResult res = MessageBox.Show("You have unsaved changes to \'" + System.IO.Path.GetFileNameWithoutExtension( fileName) + "\'. Do you wish to save these changes before exiting?", "Exiting With Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (res == DialogResult.Yes)
                    if (previouslySaved)
                        this.saveToolStripMenuItem_Click(null, null);
                    else
                        this.saveAsToolStripMenuItem_Click(null, null);
                else if (res == DialogResult.Cancel)
                    e.Cancel = true;
            }

            this.DirectXFuncSucceeded(wrapper._Shutdown());
        }

        /// <summary>
        /// Sets the maximum frame for all components that require that information.
        /// </summary>
        /// <param name="max">The last frame possible in the current cutscene.</param>
        private void SetFrameLimit(int max)
        {
            if (max < 0)
                return;

            frameSlider.Maximum = max;
            numFrame.Maximum = max;
            lblFrameCount.Text = "/ " + max.ToString();

            // Set the amount to jump by when clicking on the slider (1/10 of maximum).
            frameSlider.LargeChange = max / 10;
        }

        /// <summary>
        /// Sets the active frame for all components that require that information.
        /// </summary>
        /// <param name="frame">The active frame in the current cutscene.</param>
        private void SetFrame(int frame)
        {
            frameSlider.Value = frame;
            numFrame.Value = frame;
        }

        /// <summary>
        /// Reloads DirectX. This can be called to initialise DirectX the first time as well
        /// because if DirectX hasn't been initialised yet, then '_Shutdown' simply won't do
        /// anything except return true.
        /// </summary>
        private void DirectXReload()
        {
            this.DirectXFuncSucceeded(wrapper._Shutdown());
            this.DirectXFuncSucceeded(wrapper._Initialise(panelRender.Handle));
        }

        /// <summary>
        /// This function contains everything that is required to refresh the DirectX panel. I have
        /// to put the '_RefreshFrame()' function inside this one because there'll be other things
        /// that need to be done before that function is called.
        /// </summary>
        private void DirectXRefresh()
        {
            this.DirectXFuncSucceeded(wrapper._RefreshFrame((uint)this.frameSlider.Value));
        }

        /// <summary>
        /// Checks that the DirectX function exited correctly and didn't produce a bug... at least
        /// not from the return value.
        /// </summary>
        private void DirectXFuncSucceeded(bool result)
        {
            if (result)
                return;

            // If the function failed then display a message and then hit a breakpoint so debugging
            // can be done.
            MessageBox.Show("A DirectX function failed to run!", "DirectX Failure", MessageBoxButtons.OK);
            System.Diagnostics.Debugger.Break();
        }

        /// <summary>
        /// This timer is to see if I can have the DirectX panel refreshing at a decent speed...
        /// apparently I can.
        /// </summary>
        private void timerRefresh_Tick(object sender, EventArgs e)
        {
            this.DirectXRefresh();
        }

        /// <summary>
        /// Reloads the DirectX code. This is for in case something goes wrong and needs re-booting.
        /// It takes around five seconds, so be patient!
        /// </summary>
        private void btnReload_Click(object sender, EventArgs e)
        {
            this.DirectXReload();
        }

        /// <summary>
        /// Updates the frame slider and 'NumericalUpDown' to the same value whenever one of them
        /// is changed. I have to remove this event from the object that DIDN'T send this event so
        /// that the event isn't triggered again.
        /// </summary>
        private void frameTracker_ValueChanged(object sender, EventArgs e)
        {
            string name = sender.GetType().Name;

            if (name == "NumericUpDown")
            {
                frameSlider.ValueChanged -= frameTracker_ValueChanged;
                this.SetFrame((int)numFrame.Value);
                frameSlider.ValueChanged += frameTracker_ValueChanged;
            }
            else //if (name == "TrackBar")
            {
                numFrame.ValueChanged -= frameTracker_ValueChanged;
                this.SetFrame(frameSlider.Value);
                numFrame.ValueChanged += frameTracker_ValueChanged;
            }

            this.DirectXRefresh();
        }

        /// <summary>
        /// Tells the program to play through the entire cut scene from the current frame position
        /// to finish.
        /// </summary>
        private void btnPlay_Click(object sender, EventArgs e)
        {
            if (timerPlay.Enabled)
            {
                timerPlay.Enabled = false;
                this.DirectXFuncSucceeded(wrapper._EnableAudio(false));
            }
            else
            {
                if (this.IsLastFrame())
                    this.SetFrame(0);

                timerPlay.Enabled = true;
                this.DirectXFuncSucceeded(wrapper._EnableAudio(true));
            }

            if (!timerPlay.Enabled)
            {
                this.DirectXFuncSucceeded(wrapper._StopAudio());
            }
        }

        /// <summary>
        /// Moves on to the next frame and then waits until the timer event is triggered again.
        /// This timer disables itself once the frame slider reaches the end (i.e. when the last
        /// frame of the cut scene is displayed).
        /// </summary>
        private void timerPlay_Tick(object sender, EventArgs e)
        {
            // Move onto the next frame.
            this.SetFrame( frameSlider.Value + 1);

            //// Refresh the DirectX panel.
            //this.DirectXRefresh();

            // Disable the timer if it's the last frame.
            if (IsLastFrame())
            {
                timerPlay.Enabled = false;
                this.DirectXFuncSucceeded(wrapper._EnableAudio(false));
            }
        }

        /// <summary>
        /// Returns true if the frame position is at the start.
        /// </summary>
        private bool IsFirstFrame()
        {
            if (frameSlider.Value == 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Returns true if the frame position is at the end.
        /// </summary>
        private bool IsLastFrame()
        {
            if (frameSlider.Value == frameSlider.Maximum)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Moves the scene to the first frame.
        /// </summary>
        private void btnBackEnd_Click(object sender, EventArgs e)
        {
            if (!this.IsFirstFrame())
                this.SetFrame(0);
        }

        /// <summary>
        /// Moves the scene to the last frame.
        /// </summary>
        private void btnForwardEnd_Click(object sender, EventArgs e)
        {
            if (!this.IsLastFrame())
                this.SetFrame(frameSlider.Maximum);
        }

        /// <summary>
        /// Moves the scene back one frame.
        /// </summary>
        private void btnBack_Click(object sender, EventArgs e)
        {
            if (!this.IsFirstFrame())
                this.SetFrame(frameSlider.Value - 1);
        }
        /// <summary>
        /// Moves the scene forward one frame.
        /// </summary>
        private void btnForward_Click(object sender, EventArgs e)
        {
            if (!this.IsLastFrame())
                this.SetFrame(frameSlider.Value + 1);
        }
        
        /// <summary>
        /// TEST FUNCTION - Creates a sphere.
        /// </summary>
        private void btnSphere_Click(object sender, EventArgs e)
        {
            Button obj = (Button)sender;

            String name = "";
            this.DirectXFuncSucceeded(wrapper._TestFunction(out name));
            obj.Text = name;
        }

        /// <summary>
        /// TEST FUNCTION - Creates an action for the scene.
        /// </summary>
        private void btnAction_Click(object sender, EventArgs e)
        {
            //// Adds a movement action to the sphere.
            //this.DirectXFuncSucceeded(wrapper._AddAction());

            //// Gets the last frame of the scene, with the new action included.
            //uint lastFrame = 0;
            //this.DirectXFuncSucceeded(wrapper._GetLastFrame(out lastFrame));
            //// Including the 'buffer' that's at the end of the scene.
            //lastFrame += FrameBuffer;

            //// If the action has lengthened the overall frame length of the scene, then increase
            //// the required variables and form objects.
            //if (lastFrame > this.frameSlider.Maximum)
            //    this.SetFrameLimit((int)lastFrame);

            //this.DirectXRefresh();
        }

        /// <summary>
        /// Event handler for displaying options when a user right-clicks on an asset.
        /// </summary>
        private void treeAssets_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            // Exit if the user didn't actually select a node.
            TreeNode chosen = e.Node;
            if (chosen == null)
                return;

            // Exit if the user didn't right-click.
            if (e.Button != MouseButtons.Right)
                return;

            // Try and find the node on the top level. It it's found, then they can't add that
            // model because it's actually a catagory (e.g. "Default" or "Custom").
            TreeNode[] nodes = ((TreeView)sender).Nodes.Find(chosen.Text, false);
            if (nodes.Length > 0)
                return;

            optionsAssets.BringToFront();
            optionsAssets.Show(Control.MousePosition);
            optionsAssets.Enabled = true;
            optionsAssets.Visible = true;
            lastSelectedAsset = chosen;
        }

        /// <summary>
        /// Event handler for when the user selects 'Add' on the options menu after right-clicking
        /// an asset.
        /// </summary>
        private void AddSelection( object sender, EventArgs e)
        {
            // Do nothing if no asset has been selected.
            if (lastSelectedAsset == null)
                return;

            // Get the default name for the object and the names of parameters that the model needs.
            string name = "";
            string a = "", b = "", c = "";
            this.DirectXFuncSucceeded(wrapper._GetValidObjectName(out name));
            this.DirectXFuncSucceeded(wrapper._GetParameterNames(lastSelectedAsset.Text, out a, out b, out c));

            // Open a new form to get the input for, and create, the new object.
            Form newObject = new NewObjectForm(name, lastSelectedAsset.Text, a, b, c);
            newObject.FormClosing += new FormClosingEventHandler(AddObjectToSceneGraph);
            newObject.ShowDialog();

            // Get rid of the pop-up list.
            optionsAssets.Enabled = false;
            optionsAssets.Visible = false;
            lastSelectedAsset = null;

            this.DirectXRefresh();
        }

        /// <summary>
        /// Adds a newly created object to the main form's scene graph upon closing of the
        /// NewObjectForm.
        /// </summary>
        void AddObjectToSceneGraph(object sender, FormClosingEventArgs e)
        {
            NewObjectForm form = (NewObjectForm)sender;

            // If the form created an object, then add it to the scene graph.
            if (form.objectName != null)
            {
                treeSceneGraph.Nodes.Add(form.objectName, form.objectName);

                this.SetFormTitle(fileName, true);
            }
        }

        /// <summary>
        /// Event handler for displaying options when a user clicks on an object in the scene graph.
        /// </summary>
        private void treeSceneGraph_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            // Exit if the user didn't actually select a node.
            TreeNode chosen = e.Node;
            if (chosen == null)
                return;

            lastSelectedObject = chosen;

            // Get all the object's actions and list them in the action list area.
            this.UpdateActionList(lastSelectedObject.Text);

            // Provide options for the selected node if the user right-clicked.
            if (e.Button == MouseButtons.Right)
            {
                optionsSceneGraph.BringToFront();
                optionsSceneGraph.Show(Control.MousePosition);
                optionsSceneGraph.Enabled = true;
                optionsSceneGraph.Visible = true;
            }
        }

        /// <summary>
        /// Updates the list of actions. If the parameter is invalid then no actions will populate
        /// the list.
        /// </summary>
        private void UpdateActionList( string selectedNode)
        {
            // Erase the current selection's action list.
            listViewObjectActions.Items.Clear();

            // List the selected object's actions in the actions panel.
            string name = "";
            int start = 0, end = 0, index = 0;
            this.DirectXFuncSucceeded(wrapper._GetObjectsActionInfo(selectedNode, out name, out start, out end, index));
            while (name != "")
            {
                listViewObjectActions.Items.Add(new ListViewItem(new string[] { name, start.ToString(), end.ToString() }));

                ++index;
                this.DirectXFuncSucceeded(wrapper._GetObjectsActionInfo(selectedNode, out name, out start, out end, index));
            }
        }

        /// <summary>
        /// Event handler for when the user selects 'Add Action' on the options menu after right-
        /// clicking an object in the scene graph.
        /// </summary>
        private void AddActionSelection(object sender, EventArgs e)
        {
            // Do nothing if no asset has been selected.
            if (lastSelectedObject == null)
                return;

            // Don't allow an action to be added to the camera if no objects exist for it to follow.
            if (lastSelectedObject.Text == "Obj_Camera" && treeSceneGraph.Nodes.Count < 2)
            {
                MessageBox.Show("Unable to make the camera follow an object without another object in the scene to follow.", "Insufficient Objects", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Open a new form so the user can enter the details of the action.
            NewActionForm newAction = new NewActionForm(lastSelectedObject.Text, frameSlider.Value);
            newAction.ShowDialog();

            // Update anything that might've been affected by the addition of an action.
            this.ActionHasBeenModified();

            // Get rid of the pop-up list.
            optionsSceneGraph.Enabled = false;
            optionsSceneGraph.Visible = false;

            this.DirectXRefresh();
        }

        /// <summary>
        /// Event handler for when the user selects 'Delete' on the options menu after right-
        /// clicking an object in the scene graph.
        /// </summary>
        private void DeleteSelection(object sender, EventArgs e)
        {
            // Do nothing if no asset has been selected.
            if (lastSelectedObject == null)
                return;

            // Don't allow the user to delete the camera.
            if (lastSelectedObject.Text == "Obj_Camera")
            {
                MessageBox.Show("You cannot delete the scene camera.", "Delete Object", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Display a confirmation box to make sure the user wants to delete the object.
            DialogResult res = MessageBox.Show("Are you sure you wish to delete the following object?\n\n" + lastSelectedObject.Text, "Delete Object", MessageBoxButtons.YesNo);
            
            // Delete the object if the user confirmed deletion.
            if (res == DialogResult.Yes)
            {
                this.DirectXFuncSucceeded(wrapper._DeleteObject(lastSelectedObject.Text));

                // Remove the object from the scene graph.
                treeSceneGraph.Nodes.RemoveByKey(lastSelectedObject.Text);

                // If an object has been deleted, actions have likely gone with it.
                this.ActionHasBeenModified();
            }

            // Get rid of the pop-up list.
            optionsSceneGraph.Enabled = false;
            optionsSceneGraph.Visible = false;
        }

        /// <summary>
        /// Event handler for setting which action is currently selected.
        /// </summary>
        private void listViewObjectActions_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
                lastSelectedAction = e.Item.Text;
            else
                lastSelectedAction = null;
        }

        /// <summary>
        /// Event handler for displaying options when a user clicks on an action in the action list.
        /// </summary>
        private void listViewObjectActions_MouseClick(object sender, MouseEventArgs e)
        {
            // Exit if the user didn't actually select a node.
            if (lastSelectedAction == null)
                return;

            // Provide options for the selected node if the user right-clicked.
            if (e.Button == MouseButtons.Right)
            {
                optionsActionList.BringToFront();
                optionsActionList.Show(Control.MousePosition);
                optionsActionList.Enabled = true;
                optionsActionList.Visible = true;
            }
        }

        /// <summary>
        /// Event handler for when the user selects 'Edit Action' on the options menu after right-
        /// clicking an action in the object action list.
        /// </summary>
        private void EditActionSelection(object sender, EventArgs e)
        {
            // Do nothing if no action has been selected.
            if (lastSelectedAction == null)
                return;

            // Do nothing if the object selection has been lost (it needs to know which object the
            // action is attached to).
            if (lastSelectedObject == null)
                return;

            string actionType = null;
            int start = 0, end = 0;
            this.DirectXFuncSucceeded(wrapper._GetObjectsActionInfo(lastSelectedObject.Text, lastSelectedAction, out actionType, out start, out end));

            float start1 = 0.0f, start2 = 0.0f, start3 = 0.0f;
            float end1 = 0.0f, end2 = 0.0f, end3 = 0.0f;
            string start4 = "", end4 = "";
            this.DirectXFuncSucceeded(wrapper._GetActionInfoAtFrame(lastSelectedObject.Text, lastSelectedAction, start, out start1, out start2, out start3, out start4));
            this.DirectXFuncSucceeded(wrapper._GetActionInfoAtFrame(lastSelectedObject.Text, lastSelectedAction, end, out end1, out end2, out end3, out end4));

            // Open a new form so the user can enter the details of the action.
            NewActionForm newAction = new NewActionForm(lastSelectedObject.Text, frameSlider.Value);
            newAction.SetEditInfo(lastSelectedAction, actionType, start, end, start1, start2, start3, end1, end2, end3, start4, end4);
            newAction.ShowDialog();

            // Update anything that might've been affected by the addition of an action.
            this.ActionHasBeenModified();

            // Get rid of the pop-up list.
            optionsSceneGraph.Enabled = false;
            optionsSceneGraph.Visible = false;

            this.DirectXRefresh();
        }

        /// <summary>
        /// Event handler for when the user selects 'Delete Action' on the options menu after right-
        /// clicking an action in the object action list.
        /// </summary>
        private void DeleteActionSelection(object sender, EventArgs e)
        {
            // Do nothing if no action has been selected.
            if (lastSelectedAction == null)
                return;

            // Do nothing if the object selection has been lost (it needs to know which object the
            // action is attached to).
            if (lastSelectedObject == null)
                return;

            // Display a confirmation box to make sure the user wants to delete the object.
            DialogResult res = MessageBox.Show("Are you sure you wish to delete the following action?\n\n" + lastSelectedAction, "Delete Action", MessageBoxButtons.YesNo);

            // Delete the action if the user confirmed deletion.
            if (res == DialogResult.Yes)
            {
                this.DirectXFuncSucceeded(wrapper._DeleteAction(lastSelectedObject.Text, lastSelectedAction));

                // If an action has been deleted then other stuff will need updating.
                this.ActionHasBeenModified();
            }

            // Get rid of the pop-up list.
            optionsActionList.Enabled = false;
            optionsActionList.Visible = false;
            lastSelectedAction = null;
        }

        /// <summary>
        /// Deals with the 'aftermath' of added or removing an action from the cut scene.
        /// </summary>
        private void ActionHasBeenModified()
        {
            // Only update the action list if there's an object selected in the scene graph.
            if (lastSelectedObject == null)
                listViewObjectActions.Items.Clear();
            else
                this.UpdateActionList(lastSelectedObject.Text);

            // Gets the last frame of the scene, with the new action included.
            uint lastFrame = 0;
            this.DirectXFuncSucceeded(wrapper._GetLastFrame(out lastFrame));
            // Including the 'buffer' that's at the end of the scene.
            lastFrame += FrameBuffer;

            // If the action has lengthened the overall frame length of the scene, then increase
            // the required variables and form objects.
            if (lastFrame > this.frameSlider.Maximum)
                this.SetFrameLimit((int)lastFrame);

            this.SetFormTitle(fileName, true);
            
            this.DirectXRefresh();
        }

        /// <summary>
        /// Re-size and re-position the scene graph and object actions view when the panel splitter
        /// is moved.
        /// </summary>
        private void containerRight_SplitterMoved(object sender, SplitterEventArgs e)
        {
            // Scene graph.
            lblTitleSceneGraph.Location = new Point(3, 3);
            lblTitleSceneGraph.Width = panelSceneGraph.Width - 6;
            treeSceneGraph.Location = new Point(0, lblTitleSceneGraph.Height + 6);
            treeSceneGraph.Width = panelSceneGraph.Width;
            treeSceneGraph.Height = panelSceneGraph.Height - (lblTitleSceneGraph.Height + 6);
            // Object actions.
            lblTitleObjectActions.Location = new Point(3, 3);
            lblTitleObjectActions.Width = panelObjectActions.Width - 6;
            listViewObjectActions.Location = new Point(0, lblTitleObjectActions.Height + 6);
            listViewObjectActions.Width = panelObjectActions.Width;
            listViewObjectActions.Height = panelObjectActions.Height - (lblTitleObjectActions.Height + 6);
        }

        /// <summary>
        /// Event listener to sort a list view object when a column header is clicked.
        /// </summary>
        private void listViewObjectActions_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListView obj = (ListView)sender;

            // Exit if the column number is invalid.
            if (e.Column < 0 || e.Column > obj.Columns.Count)
                return;

            // Get rid of the arrow at the end of the active column header.
            string name = obj.Columns[listViewSorter.SortColumn].Text;

            if( name[name.Length-1] == 'v' || name[name.Length-1] == '^')
                name = name.Substring(0, name.Length - 2);

            // Determine if the clicked column is already the column that is being sorted.
            if (e.Column == listViewSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (listViewSorter.Order == SortOrder.Ascending)
                {
                    listViewSorter.Order = SortOrder.Descending;
                    obj.Columns[e.Column].Text = name += " v";
                }
                else
                {
                    listViewSorter.Order = SortOrder.Ascending;
                    obj.Columns[e.Column].Text = name += " ^";
                }
            }
            else
            {
                // Remove the tag at the end of the column's name and put it on the new column.
                obj.Columns[listViewSorter.SortColumn].Text = name;
                obj.Columns[e.Column].Text += " ^";

                // Set the column number that is to be sorted; default to ascending.
                listViewSorter.SortColumn = e.Column;
                listViewSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with the new options.
            obj.Sort();
        }

        /// <summary>
        /// Cause the render panel to be focused whenever the mouse goes over it.
        /// </summary>
        private void panelRender_MouseEnter(object sender, EventArgs e)
        {
            ((Panel)sender).Focus();
        }

        /// <summary>
        /// Manipulate the DirectX camera when the mouse is moved while the render panel is in
        /// focus.
        /// </summary>
        private void panelRender_MouseMove(object sender, MouseEventArgs e)
        {
            Panel panel = (Panel)sender;

            // Do nothing if the panel isn't in focus and the global camera should be being used.
            if (panel.Focused && cbGlobalCam.Checked)
            {
                // Do nothing if neither mouse button is down (can't check the mouse wheel in this form
                // because Microsoft are idiots and have made it 'too efficient').
                if (e.Button == MouseButtons.Left || e.Button == MouseButtons.Right)
                {
                    Point move = new Point(e.X - mousePosition.X, e.Y - mousePosition.Y);

                    // Orbit the camera when the left mouse button is pressed.
                    if (e.Button == MouseButtons.Left)
                        DirectXFuncSucceeded(wrapper._CameraOrbit(move.X, move.Y));
                    // Zoom the camera when the right mouse button is pressed.
                    else if (e.Button == MouseButtons.Right)
                        DirectXFuncSucceeded(wrapper._CameraZoom(move.X, move.Y));

                    // Refresh the DirectX panel to reflect the new camera position.
                    DirectXRefresh();
                }
            }

            mousePosition = new Point(e.X, e.Y);
        }

        /// <summary>
        /// When the user clicks 'About' on the menu.
        /// </summary>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string about = "The 'Cut Scene Creator Toolset'\n\n" +
                            "Created by: Edward Willoughby\n" +
                            "Version: " + version[0] + "\n";

            MessageBox.Show( about, "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// When the user clicks 'New' on the menu.
        /// </summary>
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult res;
            if (sceneHasBeenModified)
            {
                res = MessageBox.Show("This scene is about to be cleared. Would you like to continue?", "New Scene", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (res == DialogResult.No)
                    return;
            }

            this.DirectXFuncSucceeded(wrapper._ClearScene());
            treeSceneGraph.Nodes.Clear();
            listViewObjectActions.Items.Clear();

            this.SetFrame(0);
            this.SetFrameLimit(FrameMax);

            this.SetFormTitle(DefaultFileName, false);

            this.DirectXRefresh();
        }

        /// <summary>
        /// When the user clicks 'Open' on the menu.
        /// </summary>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult res;
            if (sceneHasBeenModified)
            {
                 res = MessageBox.Show("This scene is about to be overwritten. Would you like to continue?", "Modified Scene", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (res == DialogResult.No)
                    return;
            }

            res = openFileDialog.ShowDialog();

            if (res != DialogResult.OK)
                return;

            this.DirectXFuncSucceeded(wrapper._OpenScene(openFileDialog.FileName));

            // I'll probably need to call some other functions as well, but this'll do for now.
            this.RepopulateScenegraph();
            this.ActionHasBeenModified();
            this.DirectXRefresh();

            this.SetFormTitle(openFileDialog.FileName, false);
        }

        /// <summary>
        /// Clears the scenegraph and then gets all the object names from the DirectX code;
        /// ensuring that it's the most up-to-date version.
        /// </summary>
        private void RepopulateScenegraph()
        {
            treeSceneGraph.Nodes.Clear();

            String name = "";
            int index = 0;
            this.DirectXFuncSucceeded(wrapper._GetObjectName(out name, index));

            while (name != "")
            {
                treeSceneGraph.Nodes.Add(name);

                ++index;
                this.DirectXFuncSucceeded(wrapper._GetObjectName(out name, index));
            }
        }

        /// <summary>
        /// When the user clicks 'Save' on the menu.
        /// </summary>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!previouslySaved)
            {
                this.saveAsToolStripMenuItem_Click(sender, e);
                return;
            }

            this.DirectXFuncSucceeded(wrapper._SaveScene(fileName));

            MessageBox.Show("Cut scene saved successfully!", "Save Successful", MessageBoxButtons.OK);

            this.SetFormTitle(fileName, false);
        }

        /// <summary>
        /// When the user clicks 'Save As...' on the menu.
        /// </summary>
        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog.FileName = System.IO.Path.GetFileNameWithoutExtension( fileName);
            DialogResult res = saveFileDialog.ShowDialog();

            if (res != DialogResult.OK)
                return;

            previouslySaved = true;
            this.DirectXFuncSucceeded(wrapper._SaveScene(saveFileDialog.FileName));

            MessageBox.Show("Cut scene saved successfully!", "Save Successful", MessageBoxButtons.OK);

            this.SetFormTitle(saveFileDialog.FileName, false);
        }

        /// <summary>
        /// Sets the title for the form based on the current scene's name (i.e. the file that's
        /// open) and includes an asterisk if the scene has been modified since the last save. If
        /// you don't want to change either the file name or the modified boolean when calling this
        /// function, just use the global variable as the parameter.
        /// </summary>
        private void SetFormTitle( string file, bool modified)
        {
            fileName = file;
            sceneHasBeenModified = modified;

            this.Text = System.IO.Path.GetFileNameWithoutExtension( fileName) + (sceneHasBeenModified ? "*" : "") + " - " + formTitle;
        }

        /// <summary>
        /// Event handler for when the user clicks the global camera check box.
        /// </summary>
        private void cbGlobalCam_CheckedChanged(object sender, EventArgs e)
        {
            this.DirectXFuncSucceeded(wrapper._UseGlobalCam(cbGlobalCam.Checked));
            this.DirectXRefresh();
        }
    }
}