﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NS_CutsceneCreatorToolset
{
    public partial class NewObjectForm : Form
    {
        static DirectXWrapper wrapper = new DirectXWrapper();

        public string objectName = null;

        /// <summary>
        /// Constructor for the form.
        /// </summary>
        /// <param name="objName">The name of the object.</param>
        /// <param name="modelName">The name of the mesh that'll be used for the object.</param>
        /// <param name="param1">The name of the first parameter.</param>
        /// <param name="param2">The name of the second parameter.</param>
        /// <param name="param3">The name of the third parameter.</param>
        public NewObjectForm( string objName, string modelName, string param1, string param2, string param3)
        {
            InitializeComponent();

            tbName.Text = objName;

            tbNameMesh.Text = modelName;
            tbNameMesh.ReadOnly = true;

            tbPosX.Text = "0.0";
            tbPosY.Text = "0.0";
            tbPosZ.Text = "0.0";

            this.SetupParamBox(param1, ref lblParam1, ref tbParam1);
            this.SetupParamBox(param2, ref lblParam2, ref tbParam2);
            this.SetupParamBox(param3, ref lblParam3, ref tbParam3);
        }

        /// <summary>
        /// This is called when the form first opens and contains some positioning and
        /// initialisation that can't be done when the form is initialisation.
        /// </summary>
        private void NewObjectForm_Shown(object sender, EventArgs e)
        {
            // C# has this epic thing where the 'Visible' property will be false if its parent's is
            // false. Thus, I was re-sizing the window incorrectly because the parameter text boxes
            // were all returning 'false' in the constructor (because the form wasn't visible yet).

            // Set the size of the window to fit the number of parameters to reduce dead space.
            int reduction = 0;
            if (!tbParam1.Visible) reduction += 26;
            if (!tbParam2.Visible) reduction += 26;
            if (!tbParam3.Visible) reduction += 26;

            // Move the buttons up so they don't become hidden.
            btnCreate.Location = new Point(btnCreate.Location.X, btnCreate.Location.Y - reduction);
            btnCancel.Location = new Point(btnCancel.Location.X, btnCancel.Location.Y - reduction);

            // Shrink the window.
            Size size = this.Size;
            size.Height -= reduction;
            this.Size = size;

            // Prevent the window size from being modified.
            this.MinimumSize = size;
            this.MaximumSize = size;
            this.MaximizeBox = false;
        }

        /// <summary>
        /// Initialises the on-screen objects that a user uses for entering a parameter.
        /// </summary>
        private void SetupParamBox(string param, ref Label lbl, ref TextBox tb)
        {
            // Set this information even if the parameter isn't shown so that the 'IsValidInfo()'
            // function doesn't fail on parameters that aren't visible to the user.
            lbl.Text = param + ":";
            tb.Text = "5.0";

            if (param == "")
            {
                lbl.Visible = false;
                tb.Visible = false;
            }
            else
            {
                lbl.Visible = true;
                tb.Visible = true;
            }
        }

        /// <summary>
        /// Checks to see if the information contained in the form is valid so an object can be
        /// created from it.
        /// </summary>
        private bool IsValidInfo()
        {
            float value = 0;
            if (string.IsNullOrEmpty( tbName.Text) ||
                !float.TryParse(tbParam1.Text, out value) ||
                !float.TryParse(tbParam2.Text, out value) ||
                !float.TryParse(tbParam3.Text, out value) ||
                !float.TryParse(tbPosX.Text, out value) ||
                !float.TryParse(tbPosY.Text, out value) ||
                !float.TryParse(tbPosZ.Text, out value))
                return false;

            return true;
        }

        /// <summary>
        /// Validates the contents of a text box to ensure that it can be converted into a float.
        /// If it can't, then the background of the text box will turn red.
        /// </summary>
        private void FloatValidation(object sender, CancelEventArgs e)
        {
            TextBox obj = (TextBox)sender;
            float value = 0;

            if (!float.TryParse(obj.Text, out value))
                obj.BackColor = Color.Red;
            else
                obj.BackColor = Color.White;
        }

        /// <summary>
        /// Creates an object, assuming the data in the form is valid.
        /// </summary>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            // If the user has entered some invalid data, then indicate to them that something is
            // wrong and don't attempt to create the object.
            if (!this.IsValidInfo())
            {
                MessageBox.Show("Cannot create the object due to invalid information. Fix the properties highlighted in red.", "Invalid Properties", MessageBoxButtons.OK);
                return;
            }

            float param1 = 0.0f, param2 = 0.0f, param3 = 0.0f;
            float posX = 0.0f, posY = 0.0f, posZ = 0.0f;

            // Convert the parameter text into floats.
            if (tbParam1.Visible)
                float.TryParse(tbParam1.Text, out param1);
            if (tbParam2.Visible)
                float.TryParse(tbParam2.Text, out param2);
            if (tbParam3.Visible)
                float.TryParse(tbParam3.Text, out param3);

            // I can't yet position the object, but it needs these parameters when I can.
            float.TryParse(tbPosX.Text, out posX);
            float.TryParse(tbPosY.Text, out posY);
            float.TryParse(tbPosZ.Text, out posZ);

            wrapper._CreateObject(tbName.Text, tbNameMesh.Text, param1, param2, param3);
            objectName = tbName.Text;

            // Close this form once the object has been successfully created.
            this.Close();
        }

        /// <summary>
        /// Close the form without creating the object if the user presses 'Cancel'.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}
