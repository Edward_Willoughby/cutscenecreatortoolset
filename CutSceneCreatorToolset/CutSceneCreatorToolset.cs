﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace NS_CutsceneCreatorToolset
{
    static class CutsceneCreatorToolset
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainForm mainForm = new MainForm();

            Application.Idle += new EventHandler(mainForm.ApplicationIdle);

            Application.Run(mainForm);
        }
    }
}
