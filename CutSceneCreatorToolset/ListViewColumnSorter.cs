﻿using System.Collections;
using System.Windows.Forms;

namespace NS_CutsceneCreatorToolset
{
    public class ListViewColumnSorter : IComparer
    {
        private int ColumnToSort;
        private SortOrder OrderOfSort;
        private CaseInsensitiveComparer ObjectCompare;

        /// <summary>
        /// Constructor for the class.
        /// </summary>
        public ListViewColumnSorter()
        {
            // Initialise the column to an invalid number so the event handler SHOULD change it.
            ColumnToSort = 0;

            OrderOfSort = SortOrder.None;

            ObjectCompare = new CaseInsensitiveComparer();
        }

        /// <summary>
        /// This method is inherited from the IComparer interface. It compares the two objects
        /// passed in using a case insensitive comparison.
        /// </summary>
        /// <returns>Result of the comparison; 0 if equal, negative is 'x' is less than 'y' and 
        /// positive if 'x' is greater than 'y'.</returns>
        public int Compare(object x, object y)
        {
            int compareResult;
            ListViewItem itemX, itemY;

            // Cast the parameters so they're useable.
            itemX = (ListViewItem)x;
            itemY = (ListViewItem)y;

            // Compare the two objects.
            if (ColumnToSort == 0)
            {
                compareResult = ObjectCompare.Compare(itemX.SubItems[ColumnToSort].Text, itemY.SubItems[ColumnToSort].Text);
            }
            else
            {
                int valueX, valueY;

                int.TryParse(itemX.SubItems[ColumnToSort].Text, out valueX);
                int.TryParse(itemY.SubItems[ColumnToSort].Text, out valueY);

                compareResult = ObjectCompare.Compare(valueX, valueY);
            }

            // Calculate the correct return value based on object comparison.
            if (OrderOfSort == SortOrder.Ascending)
                // Ascending sort is selected, return normal result of compare operation.
                return compareResult;
            else if (OrderOfSort == SortOrder.Descending)
                // Descending sort is selected, return negative result of compare operation.
                return -compareResult;
            else
                // Return 0 to indicate they're equal.
                return 0;
        }

        /// <summary>
        /// Gets or sets the number of the column to apply the sorting operation.
        /// </summary>
        public int SortColumn
        {
            set
            {
                ColumnToSort = value;
            }
            get
            {
                return ColumnToSort;
            }
        }

        /// <summary>
        /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
        /// </summary>
        public SortOrder Order
        {
            set
            {
                OrderOfSort = value;
            }
            get
            {
                return OrderOfSort;
            }
        }
    }
}
