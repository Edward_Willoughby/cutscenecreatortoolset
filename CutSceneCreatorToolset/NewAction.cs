﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NS_CutsceneCreatorToolset
{
    public partial class NewActionForm : Form
    {
        static DirectXWrapper wrapper = new DirectXWrapper();

        static int startFrame;

        private bool editing;
        private string oldActionName;

        private string audioFileName;

        private enum FormSetup {
            FS_Audio,
            FS_Camera,
            FS_Other,
        };

        /// <summary>
        /// Constructor for the form.
        /// </summary>
        /// <param name="objName">The name of the object.</param>
        public NewActionForm( string objName, int frameNumber)
        {
            InitializeComponent();

            audioFileName = "";
            openFileDialog.InitialDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            tbNameObject.Text = objName;
            tbNameObject.ReadOnly = true;

            string actionName = "";
            wrapper._GetValidActionName(out actionName);
            tbNameAction.Text = actionName;

            List<string> actionTypes = new List<string>();
            string actionType = "";
            int index = 0;
            wrapper._GetActionTypeName(out actionType, index);
            while (actionType != "")
            {
                actionTypes.Add(actionType);
                ++index;
                wrapper._GetActionTypeName(out actionType, index);
            }
            if (objName == "Obj_Camera")
                actionTypes.Add("Camera");
            cbActionType.DataSource = actionTypes;

            startFrame = frameNumber;
            tbFrameStart.Text = frameNumber.ToString();
            tbFrameStart.ReadOnly = true;

            tbFrameEnd.Text = frameNumber.ToString();
            tbFrameEnd.ReadOnly = true;

            float x = 0.0f, y = 0.0f, z = 0.0f;
            wrapper._GetPosition(tbNameObject.Text, out x, out y, out z);
            tbPosXStart.Text = x.ToString();
            tbPosXStart.ReadOnly = true;
            tbPosYStart.Text = y.ToString();
            tbPosYStart.ReadOnly = true;
            tbPosZStart.Text = z.ToString();
            tbPosZStart.ReadOnly = true;

            wrapper._GetRotation(tbNameObject.Text, out x, out y, out z);
            tbRotXStart.Text = x.ToString();
            tbRotXStart.ReadOnly = true;
            tbRotYStart.Text = y.ToString();
            tbRotYStart.ReadOnly = true;
            tbRotZStart.Text = z.ToString();
            tbRotZStart.ReadOnly = true;

            bool vis = false;
            wrapper._GetVisibility(tbNameObject.Text, out vis);
            cbVisible.Checked = vis;

            editing = false;

            // Lock certain information for the camera actions.
            if (objName == "Obj_Camera")
            {
                cbActionType.SelectedIndex = cbActionType.Items.Count - 1;
                cbActionType.Enabled = false;

                List<string> objectNames = new List<string>();
                string name = "";
                index = 0;
                wrapper._GetObjectName(out name, index);
                while (name != "")
                {
                    if( name != "Obj_Camera")
                        objectNames.Add(name);
                    ++index;
                    wrapper._GetObjectName(out name, index);
                }
                cbObjToTrack.DataSource = objectNames;
            }
        }

        public void SetEditInfo(string actionName, string actionType, int start, int end, float start1, float start2, float start3, float end1, float end2, float end3, string start4, string end4)
        {
            editing = true;
            this.Text = "Edit Action";
            btnCreate.Text = "Save Edits";

            oldActionName = actionName;
            tbNameAction.Text = actionName;

            for (int i = 0; i < cbActionType.Items.Count; ++i)
                if (cbActionType.Items[i].ToString() == actionType)
                    cbActionType.SelectedIndex = i;
            cbActionType.Enabled = false;

            startFrame = start;
            tbFrameStart.Text = start.ToString();
            tbFrameEnd.Text = end.ToString();

            if (actionType == "MoveTo" || actionType == "PositionAt")
            {
                tbPosXStart.Text = start1.ToString();
                tbPosYStart.Text = start2.ToString();
                tbPosZStart.Text = start3.ToString();
                tbPosXEnd.Text = end1.ToString();
                tbPosYEnd.Text = end2.ToString();
                tbPosZEnd.Text = end3.ToString();
            }
            else if (actionType == "RotateTo")
            {
                tbRotXStart.Text = start1.ToString();
                tbRotYStart.Text = start2.ToString();
                tbRotZStart.Text = start3.ToString();
                tbRotXEnd.Text = end1.ToString();
                tbRotYEnd.Text = end2.ToString();
                tbRotZEnd.Text = end3.ToString();
            }
            else if (actionType == "Visible")
            {
                if (end1 == 0.0f)
                    cbVisible.Checked = false;
                else
                    cbVisible.Checked = true;
            }
            else if (actionType == "Audio")
            {
                audioFileName = start4;
                tbFileName.Text = System.IO.Path.GetFileName(audioFileName);
            }
            else if (actionType == "Camera")
            {
                for (int i = 0; i < cbObjToTrack.Items.Count; ++i)
                    if (cbObjToTrack.Items[i].ToString() == start4)
                        cbObjToTrack.SelectedIndex = i;
            }
        }

        private void NewActionForm_Shown(object sender, EventArgs e)
        {
            this.cbActionType_SelectedIndexChanged(cbActionType, null);
        }

        /// <summary>
        /// Checks to see if the information contained in the form is valid so an object can be
        /// created from it.
        /// </summary>
        private bool IsValidInfo()
        {
            Color invalidColour = Color.Red;

            if (cbActionType.SelectedItem.ToString() == "Camera")
            {
                return true;
            }

            if (cbActionType.SelectedItem.ToString() == "Audio")
            {
                if (tbFileName.BackColor == invalidColour)
                    return false;

                return audioFileName != "";
            }

            if ((tbNameAction.BackColor == invalidColour) ||
                (tbPosXEnd.BackColor == invalidColour) ||
                (tbPosYEnd.BackColor == invalidColour) ||
                (tbPosZEnd.BackColor == invalidColour) ||
                (tbRotXEnd.BackColor == invalidColour) ||
                (tbRotYEnd.BackColor == invalidColour) ||
                (tbRotZEnd.BackColor == invalidColour))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Validates the contents of a text box to ensure that it can be converted into a float.
        /// If it can't, then the background of the text box will turn red.
        /// </summary>
        private void FloatValidation(object sender, CancelEventArgs e)
        {
            TextBox obj = (TextBox)sender;
            float value = 0;

            if (!float.TryParse(obj.Text, out value))
                obj.BackColor = Color.Red;
            else
                obj.BackColor = Color.White;

            this.CalculateNewEndFrame();
        }

        /// <summary>
        /// Ensure the action name is valid.
        /// </summary>
        private void ActionNameValidation(object sender, CancelEventArgs e)
        {
            bool valid = false;

            if (string.IsNullOrEmpty(tbNameAction.Text))
                valid = false;
            else if (editing && tbNameAction.Text == oldActionName)
                valid = true;
            else
                wrapper._IsActionNameValid(tbNameAction.Text, out valid);

            if( !valid)
                tbNameAction.BackColor = Color.Red;
            else
                tbNameAction.BackColor = Color.White;
        }

        /// <summary>
        /// Updates the form, with regards to what values can and can't be modified, when the
        /// action type is changed.
        /// </summary>
        private void cbActionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!cbActionType.Enabled)
                return;

            string actionType = cbActionType.SelectedItem.ToString();

            tbPosXEnd.Text = tbPosXStart.Text;
            tbPosYEnd.Text = tbPosYStart.Text;
            tbPosZEnd.Text = tbPosZStart.Text;

            tbRotXEnd.Text = tbRotXStart.Text;
            tbRotYEnd.Text = tbRotYStart.Text;
            tbRotZEnd.Text = tbRotZStart.Text;

            if (actionType == "Audio")
                this.ReorganiseForm(FormSetup.FS_Audio);
            else if (actionType == "Camera")
                this.ReorganiseForm(FormSetup.FS_Camera);
            else
                this.ReorganiseForm(FormSetup.FS_Other);

            // The start position and rotations should ALWAYS be read only, so they don't need to
            // be set. The other boxes are dependent on the action type, however.
            if (actionType == "MoveTo" || actionType == "PositionAt")
            {
                this.SetPosEndReadOnly(false);
                this.SetRotEndReadOnly(true);
                this.SetVisibleReadOnly(true);
            }
            else if (actionType == "RotateTo")
            {
                this.SetPosEndReadOnly(true);
                this.SetRotEndReadOnly(false);
                this.SetVisibleReadOnly(true);
            }
            else if (actionType == "Visible")
            {
                this.SetPosEndReadOnly(true);
                this.SetRotEndReadOnly(true);
                this.SetVisibleReadOnly(false);
            }
        }

        /// <summary>
        /// Organises the objects on the form based on the action that's selected.
        /// </summary>
        private void ReorganiseForm(FormSetup setupType)
        {
            bool forAudio, forCamera, forOther;
            forAudio = setupType == FormSetup.FS_Audio;
            forCamera = setupType == FormSetup.FS_Camera;
            forOther = setupType == FormSetup.FS_Other;

            lblFileName.Visible = forAudio;
            tbFileName.Visible = forAudio;

            lblObjToTrack.Visible = forCamera;
            cbObjToTrack.Visible = forCamera;

            cbVisible.Visible = forOther;
            lblPosStart.Visible = forOther;
            lblPosXStart.Visible = forOther;
            lblPosYStart.Visible = forOther;
            lblPosZStart.Visible = forOther;
            tbPosXStart.Visible = forOther;
            tbPosYStart.Visible = forOther;
            tbPosZStart.Visible = forOther;
            lblRotStart.Visible = forOther;
            lblRotXStart.Visible = forOther;
            lblRotYStart.Visible = forOther;
            lblRotZStart.Visible = forOther;
            tbRotXStart.Visible = forOther;
            tbRotYStart.Visible = forOther;
            tbRotZStart.Visible = forOther;
            lblPosEnd.Visible = forOther;
            lblPosXEnd.Visible = forOther;
            lblPosYEnd.Visible = forOther;
            lblPosZEnd.Visible = forOther;
            tbPosXEnd.Visible = forOther;
            tbPosYEnd.Visible = forOther;
            tbPosZEnd.Visible = forOther;
            lblRotEnd.Visible = forOther;
            lblRotXEnd.Visible = forOther;
            lblRotYEnd.Visible = forOther;
            lblRotZEnd.Visible = forOther;
            tbRotXEnd.Visible = forOther;
            tbRotYEnd.Visible = forOther;
            tbRotZEnd.Visible = forOther;

            if (forAudio)
            {
                btnCreate.Location = new Point(btnCreate.Location.X, tbFileName.Location.Y + 40);
                btnCancel.Location = new Point(btnCancel.Location.X, tbFileName.Location.Y + 40);
            }
            else if (forCamera)
            {
                btnCreate.Location = new Point(btnCreate.Location.X, cbObjToTrack.Location.Y + 40);
                btnCancel.Location = new Point(btnCancel.Location.X, cbObjToTrack.Location.Y + 40);
            }
            else
            {
                btnCreate.Location = new Point(btnCreate.Location.X, tbRotXEnd.Location.Y + 40);
                btnCancel.Location = new Point(btnCancel.Location.X, tbRotXEnd.Location.Y + 40);
            }

            this.Size = new Size(this.Size.Width, btnCreate.Location.Y + 75);
        }

        /// <summary>
        /// Sets whether the position data is read only.
        /// </summary>
        private void SetPosEndReadOnly(bool readOnly)
        {
            tbPosXEnd.ReadOnly = readOnly;
            tbPosYEnd.ReadOnly = readOnly;
            tbPosZEnd.ReadOnly = readOnly;
        }

        /// <summary>
        /// Sets whether the rotation data is read only.
        /// </summary>
        private void SetRotEndReadOnly(bool readOnly)
        {
            tbRotXEnd.ReadOnly = readOnly;
            tbRotYEnd.ReadOnly = readOnly;
            tbRotZEnd.ReadOnly = readOnly;
        }

        /// <summary>
        /// Sets whether the 'Visible' checkbox is read only.
        /// </summary>
        private void SetVisibleReadOnly(bool readOnly)
        {
            cbVisible.Enabled = !readOnly;
        }

        /// <summary>
        /// Calculates the end frame of the action based on the currently entered data.
        /// </summary>
        private void CalculateNewEndFrame()
        {
            if (!this.IsValidInfo())
            {
                tbFrameEnd.Text = "Error";
                return;
            }

            string actionType = cbActionType.SelectedItem.ToString();
            float xStart, yStart, zStart, xEnd, yEnd, zEnd;
            int length;

            xStart = yStart = zStart = xEnd = yEnd = zEnd = 0.0f;
            string textParam = "";

            if (actionType == "MoveTo" || actionType == "PositionAt")
            {
                float.TryParse(tbPosXStart.Text, out xStart);
                float.TryParse(tbPosYStart.Text, out yStart);
                float.TryParse(tbPosZStart.Text, out zStart);
                float.TryParse(tbPosXEnd.Text, out xEnd);
                float.TryParse(tbPosYEnd.Text, out yEnd);
                float.TryParse(tbPosZEnd.Text, out zEnd);
            }
            else if (actionType == "RotateTo")
            {
                float.TryParse(tbRotXStart.Text, out xStart);
                float.TryParse(tbRotYStart.Text, out yStart);
                float.TryParse(tbRotZStart.Text, out zStart);
                float.TryParse(tbRotXEnd.Text, out xEnd);
                float.TryParse(tbRotYEnd.Text, out yEnd);
                float.TryParse(tbRotZEnd.Text, out zEnd);
            }
            else if (actionType == "Audio")
            {
                textParam = audioFileName;
            }
            else if (actionType == "Camera")
            {
                textParam = cbObjToTrack.SelectedItem.ToString();
            }

            wrapper._CalculateActionLength(actionType, xStart, yStart, zStart, xEnd, yEnd, zEnd, textParam, out length);

            tbFrameEnd.Text = (startFrame + length).ToString();
        }

        /// <summary>
        /// Creates an action, assuming the data in the form is valid.
        /// </summary>
        private void btnCreate_Click(object sender, EventArgs e)
        {
            // If the information entered is invalid, then don't even attempt to create the action.
            if (!this.IsValidInfo())
            {
                MessageBox.Show("Cannot create the action due to invalid information. Fix the properties highlighted in red.", "Invalid Properties", MessageBoxButtons.OK);
                return;
            }

            string nameObj = tbNameObject.Text;
            string nameAction = tbNameAction.Text;
            string nameType = cbActionType.SelectedItem.ToString();
            float param1 = 0.0f, param2 = 0.0f, param3 = 0.0f;
            string param4 = "";

            // Get the correct parameters for the action type.
            if (nameType == "MoveTo" || nameType == "PositionAt")
            {
                float.TryParse(tbPosXEnd.Text, out param1);
                float.TryParse(tbPosYEnd.Text, out param2);
                float.TryParse(tbPosZEnd.Text, out param3);
            }
            else if (nameType == "RotateTo")
            {
                float.TryParse(tbRotXEnd.Text, out param1);
                float.TryParse(tbRotYEnd.Text, out param2);
                float.TryParse(tbRotZEnd.Text, out param3);
            }
            else if (nameType == "Visible")
            {
                if (cbVisible.Checked)
                    param1 = 1.0f;
                else
                    param1 = 0.0f;
            }
            else if (nameType == "Audio")
            {
                param1 = 1.0f;
                param4 = audioFileName;
            }
            else if (nameType == "Camera")
            {
                param4 = cbObjToTrack.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("Something went wrong. Contact the tool's programmer.", "Invalid Action Type", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!editing)
            {
                // Add the action to the object.
                wrapper._AddAction(nameObj, nameAction, nameType, param1, param2, param3, param4);
            }
            else
            {
                // Edit the existing action.
                wrapper._EditAction(nameObj, oldActionName, nameAction, param1, param2, param3, param4);
            }

            // Close the form once the action has been created.
            this.Close();
        }

        /// <summary>
        /// Close the form without creating the object if the user presses 'Cancel'.
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Event handler for when the user clicks the audio file name text box.
        /// </summary>
        private void tbFileName_MouseClick(object sender, MouseEventArgs e)
        {
            DialogResult res = openFileDialog.ShowDialog();

            if (res != DialogResult.OK)
                return;

            string exePath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            audioFileName = openFileDialog.FileName;

            // Strip the path so it's relative to the tool.
            if (audioFileName.Length < exePath.Length)
            {
                tbFileName.BackColor = Color.Red;
                MessageBox.Show("Audio files must be relative to the tool's executable.", "Invalid Audio File Location", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string filePath = audioFileName.Substring(0, exePath.Length);
            if (exePath != filePath)
            {
                tbFileName.BackColor = Color.Red;
                MessageBox.Show("Audio files must be relative to the tool's executable.", "Invalid Audio File Location", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Flip all of the '\' to '/'. I have to create an array of chars because Microsoft are
            // such dumbasses that they've prevented me from accessing the characters in the string
            // using 'audioFileName[i]'.
            char[] name = new char[audioFileName.Length];
            for (int i = 0; i < audioFileName.Length; ++i)
            {
                name[i] = audioFileName[i];
                if (name[i] == '\\')
                    name[i] = '/';
            }
            audioFileName = new string(name);

            audioFileName = audioFileName.Substring(exePath.Length + 1);
            tbFileName.Text = System.IO.Path.GetFileName(audioFileName);

            tbFileName.BackColor = Color.White;
            this.CalculateNewEndFrame();
        }
    }
}
