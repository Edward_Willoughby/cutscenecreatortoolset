/********************************************************************
*	Function definitions for the DirectXWrapper.
********************************************************************/

/// This NEEDS to be at the very top, otherwise weird bugs start happening.
#include "stdafx.h"

/********************************************************************
*	Include header file.
********************************************************************/
#include "DirectXWrapper.h"

#include "App.h"
#include "Application.h"

/********************************************************************
*	Namespaces, defines, constants and local variables.
********************************************************************/
using namespace System;
using namespace System::Runtime::InteropServices;

/// The pointer to DirectX (from the C++ library).
static NS_DirectXLib::Application* gp_directX = nullptr;


/**
* Local function to easily check a return value.
*
* @param value :: The return value of a function from the C++ library.
*
* @return True if the function exited with no errors.
*/
bool Succeeded( int value)
{
	if( value == 0)
		return true;
	else
		return false;
}

/**
* Initialises DirectX by creating an instance of 'Application' and storing it in 'gp_directX' so it
* can be continually accessed.
*
* @param hwnd :: The handle of the panel in C# that DirectX will be rendering to.
*
* @return True if DirectX was initialised with no errors.
*/
bool DirectXWrapper::_Initialise( IntPtr hwnd) {
	// Only initialise DirectX if the local pointer is currently 'nullptr', otherwise it'll
	// overwrite an existing instance and lose track of it (causing all sorts of memory leaks and
	// potential errors).
	if( gp_directX == nullptr)
		gp_directX = new NS_DirectXLib::Application();

	return Succeeded( NS_DirectXLib::Run( gp_directX, HWND( hwnd.ToPointer())));
}

/**
* Refreshes DirectX and updates the 'picture' being sent by the library.
*
* @return True if DirectX was updated and rendered with no errors.
*/
bool DirectXWrapper::_RefreshFrame( unsigned int frame) {
	// Set the frame first so DirectX knows how to set up the scene.
	gp_directX->GoToFrame( frame);

	// DirectX needs to be updated first before it can render the frame.
	gp_directX->Update();
	gp_directX->Render();

	return true;
}

/**
* Closes DirectX and releases any memory that it may've been using.
*
* @return True if DirectX closed with no errors.
*/
bool DirectXWrapper::_Shutdown() {
	// If DirectX isn't initialised, then don't try to close it.
	if( gp_directX == nullptr)
		return true;

	gp_directX->Stop();
	gp_directX->StopD3D();

	delete gp_directX;
	gp_directX = nullptr;

	return true;
}

/**
* Clears the current scene.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_ClearScene() {
	if( gp_directX == nullptr)
		return false;

	gp_directX->ClearScene();

	return true;
}

/**
* Saves the current scene.
*
* @param fileName :: The name of the file to save to.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_SaveScene( System::String^ fileName) {
	if( gp_directX == nullptr)
		return false;

	char* f = static_cast<char*>( Marshal::StringToHGlobalAnsi( fileName).ToPointer());

	gp_directX->SaveScene( f);

	return true;
}

/**
* Opens a saved scene.
*
* @param fileName :: The name of the file to load from.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_OpenScene( System::String^ fileName) {
	if( gp_directX == nullptr)
		return false;

	const char* f = static_cast<const char*>( Marshal::StringToHGlobalAnsi( fileName).ToPointer());

	gp_directX->OpenScene( f);

	return true;
}

/**
* Gets the last frame of the currently stored scene. This takes into account all existing actions
* and find out the frame at which the last action finishes.
*
* @param frame :: The variable to hold the frame number.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetLastFrame( ParamOut unsigned int %frame) {
	if( gp_directX == nullptr)
		return false;

	unsigned int f = 0;
	gp_directX->GetLastFrame( f);
	frame = f;

	return true;
}

/**
* Creates a object at the origin.
*
* @param name	:: The name of the object.
* @param model	:: The name of the model that the object will use.
* @param param1	:: The first parameter.
* @param param2	:: The second parameter.
* @param param3	:: The third parameter.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_CreateObject( System::String^ name, System::String^ model, ParamDefault float param1, ParamDefault float param2, ParamDefault float param3) {
	if( gp_directX == nullptr)
		return false;

	char* n = static_cast<char*>( Marshal::StringToHGlobalAnsi( name).ToPointer());
	char* m = static_cast<char*>( Marshal::StringToHGlobalAnsi( model).ToPointer());
	gp_directX->CreateObject( n, m, param1, param2, param3);

	return true;
}

/**
* Removes an object from the scene.
*
* @param name :: The name of the object.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_DeleteObject( System::String^ name) {
	if( gp_directX == nullptr)
		return false;

	const char* n = static_cast<const char*>( Marshal::StringToHGlobalAnsi( name).ToPointer());
	gp_directX->DeleteObject( n);

	return true;
}

/**
* Adds a movement action to the sphere.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_AddAction( System::String^ objectName, System::String^ actionName, System::String^ actionType, float param1, float param2, float param3, System::String^ param4) {
	if( gp_directX == nullptr)
		return false;

	const char* obj = static_cast<const char*>( Marshal::StringToHGlobalAnsi( objectName).ToPointer());
	const char* act = static_cast<const char*>( Marshal::StringToHGlobalAnsi( actionName).ToPointer());
	const char* type = static_cast<const char*>( Marshal::StringToHGlobalAnsi( actionType).ToPointer());
	const char* p4 = static_cast<const char*>( Marshal::StringToHGlobalAnsi( param4).ToPointer());

	gp_directX->AddAction( obj, act, type, param1, param2, param3, p4);

	return true;
}

/**
* Edits an existing action to have new information.
*/
bool DirectXWrapper::_EditAction( System::String^ objectName, System::String^ actionName, System::String^ newActionName, float param1, float param2, float param3, System::String^ param4) {
	if( gp_directX == nullptr)
		return false;

	char* obj = static_cast<char*>( Marshal::StringToHGlobalAnsi( objectName).ToPointer());
	char* act = static_cast<char*>( Marshal::StringToHGlobalAnsi( actionName).ToPointer());
	char* newAct = static_cast<char*>( Marshal::StringToHGlobalAnsi( newActionName).ToPointer());
	const char* p4 = static_cast<const char*>( Marshal::StringToHGlobalAnsi( param4).ToPointer());

	gp_directX->EditAction( obj, act, newAct, param1, param2, param3, p4);

	return true;
}

/**
* Deletes an existing action.
*
* @param objectName	:: The name of the object.
* @param actionName	:: The name of the action to be deleted.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_DeleteAction( System::String^ objectName, System::String^ actionName) {
	if( gp_directX == nullptr)
		return false;

	const char* obj = static_cast<const char*>( Marshal::StringToHGlobalAnsi( objectName).ToPointer());
	const char* act = static_cast<const char*>( Marshal::StringToHGlobalAnsi( actionName).ToPointer());

	gp_directX->DeleteAction( obj, act);

	return true;
}

/**
* Gets the position of an object.
*
* @param name	:: The name of the object.
* @param x		:: The variable to hold the object's x position.
* @param y		:: The variable to hold the object's y position.
* @param z		:: The variable to hold the object's z position.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetPosition( System::String^ name, ParamOut float %x, ParamOut float %y, ParamOut float %z) {
	if( gp_directX == nullptr)
		return false;

	float xT( 0.0f), yT( 0.0f), zT( 0.0f);
	
	// I have to use this epic line of code to convert a String^ into a char*.
	char* n = static_cast<char*>( Marshal::StringToHGlobalAnsi( name).ToPointer());

	// I can't directly use the parameters sent to this function because of the whole 
	// managed/unmanaged thing, so I have to use local variables and then assign them over.
	gp_directX->GetPosition( n, xT, yT, zT);
	x = xT;
	y = yT;
	z = zT;

	return true;
}

/**
* Gets the rotation of an object.
*
* @param name	:: The name of the object.
* @param x		:: The variable to hold the object's x rotation.
* @param y		:: The variable to hold the object's y rotation.
* @param z		:: The variable to hold the object's z rotation.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetRotation( System::String^ name, ParamOut float %x, ParamOut float %y, ParamOut float %z) {
	if( gp_directX == nullptr)
		return false;

	float xT( 0.0f), yT( 0.0f), zT( 0.0f);
	
	// I have to use this epic line of code to convert a String^ into a char*.
	char* n = static_cast<char*>( Marshal::StringToHGlobalAnsi( name).ToPointer());

	// I can't directly use the parameters sent to this function because of the whole 
	// managed/unmanaged thing, so I have to use local variables and then assign them over.
	gp_directX->GetRotation( n, xT, yT, zT);
	x = xT;
	y = yT;
	z = zT;

	return true;
}

/**
* Gets the visibility of an object in the scene.
*
* @param name	:: The name of the object.
* @param vis	:: The variable to hold the object's visibility.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetVisibility( System::String^ name, ParamOut bool %vis) {
	if( gp_directX == nullptr)
		return false;

	char* n = static_cast<char*>( Marshal::StringToHGlobalAnsi( name).ToPointer());
	bool v = false;

	gp_directX->GetVisibility( n, v);

	vis = v;

	return true;
}

bool DirectXWrapper::_GetObjectName( ParamOut System::String^ %name, int index) {
	if( gp_directX == nullptr)
		return false;

	name = %String( gp_directX->GetObjectName( index));

	return true;
}

/**
* Gets the information about an action associated with an object.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetObjectsActionInfo( System::String^ nameObject, ParamOut System::String^ %nameAction, int %start, int %end, int index) {
	if( gp_directX == nullptr)
		return false;

	char* obj = static_cast<char*>( Marshal::StringToHGlobalAnsi( nameObject).ToPointer());
	int s = 0;
	int e = 0;

	nameAction = %String( gp_directX->GetObjectsActionInfo( obj, s, e, index));
	start = s;
	end = e;

	return true;
}

/**
* Gets the information about an action associated with an object.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetObjectsActionInfo( System::String^ nameObject, System::String^ nameAction, ParamOut System::String^ %actionType, int %start, int %end) {
	if( gp_directX == nullptr)
		return false;

	char* obj = static_cast<char*>( Marshal::StringToHGlobalAnsi( nameObject).ToPointer());
	char* act = static_cast<char*>( Marshal::StringToHGlobalAnsi( nameAction).ToPointer());
	const char* actType = "";
	int s = 0;
	int e = 0;

	gp_directX->GetObjectsActionInfo( obj, act, actType, s, e);
	actionType = %String( actType);
	start = s;
	end = e;

	return true;
}

/**
* Gets the information about an action at the specified frame.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetActionInfoAtFrame( System::String^ nameObject, System::String^ nameAction, int frame, ParamOut float %param1, ParamOut float %param2, ParamOut float %param3, ParamOut System::String^ %param4) {
	if( gp_directX == nullptr)
		return false;

	char* obj = static_cast<char*>( Marshal::StringToHGlobalAnsi( nameObject).ToPointer());
	char* act = static_cast<char*>( Marshal::StringToHGlobalAnsi( nameAction).ToPointer());
	float p1 = 0.0f;
	float p2 = 0.0f;
	float p3 = 0.0f;
	const char* p4 = "";

	gp_directX->GetActionInfoAtFrame( obj, act, frame, p1, p2, p3, p4);

	param1 = p1;
	param2 = p2;
	param3 = p3;
	param4 = %String( p4);

	return true;
}

/**
* Calculates the length of an action.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_CalculateActionLength( System::String^ actionType, float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd, System::String^ textParam, ParamOut int %length) {
	if( gp_directX == nullptr)
		return false;

	char* action = static_cast<char*>( Marshal::StringToHGlobalAnsi( actionType).ToPointer());
	char* t = static_cast<char*>( Marshal::StringToHGlobalAnsi( textParam).ToPointer());
	int l( 0);
	gp_directX->CalculateActionLength( action, xStart, yStart, zStart, xEnd, yEnd, zEnd, t, l);
	length = l;

	return true;
}

/**
* Gets the name of a valid mesh that an object can be created from.
*
* @param name	:: The variable to hold the name of the mesh.
* @param index	:: The number of the mesh.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetName( ParamOut System::String^ %name, int index) {
	if( gp_directX == nullptr)
		return false;

	name = %String( gp_directX->GetName( index));

	return true;
}

/**
* Gets the name of a valid action type.
*
* @param name	:: The variable to hold the name of the action type.
* @param index	:: The number of the action type.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetActionTypeName( ParamOut System::String^ %name, int index) {
	if( gp_directX == nullptr)
		return false;

	name = %String( gp_directX->GetActionTypeName( index));

	return true;
}

/**
* Gets a valid name for an object.
*
* @param name :: The variable to hold the name.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetValidObjectName( ParamOut System::String^ %name) {
	if( gp_directX == nullptr)
		return false;

	name = %String( gp_directX->GetValidObjectName());

	return true;
}

/**
* Gets a valid name for an action.
*
* @param name :: The variable to hold the name.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetValidActionName( ParamOut System::String^ %name) {
	if( gp_directX == nullptr)
		return false;

	name = %String( gp_directX->GetValidActionName());

	return true;
}

/**
* Checks an action name to see if it's valid.
*
* @param actionName	:: The name of the action.
* @param valid		:: True if the name is not being used by any existing actions.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_IsActionNameValid( System::String^ actionName, ParamOut bool %valid) {
	if( gp_directX == nullptr)
		return false;

	char* name = static_cast<char*>( Marshal::StringToHGlobalAnsi( actionName).ToPointer());

	if( gp_directX->IsActionNameValid( name))
		valid = true;
	else
		valid = false;

	return true;
}

/**
* Retrieves the names of the parameters required for the model.
*
* @param model	:: The name of the mesh.
* @param param1	:: The variable to hold the first parameter's name.
* @param param2	:: The variable to hold the second parameter's name.
* @param param3	:: The variable to hold the third parameter's name.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_GetParameterNames( System::String^ model, ParamOut System::String^ %param1, ParamOut System::String^ %param2, ParamOut System::String^ %param3) {
	if( gp_directX == nullptr)
		return false;

	// The library only requires the name of the model, the other three parameters' values are
	// irrelevant.
	const char* m = static_cast<const char*>( Marshal::StringToHGlobalAnsi( model).ToPointer());
	const char* a = "";
	const char* b = "";
	const char* c = "";

	gp_directX->GetObjectParameterName( m, a, b, c);

	param1 = %String( a);
	param2 = %String( b);
	param3 = %String( c);

	return true;
}

/**
* Enables or disables audio.
*
* @param enable :: Set to true if audio should be played.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_EnableAudio( bool enable) {
	if( gp_directX == nullptr)
		return false;

	gp_directX->SetAudioPlaying( enable);

	return true;
}

/**
* Stops any existing audio files from playing.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_StopAudio() {
	if( gp_directX == nullptr)
		return false;

	gp_directX->StopAudio();

	return true;
}

/**
* Sets whether the toolset should use the scene camera or global camera.
*
* @param globalCam :: Set to true to use the global camera.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_UseGlobalCam( bool globalCam) {
	if( gp_directX == nullptr)
		return false;

	gp_directX->UseGlobalCamera( globalCam);

	return true;
}

/**
* Orbits the camera by a specified amount.
*
* @param x	:: The mouse's x movement.
* @param y	:: The mouse's y movement.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_CameraOrbit( int x, int y) {
	if( gp_directX == nullptr)
		return false;

	gp_directX->CameraOrbit( x, y);

	return true;
}

/**
* Zooms the camera by a specified amount.
*
* @param x	:: The mouse's x movement.
* @param y	:: The mouse's y movement.
*
* @return True if there were no errors.
*/
bool DirectXWrapper::_CameraZoom( int x, int y) {
	if( gp_directX == nullptr)
		return false;

	gp_directX->CameraZoom( x, y);

	return true;
}

/**
* TEST FUNCTION - This just tests to see if something works.
*/
bool DirectXWrapper::_TestFunction( ParamOut System::String^ %test) {
	if( gp_directX == nullptr)
		return false;

	const char* t = "";
	gp_directX->TestFunction( t);

	test = %String( t);

	return true;
}