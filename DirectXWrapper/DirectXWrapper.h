/********************************************************************
*
*	WRAPPER		:: DirectXWrapper
*	DESCRIPTION	:: Exposes functions from an unmanaged C++ library so they can be used in a C#
*					application.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2014 / 01 / 12
*
********************************************************************/

#ifndef DirectXWrapperH
#define DirectXWrapperH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <Windows.h>


/********************************************************************
*	Defines and constants.
********************************************************************/
/// Allows a parameter to be optional and have a default value set (same as C++'s 'float x = 1.0f').
#define ParamDefault	[System::Runtime::InteropServices::Optional]
/// Sets a parameter to expect a different value to be returned (same as C++'s 'float& x').
#define ParamOut		[System::Runtime::InteropServices::Out]


/*******************************************************************/
public ref class DirectXWrapper {
	public:
		/// Initialises DirectX.
		bool _Initialise( System::IntPtr hwnd);
		/// Refreshes DirectX.
		bool _RefreshFrame( unsigned int frame);
		/// Closes DirectX.
		bool _Shutdown();

		/// Clears the current scene.
		bool _ClearScene();
		/// Saves the current scene.
		bool _SaveScene( System::String^ fileName);
		/// Opens a saved scene.
		bool _OpenScene( System::String^ fileName);

		/// Gets the last frame of the current scene (taking into account all existing actions).
		bool _GetLastFrame( ParamOut unsigned int %frame);

		/// TEST FUNCTION - Creates an object.
		bool _CreateObject( System::String^ name, System::String^ model, ParamDefault float param1, ParamDefault float param2, ParamDefault float param3);
		/// Deletes an existing object.
		bool _DeleteObject( System::String^ name);
		/// TEST FUNCTION - Adds a movement action to the sphere.
		bool _AddAction( System::String^ objectName, System::String^ actionName, System::String^ actionType, float param1, float param2, float param3, System::String^ param4);
		/// Edits an existing action to have new information.
		bool _EditAction( System::String^ objectName, System::String^ actionName, System::String^ newActionName, float param1, float param2, float param3, System::String^ param4);
		/// Deletes an existing action.
		bool _DeleteAction( System::String^ objectName, System::String^ actionName);
		/// Gets the position of an object in the scene.
		bool _GetPosition( System::String^ name, ParamOut float %x, ParamOut float %y, ParamOut float %z);
		/// Gets the rotation of an object in the scene.
		bool _GetRotation( System::String^ name, ParamOut float %x, ParamOut float %y, ParamOut float %z);
		/// Gets the visibility of an object in the scene.
		bool _GetVisibility( System::String^ name, ParamOut bool %vis);
		/// TEST FUNCTION - Gets the name of a valid model (... probably going to keep this as well, but it needs editing).
		bool _GetName( ParamOut System::String^ %name, int index);
		/// Gets the name of a valid action type.
		bool _GetActionTypeName( ParamOut System::String^ %name, int index);

		/// Gets an object's name.
		bool _GetObjectName( ParamOut System::String^ %name, int index);
		/// Gets the information about an action associated with an object.
		bool _GetObjectsActionInfo( System::String^ nameObject, ParamOut System::String^ %nameAction, ParamOut int %start, ParamOut int %end, int index);
		/// Gets the information about an action associated with an object.
		bool _GetObjectsActionInfo( System::String^ nameObject, System::String^ nameAction, ParamOut System::String^ %actionType, ParamOut int %start, ParamOut int %end);
		/// Gets the information about an action at the specified frame.
		bool _GetActionInfoAtFrame( System::String^ nameObject, System::String^ nameAction, int frame, ParamOut float %param1, ParamOut float %param2, ParamOut float %param3, ParamOut System::String^ %param4);
		/// Calculates the length of an action.
		bool _CalculateActionLength( System::String^ actionType, float xStart, float yStart, float zStart, float xEnd, float yEnd, float zEnd, System::String^ textParam, ParamOut int %length);
		
		/// Gets a valid name for an object.
		bool _GetValidObjectName( ParamOut System::String^ %name);
		/// Gets a valid name for an action.
		bool _GetValidActionName( ParamOut System::String^ %name);
		/// Checks an action name to see if it's valid.
		bool _IsActionNameValid( System::String^ actionName, ParamOut bool %valid);
		/// Retrieves the names of the parameters required for the model.
		bool _GetParameterNames( System::String^ model, ParamOut System::String^ %param1, ParamOut System::String^ %param2, ParamOut System::String^ %param3);
		
		/// Enables or disables audio.
		bool _EnableAudio( bool enable);
		/// Stops any existing audio files from playing.
		bool _StopAudio();
		
		/// Sets whether the toolset should use the scene camera or global camera.
		bool _UseGlobalCam( bool globalCam);

		/// Orbits the camera by a specified amount.
		bool _CameraOrbit( int x, int y);
		/// Zooms the camera by a specified amount.
		bool _CameraZoom( int x, int y);

		// Functions that need to be implemented... for any kind of reasonable functionality.
		//DllExport bool __cdecl OpenFile( char* fileName);
		//DllExport bool __cdecl SaveFile( char* fileName);
		//DllExport bool __cdecl ActionMove( char* object, unsigned int frame, float cX, float cY, float cZ, float dX, float dY, float dZ);
		//DllExport bool __cdecl ActionRotate( char* object, unsigned int frame, float degrees);
		//DllExport bool __cdecl ActionRotateTo( char* object, unsigned int frame, float x, float y, float z);

		/// TEST FUNCTION - This just tests to see if something works.
		bool _TestFunction( ParamOut System::String^ %test);

};

/*******************************************************************/
#endif	// #ifndef DirectXWrapperH